package fr.soleil.comete.bean.datarecorderbean;

import javax.swing.JLabel;

import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.StateUtilities;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;

public class StateStatusLabel extends Label {

    private static final long serialVersionUID = 218061172516479353L;

    private boolean connected = false;
    private ITextTarget statusTarget;
    private StringScalarBox stringBox;
    private TangoKey key;
    private String defaultTooltip = null;
    private CometeColor defaultColor = null;

    public StateStatusLabel() {
        setHorizontalAlignment(JLabel.CENTER);
    }


    public void connectStatus(String deviceName) {
        if (deviceName != null && !deviceName.isEmpty()) {
            statusTarget = new TextTarget();
            stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
            key = new TangoKey();
            TangoKeyTool.registerAttribute(key, deviceName, "Status");
            stringBox.connectWidget(statusTarget, key);
            connected = true;
            refreshTooltip();
        }
    }

    public void disconnectStatus() {
        if (connected) {
            stringBox.disconnectWidget(statusTarget, key);
            connected = false;
        }
    }

    @Override
    public void setText(String text) {
        if (defaultColor == null) {
            defaultColor = getCometeForeground();
        }

        if (text != null) {
            if (text.equalsIgnoreCase(StateUtilities.getNameForState(DevState.RUNNING))) {
                setCometeForeground(CometeColor.WHITE);
            } else {
                setCometeForeground(defaultColor);
            }
        }

        super.setText(text);
    }

    @Override
    public void setToolTipText(String text) {
        // save the default tooltip, ie the device name
        if (defaultTooltip == null) {
            defaultTooltip = text;
        }

        super.setToolTipText(text);
    }

    private void refreshTooltip() {
        StringBuilder sb = new StringBuilder();

        if (defaultTooltip != null) {
            sb.append("<html><body>");
            sb.append(defaultTooltip);

            if (connected) {
                sb.append("<br/>");
                sb.append(statusTarget.getText());
            }
            sb.append("</body></html>");
        }

        setToolTipText(sb.toString());
    }

    class TextTarget implements ITextTarget {
        private String currentStatus = null;

        @Override
        public void setText(String text) {
            currentStatus = text;
            refreshTooltip();
        }

        @Override
        public String getText() {
            return currentStatus;
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // nop
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // nop
        }
    }

}
