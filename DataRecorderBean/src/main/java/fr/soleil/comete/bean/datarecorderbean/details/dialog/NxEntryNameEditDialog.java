package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.INxEntryNameModelListener;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.NxEntryNameDialogModel;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.lib.project.swing.icons.Icons;

public class NxEntryNameEditDialog extends AbstractRecorderEditDialog implements INxEntryNameModelListener {

    private static final long serialVersionUID = -6916753323063599375L;

    // Experiment Name
    private JLabel experimentNameTitle;
    private TextFieldButton experimentNameEditor;
    private JButton experimentNameRevertButton;

    // Acquisition Name
    private JLabel acquisitionNameTitle;
    private TextFieldButton acquisitionNameEditor;
    private JButton acquisitionNameRevertButton;

    // Experiment Index
    private JPanel experimentIndexPanel;
    private StringButton resetExperimentIndexButton;
    private StringButton incrementExperimentIndexButton;

    // NxEntry Name
    private JLabel nxEntryNameTitle;
    private Label nxEntryNameViewer;
    private JPanel nxEntryNameEditButtonPanel;
    private JButton nxEntryNameEditButton;
    private NxEntryNameModelEditDialog nameModelEditDialog;
    private NxEntryNameDialogModel currentDialogModel;

    public NxEntryNameEditDialog(Window parent, boolean modal) {
        super(parent, "NxEntryName", modal);
    }

    @Override
    protected void clearDialog() {
        // No graphical component to clean
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderText(resourceBundle.getString("DataRecorderState.NxEntryName.Header"));
        setHeaderIcon(iconsManager.getIcon("DataRecorder.dialog.tool"));
        getNameModelEditDialog().setHeaderText(resourceBundle.getString("DataRecorderState.NxEntryName.Model.Header"));
        getNameModelEditDialog().setHeaderIcon(iconsManager.getIcon("DataRecorder.dialog.tool"));
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        model.updateNxEntryNameDialogModel(currentDialogModel);
    }

    @Override
    protected void initDialogModel() {
        currentDialogModel = new NxEntryNameDialogModel(this);
    }

    @Override
    protected void initPanelComponents() {
        nameModelEditDialog = new NxEntryNameModelEditDialog(this, "Edit nxEntryName Model", true);
        nameModelEditDialog.setDialogModel(currentDialogModel);

        experimentNameTitle = new JLabel("Experiment Name:");
        experimentNameEditor = new TextFieldButton();
        experimentNameEditor.setAlwaysUpdateTextField(true);
        experimentNameEditor.setTextButton("Apply");

        experimentNameEditor.setTextFieldColumns(30);

        experimentNameEditor.addTextFieldListener(new ITextFieldListener() {
            @Override
            public void textChanged(EventObject event) {
                String experimentName = currentDialogModel.getExperimentName();
                if (!experimentNameEditor.getText().equalsIgnoreCase(experimentName)) {
                    updateExperimentEditor(true);
                }
            }

            @Override
            public void actionPerformed(EventObject event) {
                // nothing to do
            }
        });

        experimentNameEditor.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                updateExperimentEditor(false);
            }
        });

        experimentNameRevertButton = new JButton("Revert");
        experimentNameRevertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String experimentName = currentDialogModel.getExperimentName();
                if (!experimentName.trim().isEmpty()) {
                    experimentNameEditor.setText(experimentName);
                }
            }
        });
        stringBox.setColorEnabled(experimentNameEditor, false);

        acquisitionNameTitle = new JLabel("Acquisition Name:");
        acquisitionNameEditor = new TextFieldButton();
        acquisitionNameEditor.setAlwaysUpdateTextField(true);
        acquisitionNameEditor.setTextButton("Apply");
        acquisitionNameEditor.setTextFieldColumns(30);
        acquisitionNameEditor.addTextFieldListener(new ITextFieldListener() {
            @Override
            public void textChanged(EventObject event) {
                String acquisitionName = currentDialogModel.getAcquisitionName();
                if (!acquisitionNameEditor.getText().equalsIgnoreCase(acquisitionName)) {
                    updateAcquisitionEditor(true);
                }
            }

            @Override
            public void actionPerformed(EventObject event) {
                // nothing to do
            }
        });

        acquisitionNameEditor.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                updateAcquisitionEditor(false);
            }
        });

        acquisitionNameRevertButton = new JButton("Revert");
        acquisitionNameRevertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String acquisitionName = currentDialogModel.getAcquisitionName();
                if (!acquisitionName.trim().isEmpty()) {
                    acquisitionNameEditor.setText(acquisitionName);
                }
            }
        });
        stringBox.setColorEnabled(acquisitionNameEditor, false);

        // Experiment Index
        experimentIndexPanel = new JPanel(new GridBagLayout());
        resetExperimentIndexButton = new StringButton();
        resetExperimentIndexButton.setButtonLook(true);
        resetExperimentIndexButton.setText("Reset Experiment Index");
        resetExperimentIndexButton.setCometeFont(FontTool.getCometeFont(closeButton.getFont()));
        incrementExperimentIndexButton = new StringButton();
        incrementExperimentIndexButton.setButtonLook(true);
        incrementExperimentIndexButton.setText("Increment Experiment Index");
        incrementExperimentIndexButton.setCometeFont(FontTool.getCometeFont(closeButton.getFont()));
        stringBox.setSettable(resetExperimentIndexButton, false);
        stringBox.setSettable(incrementExperimentIndexButton, false);

        GridBagConstraints resetConstraints = new GridBagConstraints();
        resetConstraints.fill = GridBagConstraints.VERTICAL;
        resetConstraints.gridx = 0;
        resetConstraints.gridy = 0;
        resetConstraints.weightx = 0;

        resetConstraints.weighty = 1;
        resetConstraints.insets = new Insets(0, 0, 0, 5);
        experimentIndexPanel.add(resetExperimentIndexButton, resetConstraints);
        GridBagConstraints incrementConstraints = new GridBagConstraints();
        incrementConstraints.fill = GridBagConstraints.VERTICAL;
        incrementConstraints.gridx = 1;
        incrementConstraints.gridy = 0;
        incrementConstraints.weightx = 0;
        incrementConstraints.weighty = 1;
        incrementConstraints.insets = new Insets(0, 0, 0, 5);
        experimentIndexPanel.add(incrementExperimentIndexButton, incrementConstraints);
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 2;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        experimentIndexPanel.add(Box.createGlue(), glueConstraints);

        // NxEntry Name
        nxEntryNameEditButtonPanel = new JPanel(new BorderLayout());
        nxEntryNameTitle = new JLabel("NxEntry Name:");
        nxEntryNameViewer = new Label();
        nxEntryNameViewer.setCometeFont(CometeFont.DEFAULT_FONT);
        nxEntryNameEditButton = new JButton("Edit NxEntryName Model");
        nxEntryNameEditButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nameModelEditDialog.updateSelectedValue(false);
                nameModelEditDialog.pack();
                nameModelEditDialog.setLocationRelativeTo(NxEntryNameEditDialog.this);
                nameModelEditDialog.setVisible(true);
            }
        });
        nxEntryNameEditButtonPanel.add(nxEntryNameEditButton, BorderLayout.WEST);
        nxEntryNameEditButtonPanel.add(Box.createGlue(), BorderLayout.CENTER);
        stringBox.setColorEnabled(nxEntryNameViewer, false);

        // Update availabilities
        updateExperimentEditor(false);
        updateAcquisitionEditor(false);

        // global layout
        mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints experimentNameTitleConstraints = new GridBagConstraints();
        experimentNameTitleConstraints.fill = GridBagConstraints.BOTH;
        experimentNameTitleConstraints.gridx = 0;
        experimentNameTitleConstraints.gridy = 0;
        experimentNameTitleConstraints.weightx = 0;
        experimentNameTitleConstraints.weighty = 0;
        experimentNameTitleConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(experimentNameTitle, experimentNameTitleConstraints);
        GridBagConstraints experimentNameEditorConstraints = new GridBagConstraints();
        experimentNameEditorConstraints.fill = GridBagConstraints.BOTH;
        experimentNameEditorConstraints.gridx = 1;
        experimentNameEditorConstraints.gridy = 0;
        experimentNameEditorConstraints.weightx = 1;
        experimentNameEditorConstraints.weighty = 0;
        experimentNameEditorConstraints.insets = new Insets(5, 0, 0, 5);
        mainPanel.add(experimentNameEditor, experimentNameEditorConstraints);
        GridBagConstraints experimentNameRevertButtonConstraints = new GridBagConstraints();
        experimentNameRevertButtonConstraints.fill = GridBagConstraints.BOTH;
        experimentNameRevertButtonConstraints.gridx = 2;
        experimentNameRevertButtonConstraints.gridy = 0;
        experimentNameRevertButtonConstraints.weightx = 0;
        experimentNameRevertButtonConstraints.weighty = 0;
        experimentNameRevertButtonConstraints.insets = new Insets(5, 0, 0, 5);
        mainPanel.add(experimentNameRevertButton, experimentNameRevertButtonConstraints);

        GridBagConstraints acquisitionNameTitleConstraints = new GridBagConstraints();
        acquisitionNameTitleConstraints.fill = GridBagConstraints.BOTH;
        acquisitionNameTitleConstraints.gridx = 0;
        acquisitionNameTitleConstraints.gridy = 1;
        acquisitionNameTitleConstraints.weightx = 0;
        acquisitionNameTitleConstraints.weighty = 0;
        acquisitionNameTitleConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(acquisitionNameTitle, acquisitionNameTitleConstraints);
        GridBagConstraints acquisitionNameEditorConstraints = new GridBagConstraints();
        acquisitionNameEditorConstraints.fill = GridBagConstraints.BOTH;
        acquisitionNameEditorConstraints.gridx = 1;
        acquisitionNameEditorConstraints.gridy = 1;
        acquisitionNameEditorConstraints.weightx = 1;
        acquisitionNameEditorConstraints.weighty = 0;
        acquisitionNameEditorConstraints.insets = new Insets(5, 0, 0, 5);
        mainPanel.add(acquisitionNameEditor, acquisitionNameEditorConstraints);
        GridBagConstraints acquisitionNameRevertButtonConstraints = new GridBagConstraints();
        acquisitionNameRevertButtonConstraints.fill = GridBagConstraints.BOTH;
        acquisitionNameRevertButtonConstraints.gridx = 2;
        acquisitionNameRevertButtonConstraints.gridy = 1;
        acquisitionNameRevertButtonConstraints.weightx = 0;
        acquisitionNameRevertButtonConstraints.weighty = 0;
        acquisitionNameRevertButtonConstraints.insets = new Insets(5, 0, 0, 5);
        mainPanel.add(acquisitionNameRevertButton, acquisitionNameRevertButtonConstraints);

        GridBagConstraints experimentIndexConstraints = new GridBagConstraints();
        experimentIndexConstraints.fill = GridBagConstraints.HORIZONTAL;
        experimentIndexConstraints.gridx = 0;
        experimentIndexConstraints.gridy = 2;
        experimentIndexConstraints.weightx = 1;
        experimentIndexConstraints.weighty = 0;
        experimentIndexConstraints.gridwidth = GridBagConstraints.REMAINDER;
        experimentIndexConstraints.insets = new Insets(10, 5, 0, 5);
        mainPanel.add(experimentIndexPanel, experimentIndexConstraints);

        GridBagConstraints nxEntryNameTitleConstraints = new GridBagConstraints();
        nxEntryNameTitleConstraints.fill = GridBagConstraints.BOTH;
        nxEntryNameTitleConstraints.gridx = 0;
        nxEntryNameTitleConstraints.gridy = 3;
        nxEntryNameTitleConstraints.weightx = 0;
        nxEntryNameTitleConstraints.weighty = 0;
        nxEntryNameTitleConstraints.insets = new Insets(10, 5, 0, 5);
        mainPanel.add(nxEntryNameTitle, nxEntryNameTitleConstraints);
        GridBagConstraints nxEntryNameViewerConstraints = new GridBagConstraints();
        nxEntryNameViewerConstraints.fill = GridBagConstraints.BOTH;
        nxEntryNameViewerConstraints.gridx = 1;
        nxEntryNameViewerConstraints.gridy = 3;
        nxEntryNameViewerConstraints.weightx = 1;
        nxEntryNameViewerConstraints.weighty = 0;
        nxEntryNameViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        nxEntryNameViewerConstraints.insets = new Insets(10, 5, 0, 5);
        mainPanel.add(nxEntryNameViewer, nxEntryNameViewerConstraints);

        GridBagConstraints nxEntryNameEditButtonConstraints = new GridBagConstraints();
        nxEntryNameEditButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
        nxEntryNameEditButtonConstraints.gridx = 0;
        nxEntryNameEditButtonConstraints.gridy = 4;
        nxEntryNameEditButtonConstraints.weightx = 1;
        nxEntryNameEditButtonConstraints.weighty = 0;
        nxEntryNameEditButtonConstraints.gridwidth = GridBagConstraints.REMAINDER;
        nxEntryNameEditButtonConstraints.insets = new Insets(10, 5, 0, 5);
        mainPanel.add(nxEntryNameEditButtonPanel, nxEntryNameEditButtonConstraints);

        GridBagConstraints closeButtonConstraints = new GridBagConstraints();
        closeButtonConstraints.fill = GridBagConstraints.NONE;
        closeButtonConstraints.gridx = 2;
        closeButtonConstraints.gridy = 5;
        closeButtonConstraints.weightx = 0;
        closeButtonConstraints.weighty = 0;
        closeButtonConstraints.insets = new Insets(20, 0, 5, 5);
        mainPanel.add(closeButton, closeButtonConstraints);
    }

    @Override
    public void setAcquisitionName(String acquisitionName) {
        acquisitionNameEditor.setText(acquisitionName);
    }

    @Override
    public void setExperimentName(String experimentName) {
        experimentNameEditor.setText(experimentName);
    }

    protected void updateAcquisitionEditor(boolean isChange) {
        acquisitionNameEditor.setButtonEnabled(isChange);
        acquisitionNameRevertButton.setEnabled(isChange);
    }

    protected void updateExperimentEditor(boolean isChange) {
        experimentNameEditor.setButtonEnabled(isChange);
        experimentNameRevertButton.setEnabled(isChange);
    }

    public NxEntryNameModelEditDialog getNameModelEditDialog() {
        return nameModelEditDialog;
    }

    @Override
    protected void connectDialog() {
        closeButton.grabFocus();
        currentDialogModel.connectTargetRedirectors(stringBox);
        nameModelEditDialog.connectDialog(stringBox);
        stringBox.connectWidget(experimentNameEditor, currentDialogModel.getExperimentNameModelKey());
        stringBox.connectWidget(acquisitionNameEditor, currentDialogModel.getAcquisitionNameModelKey());
        stringBox.connectWidget(resetExperimentIndexButton, currentDialogModel.getResetExperimentIndexModelKey());
        stringBox.connectWidget(incrementExperimentIndexButton,
                currentDialogModel.getIncrementExperimentIndexModelKey());
        stringBox.connectWidget(nxEntryNameViewer, currentDialogModel.getNxEntryNameModel());

        experimentNameEditor.setText(currentDialogModel.getExperimentName());
        acquisitionNameEditor.setText(currentDialogModel.getAcquisitionName());
    }

    @Override
    protected void clearConnections() {
        currentDialogModel.disconnectTargetRedirectors(stringBox);
        nameModelEditDialog.clearConnections(stringBox);
        stringBox.disconnectWidgetFromAll(experimentNameEditor);
        stringBox.disconnectWidgetFromAll(acquisitionNameEditor);
        stringBox.disconnectWidgetFromAll(resetExperimentIndexButton);
        stringBox.disconnectWidgetFromAll(incrementExperimentIndexButton);
        stringBox.disconnectWidgetFromAll(nxEntryNameViewer);
    }

}
