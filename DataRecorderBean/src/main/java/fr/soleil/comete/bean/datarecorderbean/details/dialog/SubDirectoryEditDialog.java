package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.SubDirectoryDialogModel;
import fr.soleil.comete.bean.datarecorderbean.details.utils.RelativeDirectoryFileFilter;
import fr.soleil.comete.bean.technicaldata.MemoryComboPref;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.lib.project.swing.icons.Icons;

public class SubDirectoryEditDialog extends AbstractFileEditionDialog {

    private static final long serialVersionUID = 4114596253532081479L;

    private JButton chooseDirectoryButton;
    private String parentDirectory;
    private TextTargetRedirector parentDirectoryRedirector;

    private SubDirectoryDialogModel currentDialogModel;

    public SubDirectoryEditDialog(Window parent, boolean modal) {
        super(parent, "Sub directory", modal);
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderIcon(iconsManager.getIcon("DataRecorder.directory.sub"));
        setHeaderText(resourceBundle.getString("DataRecorderState.SubDirectory.Header"));
        setInvalidChars(Arrays.asList(
                resourceBundle.getString("DataRecorderState.SubDirectory.InvalidCharacters").trim().split("---")));
        setDefaultIcon(iconsManager.getIcon("DataRecorder.dialog.default"));
        setApplyIcon(iconsManager.getIcon("DataRecorder.dialog.apply"));
        setRevertIcon(iconsManager.getIcon("DataRecorder.dialog.revert"));
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        super.updateModel(model);
        model.updateSubDirectoryDialogModel(currentDialogModel);
    }

    @Override
    protected void initDialogModel() {
        currentDialogModel = new SubDirectoryDialogModel();
        dialogModel = currentDialogModel;
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        super.initAndAddOtherComponentsInMainPanel();
        memoryComboTitleLabel.setText("Sub directory:");

        parentDirectoryRedirector = new TextTargetRedirector() {

            @Override
            public void methodToRedirect(String data) {
                parentDirectory = data;
                if ((data == null) || (data.trim().isEmpty()) || (!new File(data).exists())) {
                    if (chooseDirectoryButton.isEnabled()) {
                        chooseDirectoryButton.setEnabled(false);
                    }
                } else {
                    if (!chooseDirectoryButton.isEnabled()) {
                        chooseDirectoryButton.setEnabled(true);
                    }
                }
            }
        };
    }

    @Override
    protected AbstractButton getOtherActionButton() {
        if (chooseDirectoryButton == null) {
            chooseDirectoryButton = new JButton("Choose Directory...");
            chooseDirectoryButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    // open File chooser to select sub directory
                    JFileChooser fc = new JFileChooser();
                    // create files which can be choosen by the user
                    RelativeDirectoryFileFilter filter = new RelativeDirectoryFileFilter(parentDirectory);
                    filter.setParentDirectoryTitle("Project");
                    fc.setFileFilter(filter);
                    filter = null;
                    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    fc.setAcceptAllFileFilterUsed(false);
                    // set current directory to preoject directory
                    fc.setCurrentDirectory(new File(parentDirectory));
                    // set title
                    fc.setDialogTitle("Select project sub directory");
                    int iReturnVal = fc.showOpenDialog(chooseDirectoryButton);
                    if (iReturnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        if (fc.accept(file)) {
                            // get only value of sub directory choosen
                            int iLenghtProjectDirValue = parentDirectory.length();

                            if (iLenghtProjectDirValue < file.getAbsolutePath().length()) {
                                String strSubDir = file.getAbsolutePath().substring(iLenghtProjectDirValue,
                                        file.getAbsolutePath().length());
                                synchronized (memoryCombo) {
                                    memoryCombo.getTextField().setText(strSubDir);
                                }
                            }
                        } else {
                            // this is not a sub directory
                            JOptionPane.showMessageDialog(chooseDirectoryButton,
                                    "The choosen directory is not a project's sub directory", "Sub directory",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            });
            chooseDirectoryButton.setEnabled(false);
        }
        return chooseDirectoryButton;
    }

    @Override
    protected MemoryComboPref generateMemoryCombo() {
        return new MemoryComboPref("sub_directory");
    }

    @Override
    public void connectDialog() {
        stringBox.connectWidget(parentDirectoryRedirector, currentDialogModel.getParentDirectoryModelKey());
        super.connectDialog();
    }

    @Override
    protected void clearConnections() {
        super.clearConnections();
        stringBox.disconnectWidgetFromAll(parentDirectoryRedirector);
        chooseDirectoryButton.setEnabled(false);
    }

}
