package fr.soleil.comete.bean.datarecorderbean.details.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * A {@link FileFilter} that only accepts directories that are children of a previously set parent
 * directory
 * 
 * @author MARECHAL
 * @author girardot
 */
public class RelativeDirectoryFileFilter extends FileFilter {

    private File parentDirectory;
    private String parentDirectoryTitle;

    /**
     * Constructor
     * 
     * @param parentDirectoryPath The path to the referent parent directory
     */
    public RelativeDirectoryFileFilter(String parentDirectoryPath) {
        super();
        parentDirectory = new File(parentDirectoryPath);
    }

    public boolean accept(File f) {
        if (parentDirectory != null) {
            if (f.isDirectory()) {
                return f.getAbsolutePath().startsWith(parentDirectory.getAbsolutePath());
            }

        }
        return false;
    }

    /**
     * Returns the parent directory title
     * 
     * @return a {@link String}
     */
    public String getParentDirectoryTitle() {
        return parentDirectoryTitle;
    }

    /**
     * Sets a title to the parentDirectory. Used in {@link #getDescription()}
     * 
     * @param mainDirectoryTitle The title to set
     */
    public void setParentDirectoryTitle(String mainDirectoryTitle) {
        this.parentDirectoryTitle = mainDirectoryTitle;
    }

    public String getDescription() {
        return getParentDirectoryTitle() + "'s sub directories";
    }
}