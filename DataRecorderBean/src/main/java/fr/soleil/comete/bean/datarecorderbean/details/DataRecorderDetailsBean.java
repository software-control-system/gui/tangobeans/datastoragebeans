package fr.soleil.comete.bean.datarecorderbean.details;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageManager;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel.Availabilities;
import fr.soleil.comete.bean.datarecorder.model.IDataRecorderView;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.PostRecordingDialogModel;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.AbstractRecorderEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.DataModelEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.FileNameEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.LoadConfigurationEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.NxEntryNameEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.PostRecordingEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.ProjectDirectoryEditDialog;
import fr.soleil.comete.bean.datarecorderbean.details.dialog.SubDirectoryEditDialog;
import fr.soleil.comete.bean.technicaldata.full.util.Utils;
import fr.soleil.comete.bean.technicaldata.light.TechnicalDataLightBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.swing.ITextCollectionTransferable;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.lib.project.swing.dialog.JDialogUtils;
import net.miginfocom.swing.MigLayout;

public class DataRecorderDetailsBean extends AbstractTangoBox implements IDataRecorderView {

    private static final long serialVersionUID = -978117667765595216L;

    private static final String INFO_TAB = "info";
    private static final String WARNING_TAB = "warning";
    private static final String ERROR_TAB = "error";

    private static final String EDIT = "Edit...";
    private static final String SAVE_AS_BUTTON_TEXT = "Save As...";
    private static final String MANAGE_BUTTON_TEXT = "Manage...";
    private static final String ACK_BUTTON_TEXT = "Ack error(s)";
    private static final String CURRENT_CONFIGURATION_TEXT = "Data recorder configuration:";

    // components which show datarecorder's attributes values
    private TextField projectDirectoryViewer;
    private TextField subDirectoryViewer;
    private TextField fileNameViewer;
    private TextField nxEntryNameViewer;
    private TextField globalPostRecordViewer;
    private CheckBox nxentryPostRecordingCB;
    private TextField selectedDataModelViewer;

    // buttons which open corresponding dialogs to modify tango attr values
    private JButton projectDirectoryButton;
    private JButton subDirectoryButton;
    private JButton fileNameButton;
    private JButton nxEntryNameButton;
    private JButton postRecordButton;
    private JButton selectedDataModelButton;

    private JDialog postRecordDialog;
    private JButton globalPostRecordButton;
    private JButton nxEntryPostRecordButton;

    private ProjectDirectoryEditDialog projectDirectoryEditDialog;
    private SubDirectoryEditDialog subDirectoryEditDialog;
    private FileNameEditDialog fileNameEditDialog;
    private NxEntryNameEditDialog nxEntryNameEditDialog;
    private PostRecordingEditDialog globalPostRecordingEditDialog;
    private PostRecordingEditDialog nxEntryPostRecordingEditDialog;
    private DataModelEditDialog dataModelEditDialog;
    private LoadConfigurationEditDialog loadConfigurationEditDialog;

    private JPanel logLabelPanel;
    private CardLayout logLabelLayout;
    private JButton errorAckButton;
    private JButton warningAckButton;

    // Configuration viewers and connectors
    private StringMatrixComboBoxViewer configurationComboBox;
    private ItemListener configComboListener;
    private TextTargetRedirector currentConfigRedirector;
    private JButton saveConfigAsButton;
    private JButton manageConfigButton;

    // beans
    private final TechnicalDataLightBean technicalDataLightBean;

    private TitledBorder dataRecorderTitledBorder;
    private TitledBorder technicalDataTitledBorder;

    private boolean displayMessageOnConnectionError;

    private boolean isInit;
    private boolean start = false;
    private final DataRecorderModel drModel;

    private boolean isAuthentificationLightBeanConnected = false;

    private final StringMatrixBox stringMatrixBox;
    private final BooleanScalarBox booleanBox;

    public DataRecorderDetailsBean() {

        drModel = new DataRecorderModel();
        drModel.setDefaultStringBox(stringBox);
        drModel.addDataRecorderListener(this);

        isInit = false;

        displayMessageOnConnectionError = false;

        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);
        booleanBox = (BooleanScalarBox) CometeBoxProvider.getCometeBox(BooleanScalarBox.class);

        // beans instantiation
        technicalDataLightBean = new TechnicalDataLightBean();
        technicalDataLightBean.setDisplayMessageOnConnectionError(displayMessageOnConnectionError);

        initComponents();
        layoutComponents();

        projectDirectoryButton.setEnabled(false);
        subDirectoryButton.setEnabled(false);
        fileNameButton.setEnabled(false);
        nxEntryNameButton.setEnabled(false);
        postRecordButton.setEnabled(false);
        globalPostRecordButton.setEnabled(false);
        nxEntryPostRecordButton.setEnabled(false);

        saveConfigAsButton.setEnabled(false);
        manageConfigButton.setEnabled(false);
    }

    @Override
    public void setModel(String model) {
        drModel.setModel(model);
        super.setModel(model);
        updateModels();
        if (start) {
            start = false;
            start();
        }
    }

    @Override
    public void start() {
        if (!start) {
            // super.start();
            start = true;
            refreshGUI();
            technicalDataLightBean.start();
        }
    }

    @Override
    public void stop() {
        if (start) {
            start = false;
            super.stop();
            technicalDataLightBean.stop();
        }
    }

    private void initComponents() {
        // datarecorder part
        initInfosComponents();

        // warning label part
        ActionListener ackButtonListener = (e) -> {
            drModel.ackError();
        };
        errorAckButton = new JButton(ACK_BUTTON_TEXT);
        errorAckButton.addActionListener(ackButtonListener);
        warningAckButton = new JButton(ACK_BUTTON_TEXT);
        warningAckButton.addActionListener(ackButtonListener);

        // config part
        initConfigComponents();

        // technical data part
        initTechnicalComponents();
    }

    private void initInfosComponents() {
        projectDirectoryViewer = new TextField();
        subDirectoryViewer = new TextField();
        fileNameViewer = new TextField();
        nxEntryNameViewer = new TextField();
        globalPostRecordViewer = new TextField();
        nxentryPostRecordingCB = new CheckBox();
        selectedDataModelViewer = new TextField();

        stringBox.setErrorText(projectDirectoryViewer, "Unable to read attribute projectDirectory");
        stringBox.setErrorText(subDirectoryViewer, "Unable to read attribute subDirectory");

        // stringBox.setReadOnly(subDirectoryViewer, true);//bug dans ReadOnly
        stringBox.setUserEditable(subDirectoryViewer, false);
        stringBox.setUserEditable(fileNameViewer, false);
        stringBox.setUserEditable(nxEntryNameViewer, false);

        booleanBox.setColorEnabled(nxentryPostRecordingCB, false);
        booleanBox.setUserEnabled(nxentryPostRecordingCB, false);

        final Window thisWindow = CometeUtils.getWindowForComponent(this);

        projectDirectoryButton = new JButton(EDIT);
        projectDirectoryButton.addActionListener((e) -> {
            // Project Directory
            if (projectDirectoryEditDialog == null) {
                projectDirectoryEditDialog = new ProjectDirectoryEditDialog(thisWindow, true);
                projectDirectoryEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(projectDirectoryEditDialog);
        });

        subDirectoryButton = new JButton(EDIT);
        subDirectoryButton.addActionListener((e) -> {
            // Sub Directory
            if (subDirectoryEditDialog == null) {
                subDirectoryEditDialog = new SubDirectoryEditDialog(thisWindow, true);
                subDirectoryEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(subDirectoryEditDialog);
        });

        fileNameButton = new JButton(EDIT);
        fileNameButton.addActionListener((e) -> {
            // File Name
            if (fileNameEditDialog == null) {
                fileNameEditDialog = new FileNameEditDialog(thisWindow, true);
                fileNameEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(fileNameEditDialog);
        });

        nxEntryNameButton = new JButton(EDIT);
        nxEntryNameButton.addActionListener((e) -> {
            // NxEntry Name
            if (nxEntryNameEditDialog == null) {
                nxEntryNameEditDialog = new NxEntryNameEditDialog(thisWindow, true);
                nxEntryNameEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(nxEntryNameEditDialog);
        });

        postRecordButton = new JButton(EDIT);
        postRecordButton.addActionListener((e) -> {
            if (postRecordDialog == null) {
                postRecordDialog = new JDialog(thisWindow, ModalityType.APPLICATION_MODAL);
                postRecordDialog.setTitle("Post Recording Type");
                JPanel panel = new JPanel();

                JLabel message = new JLabel("This parameter is exclusive");

                panel.setLayout(new GridBagLayout());

                GridBagConstraints labelConstraint = new GridBagConstraints();
                labelConstraint.gridx = 0;
                labelConstraint.gridy = 0;
                labelConstraint.gridwidth = GridBagConstraints.REMAINDER;
                labelConstraint.insets = new Insets(0, 0, 10, 0);

                GridBagConstraints globalConstraint = new GridBagConstraints();
                globalConstraint.gridx = 0;
                globalConstraint.gridy = 1;
                globalConstraint.weightx = 1;
                globalConstraint.anchor = GridBagConstraints.BASELINE_TRAILING;

                GridBagConstraints nxEntryConstraint = new GridBagConstraints();
                nxEntryConstraint.gridx = 1;
                nxEntryConstraint.gridy = 1;
                nxEntryConstraint.weightx = 1;
                nxEntryConstraint.anchor = GridBagConstraints.BASELINE_LEADING;
                nxEntryConstraint.insets = new Insets(0, 15, 0, 0);

                panel.add(message, labelConstraint);
                panel.add(globalPostRecordButton, globalConstraint);
                panel.add(nxEntryPostRecordButton, nxEntryConstraint);

                postRecordDialog.setContentPane(panel);
                JDialogUtils.installEscapeCloseOperation(postRecordDialog);
            }

            if (drModel != null) {
                List<String> scriptList = drModel.getScriptList(DataRecorderModel.PostRecordingType.ENTRY);
                nxEntryPostRecordButton.setEnabled(scriptList != null && !scriptList.isEmpty());
            }

            postRecordDialog.setPreferredSize(new Dimension(280, 120));
            postRecordDialog.pack();
            postRecordDialog.setLocationRelativeTo(DataRecorderDetailsBean.this);
            postRecordDialog.setVisible(true);
        });

        globalPostRecordButton = new JButton(DataRecorderModel.POST_RECORDING_FILE);
        globalPostRecordButton.addActionListener((e) -> {
            postRecordDialog.setVisible(false);

            // Global Post Record
            if (globalPostRecordingEditDialog == null) {
                globalPostRecordingEditDialog = new PostRecordingEditDialog(thisWindow, true,
                        PostRecordingDialogModel.POST_RECORD_GLOBAL);
                globalPostRecordingEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(globalPostRecordingEditDialog);
        });

        nxEntryPostRecordButton = new JButton(DataRecorderModel.POST_RECORDING_ENTRY);
        nxEntryPostRecordButton.addActionListener((e) -> {
            postRecordDialog.setVisible(false);

            // NxEntry Post Record
            if (nxEntryPostRecordingEditDialog == null) {
                nxEntryPostRecordingEditDialog = new PostRecordingEditDialog(thisWindow, true,
                        PostRecordingDialogModel.POST_RECORD_NXENTRY);
                nxEntryPostRecordingEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(nxEntryPostRecordingEditDialog);
        });

        selectedDataModelButton = new JButton(EDIT);
        selectedDataModelButton.addActionListener((e) -> {
            // Data Model
            if (dataModelEditDialog == null) {
                dataModelEditDialog = new DataModelEditDialog(thisWindow, true, selectedDataModelViewer.getText());
                dataModelEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(dataModelEditDialog);
        });

        ITextCollectionTransferable nexusFilenameTransfer = new ITextCollectionTransferable() {
            @Override
            public Collection<String> getTextCollectionToTransfert() {
                return getPathToCurrentFiles();
            }
        };

        TextTansfertHandler.registerTransferHandler(nexusFilenameTransfer, projectDirectoryViewer);
        TextTansfertHandler.registerTransferHandler(nexusFilenameTransfer, subDirectoryViewer);
        TextTansfertHandler.registerTransferHandler(nexusFilenameTransfer, fileNameViewer);
    }

    private void initConfigComponents() {

        configurationComboBox = new StringMatrixComboBoxViewer() {
            private static final long serialVersionUID = 3205753333091608962L;

            @Override
            public void setEnabled(boolean b) {
                super.setEnabled(b);

                saveConfigAsButton.setEnabled(b);
                manageConfigButton.setEnabled(b);
            }

            @Override
            public void setValueList(Object... valueList) {
                removeItemListener(configComboListener);
                super.setValueList(valueList);
                // setValueList will end by selecting first item
                // so we need to select the current config after
                if (currentConfigRedirector != null) {
                    setSelectedValue(currentConfigRedirector.getText());
                }
                addItemListener(configComboListener);
            }
        };
        configurationComboBox.setLinkPopupVisibilityWithEditable(false);
        configComboListener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String selectedValue = (String) configurationComboBox.getSelectedValue();
                    if (selectedValue != null) {
                        drModel.loadRecorderConfig(selectedValue);
                    }
                }
            }
        };
        configurationComboBox.addItemListener(configComboListener);

        currentConfigRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                configurationComboBox.removeItemListener(configComboListener);
                configurationComboBox.setSelectedValue(data);
                configurationComboBox.addItemListener(configComboListener);
            }
        };

        // Init buttons and connections
        final Window thisWindow = CometeUtils.getWindowForComponent(this);

        saveConfigAsButton = new JButton(SAVE_AS_BUTTON_TEXT);
        saveConfigAsButton
                .setToolTipText("Save the parameter in blue in a data recorder configuration command SaveConfigAs");
        saveConfigAsButton.addActionListener((e) -> {
            String newConfig = null;
            boolean isAlphanumeric = false;
            do {
                newConfig = JOptionPane.showInputDialog(thisWindow,
                        "Please give a name for your new recording configuration", "New Recording Configuration",
                        JOptionPane.QUESTION_MESSAGE);
                // null result means cancel button has been used
                if (newConfig != null) {
                    if (newConfig.trim().isEmpty()) {
                        JOptionPane.showMessageDialog(thisWindow, "You must give a name for the configuration",
                                "Empty name", JOptionPane.WARNING_MESSAGE);
                    } else {
                        isAlphanumeric = Utils.isAlphanumericUnderscore(newConfig);
                        if (!isAlphanumeric) {
                            JOptionPane.showMessageDialog(thisWindow, "Only alphanumeric characters are allowed",
                                    "Illegal characters", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            } while ((newConfig != null) && (newConfig.trim().isEmpty() || !isAlphanumeric));
            if ((newConfig != null) && !newConfig.trim().isEmpty() && isAlphanumeric) {
                drModel.newRecorderConfiguration(newConfig);
            }
        });

        manageConfigButton = new JButton(MANAGE_BUTTON_TEXT);
        manageConfigButton.setToolTipText("Manage the data recorder configurations");
        manageConfigButton.addActionListener((e) -> {
            // Load Configuration
            if (loadConfigurationEditDialog == null) {
                loadConfigurationEditDialog = new LoadConfigurationEditDialog(thisWindow, true);
                loadConfigurationEditDialog.initIconsAndResources(iconsManager, resourceBundle);
            }
            openEditDialog(loadConfigurationEditDialog);
        });
    }

    private void initTechnicalComponents() {
        // TODO bean is currently initialized in constructor
        // technicalDataLightBean = new TechnicalDataLightBean();
        // technicalDataLightBean.setDisplayMessageOnConnectionError(displayMessageOnConnectionError);
    }

    private void layoutComponents() {
        JPanel dataRecorderPanel = layoutDataRecorderPanel();
        dataRecorderTitledBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "DATA RECORDER");
        dataRecorderPanel.setBorder(BorderFactory.createCompoundBorder(dataRecorderTitledBorder,
                BorderFactory.createEmptyBorder(0, 5, 5, 5)));

        JPanel technicalDataPanel = layoutTechnicalDataPanel();
        technicalDataTitledBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "TECHNICAL DATA");
        technicalDataPanel.setBorder(technicalDataTitledBorder);

        setLayout(new BorderLayout());
        add(dataRecorderPanel, BorderLayout.CENTER);
        add(technicalDataPanel, BorderLayout.SOUTH);
    }

    private JPanel layoutDataRecorderPanel() {
        JPanel infosPanel = layoutInfosComponents();
        Box configurationBox = layoutConfigComponents();
        JPanel logLabelPanel = layoutLogLabelPanel();

        JPanel dataRecorderPanel = new JPanel(new BorderLayout(0, 15));
        dataRecorderPanel.add(infosPanel, BorderLayout.NORTH);
        dataRecorderPanel.add(logLabelPanel, BorderLayout.CENTER);
        dataRecorderPanel.add(configurationBox, BorderLayout.SOUTH);

        return dataRecorderPanel;
    }

    private JPanel layoutLogLabelPanel() {
        // info tab
        JLabel infoLabel = new JLabel("No new problem.");
        infoLabel.setIcon(iconsManager.getIcon("DataRecorder.info"));

        Box infoBox = Box.createHorizontalBox();
        infoBox.add(infoLabel);
        infoBox.add(Box.createHorizontalGlue());

        // warning tab
        JLabel warningLabel = new JLabel("Some warning(s) occured. You can check it in history tab.");
        warningLabel.setIcon(iconsManager.getIcon("DataRecorder.warning"));

        Box warningBox = Box.createHorizontalBox();
        warningBox.add(warningLabel);
        warningBox.add(Box.createHorizontalStrut(5));
        warningBox.add(warningAckButton);
        warningBox.add(Box.createHorizontalGlue());

        // error tab
        JLabel errorLabel = new JLabel("Some error(s) occured. You can check it in history tab.");
        errorLabel.setIcon(iconsManager.getIcon("DataRecorder.error"));

        Box errorBox = Box.createHorizontalBox();
        errorBox.add(errorLabel);
        errorBox.add(Box.createHorizontalStrut(5));
        errorBox.add(errorAckButton);
        errorBox.add(Box.createHorizontalGlue());

        logLabelLayout = new CardLayout();
        logLabelLayout.addLayoutComponent(infoBox, INFO_TAB);
        logLabelLayout.addLayoutComponent(warningBox, WARNING_TAB);
        logLabelLayout.addLayoutComponent(errorBox, ERROR_TAB);

        logLabelPanel = new JPanel(logLabelLayout);
        logLabelPanel.add(infoBox, INFO_TAB);
        logLabelPanel.add(warningBox, WARNING_TAB);
        logLabelPanel.add(errorBox, ERROR_TAB);

        return logLabelPanel;
    }

    private JPanel layoutTechnicalDataPanel() {
        return technicalDataLightBean;
    }

    private JPanel layoutInfosComponents() {
        JLabel projectDirectoryLabel = new JLabel("Project Directory:");
        JLabel subDirectoryLabel = new JLabel("Sub Directory:");
        JLabel fileNameLabel = new JLabel("File Name:");
        JLabel nxEntryNameLabel = new JLabel("NXEntry Name:");
        JLabel golbalPostRecordLabel = new JLabel("Post Recording script:");
        JLabel nxEntryPostRecordLabel = new JLabel("NXentry");
        JLabel selectedDataModelLabel = new JLabel("Selected data model:");

        JLabel blueLabel = new JLabel("(The parameters in blue are saved in a datarecorder configurations)");
        blueLabel.setForeground(Color.BLUE);
        projectDirectoryLabel.setForeground(Color.BLUE);
        fileNameLabel.setForeground(Color.BLUE);
        nxEntryNameLabel.setForeground(Color.BLUE);
        golbalPostRecordLabel.setForeground(Color.BLUE);
        nxEntryPostRecordLabel.setForeground(Color.BLUE);

        MigLayout migLayout = new MigLayout("insets 0, wrap 3", "[left]rel[grow,fill]rel[]", "[]0[fill]10[fill]");
        JPanel infosPanel = new JPanel(migLayout);

        infosPanel.add(blueLabel, "span, right");

        infosPanel.add(projectDirectoryLabel);
        infosPanel.add(projectDirectoryViewer);
        infosPanel.add(projectDirectoryButton);

        infosPanel.add(subDirectoryLabel);
        infosPanel.add(subDirectoryViewer);
        infosPanel.add(subDirectoryButton);

        infosPanel.add(fileNameLabel);
        infosPanel.add(fileNameViewer);
        infosPanel.add(fileNameButton);

        infosPanel.add(nxEntryNameLabel);
        infosPanel.add(nxEntryNameViewer);
        infosPanel.add(nxEntryNameButton);

        infosPanel.add(golbalPostRecordLabel);
        infosPanel.add(globalPostRecordViewer, "split 3, growx");
        infosPanel.add(nxentryPostRecordingCB);
        infosPanel.add(nxEntryPostRecordLabel);
        infosPanel.add(postRecordButton);

        infosPanel.add(selectedDataModelLabel);
        infosPanel.add(selectedDataModelViewer);
        infosPanel.add(selectedDataModelButton);

        return infosPanel;
    }

    private Box layoutConfigComponents() {
        JLabel currentConfigLabel = new JLabel(CURRENT_CONFIGURATION_TEXT);
        currentConfigLabel
                .setToolTipText("Data recorder configuration that contains all the parameters displayed in blue");
        Box configPanel = Box.createHorizontalBox();

        configPanel.add(currentConfigLabel);
        configPanel.add(Box.createHorizontalStrut(5));
        configPanel.add(configurationComboBox);
        configPanel.add(Box.createHorizontalStrut(5));
        configPanel.add(Box.createHorizontalGlue());
        configPanel.add(saveConfigAsButton);
        configPanel.add(Box.createHorizontalStrut(5));
        configPanel.add(manageConfigButton);

        return configPanel;
    }

    /**
     * @return full path to nxs file, or null if at least one field is on error
     */
    public List<String> getPathToCurrentFiles() {
        List<String> result = new ArrayList<String>();

        if (model != null) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(model);
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute("currentFiles");
                    if (deviceAttribute != null) {
                        String[] resultList = deviceAttribute.extractStringArray();
                        if (resultList != null) {
                            for (String file : resultList) {
                                if (!result.contains(file)) {
                                    result.add(file);
                                }
                            }
                        }
                    }
                } catch (DevFailed e) {
                    DataRecorderMessageManager.notifyReadAttributeErrorDetected(model, "currentFiles", e, false);
                }
            }
        }
        return result;
    }

    @Override
    protected void refreshGUI() {
        if ((model != null) && !model.isEmpty()) {

            // Configuration tools connections
            stringBox.connectWidget(currentConfigRedirector,
                    generateAttributeKey(DataRecorderModel.CURRENT_CONFIG_ATTR));
            stringMatrixBox.connectWidget(configurationComboBox,
                    generateAttributeKey(DataRecorderModel.CONFIG_LIST_ATTR));

            // Connection for Global Post Recording
            stringBox.connectWidget(globalPostRecordViewer,
                    generateAttributeKey(DataRecorderModel.GLOBAL_POST_RECORDING_ATTR));
            booleanBox.connectWidget(nxentryPostRecordingCB,
                    generateAttributeKey(DataRecorderModel.NXENTRY_POST_RECORDING_ATTR));

            drModel.initConnections();
            initDetailsConnections();
            updateTabContent();
            drModel.computeDetailsAvailabilities();
            // Authentication update
            if (!isAuthentificationLightBeanConnected) {
                projectDirectoryViewer.setCometeForeground(CometeColor.RED);
                subDirectoryViewer.setCometeForeground(CometeColor.RED);
                cleanWidget(projectDirectoryViewer);
                cleanWidget(subDirectoryViewer);
            } else {
                stringBox.connectWidget(projectDirectoryViewer,
                        generateAttributeKey(DataRecorderModel.PROJECT_DIRECTORY_ATTR));
                stringBox.connectWidget(subDirectoryViewer, generateAttributeKey(DataRecorderModel.SUB_DIRECTORY_ATTR));
            }
        }
    }

    private void initDetailsConnections() {

        // Selected Data Model
        stringBox.connectWidget(selectedDataModelViewer, generateAttributeKey("currentDataModel"));

        // File Root Name
        stringBox.connectWidget(fileNameViewer, generateAttributeKey("fileName"));

        // NXEntry Name
        stringBox.connectWidget(nxEntryNameViewer, generateAttributeKey("nxentryName"));
    }

    @Override
    protected void clearGUI() {

        cleanDetailsModels();

    }

    /*
     * Cleans widgets in "Details" tab
     */
    private void cleanDetailsModels() {

        drModel.clearConnections();
        // Clean sub-beans
        technicalDataLightBean.setModel(null);

        // Clean regular attributes
        cleanWidget(projectDirectoryViewer);
        cleanWidget(subDirectoryViewer);
        cleanWidget(fileNameViewer);
        cleanWidget(nxEntryNameViewer);
        cleanWidget(globalPostRecordViewer);
        cleanWidget(nxentryPostRecordingCB);
        cleanWidget(selectedDataModelViewer);

        cleanWidget(currentConfigRedirector);
        cleanWidget(configurationComboBox);

        // Erase attributes color
        projectDirectoryViewer.setCometeForeground(CometeColor.BLACK);
        subDirectoryViewer.setCometeForeground(CometeColor.BLACK);

        projectDirectoryButton.setEnabled(false);
        subDirectoryButton.setEnabled(false);
        fileNameButton.setEnabled(false);
        postRecordButton.setEnabled(false);
        globalPostRecordButton.setEnabled(false);
        nxEntryPostRecordButton.setEnabled(false);
        nxEntryNameButton.setEnabled(false);
        selectedDataModelButton.setEnabled(false);
    }

    /*
     * Connects/disconnects widgets depending on selected tab in mainTabbedPane.
     * This is used to optimize resources consumption
     */
    private void updateTabContent() {
        if (getModel().isEmpty()) {
            cleanDetailsModels();
        } else {
            if (!isInit) {
                isInit = true;
                initDetailsConnections();
                drModel.initPostRecordingConnections();
            }
        }

    }

    private void updateModels() {

        String authModel = drModel.getAuthModel();
        String techModel = drModel.getTechModel();

        DataRecorderMessageManager.notifyNewMessageDetected("authModel=" + authModel, false);
        DataRecorderMessageManager.notifyNewMessageDetected("techModel=" + techModel, false);
        isAuthentificationLightBeanConnected = ((authModel != null) && !authModel.isEmpty());
        technicalDataLightBean.setModel(techModel);

        dataRecorderTitledBorder.setTitle(getModel());
        technicalDataTitledBorder.setTitle(drModel.getTechModel());
    }

    @Override
    public void updatePanel(final DataRecorderModel dataRecorderModel, UpdateMode mode) {
        // Update only if in details mode
        if ((dataRecorderModel != null) && (mode == UpdateMode.DETAILS_MODE)) {
            updateWarningLevel(dataRecorderModel.getLastErrorLevel());
        }
    }

    /*
     * Transforms an error level into a user readable text displayed in
     * warningLabel
     */
    private void updateWarningLevel(short level) {
        switch (level) {
            case DataRecorderModel.ERROR_LEVEL:
                logLabelLayout.show(logLabelPanel, ERROR_TAB);
                break;
            case DataRecorderModel.WARNING_LEVEL:
                logLabelLayout.show(logLabelPanel, WARNING_TAB);
                break;
            default:
                logLabelLayout.show(logLabelPanel, INFO_TAB);
                break;
        }

        revalidate();
        repaint();
    }

    @Override
    public void updateAvailabilities(Availabilities availabilities) {
        if (availabilities != null) {
            // Update dialog box button availabilities
            subDirectoryButton.setEnabled(availabilities.isSubDirAV() && isAuthentificationLightBeanConnected);
            projectDirectoryButton
                    .setEnabled(availabilities.isProjectDirectoryAV() && isAuthentificationLightBeanConnected);

            fileNameButton.setEnabled(availabilities.isFileNameAV());
            nxEntryNameButton.setEnabled(availabilities.isNXEntryNameAV());
            postRecordButton
                    .setEnabled(availabilities.isGeneralPostRecordAV() || availabilities.isNXEntryPostRecordAV());
            globalPostRecordButton.setEnabled(availabilities.isGeneralPostRecordAV());
            nxEntryPostRecordButton.setEnabled(availabilities.isNXEntryPostRecordAV());
            selectedDataModelButton.setEnabled(availabilities.isChangeDataModelAV());

            // Beans availabilities
            technicalDataLightBean.setConfigEditButtonEnabled(availabilities.isTechnicalDataAV());
        }
    }

    @Override
    protected void onConnectionError() {
        if (isDisplayMessageOnConnectionError()) {
            showMessageDialog(this, "Failed to connect to DataRecorder", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    /**
     * Open a dialog box initialized beforehand. Called on action on an 'Edit' button.
     * 
     * @param dialog the dialog box to open
     */
    private void openEditDialog(final AbstractRecorderEditDialog dialog) {
        dialog.updateDialogModel(drModel);
        dialog.pack();
        dialog.setLocationRelativeTo(DataRecorderDetailsBean.this);
        // XXX opening a dialog in a separate Thread is a bad practice
//        if (dialog instanceof PostRecordingEditDialog) {
//            new Thread() {
//                @Override
//                public void run() {
//                    dialog.setVisible(true);
//                }
//            }.start();
//        } else {
        dialog.setVisible(true);
//        }
    }

    private static void createAndShowGUI(final String... args) {
        String deviceName = null;
        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if ((input != null) && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }
        if (deviceName != null) {
            DataRecorderDetailsBean bean = new DataRecorderDetailsBean();
            bean.setModel(deviceName);
            bean.start();
            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args [0]: path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);
        SwingUtilities.invokeLater(() -> {
            createAndShowGUI(args);
        });
    }
}
