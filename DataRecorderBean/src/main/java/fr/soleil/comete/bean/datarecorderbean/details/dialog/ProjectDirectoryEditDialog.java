package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.SwingUtilities;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.FileEditionModel;
import fr.soleil.comete.bean.technicaldata.MemoryComboPref;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * This dialog is used to edit DataRecorder project directory. Its code is based on
 * "DgProjectDirectory" from former DataRecorderStateBean ATK based project.
 * 
 * @author MARECHAL
 * @author girardot
 */
public class ProjectDirectoryEditDialog extends MemoryComboPrefEditDialog {

    private static final long serialVersionUID = 3602614280935089926L;

    public final static String PROJECTS_DIRECTORIES_FILE = "proj_dir";

    public ProjectDirectoryEditDialog(Window parent, boolean modal) {
        super(parent, "Project Directory", modal);
        setHeaderText("Set the directory in which data will be recorded for the current project");
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderIcon(iconsManager.getIcon("DataRecorder.directory.root"));
        setDefaultIcon(iconsManager.getIcon("DataRecorder.dialog.default"));
        setApplyIcon(iconsManager.getIcon("DataRecorder.dialog.apply"));
        setRevertIcon(iconsManager.getIcon("DataRecorder.dialog.revert"));
    }

    @Override
    protected void initDialogModel() {
        dialogModel = new FileEditionModel();
    }

    @Override
    protected void clearDialog() {
        savePreferences();
        cleanSelectionCombo();
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        super.updateModel(model);
        model.updateProjectDirectoryDialogModel(dialogModel);
    }

    @Override
    protected void checksInputText() {
        excludeCharacter(true);
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        super.initAndAddOtherComponentsInMainPanel();

        mainPanel.setLayout(new GridBagLayout());

        memoryComboTitleLabel.setText("Project Directory:");

        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.NONE;
        titleConstraints.gridx = 0;
        titleConstraints.gridy = 0;
        titleConstraints.weightx = 0;
        titleConstraints.weighty = 0;
        titleConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(memoryComboTitleLabel, titleConstraints);
        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.fill = GridBagConstraints.HORIZONTAL;
        comboConstraints.gridx = 1;
        comboConstraints.gridy = 0;
        comboConstraints.weightx = 1;
        comboConstraints.weighty = 0;
        comboConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(memoryCombo, comboConstraints);
        GridBagConstraints applyConstraints = new GridBagConstraints();
        applyConstraints.fill = GridBagConstraints.NONE;
        applyConstraints.gridx = 2;
        applyConstraints.gridy = 0;
        applyConstraints.weightx = 0;
        applyConstraints.weighty = 0;
        applyConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(applyButton, applyConstraints);
        GridBagConstraints revertConstraints = new GridBagConstraints();
        revertConstraints.fill = GridBagConstraints.NONE;
        revertConstraints.gridx = 3;
        revertConstraints.gridy = 0;
        revertConstraints.weightx = 0;
        revertConstraints.weighty = 0;
        revertConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(revertButton, revertConstraints);
        GridBagConstraints defaultConstraints = new GridBagConstraints();
        defaultConstraints.fill = GridBagConstraints.NONE;
        defaultConstraints.gridx = 4;
        defaultConstraints.gridy = 0;
        defaultConstraints.weightx = 0;
        defaultConstraints.weighty = 0;
        defaultConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(setDefaultValueButton, defaultConstraints);

        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.NONE;
        closeConstraints.gridx = 4;
        closeConstraints.gridy = 1;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 0;
        closeConstraints.insets = new Insets(5, 0, 5, 5);
        mainPanel.add(applyButton, closeConstraints);
    }

    @Override
    protected MemoryComboPref generateMemoryCombo() {
        MemoryComboPref memoryComboPref = new MemoryComboPref(PROJECTS_DIRECTORIES_FILE);
        return memoryComboPref;
    }

    @Override
    protected ActionListener generateMemoryComboActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateCombo(true);
                synchronized (memoryCombo) {
                    String projectDirectory = (String) memoryCombo.getSelectedItem();
                    if ((projectDirectory != null) && (!projectDirectory.trim().endsWith("/"))) {
                        memoryCombo.removeItem(projectDirectory);
                        projectDirectory = projectDirectory.trim() + "/";
                        memoryCombo.addItem(projectDirectory);
                        memoryCombo.setSelectedItem(projectDirectory);
                    }
                    applyButton.setParameter((String) memoryCombo.getSelectedItem());
                }
            }
        };
    }

    @Override
    protected void onValueChange(final String data) {
        if (canSetData && (data != null) && (!data.trim().isEmpty())) {
            canSetData = false;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    synchronized (memoryCombo) {
                        memoryCombo.setSelectedItem(data);
                    }
                    updateCombo(false);
                }
            });
        }
    }

    @Override
    public void connectDialog() {
        super.connectDialog();
        canSetData = true;
    }

    @Override
    protected void clearConnections() {
        canSetData = false;
        super.clearConnections();
    }
}
