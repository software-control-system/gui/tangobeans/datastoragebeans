package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.DataModelDialogModel;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.icons.Icons;

public class DataModelEditDialog extends AbstractRecorderEditDialog {

    private static final long serialVersionUID = -7905397197820782230L;

    private ComboBox dataModelSelectionComboBox;
    private TextField setModelTarget;
    private JLabel dataModelSelectionTitle;
    private JPanel applyButtonPanel;
    private String currentDataModel;
    private DocumentListener dataModelDocumentListener;

    private DataModelDialogModel currentDialogModel;

    public DataModelEditDialog(Window parent, boolean modal, String currentDataModel) {
        super(parent, "Change current datamodel", modal);
        this.currentDataModel = currentDataModel;
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
    }

    @Override
    protected void initDialogModel() {
        currentDialogModel = new DataModelDialogModel();
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderText("You can select here the datamodel you want to use.");
        setHeaderIcon(iconsManager.getIcon("DataRecorder.dialog.tool"));
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        model.updateDataModelDialogModel(currentDialogModel);
    }

    @Override
    protected void initPanelComponents() {
        dataModelSelectionComboBox = new ComboBox();
        dataModelSelectionTitle = new JLabel("Current datamodel:");
        setModelTarget = new TextField();

        dataModelDocumentListener = new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                updateCombo(true);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateCombo(true);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateCombo(true);
            }
        };
        dataModelSelectionComboBox.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                Object selectedItem = dataModelSelectionComboBox.getSelectedItem();
                boolean changed = !ObjectUtils.sameObject(selectedItem, currentDataModel);
                applyButton.setEnabled(changed);
            }
        });
        applyButtonPanel = new JPanel(new BorderLayout());
        applyButtonPanel.add(Box.createGlue(), BorderLayout.CENTER);
        applyButtonPanel.add(applyButton, BorderLayout.WEST);
        applyButtonPanel.add(closeButton, BorderLayout.EAST);
        mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.NONE;
        titleConstraints.gridx = 0;
        titleConstraints.gridy = 0;
        titleConstraints.weightx = 0;
        titleConstraints.weighty = 0;
        titleConstraints.insets = new Insets(5, 5, 10, 5);
        mainPanel.add(dataModelSelectionTitle, titleConstraints);
        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.fill = GridBagConstraints.HORIZONTAL;
        comboConstraints.gridx = 1;
        comboConstraints.gridy = 0;
        comboConstraints.weightx = 1;
        comboConstraints.weighty = 0;
        comboConstraints.insets = new Insets(5, 0, 10, 5);
        mainPanel.add(dataModelSelectionComboBox, comboConstraints);
        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.HORIZONTAL;
        closeConstraints.gridx = 0;
        closeConstraints.gridy = 1;
        closeConstraints.weightx = 1;
        closeConstraints.weighty = 0;
        closeConstraints.insets = new Insets(0, 5, 5, 5);
        closeConstraints.gridwidth = GridBagConstraints.REMAINDER;
        mainPanel.add(applyButtonPanel, closeConstraints);

    }

    private void updateCombo(boolean modified) {
        updateComponent(getComboBoxEditor(), modified);
    }

    private JTextComponent getComboBoxEditor() {
        return (JTextComponent) ((JComboBox<?>) dataModelSelectionComboBox).getEditor().getEditorComponent();
    }

    public void setComboBoxEnabled(boolean enabled) {
        if (enabled != dataModelSelectionComboBox.isEditable()) {
            dataModelSelectionComboBox.setEditable(enabled);
        }
    }

    @Override
    public void connectDialog() {
        getComboBoxEditor().getDocument().removeDocumentListener(dataModelDocumentListener);
        stringBox.connectWidget(setModelTarget, currentDialogModel.getDataModelSelectionModelKey());
        dataModelSelectionComboBox.setValueList((Object[]) currentDialogModel.getTabDataModel());
        dataModelSelectionComboBox.setSelectedItem(currentDataModel);
        updateCombo(false);
        getComboBoxEditor().getDocument().addDocumentListener(dataModelDocumentListener);
    }

    @Override
    protected void clearConnections() {
        getComboBoxEditor().getDocument().removeDocumentListener(dataModelDocumentListener);
        stringBox.disconnectWidgetFromAll(dataModelSelectionComboBox);
        dataModelSelectionComboBox.setValueList((Object[]) null);
        dataModelSelectionComboBox.setSelectedItem(null);
    }

    @Override
    protected void clearDialog() {
        // nothing to clear on graphical part
    }

    @Override
    protected void apply() {
        Object selectedItem = dataModelSelectionComboBox.getSelectedItem();
        if (selectedItem != null) {
            currentDataModel = selectedItem.toString();
            setModelTarget.setText(selectedItem.toString());
            setModelTarget.send();
        }
    }
}
