package fr.soleil.comete.bean.datarecorderbean.history;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.bean.datarecorderbean.history.utils.LevelListCellRenderer;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.LogViewer;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.util.Level;

/**
 * This bean manages the history part of the DataRecorder device
 * 
 * @author girardot
 */
@Deprecated
public class DataRecorderHistoryBean extends AbstractTangoBox implements ActionListener {

    private static final long serialVersionUID = -4211309369789258804L;

    // the text area that displays history
    private LogViewer historyViewer;
    private JScrollPane historyScrollPane;
    // the button to clear history
    private StringButton clearHistoryButton;
    // the combobox to filter what kind of history you wan to view
    private JComboBox<Level> levelFilterComboBox;
    private boolean displayMessageOnConnectionError;

    private final StringMatrixBox stringMatrixBox;

    public DataRecorderHistoryBean() {
        super();
        displayMessageOnConnectionError = true;
        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);
        initComponents();
        layoutComponents();
    }

    // components initialization
    private void initComponents() {
        historyViewer = new LogViewer();
        historyViewer.setReversed(true);
        historyScrollPane = new JScrollPane(historyViewer);
        clearHistoryButton = generateStringButton();
        clearHistoryButton.setText("not connected");
        stringBox.setSettable(clearHistoryButton, false);

        levelFilterComboBox = new JComboBox<>();
        levelFilterComboBox.addItem(Level.ALL);
        levelFilterComboBox.addItem(Level.WARN);
        levelFilterComboBox.addItem(Level.ERROR);
        levelFilterComboBox.addActionListener(this);
        LevelListCellRenderer renderer = new LevelListCellRenderer();
        renderer.addIcon(Level.ALL, iconsManager.getIcon("DataRecorderHistory.level.info"));
        renderer.addIcon(Level.WARN, iconsManager.getIcon("DataRecorderHistory.level.warn"));
        renderer.addIcon(Level.ERROR, iconsManager.getIcon("DataRecorderHistory.level.error"));
        levelFilterComboBox.setRenderer(renderer);
    }

    // layout components in panel
    private void layoutComponents() {
        levelFilterComboBox.setMaximumSize(levelFilterComboBox.getPreferredSize());

        Box controlBox = Box.createHorizontalBox();
        controlBox.add(new JLabel("Log level:"));
        controlBox.add(Box.createHorizontalStrut(5));
        controlBox.add(levelFilterComboBox);
        controlBox.add(Box.createHorizontalStrut(5));
        controlBox.add(Box.createHorizontalGlue());
        controlBox.add(clearHistoryButton);

        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        setLayout(new BorderLayout(0, 5));
        add(historyScrollPane, BorderLayout.CENTER);
        add(controlBox, BorderLayout.SOUTH);
    }

    @Override
    protected void clearGUI() {
        cleanWidget(historyViewer);
        cleanWidget(clearHistoryButton);
        clearHistoryButton.setText("not connected");
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void onConnectionError() {
        if (isDisplayMessageOnConnectionError()) {
            showMessageDialog(this, "Failed to connect to DataRecorder", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void refreshGUI() {
        setWidgetModel(historyViewer, stringMatrixBox, generateReadOnlyAttributeKey("history"));
        setWidgetModel(clearHistoryButton, stringBox, generateCommandKey("clearHistory"));
        clearHistoryButton.setText("Clear History");
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e != null) && (e.getSource() == levelFilterComboBox)) {
            historyViewer.setFilteredLevel((Level) levelFilterComboBox.getSelectedItem());
        }
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if ((input != null) && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            DataRecorderHistoryBean bean = new DataRecorderHistoryBean();

            bean.setModel(deviceName);
            bean.start();

            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
