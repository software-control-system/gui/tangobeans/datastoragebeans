package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.LoadConfigurationDialogModel;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.swing.icons.Icons;

public class LoadConfigurationEditDialog extends AbstractRecorderEditDialog {

    private static final long serialVersionUID = 4082295188184901772L;

    private static final String HEADER_ICON = "DataRecorder.directory.root";
    private static final String DELETE_ICON = "DataRecorder.dialog.delete";

    private static final String DIALOG_TITLE = "Manage configurations";
    private static final String HEADER_TEXT = "Manage DataRecorder configurations";
    private final static String CONFIGURATION_VALUE = "Recorder configuration:";
    private static final String DELETE = "Delete";

    private LoadConfigurationDialogModel dialogModel;

    private ComboBox configurationComboBox;
    private StringButton deleteConfigButton;

    public LoadConfigurationEditDialog(Window parent, boolean modal) {
        super(parent, DIALOG_TITLE, modal);
        setHeaderText(HEADER_TEXT);
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderIcon(iconsManager.getIcon(HEADER_ICON));
        setDeleteButtonIcon(iconsManager.getIcon(DELETE_ICON));
    }

    @Override
    protected void initDialogModel() {
        dialogModel = new LoadConfigurationDialogModel();
    }

    @Override
    protected void clearDialog() {
        synchronized (configurationComboBox) {
            configurationComboBox.setObjectArray(null);
        }
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        model.updateLoadConfigurationDialogModel(dialogModel);
        updateComboContent(dialogModel.getRecorderConfigList());
    }

    @Override
    protected void initPanelComponents() {

        deleteConfigButton = new StringButton() {
            private static final long serialVersionUID = 3504983120123323412L;

            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedValue = (String) configurationComboBox.getSelectedValue();

                String confirmationTitle = "Delete configuration '" + selectedValue + "'?";
                String confirmationText = "WARNING: This operation is irreversible!"
                        + " Do you really want to delete the configuration '" + selectedValue + "'";
                stringBox.setConfirmationTitle(deleteConfigButton, confirmationTitle);
                stringBox.setConfirmationMessage(deleteConfigButton, confirmationText);
                setParameter(selectedValue);
                super.actionPerformed(e);
                updateComboContent(dialogModel.getRecorderConfigList());
            }
        };
        stringBox.setSettable(deleteConfigButton, false);
        stringBox.setConfirmation(deleteConfigButton, true);
        deleteConfigButton.setText(DELETE);
        deleteConfigButton.setButtonLook(true);

        configurationComboBox = new ComboBox();
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        super.initAndAddOtherComponentsInMainPanel();

        JLabel comboTitleLabel = new JLabel(CONFIGURATION_VALUE);

        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.gridx = 0;
        titleConstraints.gridy = 0;
        titleConstraints.anchor = GridBagConstraints.BASELINE_LEADING;
        titleConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(comboTitleLabel, titleConstraints);

        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.gridx = 1;
        comboConstraints.gridy = 0;
        comboConstraints.anchor = GridBagConstraints.BASELINE_LEADING;
        comboConstraints.insets = new Insets(0, 0, 0, 5);
        mainPanel.add(configurationComboBox, comboConstraints);

        GridBagConstraints deleteConstraints = new GridBagConstraints();
        deleteConstraints.gridx = 2;
        deleteConstraints.gridy = 0;
        deleteConstraints.weightx = 1;
        deleteConstraints.anchor = GridBagConstraints.BASELINE_LEADING;
        mainPanel.add(deleteConfigButton, deleteConstraints);
    }

    @Override
    protected JPanel generateBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));

        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
        bottomPanel.add(Box.createHorizontalGlue());
        bottomPanel.add(closeButton);

        return bottomPanel;
    }

    private void updateComboContent(String[] data) {
        configurationComboBox.setObjectArray(data);

        if (data != null && data.length > 0) {
            configurationComboBox.setSelectedItem(data[0]);
        }

        stringBox.setUserEnabled(deleteConfigButton, data != null && configurationComboBox.getSelectedValue() != null);
    }

    private void setDeleteButtonIcon(Icon icon) {
        ((AbstractButton) deleteConfigButton).setIcon(icon);
    }

    @Override
    public void connectDialog() {
        if (dialogModel instanceof LoadConfigurationDialogModel) {
            stringBox.connectWidget(deleteConfigButton, dialogModel.getDeleteKey());
        }
    }

    @Override
    protected void clearConnections() {
        stringBox.disconnectWidgetFromAll(deleteConfigButton);
    }
}
