package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.PostRecordingDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.PostRecordingDialogModel.ScriptModel;
import fr.soleil.comete.bean.datarecorder.model.utils.IPostRecordingModelListener;
import fr.soleil.comete.bean.datarecorder.model.utils.Parameter;
import fr.soleil.comete.bean.datarecorder.model.utils.ValidationListener;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * This dialog allows to show the current value for the tango attribute : Command Line. Thanks to
 * this dialog, we can modify this value and save this modifications in the Tango device.
 * 
 * @author MARECHAL
 */
public class PostRecordingEditDialog extends AbstractRecorderEditDialog implements IPostRecordingModelListener {

    private static final long serialVersionUID = 5149501185467491077L;

    private static final String LOADING_TEXT = "Loading...";
    private static final String LOADED_TEXT = "Data fully loaded!";
    public static final String DO_NOTHING = "Do nothing";

    private StringButton okButton;
    private JPanel buttonPanel;
    private JLabel scriptSelectionTitle;
    private JComboBox<ScriptModel> scriptSelectionComboBox;
    private ActionListener scriptSelectionActionListener;
    private JPanel doNothingPanel;
    private JPanel modelSelectionPanel;
    private JRadioButton doNothingRadioButton;
    private JRadioButton chooseScriptRadioButton;
    private ButtonGroup actionGroup;
    private JLabel noNxExtractorPathLabel;
    private JButton setParametersButton;
    private JLabel modelDescriptionTitleLabel;
    private JLabel modelDescriptionLabel;
    private Icon customDialogIcon;
    private ItemListener actionItemListener;

    private JLabel loadingLabel;
    private Icon loadingIcon;
    private Icon loadedIcon;

    private final int iType;
    private PostRecordingDialogModel dialogModel;

    /**
     * Constructor
     * 
     * @param parent
     * @param modal
     * @param iType
     */
    public PostRecordingEditDialog(Window parent, boolean modal, int iType) {
        super(parent, getTitleForType(iType), modal);
        dialogModel.setLoadingListener(this);
        this.iType = iType;
        dialogModel.setType(iType);
        setAsLoading();
    }

    @Override
    protected void initDialogModel() {
        dialogModel = new PostRecordingDialogModel();
    }

    public final static String getTitleForType(int type) {
        String result = ObjectUtils.EMPTY_STRING;
        if (type == PostRecordingDialogModel.POST_RECORD_GLOBAL) {
            result = "Post Recording";
        } else if (type == PostRecordingDialogModel.POST_RECORD_NXENTRY) {
            result = "Post Recording NXentry";
        }
        return result;
    }

    @Override
    protected void clearDialog() {
        // No graphical component to clean
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderText(resourceBundle.getString("DataRecorderState.PostRecording.Header"));
        setHeaderIcon(iconsManager.getIcon("DataRecorder.dialog.tool"));
        setCustomDialogIcon(iconsManager.getIcon("DataRecorder.dialog.custom"));
        setLoadingIcon(iconsManager.getIcon("DataRecorder.dialog.loading"));
        setLoadedIcon(iconsManager.getIcon("DataRecorder.dialog.loaded"));
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        if (iType == PostRecordingDialogModel.POST_RECORD_GLOBAL) {
            model.updateGlobalPostRecordingDialog(dialogModel);
        } else if (iType == PostRecordingDialogModel.POST_RECORD_NXENTRY) {
            model.updateNxEntryPostRecordingDialog(dialogModel);
        }
    }

    @Override
    public void updateValueSelection(final String modelPostRecording) {
        if (DataRecorderModel.DO_NOTHING.equalsIgnoreCase(modelPostRecording)) {
            doNothingRadioButton.setSelected(true);
        } else {
            chooseScriptRadioButton.setSelected(true);
            for (int index = 0; index < scriptSelectionComboBox.getItemCount(); ++index) {
                ScriptModel model = scriptSelectionComboBox.getItemAt(index);
                if (model != null && model.toString().equalsIgnoreCase(modelPostRecording)) {
                    scriptSelectionComboBox.setSelectedIndex(index);
                }
            }
        }
    }

    @Override
    protected JPanel generateBottomPanel() {
        JPanel loadingPanel = new JPanel(new BorderLayout());
        loadingLabel = new JLabel(LOADING_TEXT);
        loadingLabel.setFont(new Font(Font.MONOSPACED, Font.ITALIC, 12));
        loadingPanel.add(loadingLabel, BorderLayout.CENTER);
        return loadingPanel;
    }

    public void setLoadingIcon(Icon loadingIcon) {
        this.loadingIcon = loadingIcon;
        if (LOADING_TEXT.equals(loadingLabel.getText())) {
            loadingLabel.setIcon(loadingIcon);
        }
    }

    public void setLoadedIcon(Icon loadedIcon) {
        this.loadedIcon = loadedIcon;
        if (LOADED_TEXT.equals(loadingLabel.getText())) {
            loadingLabel.setIcon(loadedIcon);
        }
    }

    @Override
    public void setAsLoading() {
        loadingLabel.setText(LOADING_TEXT);
        loadingLabel.setIcon(loadingIcon);
        doNothingRadioButton.setEnabled(false);
        chooseScriptRadioButton.setEnabled(false);
        scriptSelectionTitle.setEnabled(false);
        scriptSelectionComboBox.setEnabled(false);
        setParametersButton.setEnabled(false);
        modelDescriptionTitleLabel.setEnabled(false);
        modelDescriptionLabel.setEnabled(false);
    }

    @Override
    public void setAsLoaded() {
        loadingLabel.setText(LOADED_TEXT);
        loadingLabel.setIcon(loadedIcon);
    }

    @Override
    protected void initPanelComponents() {

        dialogModel.initModel();

        modelDescriptionTitleLabel = new JLabel("Description:", JLabel.RIGHT);
        modelDescriptionLabel = new JLabel();
        modelDescriptionLabel.setFont(CometeUtils.getLabelFont());
        modelDescriptionLabel.setPreferredSize(new Dimension(600, 30));
        noNxExtractorPathLabel = new JLabel("No Path for nxextractor is defined.");
        scriptSelectionTitle = new JLabel("Select a template:", JLabel.RIGHT);
        scriptSelectionTitle.setEnabled(false);

        customDialogIcon = null;
        scriptSelectionComboBox = new JComboBox<>();
        scriptSelectionComboBox.setEnabled(false);
        scriptSelectionActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (scriptSelectionComboBox.getSelectedItem() != null) {
                    dialogModel.setCurrentScriptModel((ScriptModel) scriptSelectionComboBox.getSelectedItem());

                    // update buttons state
                    chooseScriptRadioButton.setSelected(true);
                    setParametersButton.setEnabled(dialogModel.isParametersEnabled());
                    // set description of the selected Model
                    updateModelDescription();
                    dialogModel.resetParametersLine();
                } else {
                    setParametersButton.setEnabled(false);
                }
            }
        };

        setParametersButton = new JButton("Set parameters...");
        setParametersButton.setEnabled(false);
        setParametersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedScript = dialogModel.getSelectedScript();
                if (scriptSelectionComboBox != null && scriptSelectionComboBox.getSelectedItem() != null) {
                    selectedScript = scriptSelectionComboBox.getSelectedItem().toString();
                    dialogModel.updateParameters(selectedScript);
                }
            }
        });

        okButton = new StringButton() {
            private static final long serialVersionUID = 1574016093195895807L;

            @Override
            public void actionPerformed(ActionEvent event) {
                String valueToSave = dialogModel.computeValueToSave();
                if ((valueToSave != null) && (!valueToSave.trim().isEmpty())) {
                    setParameter(valueToSave);
                    super.actionPerformed(event);
                    for (ValidationListener listener : dialogModel.getValidationList()) {
                        listener.onValidation();
                    }
                    close();
                }
            }
        };
        okButton.setButtonLook(true);
        okButton.setCometeFont(FontTool.getCometeFont(setParametersButton.getFont()));
        okButton.setText("OK");
        stringBox.setSettable(okButton, false);

        closeButton.setText("Cancel");

        buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 0;
        buttonPanel.add(Box.createGlue(), glueConstraints);
        GridBagConstraints okConstraints = new GridBagConstraints();
        okConstraints.fill = GridBagConstraints.NONE;
        okConstraints.gridx = 1;
        okConstraints.gridy = 0;
        okConstraints.weightx = 0;
        okConstraints.weighty = 0;
        okConstraints.insets = new Insets(0, 0, 5, 5);
        buttonPanel.add(okButton, okConstraints);
        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.NONE;
        closeConstraints.gridx = 2;
        closeConstraints.gridy = 0;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 0;
        closeConstraints.insets = new Insets(0, 0, 5, 5);
        buttonPanel.add(closeButton, closeConstraints);

        actionGroup = new ButtonGroup();

        actionItemListener = new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if ((e.getSource() == doNothingRadioButton) && doNothingRadioButton.isSelected()) {
                    dialogModel.setChooseScriptMode(false);
                    scriptSelectionComboBox.setEnabled(false);
                    setParametersButton.setEnabled(false);
                    updateModelDescription();
                    scriptSelectionTitle.setEnabled(false);
                } else if ((e.getSource() == chooseScriptRadioButton) && chooseScriptRadioButton.isSelected()) {
                    dialogModel.setChooseScriptMode(true);
                    if (scriptSelectionComboBox.getItemCount() > 0) {
                        scriptSelectionComboBox.setEnabled(true);
                        scriptSelectionComboBox.setSelectedIndex(0);
                    }
                    updateModelDescription();
                    scriptSelectionTitle.setEnabled(true);
                }
            }
        };

        doNothingPanel = new JPanel(new BorderLayout());
        doNothingPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
        doNothingRadioButton = new JRadioButton(DO_NOTHING);
        doNothingRadioButton.setHorizontalAlignment(JRadioButton.LEFT);
        doNothingRadioButton.addItemListener(actionItemListener);
        doNothingRadioButton.setSelected(true);
        actionGroup.add(doNothingRadioButton);
        doNothingPanel.add(doNothingRadioButton, BorderLayout.CENTER);

        chooseScriptRadioButton = new JRadioButton("Build text(s) or binary(ies) files from each NeXus file.");
        chooseScriptRadioButton.addItemListener(actionItemListener);
        chooseScriptRadioButton.setSelected(false);
        actionGroup.add(chooseScriptRadioButton);

        modelSelectionPanel = new JPanel(new GridBagLayout());
        modelSelectionPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));

        GridBagConstraints noExtractorConstraints = new GridBagConstraints();
        noExtractorConstraints.fill = GridBagConstraints.HORIZONTAL;
        noExtractorConstraints.gridx = 0;
        noExtractorConstraints.gridy = 0;
        noExtractorConstraints.weightx = 1;
        noExtractorConstraints.weighty = 0;
        noExtractorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        noExtractorConstraints.insets = new Insets(5, 5, 0, 5);
        modelSelectionPanel.add(noNxExtractorPathLabel, noExtractorConstraints);

        GridBagConstraints chooseScriptConstraints = new GridBagConstraints();
        chooseScriptConstraints.fill = GridBagConstraints.HORIZONTAL;
        chooseScriptConstraints.gridx = 0;
        chooseScriptConstraints.gridy = 1;
        chooseScriptConstraints.weightx = 1;
        chooseScriptConstraints.weighty = 0;
        chooseScriptConstraints.gridwidth = GridBagConstraints.REMAINDER;
        chooseScriptConstraints.insets = new Insets(0, 0, 0, 5);
        modelSelectionPanel.add(chooseScriptRadioButton, chooseScriptConstraints);

        GridBagConstraints scriptSelectionTitleConstraints = new GridBagConstraints();
        scriptSelectionTitleConstraints.fill = GridBagConstraints.NONE;
        scriptSelectionTitleConstraints.gridx = 0;
        scriptSelectionTitleConstraints.gridy = 2;
        scriptSelectionTitleConstraints.weightx = 0;
        scriptSelectionTitleConstraints.weighty = 0;
        scriptSelectionTitleConstraints.insets = new Insets(5, 5, 0, 5);
        modelSelectionPanel.add(scriptSelectionTitle, scriptSelectionTitleConstraints);
        GridBagConstraints scriptSelectionComboBoxConstraints = new GridBagConstraints();
        scriptSelectionComboBoxConstraints.fill = GridBagConstraints.BOTH;
        scriptSelectionComboBoxConstraints.gridx = 1;
        scriptSelectionComboBoxConstraints.gridy = 2;
        scriptSelectionComboBoxConstraints.weightx = 1;
        scriptSelectionComboBoxConstraints.weighty = 0;
        scriptSelectionComboBoxConstraints.insets = new Insets(5, 0, 0, 5);
        modelSelectionPanel.add(scriptSelectionComboBox, scriptSelectionComboBoxConstraints);
        GridBagConstraints setParametersButtonConstraints = new GridBagConstraints();
        setParametersButtonConstraints.fill = GridBagConstraints.NONE;
        setParametersButtonConstraints.gridx = 2;
        setParametersButtonConstraints.gridy = 2;
        setParametersButtonConstraints.weightx = 0;
        setParametersButtonConstraints.weighty = 0;
        setParametersButtonConstraints.insets = new Insets(5, 0, 0, 5);
        modelSelectionPanel.add(setParametersButton, setParametersButtonConstraints);

        GridBagConstraints modelDescriptionTitleConstraints = new GridBagConstraints();
        modelDescriptionTitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        modelDescriptionTitleConstraints.gridx = 0;
        modelDescriptionTitleConstraints.gridy = 3;
        modelDescriptionTitleConstraints.weightx = 0;
        modelDescriptionTitleConstraints.weighty = 0;
        modelDescriptionTitleConstraints.insets = new Insets(5, 5, 5, 5);
        modelSelectionPanel.add(modelDescriptionTitleLabel, modelDescriptionTitleConstraints);
        GridBagConstraints modelDescriptionConstraints = new GridBagConstraints();
        modelDescriptionConstraints.fill = GridBagConstraints.HORIZONTAL;
        modelDescriptionConstraints.gridx = 1;
        modelDescriptionConstraints.gridy = 3;
        modelDescriptionConstraints.weightx = 1;
        modelDescriptionConstraints.weighty = 0;
        modelDescriptionConstraints.gridwidth = GridBagConstraints.REMAINDER;
        modelDescriptionConstraints.insets = new Insets(5, 0, 5, 5);
        modelSelectionPanel.add(modelDescriptionLabel, modelDescriptionConstraints);

        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints doNothingConstraints = new GridBagConstraints();
        doNothingConstraints.fill = GridBagConstraints.HORIZONTAL;
        doNothingConstraints.gridx = 0;
        doNothingConstraints.gridy = 0;
        doNothingConstraints.weightx = 1;
        doNothingConstraints.weighty = 0;
        doNothingConstraints.gridwidth = GridBagConstraints.REMAINDER;
        doNothingConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(doNothingPanel, doNothingConstraints);
        GridBagConstraints modelSelectionConstraints = new GridBagConstraints();
        modelSelectionConstraints.fill = GridBagConstraints.BOTH;
        modelSelectionConstraints.gridx = 0;
        modelSelectionConstraints.gridy = 1;
        modelSelectionConstraints.weightx = 1;
        modelSelectionConstraints.weighty = 1;
        modelSelectionConstraints.gridwidth = GridBagConstraints.REMAINDER;
        modelSelectionConstraints.insets = new Insets(10, 5, 0, 5);
        mainPanel.add(modelSelectionPanel, modelSelectionConstraints);
        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonConstraints.gridx = 0;
        buttonConstraints.gridy = 2;
        buttonConstraints.weightx = 1;
        buttonConstraints.weighty = 0;
        buttonConstraints.gridwidth = GridBagConstraints.REMAINDER;
        buttonConstraints.insets = new Insets(20, 0, 5, 0);
        mainPanel.add(buttonPanel, buttonConstraints);

    }

    public void updateModelDescription() {
        if (doNothingRadioButton.isSelected()) {
            modelDescriptionTitleLabel.setEnabled(false);
            modelDescriptionLabel.setEnabled(false);
        } else {
            modelDescriptionTitleLabel.setEnabled(true);
            modelDescriptionLabel.setEnabled(true);
        }

        String selectedScript = dialogModel.getSelectedScript();
        if (scriptSelectionComboBox != null && scriptSelectionComboBox.getSelectedItem() != null) {
            selectedScript = scriptSelectionComboBox.getSelectedItem().toString();
        }
        String selectedModelDescription = dialogModel.getSelectedModelDescription(selectedScript);
        if (selectedModelDescription != null) {
            modelDescriptionLabel.setText(selectedModelDescription);
        } else {
            modelDescriptionLabel.setText("No description");
        }
    }

    @Override
    public void updateListSelectionModel(List<String> modelList, DefaultComboBoxModel<ScriptModel> model,
            boolean shouldListenToComboBox) {
        doNothingRadioButton.setEnabled(true);
        scriptSelectionComboBox.removeActionListener(scriptSelectionActionListener);

        if ((modelList.size() == 1) && modelList.get(0).toString().equals("no model")) {
            scriptSelectionComboBox.setEnabled(false);
            chooseScriptRadioButton.setEnabled(false);
            noNxExtractorPathLabel.setVisible(false);
        } else {
            scriptSelectionComboBox.setEnabled(chooseScriptRadioButton.isSelected());
            chooseScriptRadioButton.setEnabled(true);
            noNxExtractorPathLabel.setVisible(!dialogModel.isNXExtractorPathAvailable());
        }
        if (shouldListenToComboBox) {
            scriptSelectionComboBox.addActionListener(scriptSelectionActionListener);
        }
        scriptSelectionComboBox.setModel(model);
        updateScriptSelection();
    }

    /**
     * Determines type of script with value of post record value.
     */
    protected void updateScriptSelection() {
        // determine type of script with value of post record value
        // remember old choice if user choose cancel, we must memorize
        // what was the previous choice
        String selectedScript = dialogModel.getSelectedScript();
        if (selectedScript != null) {
            // test if string contains spaces
            int iPosSpace = selectedScript.indexOf(" ");
            // get value from start to first space ans test if it ends with nxextractor
            String strPathNxExtractor = null;
            if (iPosSpace != -1) {
                strPathNxExtractor = selectedScript.substring(0, iPosSpace).trim();
            }

            if (strPathNxExtractor == null) {
                doNothingRadioButton.setSelected(true);
            } else {
                if (strPathNxExtractor.endsWith("nxextractor")) {
                    chooseScriptRadioButton.setSelected(true);
                    // set good model for combo box
                    String strModel = dialogModel.getModelForScript(selectedScript);
                    ScriptModel scriptModel = containsModel(strModel);
                    if (scriptModel != null) {
                        scriptSelectionComboBox.setSelectedItem(scriptModel);
                    }
                    scriptModel = null;
                } else {
                    // --m_rbCreateScript.setSelected(true);
                    // --m_tfScript.setText(m_strScriptValue);
                    doNothingRadioButton.setSelected(true);
                }
            }
        }
    }

    /**
     * This method allows to know if combobox contains the specified model
     * 
     * @param String strModel
     * @return boolean
     */
    public ScriptModel containsModel(String strModel) {
        for (int i = 0; i < scriptSelectionComboBox.getModel().getSize(); i++) {
            // add each model in the vector
            if (strModel.equals(scriptSelectionComboBox.getItemAt(i).toString())) {
                return scriptSelectionComboBox.getItemAt(i);
            }
        }
        return null;
    }

    @Override
    public Vector<String> getCustumModelParameters(Vector<Parameter> vectParam, String strExplanations) {
        return openDgParameters(vectParam, strExplanations);
    }

    private Vector<String> openDgParameters(Vector<Parameter> vectParam, String strExplanations) {
        Vector<String> result = null;
        // create Custom Dialog
        CustomDialog paramDial = new CustomDialog(this, "Model's parameters", true, vectParam, strExplanations,
                getCustomDialogIcon());
        paramDial.pack();
        paramDial.setLocationRelativeTo(this);
        paramDial.setVisible(true);
        if (paramDial.getReturnValue() == CustomDialog.OK_VALUE) {
            result = paramDial.getVectValues();
        }
        return result;
    }

    @Override
    public void executeConfirmation() {
        okButton.execute();
    }

    public Icon getCustomDialogIcon() {
        return customDialogIcon;
    }

    public void setCustomDialogIcon(Icon customDialogIcon) {
        this.customDialogIcon = customDialogIcon;
    }

    @Override
    protected void connectDialog() {
        dialogModel.connectDialog(stringBox);
        stringBox.connectWidgetNoMetaData(okButton, dialogModel.getPostRecordingCommandModelKey());
        updateModelDescription();
    }

    @Override
    protected void clearConnections() {
        dialogModel.clearConnections(stringBox);
        stringBox.disconnectWidgetFromAll(okButton);
    }

}
