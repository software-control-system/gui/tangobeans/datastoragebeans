package fr.soleil.comete.bean.datarecorderbean.admin.utils;

import fr.soleil.comete.swing.Spinner;

public class PositiveActivableNumberSpinner extends Spinner {

    private static final long serialVersionUID = 7842145215878441313L;

    private boolean activateWithValue;
    private Number lastValue;

    public PositiveActivableNumberSpinner() {
        super();
        activateWithValue = false;
    }

    @Override
    public void setNumberValue(Number value) {
        if (isActivateWithValue()) {
            refreshAvailability(value);
        }
        if ((value != null) && (value.doubleValue() < 0)) {
            value = getOppositeValue(value);
        }
        super.setNumberValue(value);
    }

    private Number getOppositeValue(Number value) {
        if (value instanceof Byte) {
            value = Byte.valueOf((byte) (-1 * value.byteValue()));
        }
        else if (value instanceof Short) {
            value = Short.valueOf((short) (-1 * value.shortValue()));
        }
        else if (value instanceof Integer) {
            value = Integer.valueOf(-1 * value.intValue());
        }
        else if (value instanceof Long) {
            value = Long.valueOf(-1L * value.longValue());
        }
        else if (value instanceof Float) {
            value = Float.valueOf(-1 * value.floatValue());
        }
        else if (value instanceof Double) {
            value = Double.valueOf(-1 * value.doubleValue());
        }
        return value;
    }

    public boolean isActivateWithValue() {
        return activateWithValue;
    }

    public void setActivateWithValue(boolean activateWithValue) {
        this.activateWithValue = activateWithValue;
        if (activateWithValue) {
            refreshAvailability(lastValue);
        }
    }

    private void refreshAvailability(Number value) {
        if ((value == null) || Double.isNaN(value.doubleValue()) || value.doubleValue() < 0) {
            setEditable(false);
        }
        else {
            setEditable(true);
        }
    }

    public synchronized void forcePositiveData() {
        if ((getNumberValue() != null) && (!Double.isNaN(getNumberValue().doubleValue()))
                && (lastValue != null) && (lastValue.doubleValue() < 0)) {
            setValue(getNumberValue());
        }
    }

    public synchronized void forceNegativeData() {
        if ((getNumberValue() != null) && (!Double.isNaN(getNumberValue().doubleValue()))
                && (lastValue != null) && (lastValue.doubleValue() > 0)) {
            setValue(getOppositeValue(getNumberValue()));
        }
    }

}
