package fr.soleil.comete.bean.datarecorderbean;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel.Availabilities;
import fr.soleil.comete.bean.datarecorder.model.IDataRecorderView;
import fr.soleil.comete.bean.datarecorder.model.utils.DataRecorderStatus.RunningState;
import fr.soleil.comete.bean.datarecorderbean.admin.DataRecorderAdminBean;
import fr.soleil.comete.bean.datarecorderbean.details.DataRecorderDetailsBean;
import fr.soleil.comete.bean.datarecorderbean.history.DataRecorderHistoryBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;

@Deprecated
public class DataRecorderBean extends AbstractTangoBox implements IDataRecorderView {

    private static final long serialVersionUID = 1351910880891198069L;

    private final ImageIcon startIcon;
    private final ImageIcon stopIcon;

    private boolean start = false;
    private final DataRecorderModel drModel;

    // components which show datarecorder's attributes values
    private Label beamlineNameLabel;
    private StateStatusLabel stateStatusLabel;
    private JButton startStopRecordingButton;

    // Main tabbed pain
    private JTabbedPane mainTabbedPane;
    // beans
    private final DataRecorderDetailsBean dataRecorderDetailsBean;
    private final DataRecorderHistoryBean dataRecorderHistoryBean;
    private DataRecorderAdminBean dataRecorderAdminBean;

    private boolean displayMessageOnConnectionError = false;

    // full mode shows the admin tab and disable start button
    private boolean fullMode = false;

    public DataRecorderBean() {
        this(true);
    }

    public DataRecorderBean(boolean fullMode) {
        this.fullMode = fullMode;

        drModel = new DataRecorderModel();
        drModel.setDefaultStringBox(stringBox);
        drModel.addDataRecorderListener(this);

        startIcon = iconsManager.getIcon("DataRecorder.start");
        stopIcon = iconsManager.getIcon("DataRecorder.stop");

        // beans instantiation
        dataRecorderDetailsBean = new DataRecorderDetailsBean();
        dataRecorderDetailsBean.setDisplayMessageOnConnectionError(displayMessageOnConnectionError);

        dataRecorderHistoryBean = new DataRecorderHistoryBean();
        dataRecorderHistoryBean.setDisplayMessageOnConnectionError(displayMessageOnConnectionError);

        if (fullMode) {
            dataRecorderAdminBean = new DataRecorderAdminBean();
            dataRecorderAdminBean.setDisplayMessageOnConnectionError(displayMessageOnConnectionError);
        }

        initDisplayedComponents();
        layoutComponents();
    }

    @Override
    public void setModel(String model) {
        drModel.setModel(model);
        super.setModel(model);
        if (start) {
            start = false;
            start();
        }
    }

    @Override
    public void stop() {
        if (start) {
            start = false;
            super.stop();
            dataRecorderDetailsBean.stop();
            dataRecorderHistoryBean.stop();
            if (fullMode) {
                dataRecorderAdminBean.stop();
            }
        }
    }

    /*
     * Initializes the displayed components/widgets
     */
    private void initDisplayedComponents() {
        // top panel components
        beamlineNameLabel = generateBeamLineLabel();
        stateStatusLabel = new StateStatusLabel();

        startStopRecordingButton = new JButton();
        startStopRecordingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Icon icon = startStopRecordingButton.getIcon();
                startStopActionPerformed(icon == startIcon);
            }
        });
        startStopRecordingButton.setIcon(startIcon);
        startStopRecordingButton.setEnabled(false);

        mainTabbedPane = new JTabbedPane();
        mainTabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
        mainTabbedPane.addTab("Details", new JScrollPane(dataRecorderDetailsBean));
        mainTabbedPane.addTab("History", dataRecorderHistoryBean);
        if (fullMode) {
            mainTabbedPane.addTab("Admin", new JScrollPane(dataRecorderAdminBean));
        }

        mainTabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateTabContent();
            }
        });
    }

    private Label generateBeamLineLabel() {
        Label label = new Label();

        label.setCometeFont(FontTool.getCometeFont(new Font("Arial", Font.BOLD, 30)));

        label.setOpaque(false);
        stringBox.setColorAsForeground(label, false);
        stringBox.setColorEnabled(label, false);

        stringBox.setErrorText(label, "NO BEAMLINE");

        return label;
    }

    private JPanel createTopPanel() {
        JPanel topPanel = new JPanel(new GridBagLayout());
        topPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));

        GridBagConstraints beamLineConstraints = new GridBagConstraints();
        beamLineConstraints.gridx = 0;
        beamLineConstraints.insets = new Insets(0, 0, 0, 5);
        topPanel.add(beamlineNameLabel, beamLineConstraints);

        GridBagConstraints statusConstraints = new GridBagConstraints();
        statusConstraints.gridx = 1;
        statusConstraints.weightx = 1;
        statusConstraints.fill = GridBagConstraints.BOTH;
        statusConstraints.insets = new Insets(0, 0, 0, 5);
        topPanel.add(stateStatusLabel, statusConstraints);

        GridBagConstraints startStopButtonConstraints = new GridBagConstraints();
        startStopButtonConstraints.gridx = 2;
        topPanel.add(startStopRecordingButton, startStopButtonConstraints);

        return topPanel;
    }

    private void layoutComponents() {
        JPanel topPanel = createTopPanel();

        setLayout(new BorderLayout());

        add(topPanel, BorderLayout.NORTH);
        add(mainTabbedPane, BorderLayout.CENTER);
    }

    public DataRecorderDetailsBean getDataRecorderDetailsBean() {
        return dataRecorderDetailsBean;
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            drModel.initConnections();

            // Beamline
            stringBox.connectWidget(beamlineNameLabel, generateAttributeKey(DataRecorderModel.BEAMLINE_ATTR));
            // State
            stringBox.connectWidget(stateStatusLabel, generateAttributeKey(DataRecorderModel.STATE_ATTR));
            stateStatusLabel.connectStatus(getModel());

            updateTabContent();
            drModel.computeDetailsAvailabilities();
        }
    }

    @Override
    protected void clearGUI() {
        drModel.clearConnections();

        cleanWidget(beamlineNameLabel);
        cleanWidget(stateStatusLabel);
        stateStatusLabel.disconnectStatus();

        // Clean start / stop button
        // startStopRecordingButton.deepClean();
        startStopRecordingButton.setEnabled(false);

        // Clean all tabs
        dataRecorderDetailsBean.setModel(null);
        dataRecorderHistoryBean.setModel(null);
        if (fullMode) {
            dataRecorderAdminBean.setModel(null);
        }
    }

    /*
     * Connects/disconnects widgets depending on selected tab in mainTabbedPane.
     * This is used to optimize resources consumption
     */
    private void updateTabContent() {
        switch (mainTabbedPane.getSelectedIndex()) {
            case 0:
                dataRecorderDetailsBean.setModel(model);
                dataRecorderDetailsBean.start();

                dataRecorderHistoryBean.stop();
                if (fullMode) {
                    dataRecorderAdminBean.stop();
                }
                break;
            case 1:
                dataRecorderHistoryBean.setModel(model);
                dataRecorderHistoryBean.start();

                dataRecorderDetailsBean.stop();
                if (fullMode) {
                    dataRecorderAdminBean.stop();
                }
                break;
            case 2:
                if (fullMode) {
                    dataRecorderAdminBean.setModel(model);
                    dataRecorderAdminBean.start();
                }
                dataRecorderDetailsBean.stop();
                dataRecorderHistoryBean.stop();
                break;
        }
    }

    @Override
    public void updatePanel(final DataRecorderModel dataRecorderModel, UpdateMode mode) {
        // nop
    }

    private void startStopActionPerformed(boolean start) {
        if (drModel != null) {
            drModel.startStopRecording(start);
        }
    }

    @Override
    public void updateAvailabilities(Availabilities availabilities) {
        if (availabilities != null) {
            // Start / Stop button availabilities
            if (drModel.getState() instanceof RunningState) {
                startStopRecordingButton.setIcon(stopIcon);
                startStopRecordingButton.setEnabled(availabilities.isStartStopAV());
            } else {
                startStopRecordingButton.setIcon(startIcon);
                // only enable start button in full mode
                startStopRecordingButton.setEnabled(fullMode && availabilities.isStartStopAV());
            }
        }
    }

    @Override
    protected void onConnectionError() {
        if (displayMessageOnConnectionError) {
            showMessageDialog(this, "Failed to connect to DataRecorder", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    public boolean isFullMode() {
        return fullMode;
    }

    private static void createAndShowGUI(final String[] args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if (input != null && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            DataRecorderBean bean = new DataRecorderBean();

            bean.setModel(deviceName);
            bean.start();
            // TODO justification inconnue
            // try {
            // Thread.sleep(500);
            // } catch (Exception e) {
            // // TODO: handle exception
            // }
            // bean.stop();
            // bean.start();

            final JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);

            // trick to get the frame size correctly with the pack!
            // SwingUtilities.invokeLater(new Runnable() {
            // @Override
            // public void run() {
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
            // }
            // });
        }
    }

    public static void main(final String[] args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }
}
