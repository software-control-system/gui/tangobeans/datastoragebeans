package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.FileEditionModel;
import fr.soleil.comete.bean.technicaldata.MemoryComboPref;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.swing.StringButton;

/**
 * A Dialog used for data edition, and that contains a {@link MemoryComboPref}
 * 
 * @author girardot
 * 
 */
public abstract class MemoryComboPrefEditDialog extends AbstractRecorderEditDialog {

    private static final long serialVersionUID = 2123308183899746523L;

    public static final String TEXT_N = "\\[" + "n" + "\\]";

    protected JLabel memoryComboTitleLabel;

    protected MemoryComboPref memoryCombo;
    protected ActionListener memoryComboActionListener;
    protected DocumentListener memoryComboTextChangeListener;

    protected StringButton applyButton;
    protected JButton revertButton;
    protected StringButton setDefaultValueButton;

    protected TextTargetRedirector valueRedirector;
    protected String currentValue;
    protected boolean canSetData;

    protected FileEditionModel dialogModel;

    public MemoryComboPrefEditDialog(Window parent, String title, boolean modal) {
        super(parent, title, modal);
    }

    @Override
    protected void initPanelComponents() {
        valueRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                currentValue = data;
                onValueChange(data);
            }
        };

        applyButton = new StringButton() {

            private static final long serialVersionUID = -5487851227027663615L;

            @Override
            public void actionPerformed(ActionEvent event) {
                super.actionPerformed(event);
                updateCombo(false);
            }
        };
        stringBox.setSettable(applyButton, false);
        stringBox.setOutputInPopup(applyButton, false);
        applyButton.setText("Apply");
        applyButton.setButtonLook(true);

        memoryComboTitleLabel = new JLabel();

        memoryComboActionListener = generateMemoryComboActionListener();
        memoryCombo = generateMemoryCombo();
        if (memoryComboActionListener != null) {
            memoryCombo.addActionListener(memoryComboActionListener);
        }
        memoryComboTextChangeListener = generateMemoryComboTextListener();
        addTextListener();

        setDefaultValueButton = new StringButton() {

            private static final long serialVersionUID = 5825723558936352268L;

            @Override
            public void actionPerformed(ActionEvent event) {
                super.actionPerformed(event);
                canSetData = true;
            }
        };
        stringBox.setSettable(setDefaultValueButton, false);
        setDefaultValueButton.setText("Default");
        setDefaultValueButton.setButtonLook(true);

        revertButton = new JButton("Revert");
        revertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                revert();
            }
        });
        updateCombo(false);
    }

    protected void revert() {
        if ((!currentValue.trim().isEmpty())) {
            synchronized (memoryCombo) {
                memoryCombo.setSelectedItem(currentValue);
            }
            updateCombo(false);
        }
    }

    protected abstract MemoryComboPref generateMemoryCombo();

    protected abstract ActionListener generateMemoryComboActionListener();

    protected abstract void onValueChange(String data);

    /**
     * Generate a listener that register if a change happens in the {@link MemoryComboPref} Combobox.
     * 
     * @return
     */
    protected DocumentListener generateMemoryComboTextListener() {
        return new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent arg0) {
                checksInputText();
                updateCombo(true);
            }

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                checksInputText();
                updateCombo(true);

            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                checksInputText();
                updateCombo(true);

            }
        };
    }

    protected void checksInputText() {
    }

    /**
     * This method allows to color {@link #memoryCombo} in orange if it was modified. It also
     * activates/deactivates "apply" and "revert" buttons depending on modifications
     * 
     * @param modificationOccurred a boolean. <code>TRUE</code> to display {@link #memoryCombo} as
     *            modified and activate "apply" and "revert" buttons, <code>FALSE</code> to display {@link #memoryCombo}
     *            as not modified and deactivate "apply" and "revert" buttons
     */
    protected void updateCombo(boolean modificationOccurred) {
        updateComponent(memoryCombo.getTextField(), modificationOccurred);
        applyButton.setEnabled(modificationOccurred);
        revertButton.setEnabled(modificationOccurred);
    }

    // Method create for exclude the character [n] in Combo project directory
    protected void excludeCharacter(boolean isExcludeCharacter) {
        if (isExcludeCharacter) {
            final JTextField textField = memoryCombo.getTextField();
            if (textField != null) {
                String stringMemory = textField.getText();
                if ((stringMemory != null) && stringMemory.contains("[n]")) {
                    final String replaceAll = stringMemory.replaceAll(TEXT_N, "");
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            textField.setText(replaceAll);
                        }
                    });
                }
            }
        }
    }

    protected void loadPreferences() {
        synchronized (memoryCombo) {
            memoryCombo.removeAllItems();
            memoryCombo.loadMemorizedItems();
            memoryCombo.setSelectedValue();
            updateCombo(false);
        }
    }

    public void savePreferences() {
        synchronized (memoryCombo) {
            memoryCombo.save();
        }
    }

    protected void addTextListener() {
        JTextComponent c = (JTextComponent) memoryCombo.getEditor().getEditorComponent();
        c.getDocument().addDocumentListener(memoryComboTextChangeListener);
    }

    public void setDefaultIcon(Icon icon) {
        ((AbstractButton) setDefaultValueButton).setIcon(icon);
    }

    public void setApplyIcon(Icon icon) {
        ((AbstractButton) applyButton).setIcon(icon);
    }

    public void setRevertIcon(Icon icon) {
        revertButton.setIcon(icon);
    }

    public void cleanSelectionCombo() {
        synchronized (memoryCombo) {
            memoryCombo.removeAllItems();
            memoryCombo.setSelectedItem(null);
        }
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        loadPreferences();
    }

    @Override
    public void connectDialog() {
        stringBox.connectWidget(valueRedirector, dialogModel.getValueModelKey());
        stringBox.connectWidget(applyButton, dialogModel.getApplyModelKey());
        stringBox.connectWidget(setDefaultValueButton, dialogModel.getDefaultModelKey());
        updateCombo(false);
    }

    @Override
    protected void clearConnections() {
        stringBox.disconnectWidgetFromAll(valueRedirector);
        stringBox.disconnectWidgetFromAll(applyButton);
        stringBox.disconnectWidgetFromAll(setDefaultValueButton);
    }
}
