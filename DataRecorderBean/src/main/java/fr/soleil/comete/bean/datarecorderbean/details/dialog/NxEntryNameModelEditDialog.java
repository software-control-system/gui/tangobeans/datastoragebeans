package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.NxEntryNameDialogModel;
import fr.soleil.comete.bean.technicaldata.HeaderedDialog;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.StringButton;

/**
 * Allows to set model for nxentryName (writable part)
 * 
 * @author MARECHAL
 * @author girardot
 */
public class NxEntryNameModelEditDialog extends HeaderedDialog {

    private static final long serialVersionUID = 7150788769985849871L;

    private JLabel nxEntryNameTitle;
    private JLabel nxEntryNameDisplayer;
    private JComboBox<String> nxEntryNameEditor;
    private JButton setDefaultValueButton;
    private StringButton okButton;
    private JPanel viewPanel;
    private JPanel editPanel;
    private JPanel buttonPanel;
    // private String recupExperimentName;

    private NxEntryNameDialogModel dialogModel;

    public NxEntryNameModelEditDialog(Window parent, String title, boolean modal) {
        super(parent, title, modal);
    }

    public void setDialogModel(NxEntryNameDialogModel model) {
        dialogModel = model;
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {

        // view part
        viewPanel = new JPanel(new BorderLayout());
        nxEntryNameTitle = new JLabel("Current Value:");
        nxEntryNameTitle.setBorder(new EmptyBorder(0, 0, 0, 5));
        nxEntryNameDisplayer = new JLabel();
        viewPanel.add(nxEntryNameTitle, BorderLayout.WEST);
        viewPanel.add(nxEntryNameDisplayer, BorderLayout.CENTER);

        // edit part
        editPanel = new JPanel(new GridBagLayout());
        nxEntryNameEditor = new JComboBox<>(NxEntryNameDialogModel.MODEL_POSSIBLE_VALUES);

        nxEntryNameEditor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nxEntryNameDisplayer.setText((String) nxEntryNameEditor.getSelectedItem());
            }
        });
        setDefaultValueButton = new JButton("Default");
        setDefaultValueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogModel.resetDefaultValue();
                updateSelectedValue(true);
            }
        });
        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.fill = GridBagConstraints.BOTH;
        comboConstraints.gridx = 0;
        comboConstraints.gridy = 0;
        comboConstraints.weightx = 1;
        comboConstraints.weighty = 1;
        comboConstraints.insets = new Insets(0, 0, 0, 5);
        editPanel.add(nxEntryNameEditor, comboConstraints);
        GridBagConstraints defaultButtonConstraints = new GridBagConstraints();
        defaultButtonConstraints.fill = GridBagConstraints.VERTICAL;
        defaultButtonConstraints.gridx = 1;
        defaultButtonConstraints.gridy = 0;
        defaultButtonConstraints.weightx = 0;
        defaultButtonConstraints.weighty = 1;
        editPanel.add(setDefaultValueButton, defaultButtonConstraints);

        // buttons
        buttonPanel = new JPanel(new GridBagLayout());
        closeButton.setText("Cancel");
        okButton = new StringButton() {

            private static final long serialVersionUID = 6216360703760072552L;

            @Override
            public void actionPerformed(ActionEvent event) {
                setParameter((String) nxEntryNameEditor.getSelectedItem());
                super.actionPerformed(event);
                close();
            }
        };
        okButton.setButtonLook(true);
        okButton.setText("Ok");
        okButton.setCometeFont(FontTool.getCometeFont(closeButton.getFont()));
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        buttonPanel.add(Box.createGlue(), glueConstraints);
        GridBagConstraints okConstraints = new GridBagConstraints();
        okConstraints.fill = GridBagConstraints.VERTICAL;
        okConstraints.gridx = 1;
        okConstraints.gridy = 0;
        okConstraints.weightx = 0;
        okConstraints.weighty = 1;
        okConstraints.insets = new Insets(0, 0, 5, 5);
        buttonPanel.add(okButton, okConstraints);
        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.VERTICAL;
        closeConstraints.gridx = 2;
        closeConstraints.gridy = 0;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 1;
        closeConstraints.insets = new Insets(0, 0, 5, 5);
        buttonPanel.add(closeButton, closeConstraints);

        // global layout
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints viewConstraints = new GridBagConstraints();
        viewConstraints.fill = GridBagConstraints.HORIZONTAL;
        viewConstraints.gridx = 0;
        viewConstraints.gridy = 0;
        viewConstraints.weightx = 1;
        viewConstraints.weighty = 0;
        viewConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(viewPanel, viewConstraints);
        GridBagConstraints editConstraints = new GridBagConstraints();
        editConstraints.fill = GridBagConstraints.HORIZONTAL;
        editConstraints.gridx = 0;
        editConstraints.gridy = 1;
        editConstraints.weightx = 1;
        editConstraints.weighty = 0;
        editConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(editPanel, editConstraints);
        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonConstraints.gridx = 0;
        buttonConstraints.gridy = 2;
        buttonConstraints.weightx = 1;
        buttonConstraints.weighty = 0;
        buttonConstraints.insets = new Insets(20, 5, 5, 5);
        mainPanel.add(buttonPanel, buttonConstraints);
    }

    /**
     * Updates combobox selected value
     * 
     * @param fromDefault A boolean value. If <code>TRUE</code>, the selected value is read from the
     *            default value. Otherwise, it is read from the attribute value
     */
    public void updateSelectedValue(boolean fromDefault) {
        String value;
        if (fromDefault) {
            value = dialogModel.getDefaultValue();
            if ((value != null) && ("...".equals(value) || value.trim().isEmpty())) {
                value = null;
            }
        } else {
            value = dialogModel.getNxEntryName();
            if ((value != null) && value.trim().isEmpty()) {
                value = null;
            }
        }
        for (String possibleValue : dialogModel.getModelPossibleValues()) {
            if (possibleValue.equals(value)) {
                nxEntryNameEditor.setSelectedItem(possibleValue);
                break;
            }
        }
        value = null;
    }

    public void connectDialog(StringScalarBox stringBox) {
        if (stringBox.isSettable(okButton)) {
            stringBox.setSettable(okButton, false);
        }
        stringBox.connectWidget(okButton, dialogModel.getNxEntryNameWriteModelKey());

    }

    public void clearConnections(StringScalarBox stringBox) {
        stringBox.disconnectWidgetFromAll(okButton);
    }
}
