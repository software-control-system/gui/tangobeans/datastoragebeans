package fr.soleil.comete.bean.datarecorderbean.admin;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.bean.datarecorderbean.admin.utils.CryptUtilities;
import fr.soleil.comete.bean.datarecorderbean.admin.utils.PositiveActivableNumberSpinner;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.target.basic.BasicStringMatrixTarget;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class DataRecorderAdminBean extends AbstractTangoBox {

    private static final long serialVersionUID = 115398185465950067L;

    private final static String FILES_DELETION_WARNING = ":\nWARNING: This action is unreversible. All files will be deleted and it will be impossible to recover them.\nAre you sure you want to do this";
    private final static String DEVICE_INITIALIZATION_WARNING = ":\nDo you really want to initialize the device";
    private final static String FILE_NAME_INDEX_WARNING = ":\nDo you really want to increments the index of the next file name of 1";

    private StateListener stateListener;

    // Configuration
    private JPanel configurationPanel;
    private JLabel libDataStorageVersionTitle;
    private Label libDataStorageVersionViewer;
    private StringButton initDeviceButton;
    private StringButton deleteNexusFilesButton;
    private StringButton incFileNameIndexButton;
    private RecordedFilesListener recordedFilesListener;

    // Misc
    private JPanel miscPanel;
    private JLabel maxNXEntryTitle;
    private Spinner maxNXEntryEditor;
    private JLabel indexDigitsTitle;
    private Spinner indexDigitsEditor;

    // Storage policy
    private JPanel storagePolicyPanel;
    private JLabel warningLabel;
    private ConstrainedCheckBox filesCopyCheckBox;
    private JLabel timeIntervalTitle;
    private PositiveActivableNumberSpinner timeIntervalEditor;

    private final NumberScalarBox numberBox;
    private final StringMatrixBox stringMatrixBox;

    private boolean displayMessageOnConnectionError;

    public DataRecorderAdminBean() {
        super();
        displayMessageOnConnectionError = true;
        numberBox = (NumberScalarBox) CometeBoxProvider.getCometeBox(NumberScalarBox.class);
        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);
        initComponents();
        initWidgetProperties();
    }

    private void initComponents() {
        stateListener = new StateListener();

        initConfigurationComponents();
        initMiscComponents();
        initStoragePolicyComponents();

        setLayout(new GridBagLayout());

        GridBagConstraints configurationConstraints = new GridBagConstraints();
        configurationConstraints.fill = GridBagConstraints.BOTH;
        configurationConstraints.gridx = 0;
        configurationConstraints.gridy = 0;
        configurationConstraints.weightx = 1;
        configurationConstraints.weighty = 1.0d / 3.0d;
        configurationConstraints.insets = new Insets(5, 5, 5, 5);
        add(configurationPanel, configurationConstraints);

        GridBagConstraints miscConstraints = new GridBagConstraints();
        miscConstraints.fill = GridBagConstraints.BOTH;
        miscConstraints.gridx = 0;
        miscConstraints.gridy = 1;
        miscConstraints.weightx = 1;
        miscConstraints.weighty = 1.0d / 3.0d;
        miscConstraints.insets = new Insets(0, 5, 5, 5);
        add(miscPanel, miscConstraints);

        GridBagConstraints storagePolicyConstraints = new GridBagConstraints();
        storagePolicyConstraints.fill = GridBagConstraints.BOTH;
        storagePolicyConstraints.gridx = 0;
        storagePolicyConstraints.gridy = 2;
        storagePolicyConstraints.weightx = 1;
        storagePolicyConstraints.weighty = 1.0d / 3.0d;
        storagePolicyConstraints.insets = new Insets(0, 5, 5, 5);
        add(storagePolicyPanel, storagePolicyConstraints);
    }

    private void initConfigurationComponents() {
        configurationPanel = new JPanel(new GridBagLayout());

        libDataStorageVersionTitle = new JLabel("LibDataStorage Version:");
        libDataStorageVersionViewer = generateLabel();
        libDataStorageVersionViewer.setOpaque(false);
        libDataStorageVersionViewer.setHorizontalAlignment(IComponent.LEFT);

        initDeviceButton = generateStringButton();
        initDeviceButton.setText("Initialize DataRecorder device");
        // delete command initialization
        deleteNexusFilesButton = new StringButton() {
            private static final long serialVersionUID = 111544979689639790L;

            @Override
            public void actionPerformed(ActionEvent event) {
                // encryption hack
                long currentDate = (new Date().getTime()
                        + new GregorianCalendar().getTimeZone().getOffset(new Date().getTime())) / 1000;
                String valueToInsert = "c:" + CryptUtilities.ReversibleCrypt(new Long(currentDate).toString(),
                        "gTZ4%!h:Q$*", "F|7Po@m+0Q#_x");
                setParameter(valueToInsert);
                valueToInsert = null;
                super.actionPerformed(event);
            };
        };
        deleteNexusFilesButton.setButtonLook(true);
        deleteNexusFilesButton.setText("Delete last recorded NeXus files");

        incFileNameIndexButton = generateStringButton();
        incFileNameIndexButton.setText("Force file name index incrementation");

        recordedFilesListener = new RecordedFilesListener();

        GridBagConstraints libDataStorageVersionTitleConstraints = new GridBagConstraints();
        libDataStorageVersionTitleConstraints.gridx = 0;
        libDataStorageVersionTitleConstraints.gridy = 0;
        libDataStorageVersionTitleConstraints.insets = new Insets(0, 0, 0, 5);
        configurationPanel.add(libDataStorageVersionTitle, libDataStorageVersionTitleConstraints);

        GridBagConstraints libDataStorageVersionViewerConstraints = new GridBagConstraints();
        libDataStorageVersionViewerConstraints.gridx = 1;
        libDataStorageVersionViewerConstraints.gridy = 0;
        libDataStorageVersionViewerConstraints.weightx = 1;
        libDataStorageVersionViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        configurationPanel.add(libDataStorageVersionViewer, libDataStorageVersionViewerConstraints);

        GridBagConstraints initDeviceButtonConstraints = new GridBagConstraints();
        initDeviceButtonConstraints.gridx = 0;
        initDeviceButtonConstraints.gridy = 1;
        initDeviceButtonConstraints.gridwidth = 2;
        initDeviceButtonConstraints.weightx = 1;
        initDeviceButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
        initDeviceButtonConstraints.insets = new Insets(10, 0, 0, 0);
        configurationPanel.add(initDeviceButton, initDeviceButtonConstraints);

        GridBagConstraints deleteNexusFilesButtonConstraints = new GridBagConstraints();
        deleteNexusFilesButtonConstraints.gridx = 0;
        deleteNexusFilesButtonConstraints.gridy = 2;
        deleteNexusFilesButtonConstraints.gridwidth = 2;
        deleteNexusFilesButtonConstraints.weightx = 1;
        deleteNexusFilesButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
        deleteNexusFilesButtonConstraints.insets = new Insets(5, 0, 0, 0);
        configurationPanel.add(deleteNexusFilesButton, deleteNexusFilesButtonConstraints);

        GridBagConstraints incFileNameIndexButtonConstraints = new GridBagConstraints();
        incFileNameIndexButtonConstraints.gridx = 0;
        incFileNameIndexButtonConstraints.gridy = 3;
        incFileNameIndexButtonConstraints.gridwidth = 2;
        incFileNameIndexButtonConstraints.weightx = 1;
        incFileNameIndexButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
        incFileNameIndexButtonConstraints.insets = new Insets(5, 0, 0, 0);
        configurationPanel.add(incFileNameIndexButton, incFileNameIndexButtonConstraints);
    }

    private void initMiscComponents() {
        miscPanel = new JPanel(new GridBagLayout());
        miscPanel.setBorder(new TitledBorder("Misc"));

        maxNXEntryTitle = new JLabel("Max NXentry per file:");
        maxNXEntryEditor = new Spinner();
        maxNXEntryEditor.setMinimum(0);
        maxNXEntryEditor.setMaximum(10);
        maxNXEntryEditor.setStep(1);

        indexDigitsTitle = new JLabel("Index digits:");
        indexDigitsEditor = new Spinner();
        indexDigitsEditor.setMinimum(0);
        indexDigitsEditor.setMaximum(10);
        indexDigitsEditor.setStep(1);

        GridBagConstraints maxNXEntryTitleConstraints = new GridBagConstraints();
        maxNXEntryTitleConstraints.gridx = 0;
        maxNXEntryTitleConstraints.gridy = 0;
        maxNXEntryTitleConstraints.weightx = 0;
        maxNXEntryTitleConstraints.anchor = GridBagConstraints.WEST;
        maxNXEntryTitleConstraints.insets = new Insets(0, 0, 5, 5);
        miscPanel.add(maxNXEntryTitle, maxNXEntryTitleConstraints);

        GridBagConstraints maxNXEntryEditorConstraints = new GridBagConstraints();
        maxNXEntryEditorConstraints.gridx = 1;
        maxNXEntryEditorConstraints.gridy = 0;
        maxNXEntryEditorConstraints.weightx = 1;
        maxNXEntryEditorConstraints.anchor = GridBagConstraints.WEST;
        maxNXEntryEditorConstraints.insets = new Insets(0, 0, 5, 0);
        miscPanel.add(maxNXEntryEditor, maxNXEntryEditorConstraints);

        GridBagConstraints indexDigitsTitleConstraints = new GridBagConstraints();
        indexDigitsTitleConstraints.gridx = 0;
        indexDigitsTitleConstraints.gridy = 1;
        indexDigitsTitleConstraints.weightx = 0;
        indexDigitsTitleConstraints.anchor = GridBagConstraints.WEST;
        indexDigitsTitleConstraints.insets = new Insets(0, 0, 0, 5);
        miscPanel.add(indexDigitsTitle, indexDigitsTitleConstraints);

        GridBagConstraints indexDigitsEditorConstraints = new GridBagConstraints();
        indexDigitsEditorConstraints.gridx = 1;
        indexDigitsEditorConstraints.gridy = 1;
        indexDigitsEditorConstraints.weightx = 1;
        indexDigitsEditorConstraints.anchor = GridBagConstraints.WEST;
        miscPanel.add(indexDigitsEditor, indexDigitsEditorConstraints);
    }

    private void initStoragePolicyComponents() {
        storagePolicyPanel = new JPanel(new GridBagLayout());
        storagePolicyPanel.setBorder(new TitledBorder("Storage Policy"));

        warningLabel = new JLabel("WARNING: You should not modify these settings");
        warningLabel.setIcon(iconsManager.getIcon("DataRecorderAdmin.warning"));

        filesCopyCheckBox = new ConstrainedCheckBox("Copy files to storage.");
        filesCopyCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (filesCopyCheckBox.isSelected()) {
                    timeIntervalEditor.forcePositiveData();
                } else {
                    timeIntervalEditor.forceNegativeData();
                }
            }
        });
        timeIntervalTitle = new JLabel("Time interval (minutes)");
        timeIntervalEditor = new PositiveActivableNumberSpinner();

        timeIntervalEditor.setNumberValue(15);
        timeIntervalEditor.setMinimum(5);
        timeIntervalEditor.setMaximum(1000);
        timeIntervalEditor.setStep(1);
        timeIntervalEditor.setEditable(false);

        GridBagConstraints warningLabelConstraints = new GridBagConstraints();
        warningLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        warningLabelConstraints.gridx = 0;
        warningLabelConstraints.gridy = 0;
        warningLabelConstraints.weightx = 1;
        warningLabelConstraints.weighty = 0;
        warningLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        warningLabelConstraints.insets = new Insets(0, 5, 0, 0);
        storagePolicyPanel.add(warningLabel, warningLabelConstraints);

        GridBagConstraints filesCopyCheckBoxConstraints = new GridBagConstraints();
        filesCopyCheckBoxConstraints.fill = GridBagConstraints.NONE;
        filesCopyCheckBoxConstraints.gridx = 0;
        filesCopyCheckBoxConstraints.gridy = 1;
        filesCopyCheckBoxConstraints.weightx = 0;
        filesCopyCheckBoxConstraints.weighty = 0;
        filesCopyCheckBoxConstraints.insets = new Insets(5, 5, 0, 10);
        storagePolicyPanel.add(filesCopyCheckBox, filesCopyCheckBoxConstraints);
        GridBagConstraints timeIntervalTitleConstraints = new GridBagConstraints();
        timeIntervalTitleConstraints.fill = GridBagConstraints.NONE;
        timeIntervalTitleConstraints.gridx = 1;
        timeIntervalTitleConstraints.gridy = 1;
        timeIntervalTitleConstraints.weightx = 0;
        timeIntervalTitleConstraints.weighty = 0;
        timeIntervalTitleConstraints.insets = new Insets(5, 0, 0, 5);
        storagePolicyPanel.add(timeIntervalTitle, timeIntervalTitleConstraints);
        GridBagConstraints timeIntervalEditorConstraints = new GridBagConstraints();
        timeIntervalEditorConstraints.fill = GridBagConstraints.NONE;
        timeIntervalEditorConstraints.gridx = 2;
        timeIntervalEditorConstraints.gridy = 1;
        timeIntervalEditorConstraints.weightx = 0;
        timeIntervalEditorConstraints.weighty = 0;
        timeIntervalEditorConstraints.insets = new Insets(5, 0, 0, 0);
        storagePolicyPanel.add(timeIntervalEditor, timeIntervalEditorConstraints);
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.HORIZONTAL;
        glueConstraints.gridx = 3;
        glueConstraints.gridy = 1;
        glueConstraints.weightx = 0;
        glueConstraints.weighty = 0;
        glueConstraints.insets = new Insets(5, 0, 0, 0);
        storagePolicyPanel.add(Box.createGlue(), glueConstraints);

        // This panel musnn't be used for now, so we disabled it
        storagePolicyPanel.setEnabled(false);
        warningLabel.setEnabled(false);
        timeIntervalEditor.setEnabled(false);
        filesCopyCheckBox.setEnabled(false);
        timeIntervalTitle.setEnabled(false);
    }

    @Override
    protected void refreshGUI() {
        setWidgetModel(stateListener, stringBox, generateAttributeKey("State"));
        setWidgetModel(libDataStorageVersionViewer, stringBox, generateAttributeKey("libDataStorageVersion"));
        setWidgetModel(initDeviceButton, stringBox, generateCommandKey("Init"));
        setWidgetModel(incFileNameIndexButton, stringBox, generateCommandKey("IncFileNameIndex"));
        setWidgetModel(recordedFilesListener, stringMatrixBox, generateReadOnlyAttributeKey("recordedFiles"));
        setWidgetModel(deleteNexusFilesButton, stringBox, generateCommandKey("DeleteLastRecordedFiles"));

        setWidgetModel(maxNXEntryEditor, numberBox, generateAttributeKey("maxNXentryPerFile"));
        setWidgetModel(indexDigitsEditor, numberBox, generateAttributeKey("indexDigits"));

        setWidgetModel(timeIntervalEditor, numberBox, generateAttributeKey("storagePolicy"));
    }

    private void initWidgetProperties() {
        stringBox.setOutputInPopup(initDeviceButton, true);
        stringBox.setOutputInPopup(deleteNexusFilesButton, true);
        stringBox.setOutputInPopup(incFileNameIndexButton, true);

        stringBox.setConfirmationMessage(initDeviceButton, DEVICE_INITIALIZATION_WARNING);
        stringBox.setConfirmationMessage(incFileNameIndexButton, FILE_NAME_INDEX_WARNING);
        stringBox.setConfirmationMessage(deleteNexusFilesButton, FILES_DELETION_WARNING);

        stringBox.setConfirmation(initDeviceButton, true);
        stringBox.setConfirmation(deleteNexusFilesButton, true);
        stringBox.setConfirmation(incFileNameIndexButton, true);

        stringBox.setColorEnabled(libDataStorageVersionViewer, false);
        numberBox.setColorEnabled(timeIntervalEditor, false);
        numberBox.setColorEnabled(maxNXEntryEditor, false);
        numberBox.setColorEnabled(indexDigitsEditor, false);
        numberBox.setColorAsForeground(timeIntervalEditor, true);
    }

    @Override
    protected void clearGUI() {
        cleanWidget(stateListener);
        cleanWidget(libDataStorageVersionViewer);
        cleanWidget(initDeviceButton);
        cleanWidget(incFileNameIndexButton);
        cleanWidget(recordedFilesListener);
        cleanWidget(deleteNexusFilesButton);
        cleanWidget(maxNXEntryEditor);
        cleanWidget(indexDigitsEditor);
        indexDigitsEditor.setEditable(false);
        maxNXEntryEditor.setEditable(false);
        deleteNexusFilesButton.setEnabled(false);
        filesCopyCheckBox.setEnabled(false);
        timeIntervalEditor.setActivateWithValue(false);
        timeIntervalEditor.setEditable(false);
        // Set buttons text again because stringBox put them in error
        initDeviceButton.setText("Initialize DataRecorder device");
        deleteNexusFilesButton.setText("Delete last recorded NeXus files");
        incFileNameIndexButton.setText("Force file name index incrementation");
    }

    private void updateAvailabilities() {
        String state = stateListener.getText();
        if (state == null) {
            state = "";
        }
        state = state.trim();
        boolean bStateOn = "on".equalsIgnoreCase(state);
        indexDigitsEditor.setEditable(bStateOn);
        maxNXEntryEditor.setEditable(bStateOn);
        String[] recordedFiles = recordedFilesListener.getFlatStringMatrix();
        if ((recordedFiles != null) && (recordedFiles.length > 0)) {
            // There are some recorded files: "delete files" button can be activated
            deleteNexusFilesButton.setEnabled(bStateOn);
        } else {
            // No recorded file: "delete files" button can not be activated
            deleteNexusFilesButton.setEnabled(false);
        }
        // filesCopyCheckBox.setEnabled(bStateOn);
        if (bStateOn) {
            if (!timeIntervalEditor.isActivateWithValue()) {
                timeIntervalEditor.setActivateWithValue(true);
            }
        } else {
            if (timeIntervalEditor.isActivateWithValue()) {
                timeIntervalEditor.setActivateWithValue(false);
            }
            if (timeIntervalEditor.isEditable()) {
                timeIntervalEditor.setEditable(false);
            }
        }
    }

    @Override
    protected void onConnectionError() {
        if (isDisplayMessageOnConnectionError()) {
            showMessageDialog(this, "Failed to connect to DataRecorder", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    @Override
    protected void loadPreferences(Preferences arg0) {
        // not managed
    }

    @Override
    protected void savePreferences(Preferences arg0) {
        // not managed
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Hidden class used to update components availability depending on device state
     *
     * @author girardot
     */
    protected class StateListener implements ITextTarget {
        private String text = null;

        @Override
        public String getText() {
            return text;
        }

        @Override
        public void setText(String text) {
            this.text = text;
            updateAvailabilities();
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }
    }

    /**
     * Hidden class used to update components availability depending on recorded files
     *
     * @author girardot
     */
    private class RecordedFilesListener extends BasicStringMatrixTarget {
        private String[] recordedFiles;

        @Override
        public boolean isPreferFlatValues(Class<?> concernedDataClass) {
            return true;
        }

        @Override
        public int getMatrixDataWidth(Class<?> concernedDataClass) {
            String[] tmp = recordedFiles;
            return (tmp == null ? 0 : tmp.length);
        }

        @Override
        public int getMatrixDataHeight(Class<?> concernedDataClass) {
            String[] tmp = recordedFiles;
            return (tmp == null ? 0 : 1);
        }

        @Override
        public String[][] getStringMatrix() {
            String[] tmp = recordedFiles;
            String[][] result;
            if (tmp == null) {
                result = null;
            } else {
                result = new String[][] { tmp };
            }
            return result;
        }

        @Override
        public void setStringMatrix(String[][] value) {
            if ((value == null) || (value.length == 0)) {
                recordedFiles = null;
            } else {
                recordedFiles = value[0];
            }
            updateAvailabilities();
        }

        @Override
        public String[] getFlatStringMatrix() {
            return recordedFiles;
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            recordedFiles = value;
            updateAvailabilities();
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

    }

    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if (input != null && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            DataRecorderAdminBean bean = new DataRecorderAdminBean();

            bean.setModel(deviceName);
            bean.start();

            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
