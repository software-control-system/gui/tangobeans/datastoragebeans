package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.lib.project.swing.icons.Icons;

public abstract class AbstractRecorderEditDialog extends CommonDialog {

    private static final long serialVersionUID = 7396168957883780099L;

    protected StringScalarBox stringBox;

    /**
     * Constructor
     * 
     * @param parent
     * @param title
     * @param modal
     */
    public AbstractRecorderEditDialog(Window parent, String title, boolean modal) {
        super(parent, title, modal);
        addWindowListener(initWindowClosingBehavior());
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
        initDialogModel();
        initPanelComponents();
    }

    protected abstract void initPanelComponents();

    private WindowAdapter initWindowClosingBehavior() {
        WindowAdapter result = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                processDialogClean();
            }
        };
        return result;
    }

    /**
     * Call for model update and connect component into the dialog box with the updated informations.
     * 
     * @param drModel
     */
    public void updateDialogModel(DataRecorderModel drModel) {
        updateModel(drModel);
        connectDialog();
    }

    public abstract void updateModel(final DataRecorderModel model);

    public abstract void initIconsAndResources(final Icons iconsManager, final ResourceBundle resourceBundle);

    /**
     * Make a complete clean of the dialog box (graphical component and their connections).
     */
    private void processDialogClean() {
        clearDialog();
        clearConnections();
    }

    protected abstract void connectDialog();

    /**
     * Clear graphical component
     */
    protected abstract void clearDialog();

    /**
     * Clear all {@link AbstractCometeBox}s connections
     */
    protected abstract void clearConnections();

    /**
     * Init the dialog Model that contains the keys and datas shown into the dialog box
     */
    protected abstract void initDialogModel();

    // TODO Remove this method later
    @Override
    protected void apply() {
        // TODO Auto-generated method stub

    }

}
