package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;

import fr.soleil.comete.bean.datarecorder.model.dialogmodels.ExprFileEditionModel;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;

/**
 * This class allows to create a panel which contains a textfield to modify an attribute's value
 * (file name, sub directory...) This dialog reads a properties file There are buttons : Apply,
 * Revert & Default to edit attribute's value
 * 
 * Its code is based on "DgGeneric" from former DataRecorderStateBean ATK based project.
 * 
 * @author MARECHAL
 * @author girardot
 */
public abstract class AbstractFileEditionDialog extends MemoryComboPrefEditDialog {

    private static final long serialVersionUID = -4064608769374498981L;

    // texts to show if an/no error was detected by DocumentListener
    public final static String ERROR = "Input text contains an invalid character.";
    public final static String NO_ERROR = " ";
    public final static String SYMBOLS_ERROR = "no symbols founded";

    // label which contains attribute's read value
    private Label displayedValueLabel;
    private JLabel valueTitleLabel;

    // hidden widget that monitors attribute's write value
    private TextTargetRedirector writeValueRedirector;

    private JPanel symbolPanel;
    // combo which contains all available symbols that attribute can contain
    private ComboBox symbolListComboBox;
    // button used to insert selected symbol in memory combo box editor
    private JButton insertSymbolButton;
    // button used to append selected symbol to memory combo box editor
    private JButton appendSymbolButton;

    // panel which contains "Apply", "Revert", "Default" and "Close" buttons
    private JPanel buttonPanel;

    private List<String> invalidChars;
    protected boolean inError;

    public AbstractFileEditionDialog(Window parent, String title, boolean modal) {
        super(parent, title, modal);
    }

    @Override
    protected void clearDialog() {
        if (shouldWriteBeforeQuit()) {
            doApply();
        }
        savePreferences();
        cleanSelectionCombo();
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        super.initAndAddOtherComponentsInMainPanel();
        mainPanel.setLayout(new GridBagLayout());

        writeValueRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(final String data) {
                canSetData = false;
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (memoryCombo) {
                            currentValue = data;
                            memoryCombo.setSelectedItem(data);
                        }
                        updateCombo(false);
                    }
                });
            }
        };

        valueTitleLabel = new JLabel("Effective value:");
        displayedValueLabel = new Label();
        stringBox.setColorEnabled(displayedValueLabel, false);

        initSymbolPanel();
        initButtonPanel();

        GridBagConstraints memoryComboTitleConstraints = new GridBagConstraints();
        memoryComboTitleConstraints.fill = GridBagConstraints.BOTH;
        memoryComboTitleConstraints.gridx = 0;
        memoryComboTitleConstraints.gridy = 0;
        memoryComboTitleConstraints.weightx = 0;
        memoryComboTitleConstraints.weighty = 0;
        memoryComboTitleConstraints.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(memoryComboTitleLabel, memoryComboTitleConstraints);
        GridBagConstraints memoryComboConstraints = new GridBagConstraints();
        memoryComboConstraints.fill = GridBagConstraints.BOTH;
        memoryComboConstraints.gridx = 1;
        memoryComboConstraints.gridy = 0;
        memoryComboConstraints.weightx = 1;
        memoryComboConstraints.weighty = 0;
        memoryComboConstraints.insets = new Insets(5, 0, 5, 5);
        mainPanel.add(memoryCombo, memoryComboConstraints);

        GridBagConstraints valueTitleConstraints = new GridBagConstraints();
        valueTitleConstraints.fill = GridBagConstraints.BOTH;
        valueTitleConstraints.gridx = 0;
        valueTitleConstraints.gridy = 1;
        valueTitleConstraints.weightx = 0;
        valueTitleConstraints.weighty = 0;
        valueTitleConstraints.insets = new Insets(0, 5, 5, 5);
        mainPanel.add(valueTitleLabel, valueTitleConstraints);
        GridBagConstraints valueConstraints = new GridBagConstraints();
        valueConstraints.fill = GridBagConstraints.BOTH;
        valueConstraints.gridx = 1;
        valueConstraints.gridy = 1;
        valueConstraints.weightx = 1;
        valueConstraints.weighty = 0;
        valueConstraints.insets = new Insets(0, 0, 5, 5);
        mainPanel.add(displayedValueLabel, valueConstraints);

        GridBagConstraints symbolConstraints = new GridBagConstraints();
        symbolConstraints.fill = GridBagConstraints.BOTH;
        symbolConstraints.gridx = 0;
        symbolConstraints.gridy = 2;
        symbolConstraints.weightx = 1;
        symbolConstraints.weighty = 1;
        symbolConstraints.gridwidth = GridBagConstraints.REMAINDER;
        symbolConstraints.insets = new Insets(0, 5, 30, 5);
        mainPanel.add(symbolPanel, symbolConstraints);

        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonConstraints.gridx = 0;
        buttonConstraints.gridy = 3;
        buttonConstraints.weightx = 1;
        buttonConstraints.weighty = 0;
        buttonConstraints.gridwidth = GridBagConstraints.REMAINDER;
        buttonConstraints.insets = new Insets(0, 5, 5, 5);
        mainPanel.add(buttonPanel, buttonConstraints);

        setError(false);
    }

    protected void initSymbolPanel() {
        symbolPanel = new JPanel(new GridBagLayout());

        symbolListComboBox = new ComboBox();

        insertSymbolButton = new JButton("Insert");
        insertSymbolButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertValue();
            }
        });

        appendSymbolButton = new JButton("Append");
        appendSymbolButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appendValue();
            }
        });

        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.fill = GridBagConstraints.BOTH;
        comboConstraints.gridx = 0;
        comboConstraints.gridy = 0;
        comboConstraints.weightx = 0.5;
        comboConstraints.weighty = 1;
        comboConstraints.insets = new Insets(5, 5, 5, 5);
        symbolPanel.add(symbolListComboBox, comboConstraints);

        GridBagConstraints insertConstraints = new GridBagConstraints();
        insertConstraints.fill = GridBagConstraints.VERTICAL;
        insertConstraints.gridx = 1;
        insertConstraints.gridy = 0;
        insertConstraints.weightx = 0;
        insertConstraints.weighty = 1;
        insertConstraints.insets = new Insets(5, 0, 5, 5);
        symbolPanel.add(insertSymbolButton, insertConstraints);
        GridBagConstraints appendConstraints = new GridBagConstraints();
        appendConstraints.fill = GridBagConstraints.VERTICAL;
        appendConstraints.gridx = 2;
        appendConstraints.gridy = 0;
        appendConstraints.weightx = 0;
        appendConstraints.weighty = 1;
        appendConstraints.insets = new Insets(5, 0, 5, 5);
        symbolPanel.add(appendSymbolButton, appendConstraints);

        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 3;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 0.5;
        glueConstraints.weighty = 1;
        glueConstraints.insets = new Insets(5, 0, 5, 0);
        symbolPanel.add(Box.createGlue(), glueConstraints);
    }

    protected void initButtonPanel() {
        buttonPanel = new JPanel(new GridBagLayout());

        GridBagConstraints applyConstraints = new GridBagConstraints();
        applyConstraints.fill = GridBagConstraints.VERTICAL;
        applyConstraints.gridx = 0;
        applyConstraints.gridy = 0;
        applyConstraints.weightx = 0;
        applyConstraints.weighty = 1;
        applyConstraints.insets = new Insets(5, 5, 5, 5);
        buttonPanel.add(applyButton, applyConstraints);
        buttonPanel.add(closeButton, applyConstraints);
        GridBagConstraints revertConstraints = new GridBagConstraints();
        revertConstraints.fill = GridBagConstraints.VERTICAL;
        revertConstraints.gridx = 1;
        revertConstraints.gridy = 0;
        revertConstraints.weightx = 0;
        revertConstraints.weighty = 1;
        revertConstraints.insets = new Insets(5, 0, 5, 5);
        buttonPanel.add(revertButton, revertConstraints);
        GridBagConstraints defaultConstraints = new GridBagConstraints();
        defaultConstraints.fill = GridBagConstraints.VERTICAL;
        defaultConstraints.gridx = 2;
        defaultConstraints.gridy = 0;
        defaultConstraints.weightx = 0;
        defaultConstraints.weighty = 1;
        defaultConstraints.insets = new Insets(5, 0, 5, 5);
        buttonPanel.add(setDefaultValueButton, defaultConstraints);

        AbstractButton otherActionButton = getOtherActionButton();
        if (otherActionButton != null) {
            GridBagConstraints otherActionConstraints = new GridBagConstraints();
            otherActionConstraints.fill = GridBagConstraints.VERTICAL;
            otherActionConstraints.gridx = 3;
            otherActionConstraints.gridy = 0;
            otherActionConstraints.weightx = 0;
            otherActionConstraints.weighty = 1;
            otherActionConstraints.insets = new Insets(5, 0, 5, 5);
            buttonPanel.add(otherActionButton, otherActionConstraints);
        }

        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 4;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        glueConstraints.insets = new Insets(5, 0, 5, 0);
        buttonPanel.add(Box.createGlue(), glueConstraints);

        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.VERTICAL;
        closeConstraints.gridx = 5;
        closeConstraints.gridy = 0;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 1;
        closeConstraints.insets = new Insets(5, 0, 5, 5);
        buttonPanel.add(applyButton, closeConstraints);
        buttonPanel.add(closeButton);
    }

    protected final void setValueTitle(String title) {
        if (title == null) {
            title = "";
        }
        valueTitleLabel.setText(title);
    }

    protected abstract AbstractButton getOtherActionButton();

    @Override
    public void addTextListener() {
        memoryCombo.getDocument().addDocumentListener(memoryComboTextChangeListener);
    }

    public void removeTextListener() {
        memoryCombo.getDocument().removeDocumentListener(memoryComboTextChangeListener);
    }

    @Override
    protected ActionListener generateMemoryComboActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateCombo(true);
                applyButton.setParameter(memoryCombo.getTextField().getText().trim());
            }
        };
    }

    @Override
    protected void onValueChange(String data) {
        if (canSetData) {
            if ((!inError) && (!displayedValueLabel.getText().equals(data))) {
                setError(false);
                canSetData = false;
            }
        }
    }

    /**
     * Inserts the selected value at the caret position in memory combo box editor text
     */
    public void insertValue() {
        synchronized (memoryCombo) {
            int iCaretPOs = memoryCombo.getCaretPosition();
            String selectedValue = extractSymbol(symbolListComboBox.getSelectedValue());
            StringBuilder text = new StringBuilder(memoryCombo.getTextField().getText());
            text.insert(iCaretPOs, selectedValue);
            memoryCombo.getTextField().setText(text.toString());
            memoryCombo.setCaretPosition(iCaretPOs + selectedValue.length());
            showCaret();
        }
    }

    /**
     * Add the selected value at the end of the memory combo box editor text
     * 
     */
    public void appendValue() {
        synchronized (memoryCombo) {
            Object selectedValue = extractSymbol(symbolListComboBox.getSelectedValue());
            if (selectedValue != null) {
                try {
                    memoryCombo.getDocument().insertString(memoryCombo.getDocument().getLength(),
                            selectedValue.toString(), null);
                    memoryCombo.setCaretPosition(memoryCombo.getDocument().getLength());
                    showCaret();
                } catch (BadLocationException e) {
                    // Should not happen
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method allows to show caret in memory combobox editor
     */
    protected void showCaret() {
        synchronized (memoryCombo) {
            memoryCombo.getCaret().setVisible(true);
            memoryCombo.setRequestFocusEnabled(true);
            memoryCombo.requestFocus();
        }
    }

    /**
     * This method allows to checks if input text contains or not invalid characters to disable
     * button OK and show a label to warn user
     * 
     */
    @Override
    protected void checksInputText() {
        setError(contains(memoryCombo.getTextField().getText(), getInvalidChars()));
    }

    /**
     * Update panel if an error occured : invalid char is put into the file root name
     * 
     * @param boolean bError
     */
    protected synchronized void setError(boolean bError) {
        inError = bError;
        if (bError || (currentValue == null) || currentValue.trim().isEmpty()) {
            setValueInError();
        } else {
            setValueAvailable();
        }
    }

    /**
     * Sets the value label in ERROR mode.
     */
    protected void setValueInError() {
        displayedValueLabel.setText(ERROR);
        displayedValueLabel.setForeground(Color.RED);
        applyButton.setEnabled(false);
    }

    /**
     * Sets the value label in OK mode, and sets its text with the surveyed value
     */
    protected void setValueAvailable() {
        displayedValueLabel.setForeground(Color.BLUE);
        if (MODIFICATION_COLOR.equals(memoryCombo.getTextField().getBackground())) {
            applyButton.setEnabled(true);
            closeButton.setEnabled(true);
        }
    }

    /**
     * Returns the {@link List} of known invalid characters
     * 
     * @return A {@link String} {@link List}
     */
    public List<String> getInvalidChars() {
        if (invalidChars == null) {
            invalidChars = new ArrayList<String>();
        }
        return invalidChars;
    }

    /**
     * Sets the {@link List} of known invalid characters
     * 
     * @param invalidChars The {@link List} to set
     */
    public void setInvalidChars(List<String> invalidChars) {
        this.invalidChars = invalidChars;
    }

    @Override
    protected void updateCombo(boolean modificationOccurred) {
        super.updateCombo(modificationOccurred);
        if (inError) {
            applyButton.setEnabled(false);
        }
    }

    public boolean shouldWriteBeforeQuit() {
        int result = JOptionPane.NO_OPTION;
        if (applyButton.isEnabled()) {
            // applyButton is enabled when there is at least one modification
            result = JOptionPane.showConfirmDialog(this,
                    "Changes was not saved.\nDo you want to save modifications first?", "Confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        }
        return (result == JOptionPane.YES_OPTION);
    }

    public void doApply() {
        applyButton.execute();
    }

    /**
     * Returns whether a {@link String} contains any of the {@link String}s stored in a {@link List}
     * 
     * @param text The {@link String} that may contain one of the other {@link String}s
     * @param possibleValues The {@link String} {@link List}
     * @return A boolean value. <code>TRUE</code> if the given {@link String} contains at least one
     *         of the {@link String}s in {@link List}. <code>FALSE</code> otherwise
     */
    public boolean contains(String text, List<String> possibleValues) {
        if ((text != null) && (possibleValues != null)) {
            for (String value : possibleValues) {
                if (text.contains(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    public String extractSymbol(Object value) {
        String stringValue = null;
        if (value instanceof String) {
            stringValue = (String) value;
            int separatorIndex = stringValue.indexOf(":");
            if (separatorIndex > -1) {
                stringValue = stringValue.substring(0, separatorIndex).trim();
            }
        }
        return stringValue;
    }

    public void setSymbolListModel(String[] symbols) {
        if ((symbols != null) && (symbols.length > 0)) {
            symbolListComboBox.setValueList((Object[]) symbols);
        } else {
            clearSymbolListModel();
            symbolListComboBox.setEnabled(false);
        }
    }

    public void clearSymbolListModel() {
        symbolListComboBox.setValueList(SYMBOLS_ERROR);
    }

    @Override
    public void connectDialog() {
        stringBox.connectWidget(displayedValueLabel, dialogModel.getValueModelKey());
        if (dialogModel instanceof ExprFileEditionModel) {
            stringBox.connectWidget(writeValueRedirector, ((ExprFileEditionModel) dialogModel).getWriteValueModelKey());
            setSymbolListModel(((ExprFileEditionModel) dialogModel).getSymbols());
        }

        // Set booleans that allow data modification and remove error state
        canSetData = true;
        setError(false);

        super.connectDialog();
    }

    @Override
    protected void clearConnections() {
        super.clearConnections();
        canSetData = false;
        clearSymbolListModel();
        stringBox.disconnectWidgetFromAll(writeValueRedirector);
    }

}
