package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;

import fr.soleil.comete.bean.datarecorder.model.utils.Parameter;
import fr.soleil.comete.bean.datarecorderbean.details.utils.SpringUtilities;
import fr.soleil.comete.bean.technicaldata.HeaderedDialog;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class CustomDialog extends HeaderedDialog {

    private static final long serialVersionUID = 1434839295077363886L;

    public static final String STRING_SEPARATOR = "%";
    public static final String OK_STRING = "ok";
    public static final String CANCEL_STRING = "cancel";
    public static final int OK_VALUE = 1;
    public static final int CANCEL_VALUE = 0;

    private final Vector<Parameter> parameterVector;
    private Vector<String> vectValues;
    private JPanel infoPanel;
    private JPanel buttonPanel;
    private JButton okButton;
    private int returnValue;

    public CustomDialog(Window parent, String title, boolean modal, Vector<Parameter> vectParam, String headerText,
            Icon headerIcon) {
        super(parent, title, modal);
        parameterVector = vectParam;
        setHeaderText(headerText);
        setHeaderIcon(headerIcon);
        buildInfoPanel();
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        returnValue = -1;
        infoPanel = new JPanel(new GridBagLayout());
        closeButton.setText(CANCEL_STRING);
        okButton = new JButton(OK_STRING);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateVectValues();
                returnValue = OK_VALUE;
                close();
            }
        });

        buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        buttonPanel.add(Box.createGlue(), glueConstraints);
        GridBagConstraints okConstraints = new GridBagConstraints();
        okConstraints.fill = GridBagConstraints.VERTICAL;
        okConstraints.gridx = 1;
        okConstraints.gridy = 0;
        okConstraints.weightx = 0;
        okConstraints.weighty = 1;
        okConstraints.insets = new Insets(5, 0, 5, 5);
        buttonPanel.add(okButton, okConstraints);
        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.VERTICAL;
        closeConstraints.gridx = 2;
        closeConstraints.gridy = 0;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 1;
        closeConstraints.insets = new Insets(5, 0, 5, 5);
        buttonPanel.add(closeButton, closeConstraints);

        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints infoPanelConstraints = new GridBagConstraints();
        infoPanelConstraints.fill = GridBagConstraints.BOTH;
        infoPanelConstraints.gridx = 0;
        infoPanelConstraints.gridy = 0;
        infoPanelConstraints.weightx = 1;
        infoPanelConstraints.weighty = 1;
        infoPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        mainPanel.add(infoPanel, infoPanelConstraints);
        GridBagConstraints buttonPanelConstraints = new GridBagConstraints();
        buttonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonPanelConstraints.gridx = 0;
        buttonPanelConstraints.gridy = 1;
        buttonPanelConstraints.weightx = 1;
        buttonPanelConstraints.weighty = 0;
        buttonPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        mainPanel.add(buttonPanel, buttonPanelConstraints);
    }

    @Override
    protected ActionListener generateCloseButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                returnValue = CANCEL_VALUE;
                close();
            }
        };
    }

    private void buildInfoPanel() {
        infoPanel.removeAll();
        int iNbLabels = parameterVector.size();
        // for each item of the vector, create the label with the name specified in
        // the vector, create the textfield associated and add them
        // to the panel
        for (int i = 0; i < iNbLabels; i++) {
            Parameter param = parameterVector.get(i);
            // create label
            JLabel lb = new JLabel(param.getDescription());
            // get indice of current element
            String strIndice = new Integer(i).toString();
            // create a component associated to type
            JComponent cp = createComponent(param, strIndice);
            // add labels & textfiel to the panel
            GridBagConstraints labelConstraints = new GridBagConstraints();
            labelConstraints.fill = GridBagConstraints.BOTH;
            labelConstraints.gridx = 0;
            labelConstraints.gridy = i;
            labelConstraints.weightx = 0;
            labelConstraints.weighty = 0;
            labelConstraints.insets = new Insets(5, 5, 0, 5);
            infoPanel.add(lb, labelConstraints);
            GridBagConstraints componentConstraints = new GridBagConstraints();
            componentConstraints.fill = GridBagConstraints.BOTH;
            componentConstraints.gridx = 1;
            componentConstraints.gridy = i;
            componentConstraints.weightx = 1;
            componentConstraints.weighty = 0;
            componentConstraints.insets = new Insets(5, 0, 0, 5);
            infoPanel.add(cp, componentConstraints);
            GridBagConstraints buttonConstraints = new GridBagConstraints();
            buttonConstraints.fill = GridBagConstraints.BOTH;
            buttonConstraints.gridx = 2;
            buttonConstraints.gridy = i;
            buttonConstraints.weightx = 0;
            buttonConstraints.weighty = 0;
            buttonConstraints.insets = new Insets(5, 0, 0, 5);
            infoPanel.add(getBtDefaultValue(cp, param), buttonConstraints);
        }
    }

    /**
     * This method allows to return the good component in fonction of the specified type
     * 
     * @param String strType
     * @param String strIndice which corresponds to component's name to easily find itself in the
     *            dialog
     * @param String strValue
     * @param String strName
     * @return JComponent
     */
    private JComponent createComponent(Parameter param, String strIndice) {
        JComponent component = null;

        if (param.getType().indexOf("directory") != -1) {
            component = new PnlFileChooser(strIndice, param);
            // set name and indice
            component.setName(param.getName() + STRING_SEPARATOR + strIndice);
        }

        else if (param.getType().indexOf("number") != -1) {
            if (param.getValue().length() > 0) {
                SpinnerModel model = new SpinnerNumberModel(new Integer(param.getValue()).intValue(), // initial value
                        0, // min
                        null, // no max limit
                        1); // step
                component = new JSpinner(model);
            } else {
                component = new JSpinner();
            }
            component.setName(param.getName() + STRING_SEPARATOR + strIndice);
        }

        else if (param.getType().indexOf("choice") != -1) {
            // str type is for example : choice(L1/L2/L3)
            // contains in () list of values to set in combobox

            /** get list of values */
            component = new JComboBox<>(new Vector<String>(getVectValuesCb(param.getType())));
            component.setName(param.getName() + STRING_SEPARATOR + strIndice);
            ((JComboBox<?>) component).setSelectedItem(param.getValue());
        }

        else if (param.getType().indexOf("switch") != -1) {
            component = new ConstrainedCheckBox();
            component.setName(param.getName() + STRING_SEPARATOR + strIndice);
            ((ConstrainedCheckBox) component).setSelected(new Boolean(param.getValue()).booleanValue());
        }

        else {
            component = new JTextField();
            component.setName(param.getName() + STRING_SEPARATOR + strIndice);
            ((JTextField) component).setText(param.getValue());
        }

        return component;
    }

    /**
     * Generates a {@link JButton} that allows to set component to its default value
     * 
     * @return a {@link JButton}
     */
    public JButton getBtDefaultValue(final JComponent cp, Parameter param) {
        JButton bt = new JButton("Default Value");
        final String strType = param.getType();
        final String strDefaultValue = param.getDefaultValue();
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (strType.indexOf("directory") != -1) {
                    PnlFileChooser chooser = (PnlFileChooser) cp;
                    chooser.getTextField().setText(strDefaultValue);
                } else if (strType.indexOf("number") != -1) {
                    JSpinner sp = (JSpinner) cp;
                    if (strDefaultValue.length() > 0) {
                        int i = new Integer(strDefaultValue).intValue();
                        sp.setValue(i);
                    }
                } else if (strType.indexOf("choice") != -1) {
                    JComboBox<?> cb = (JComboBox<?>) cp;
                    if (strDefaultValue.length() > 0) {
                        cb.setSelectedItem(strDefaultValue);
                    }
                } else if (strType.indexOf("switch") != -1) {
                    ConstrainedCheckBox cb = (ConstrainedCheckBox) cp;
                    if (strDefaultValue.length() > 0) {
                        cb.setSelected(Boolean.parseBoolean(strDefaultValue));
                    }
                } else {
                    JTextField tf = (JTextField) cp;
                    tf.setText(strDefaultValue);
                }
            }
        });
        return bt;
    }

    /**
     * This method allows to create a vector which contains list of values that cb must contain.
     * This list of values is contained in the string type between '( )'
     * 
     * @return Vector vectValues
     */
    private List<String> getVectValuesCb(String strTypeCombo) {
        List<String> vectValues;
        if ((strTypeCombo.indexOf("(") != -1) && (strTypeCombo.lastIndexOf(")") != -1)) {
            String strListValues = strTypeCombo.substring(strTypeCombo.indexOf("(") + 1, strTypeCombo.lastIndexOf(")"));
            // value are separated with "|"
            vectValues = Arrays.asList(strListValues.split("|"));
        } else {
            vectValues = new ArrayList<String>();
        }
        return vectValues;
    }

    /**
     * This method allows tp get corresponding Parameter with its name
     * 
     * @param String strName
     * @return Parameter param
     */
    private Parameter getParam(String strName) {
        for (Parameter param : parameterVector) {
            if (param.getName().equals(strName)) {
                return param;
            }
        }
        return null;
    }

    /**
     * This method allows to get the value contained in the component associated with the specified
     * name in the tab of all components contained in this dialog
     * 
     * @param String strIndice -> component's indice
     * @param Component[] -> Tab which contains all components of this dialog
     * @return JComponent
     */
    private String getValue(String strInd, Component[] tabComponents) {
        for (Component cp : tabComponents) {
            if (cp.getName() != null) {
                // get indice from string strIndice
                // strIndice is like "strName + m_strSeparator + strIndice"
                List<String> vectNameIndice = Arrays.asList(cp.getName().split(STRING_SEPARATOR));

                // !!!!! Set object Parameter

                String strName = vectNameIndice.get(0);
                String strIndice = vectNameIndice.get(1);

                // getParameter whose name is strName
                Parameter param = getParam(strName);

                if (strIndice.equals(strInd)) {
                    // test if this component is a textfield
                    if (cp instanceof JTextField) {
                        JTextField tf = (JTextField) cp;
                        if (tf.getText().length() > 0) {
                            return param.getName() + "=" + tf.getText();
                        } else {
                            return param.getName() + "=" + param.getDefaultValue();
                        }

                    } else if (cp instanceof JComboBox<?>) {
                        JComboBox<?> cb = (JComboBox<?>) cp;
                        if (cb.getSelectedItem() != null) {
                            return param.getName() + "=" + cb.getSelectedItem().toString();
                        } else {
                            return param.getName() + "=" + param.getDefaultValue();
                        }
                    } else if (cp instanceof JSpinner) {
                        JSpinner sp = (JSpinner) cp;
                        if (sp.getValue() != null) {
                            return param.getName() + "=" + sp.getValue().toString();
                        } else {
                            return param.getName() + "=" + param.getDefaultValue();
                        }
                    } else if (cp instanceof PnlFileChooser) {
                        PnlFileChooser pnl = (PnlFileChooser) cp;
                        if (pnl.getTextField().getText().length() > 0) {
                            return param.getName() + "=" + pnl.getTextField().getText();
                        } else {
                            return param.getName() + "=" + param.getDefaultValue();
                        }
                    } else if (cp instanceof ConstrainedCheckBox) {
                        ConstrainedCheckBox cb = (ConstrainedCheckBox) cp;
                        Boolean bool = Boolean.valueOf(cb.isSelected());
                        return param.getName() + "=" + bool.toString();
                    }
                }
            }
        }
        return null;
    }

    /**
     * This method allows to save textfields' values in vector m_vectValues
     * 
     * @return boolean (false-> if a field is empty)
     */
    private void updateVectValues() {
        vectValues = new Vector<String>();
        // get all values from textfields
        for (int i = 0; i < parameterVector.size(); i++) {
            String strIndice = Integer.toString(i);
            // get text field corresponding
            Component[] tabComponents = infoPanel.getComponents();
            // get value selected in the component named "strIndice"
            String strValue = getValue(strIndice, tabComponents);
            if (strValue != null) {
                vectValues.add(strValue);
            }
        }
    }

    public Vector<String> getVectValues() {
        return vectValues;
    }

    public int getReturnValue() {
        return returnValue;
    }

    // --------------------------------------------------------------------- //

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Inner class which creates and returns the panel which contains a {@link JButton} to start a
     * {@link JFileChooser} and a {@link JTextField} to show the result of the selection (path's
     * selection)
     */
    private class PnlFileChooser extends JPanel {

        private static final long serialVersionUID = 494959683953112542L;
        private final JTextField m_tf = new JTextField(50);

        public PnlFileChooser(String strIndice, Parameter param) {
            this.setLayout(new SpringLayout());
            // set number name to recognize the component and then save its value
            // in a vector
            m_tf.setName(param.getName() + STRING_SEPARATOR + strIndice);
            m_tf.setText(param.getValue());
            // add components
            this.add(m_tf);
            this.add(getBtFileChooser());
            SpringUtilities.makeCompactGrid(this, 1, 2, 0, 0, 5, 0);
        }

        public JButton getBtFileChooser() {
            JButton btFileChooser = new JButton("Browse...");
            btFileChooser.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // open JFileChooser
                    JFileChooser fileChooser = new JFileChooser();
                    int returnVal = fileChooser.showOpenDialog(CustomDialog.this);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        // get selected path
                        m_tf.setText(file.getAbsolutePath());
                    }
                }
            });
            return btFileChooser;
        }

        public JTextField getTextField() {
            return m_tf;
        }

    }

    // --------------------------------------------------------------------- //

}
