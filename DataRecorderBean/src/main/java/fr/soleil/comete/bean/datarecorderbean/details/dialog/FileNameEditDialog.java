package fr.soleil.comete.bean.datarecorderbean.details.dialog;

import java.awt.Window;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.swing.AbstractButton;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.FileNameDialogModel;
import fr.soleil.comete.bean.technicaldata.MemoryComboPref;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.swing.icons.Icons;

public class FileNameEditDialog extends AbstractFileEditionDialog {

    private static final long serialVersionUID = -2180215345957919403L;
    private StringButton resetFileNameIndexButton;
    private FileNameDialogModel currentDialogModel;

    /**
     * Constructor
     * 
     * @param parent
     * @param modal
     */
    public FileNameEditDialog(Window parent, boolean modal) {
        super(parent, "File name", modal);
    }

    @Override
    protected void initDialogModel() {
        dialogModel = new FileNameDialogModel();
        currentDialogModel = (FileNameDialogModel) dialogModel;
    }

    @Override
    public void initIconsAndResources(Icons iconsManager, ResourceBundle resourceBundle) {
        setHeaderIcon(iconsManager.getIcon("DataRecorder.FileName"));
        setHeaderText(resourceBundle.getString("DataRecorderState.FileName.Header"));
        setInvalidChars(Arrays.asList(resourceBundle.getString(
        "DataRecorderState.FileName.InvalidCharacters").trim().split("---")));
        setDefaultIcon(iconsManager.getIcon("DataRecorder.dialog.default"));
        setApplyIcon(iconsManager.getIcon("DataRecorder.dialog.apply"));
        setRevertIcon(iconsManager.getIcon("DataRecorder.dialog.revert"));
    }

    @Override
    public void updateModel(DataRecorderModel model) {
        super.updateModel(model);
        model.updateFileNameDialogModel(currentDialogModel);
    }

    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
        super.initAndAddOtherComponentsInMainPanel();
        memoryComboTitleLabel.setText("File name:");
    }

    @Override
    protected AbstractButton getOtherActionButton() {
        if (resetFileNameIndexButton == null) {
            resetFileNameIndexButton = new StringButton();
            resetFileNameIndexButton.setButtonLook(true);
            resetFileNameIndexButton.setText("Reset File counter");
            stringBox.setSettable(resetFileNameIndexButton, false);
        }
        return resetFileNameIndexButton;
    }

    @Override
    protected MemoryComboPref generateMemoryCombo() {
        return new MemoryComboPref("file_names");
    }

    @Override
    public void connectDialog() {
        stringBox.connectWidget(resetFileNameIndexButton, currentDialogModel.getResetFileNameIndexModelKey());
        stringBox.setConfirmationMessage(resetFileNameIndexButton,
        "\nDo you really want to reset file name index");
        stringBox.setConfirmation(resetFileNameIndexButton, true);
        super.connectDialog();
    }

    @Override
    protected void clearConnections() {
        stringBox.disconnectWidgetFromAll(resetFileNameIndexButton);
        super.clearConnections();
    }


}
