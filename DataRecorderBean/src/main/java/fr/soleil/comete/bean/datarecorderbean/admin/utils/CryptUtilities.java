package fr.soleil.comete.bean.datarecorderbean.admin.utils;

import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.Vector;

public class CryptUtilities {

    private static long m_lMagicKey = 0x20544212;
    static String m_strPSW_HEADER = "CmjBlurp";
    static String m_strPSW_FOOTER = "ZnortGnap";

    /**
     * Decrypt method
     * 
     * @param String strToDecrypt : the Hexadecimal crypted string
     * @param String strXorKey
     * @param String strRotKey
     * @return int (-1 if the end of the method is not correct, else 0)
     */
    public static String ReversibleDecrypt(String strToDecrypt, String strXorKey, String strRotKey) {
        String strValue = null;
        InnerClassBuffer buff = (new CryptUtilities().new InnerClassBuffer());
        buff.setHexString(strToDecrypt);
        if (buff.m_vectBytes.size() == 0)
            strValue = null;
        // Get crypting version
        short sVersion;
        long lMagic;
        int iMinSize = 6; // sizeof(sVersion)+sizeof(lMagic);
        // get the buffer's size
        int iBuffLength = buff.m_vectBytes.size();

        if (iBuffLength < iMinSize)
            strValue = null;

        // set current pos
        buff.m_iCurrentPos = iBuffLength - iMinSize;
        // get the magic number
        lMagic = buff.getUnsignedInt();
        if (lMagic == m_lMagicKey) {
            // get the version number
            sVersion = buff.getShort();
            if (sVersion == 2) {
                // remove Magic Key & Version from vector
                Cut(buff.m_vectBytes, iBuffLength - 6);
                // Set current Position
                buff.m_iCurrentPos = buff.m_vectBytes.size() - 8;
                // Decrypt
                short asKey[] = { 0, 0, 0, 0 };

                // Decrypt using given keys
                // get short tab
                byte[] tabRotKey;
                try {
                    tabRotKey = stringToByteTab(strRotKey);
                    buff.BinRotRight(tabRotKey);
                    byte[] tabXorKey = stringToByteTab(strXorKey);
                    buff.Xor(tabXorKey);
                }
                catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // Get computed key
                asKey[0] = buff.getShort();
                asKey[1] = buff.getShort();
                asKey[2] = buff.getShort();
                asKey[3] = buff.getShort();

                // transform short tab to byte tab
                byte[] asKeyByteTab = shortTabToByteTab(asKey);

                // Delete the computed Key from Buffer
                Cut(buff.m_vectBytes, buff.m_vectBytes.size() - 8);

                // Decrypt using computed key
                buff.BinRotRight(asKeyByteTab);
                buff.Xor(asKeyByteTab);

                // get CRC
                long lStoredCRC = buff.getUnsignedInt(buff.m_vectBytes.size() - 4);
                // remove CRC
                Cut(buff.m_vectBytes, buff.m_vectBytes.size() - 4);

                // Get buffer CRC for comparison
                long lCRC = buff.getCrc();
                if (lCRC != lStoredCRC)
                    strValue = null;

                // Remove header
                short shGarbageLength = buff.getShort(0);
                Cut2(buff.m_vectBytes, shGarbageLength + 2);
                // Remove footer
                shGarbageLength = buff.getShort(buff.m_vectBytes.size() - 2);
                Cut(buff.m_vectBytes, buff.m_vectBytes.size() - shGarbageLength - 2);
                // remove the last character which is '0'
                Cut(buff.m_vectBytes, buff.m_vectBytes.size() - 1);
                // Le buffer contient uniquement les donnees
                strValue = buff.getString(0);
            }
        }
        return strValue;
    }

    /**
     * Crypt method
     * 
     * @param String : strToCryp : the string to Crypt
     * @param String strXorKey
     * @param String strRotKey
     * @return String : str : the Hexadecimal crypted string
     */
    public static String ReversibleCrypt(String strToCrypt, String strXorKey, String strRotKey) {
        InnerClassBuffer buff = (new CryptUtilities().new InnerClassBuffer());
        short sVersion = 2;
        Random rand = new Random();

        short sl1 = (short) (rand.nextInt(Integer.MAX_VALUE) * (long) m_strPSW_HEADER.length() / Integer.MAX_VALUE);
        short sl2 = (short) (rand.nextInt(Integer.MAX_VALUE) * (long) m_strPSW_FOOTER.length() / Integer.MAX_VALUE);

        // add string to crypt
        buff.putString(strToCrypt);

        // header
        buff.insertString(m_strPSW_HEADER, sl1, 0);
        buff.insertShort(sl1, 0);
        // footer
        buff.putString(m_strPSW_FOOTER, sl2);
        buff.putShort(sl2);
        // add CRC
        buff.putUnsignedInt(buff.getCrc());

        // compute a key
        short asKey[] = { 0, 0, 0, 0 };
        asKey[0] = (short) rand.nextInt();
        asKey[1] = (short) rand.nextInt();
        asKey[2] = (short) rand.nextInt();
        asKey[3] = (short) rand.nextInt();
        byte[] asKeyByte = shortTabToByteTab(asKey);
        // Crypt using computed key
        buff.Xor(asKeyByte);
        buff.BinRotLeft(asKeyByte);
        // Add key at the end of the buffer
        buff.putShort(asKey[0]);
        buff.putShort(asKey[1]);
        buff.putShort(asKey[2]);
        buff.putShort(asKey[3]);
        // Crypt using given keys
        // get short tab
        try {
            byte[] tabXorKey = stringToByteTab(strXorKey);
            buff.Xor(tabXorKey);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // BinRotLeft(RotKey, iRotKeyLen);
        // get short tab
        try {
            byte[] tabRotKey = stringToByteTab(strRotKey);
            buff.BinRotLeft(tabRotKey);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // add magic word
        buff.putUnsignedInt(m_lMagicKey);
        buff.putShort(sVersion);
        return buff.getHexString();
    }

    /**
     * This is an inner class which allows to create a buffer which contains Bytes. Moreover, ther
     * is a lot of methods to put/insert/get String, Short & Long to/from the buffer.
     * 
     * @author MARECHAL
     * 
     */
    public class InnerClassBuffer {
        private Vector<Byte> m_vectBytes = null; // this vector contains objects "Byte"
        private int m_iCurrentPos = 0; // Current position for reading in the buffer

        public InnerClassBuffer() {
            m_vectBytes = new Vector<Byte>();
        }

        /**
         * This method allows to add a long converted to a byte at the end of the vector
         * 
         * @param long : anUnsignedShort
         */
        public void putUnsignedInt(long anUnsignedInt) {
            // prend les 4 premiers octets du long en commencant par le poids le plus fort
            byte b1 = (byte) ((anUnsignedInt & 0x000000FFL));
            byte b2 = (byte) ((anUnsignedInt & 0x0000FF00L) >> 8);
            byte b3 = (byte) ((anUnsignedInt & 0x00FF0000L) >> 16);
            byte b4 = (byte) ((anUnsignedInt & 0xFF000000L) >> 24);

            // ajoute au vecteur
            m_vectBytes.add(new Byte(b1));
            m_vectBytes.add(new Byte(b2));
            m_vectBytes.add(new Byte(b3));
            m_vectBytes.add(new Byte(b4));
        }

        /**
         * This method allows to add a short converted to a byte at the end of the vector
         * 
         * @param short : aShort
         */
        public void putShort(short aShort) {
            byte b1 = (byte) ((aShort & 0x00FF));
            byte b2 = (byte) ((aShort & 0xFF00) >> 8);

            // ajoute au vecteur
            m_vectBytes.add(new Byte(b1));
            m_vectBytes.add(new Byte(b2));
        }

        /**
         * This method allows to add a string converted to a byte at the end of the vector
         * 
         * @param String : aString
         */
        public void putString(String aString) {
            byte[] bytes;
            try {
                bytes = aString.getBytes("ISO-8859-1");
                // affichage ecran :
                for (int i = 0; i < bytes.length; i++) {
                    Byte B = new Byte(bytes[i]);
                    m_vectBytes.add(B);
                }
                Byte B = new Byte((byte) 0);
                m_vectBytes.add(B);
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        /**
         * This method allows to add a string converted to a byte at the end of the vector
         * 
         * @param String : aString
         */
        public void putString(String aString, int iSize) {
            byte[] bytes;
            try {
                bytes = aString.getBytes("ISO-8859-1");
                // affichage  ecran :
                for (int i = 0; i < iSize; i++) {
                    Byte B = new Byte(bytes[i]);
                    m_vectBytes.add(B);
                }
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        /**
         * Insert a long converted in bytes into the vector at the position iIndex
         * 
         * @param long :anUnsignedInt
         * @param int :iIndex
         */
        public void insertUnisgnedInt(long anUnsignedInt, int iIndex) {
            // prend les 4 premiers octets du long en commencant par le poids le plus fort
            byte b1 = (byte) ((anUnsignedInt & 0x000000FFL));
            byte b2 = (byte) ((anUnsignedInt & 0x0000FF00L) >> 8);
            byte b3 = (byte) ((anUnsignedInt & 0x00FF0000L) >> 16);
            byte b4 = (byte) ((anUnsignedInt & 0xFF000000L) >> 24);

            // ajoute au vecteur
            m_vectBytes.add(iIndex, new Byte(b1));
            m_vectBytes.add(iIndex + 1, new Byte(b2));
            m_vectBytes.add(iIndex + 2, new Byte(b3));
            m_vectBytes.add(iIndex + 3, new Byte(b4));
        }

        /**
         * This method allows to insert a short at the index iIndex in the buffer
         * 
         * @param short : aShort
         * @param int :iIndex
         */
        public void insertShort(short aShort, int iIndex) {
            byte b1 = (byte) ((aShort & 0x00FF));
            byte b2 = (byte) ((aShort & 0xFF00) >> 8);

            Byte B1 = new Byte(b1);
            Byte B2 = new Byte(b2);

            // ajoute au vecteur
            m_vectBytes.add(iIndex, B1);
            m_vectBytes.add(iIndex + 1, B2);
        }

        /**
         * This method allows to insert a string in the buffer at the position i
         * 
         * @param String : aString
         * @param int :iIndex
         */
        public void insertString(String aString, int iIndex) {
            char[] charArray = aString.toCharArray();
            for (int j = 0; j < charArray.length; j++) {
                int i = charArray[j];
                byte b = (byte) (i & 0x000000FFL);
                // ajoute au vecteur
                m_vectBytes.add(iIndex, new Byte(b));
            }
        }

        public void insertString(String aString, int iSize, int iIndex) {
            char[] charArray = aString.toCharArray();
            for (int j = 0; j < iSize; j++) {
                int i = charArray[j];
                byte b = (byte) (i & 0x000000FFL);
                // ajoute au vecteur
                m_vectBytes.add(iIndex, new Byte(b));
            }
        }

        /**
         * This method allows to get a long from the buffer
         * 
         * @return long
         */
        public long getUnsignedInt() {
            long anUnsignedInt = 0;
            // test if the vector is null
            if (m_vectBytes != null) {
                // get the Byte at the current position
                Byte Byte1 = (Byte) m_vectBytes.elementAt(m_iCurrentPos + 3);
                byte first = Byte1.byteValue();
                Byte Byte2 = (Byte) m_vectBytes.elementAt(m_iCurrentPos + 2);
                byte second = Byte2.byteValue();
                Byte Byte3 = (Byte) m_vectBytes.elementAt(m_iCurrentPos + 1);
                byte third = Byte3.byteValue();
                Byte Byte4 = (Byte) m_vectBytes.elementAt(m_iCurrentPos);
                byte fourth = Byte4.byteValue();

                // update the current position
                m_iCurrentPos = m_iCurrentPos + 4;

                int firstByte = (0x000000FF & ((int) first));
                int secondByte = (0x000000FF & ((int) second));
                int thirdByte = (0x000000FF & ((int) third));
                int fourthByte = (0x000000FF & ((int) fourth));

                anUnsignedInt = ((long) (firstByte << 24 | secondByte << 16 | thirdByte << 8 | fourthByte)) & 0xFFFFFFFFL;
            }
            return anUnsignedInt;
        }

        /**
         * This method allows to get a short to the current position from the buffer
         * 
         * @return short
         */
        public short getShort() {
            short aShort = 0;
            // test if the vector is null
            if (m_vectBytes != null) {
                // get the Byte at the current position
                Byte Byte1 = (Byte) m_vectBytes.elementAt(m_iCurrentPos + 1);
                byte first = Byte1.byteValue();
                Byte Byte2 = (Byte) m_vectBytes.elementAt(m_iCurrentPos);
                byte second = Byte2.byteValue();

                // update the current position
                m_iCurrentPos = m_iCurrentPos + 2;

                int firstByte = (0x000000FF & ((int) first));
                int secondByte = (0x000000FF & ((int) second));

                aShort = (short) (firstByte << 8 | secondByte);
            }
            return aShort;
        }

        /**
         * This method allows to get a String to the current position from the buffer
         * 
         * @return String
         */
        public String getString() {
            String str = "";
            for (int i = m_iCurrentPos; i < m_vectBytes.size(); i++) {
                // get Byte
                Byte B = (Byte) m_vectBytes.elementAt(i);
                // get byte
                byte byt = B.byteValue();
                byte[] tab = { byt };
                try {
                    str = str + new String(tab, "ISO-8859-1");
                }
                catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            return str;
        }

        /**
         * THis method allows to get a long at the position iIndex from the buffer
         * 
         * @param int : iIndex
         * @return long
         */
        public long getUnsignedInt(int iIndex) {
            long anUnsignedInt = 0;
            // test if the vector is null
            if (m_vectBytes != null) {
                // get the Byte at the current position
                Byte Byte1 = (Byte) m_vectBytes.elementAt(iIndex + 3);
                byte first = Byte1.byteValue();
                Byte Byte2 = (Byte) m_vectBytes.elementAt(iIndex + 2);
                byte second = Byte2.byteValue();
                Byte Byte3 = (Byte) m_vectBytes.elementAt(iIndex + 1);
                byte third = Byte3.byteValue();
                Byte Byte4 = (Byte) m_vectBytes.elementAt(iIndex);
                byte fourth = Byte4.byteValue();

                int firstByte = (0x000000FF & ((int) first));
                int secondByte = (0x000000FF & ((int) second));
                int thirdByte = (0x000000FF & ((int) third));
                int fourthByte = (0x000000FF & ((int) fourth));

                anUnsignedInt = ((long) (firstByte << 24 | secondByte << 16 | thirdByte << 8 | fourthByte)) & 0xFFFFFFFFL;
                m_iCurrentPos = m_iCurrentPos + 4;

            }
            return anUnsignedInt;
        }

        /**
         * This method allows to get a short from the buffer at the position iIndex
         * 
         * @param int :iIndex
         * @return short
         */
        public short getShort(int iIndex) {
            short aShort = 0;
            // test if the vector is null
            if (m_vectBytes != null) {
                // get the Byte at the current position
                Byte Byte1 = (Byte) m_vectBytes.elementAt(iIndex + 1);
                byte first = Byte1.byteValue();
                Byte Byte2 = (Byte) m_vectBytes.elementAt(iIndex);
                byte second = Byte2.byteValue();

                int firstByte = (0x000000FF & ((int) first));
                int secondByte = (0x000000FF & ((int) second));

                aShort = (short) (firstByte << 8 | secondByte);
            }
            return aShort;
        }

        /**
         * This method allows to get a String at the position iIndex from the buffer
         * 
         * @param int : iIndex
         * @return String
         */
        public String getString(int iIndex) {
            String str = "";
            for (int i = iIndex; i < m_vectBytes.size(); i++) {
                // get Byte
                Byte B = (Byte) m_vectBytes.elementAt(i);
                // get byte
                byte byt = B.byteValue();
                byte[] tab = { byt };
                try {
                    str = str + new String(tab, "ISO-8859-1");
                }
                catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            return str;
        }

        /**
         * Get buffer content as hexadecimal string
         * 
         * @return String
         */
        public String getHexString() {
            String strHexValue = "";
            for (int i = 0; i < m_vectBytes.size(); i++) {
                // get Byte
                Byte B = (Byte) m_vectBytes.elementAt(i);
                // get byte
                byte byt = B.byteValue();
                // get int
                int j = byt & 0xFF;
                // we must encode on two characters
                if (j < 16)
                    strHexValue = strHexValue + "0";
                // get hexadecimal value
                strHexValue = strHexValue + Integer.toHexString(j).toUpperCase();
            }
            return strHexValue;
        }

        /**
         * Put an hexadecimal string into the buffer
         * 
         * @param String str
         */
        public void setHexString(String str) {
            m_vectBytes.removeAllElements();
            for (int i = 0; i < str.length(); i = i + 2) {
                String strValue = str.substring(i, i + 2);
                int value = Integer.parseInt(strValue, 16);
                // add this value at the end of the buffer
                byte b = (byte) ((value & 0x000000FFL));
                // ajoute au vecteur
                m_vectBytes.add(new Byte(b));
            }
        }

        /**
         * Binary shifting
         * 
         * @param bOctet
         * @param bRotCoeff
         * @return
         */
        private byte BinRotLeft(byte bOctet, byte bRotCoeff) {
            int chLeftShift = unsignedByteToInt(bOctet) << bRotCoeff;
            int chRightShift = unsignedByteToInt(bOctet) >> (8 - bRotCoeff);
            return (byte) ((chLeftShift & 0xff) | chRightShift);
        }

        /**
         * Binary shifting
         * 
         * @param bOctet
         * @param bRotCoeff
         * @return byte
         */
        private byte BinRotRight(byte bOctet, byte bRotCoeff) {
            int chRightShift = unsignedByteToInt(bOctet) >> bRotCoeff;
            int chLeftShift = unsignedByteToInt(bOctet) << (8 - bRotCoeff);
            return (byte) ((chLeftShift & 0xff) | chRightShift);
        }

        /**
         * BinRotLeft
         * 
         * @param tabRot
         * @param iRotLen
         */
        public void BinRotLeft(byte[] tabRot) {
            Vector<Byte> vHuge = m_vectBytes;

            int iRot = 0;
            for (int i = 0; i < m_vectBytes.size(); i++) {
                Byte b1 = (Byte) vHuge.elementAt(i);
                byte byte1 = b1.byteValue();

                byte byte2 = (byte) (tabRot[iRot] & 7);

                byte byteToAdd = BinRotLeft(byte1, byte2);
                vHuge.setElementAt(new Byte(byteToAdd), i);

                iRot++;
                if (iRot >= tabRot.length)
                    iRot = 0;
            }
            m_vectBytes = vHuge;
        }

        /**
         * BinToRight
         * 
         * @param tabRot
         * @param iRotLen
         */
        public void BinRotRight(byte[] tabRot) {
            Vector<Byte> vHuge = m_vectBytes;

            int iRot = 0;
            for (int i = 0; i < m_vectBytes.size(); i++) {
                Byte b1 = (Byte) vHuge.elementAt(i);
                byte byte1 = b1.byteValue();

                byte byte2 = (byte) (tabRot[iRot] & 7);

                byte byteToAdd = BinRotRight(byte1, byte2);

                vHuge.setElementAt(new Byte(byteToAdd), i);

                iRot++;
                if (iRot >= tabRot.length)
                    iRot = 0;
            }
            m_vectBytes = vHuge;
        }

        /**
         * XOr
         * 
         * @param short [] tab
         */
        public void Xor(byte[] tab) {
            Vector<Byte> vHuge = m_vectBytes;
            int iXor = 0;

            for (int i = 0; i < m_vectBytes.size(); i++) {
                Byte myByte = (Byte) vHuge.elementAt(i);
                byte byteXor = tab[iXor];

                byte b = (byte) (myByte.byteValue() ^ unsignedByteToInt(byteXor));

                iXor++;
                if (iXor >= tab.length)
                    iXor = 0;

                Byte B = new Byte(b);
                vHuge.setElementAt(B, i);
            }
            m_vectBytes = vHuge;
        }

        /**
         * CRC Table
         */
        long crc_32_tab[] = { 0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
                0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L, 0xe0d5e91eL,
                0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L, 0x90bf1d91L, 0x1db71064L,
                0x6ab020f2L, 0xf3b97148L, 0x84be41deL, 0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L,
                0x83d385c7L, 0x136c9856L, 0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL,
                0x63066cd9L, 0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
                0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL, 0x35b5a8faL,
                0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L, 0x45df5c75L, 0xdcd60dcfL,
                0xabd13d59L, 0x26d930acL, 0x51de003aL, 0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L,
                0x56b3c423L, 0xcfba9599L, 0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L,
                0xb10be924L, 0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
                0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL, 0x9fbfe4a5L,
                0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL, 0xe10e9818L, 0x7f6a0dbbL,
                0x086d3d2dL, 0x91646c97L, 0xe6635c01L, 0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L,
                0xf262004eL, 0x6c0695edL, 0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L,
                0x12b7e950L, 0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
                0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L, 0x4adfa541L,
                0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL, 0x346ed9fcL, 0xad678846L,
                0xda60b8d0L, 0x44042d73L, 0x33031de5L, 0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL,
                0x270241aaL, 0xbe0b1010L, 0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L,
                0xce61e49fL, 0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
                0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L, 0x03b6e20cL,
                0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L, 0x73dc1683L, 0xe3630b12L,
                0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L, 0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L,
                0x7d079eb1L, 0xf00f9344L, 0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL,
                0x806567cbL, 0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
                0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L, 0xd6d6a3e8L,
                0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L, 0xa6bc5767L, 0x3fb506ddL,
                0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL, 0x36034af6L, 0x41047a60L, 0xdf60efc3L,
                0xa867df55L, 0x316e8eefL, 0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L,
                0x5268e236L, 0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
                0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L, 0x2cd99e8bL,
                0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL, 0x026d930aL, 0x9c0906a9L,
                0xeb0e363fL, 0x72076785L, 0x05005713L, 0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL,
                0x0cb61b38L, 0x92d28e9bL, 0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L,
                0xf1d4e242L, 0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
                0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL, 0x8f659effL,
                0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L, 0xd70dd2eeL, 0x4e048354L,
                0x3903b3c2L, 0xa7672661L, 0xd06016f7L, 0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL,
                0xd9d65adcL, 0x40df0b66L, 0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL,
                0x30b5ffe9L, 0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
                0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L, 0x5d681b02L,
                0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL, 0x2d02ef8dL };

        /**
         * Calculates the API on 32 bits
         */
        public long createCrc(int iLen, long lInitValue) {
            long lVal = 0xffffffffL;

            for (int i = 0; i < iLen; i++) {
                // get byte at the position "i" in the vector
                Byte myByte = (Byte) m_vectBytes.elementAt(i);
                byte b = myByte.byteValue();

                lVal = crc_32_tab[((int) lVal ^ b) & 0xff] ^ (lVal >> 8);
            }

            lVal = lVal ^ 0xffffffffL;
            return lVal;
        }

        /**
         * get the CRC
         * 
         * @return long CRC
         */
        public long getCrc() {
            return createCrc(m_vectBytes.size(), 0);
        }
    }

    /**
     * This method transform a short's tab into a Byte's vector
     * 
     * @param short []tab
     * @return Vector vect
     */
    public static Vector<Byte> transformToByte(short[] tab) {
        Vector<Byte> vect = new Vector<Byte>();
        for (int i = 0; i < tab.length; i++) {
            short aShort = tab[i];
            byte b1 = (byte) ((aShort & 0x00FF));
            byte b2 = (byte) ((aShort & 0xFF00) >> 8);
            // ajoute au vecteur
            vect.add(new Byte(b1));
            vect.add(new Byte(b2));
        }
        return vect;
    }

    /**
     * Delete elements in the vector from iIndex to the end
     * 
     * @param m_vect
     * @param iIndex
     */
    private static void Cut(Vector<Byte> vect, int iIndex) {
        // get the size of the vector
        int iSize = vect.size();
        // remove all element from iIndex to iSize
        for (int i = iSize; i > iIndex; i--) {
            vect.removeElementAt(i - 1);
        }
    }

    /**
     * 
     */
    private static void Cut2(Vector<Byte> vect, int iIndex) {
        // remove all elements from 0 to iIndex
        for (int i = iIndex - 1; i >= 0; i--) {
            vect.removeElementAt(i);
        }
    }

    /**
     * Convert an UNSIGNED byte to a JAVA type
     * 
     * @param byte b
     * @return int
     */
    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

    public static byte[] stringToByteTab(String aString) throws UnsupportedEncodingException {
        return aString.getBytes("ISO-8859-1");
    }

    public static byte[] shortTabToByteTab(short[] tab) {
        byte[] tabbytes = new byte[8];
        int iIndexTabBytes = 0;
        for (int i = 0; i < tab.length; i++) {
            // get short
            short aShort = tab[i];
            byte b1 = (byte) ((aShort & 0x00FF));
            byte b2 = (byte) ((aShort & 0xFF00) >> 8);

            // add to tabbytes
            tabbytes[iIndexTabBytes] = b1;
            tabbytes[iIndexTabBytes + 1] = b2;
            iIndexTabBytes = iIndexTabBytes + 2;
        }
        return tabbytes;
    }
}
