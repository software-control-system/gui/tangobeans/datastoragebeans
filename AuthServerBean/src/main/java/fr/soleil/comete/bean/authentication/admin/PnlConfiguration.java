package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;



/**
 * This class defines the panel to show tango
 * attributes' values : LDap values, Key Informations values,
 * and buttons to generate Emergenc
 * @author MARECHAL
 *
 */

public class PnlConfiguration extends JPanel
{
    private static final long serialVersionUID = -8911253016857459017L;

    private JButton m_btGetEmergencyKey = null;
    private JButton m_btChangeLdap = null;
    private JButton m_btChangeStorageParameters = null;

    JLabel m_lbBaseDataDirectory = null;
    JLabel m_lbExtProjDir = null;
    JLabel m_lbIntProjDir = null;

    private PnlAuthAdmin pnlAuthAdmin = null;


    /**
     * Default Constructor
     * @param JDialog : dg -> the dialog from which the dialog is displayed
     *
     */
    public PnlConfiguration(PnlAuthAdmin pnl)
    {
        pnlAuthAdmin = pnl;
        initialize();
    }

    /**
     * Add components to the panel
     * Add pnlParameters & pnlEmergencyClose to this one.
     *
     */
    public void initialize()
    {
        setLayout(new BorderLayout(0, 5));
        setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
        add(getPnlParameters(), BorderLayout.CENTER);
        add(getPnlEmergencyClose(), BorderLayout.SOUTH);
    }

    /**
     * Create and return the Panel which contains
     * explanations about changing values
     * @return JPanel : pnlEplanations
     */
    public JPanel getPnlExplanations()
    {
        JPanel pnlExplanations = new JPanel();
        ImageIcon imgWarning = new ImageIcon(
                PnlConfiguration.class.getResource("/fr/soleil/comete/bean/authentication/admin/icons/warning.png"));
        JLabel lbExplanations = new JLabel("Caution, these followings parameters are the " +
                "foundation of the beamline's configuration.");
        lbExplanations.setIcon(imgWarning);
        pnlExplanations.add(lbExplanations);
        return pnlExplanations;
    }

    /**
     * Create and return the panel which contains the panels
     * KeyValue ( with the informations about the session key),
     * Exaplnations (about the danger of changing values,
     * Beamline & Ldap.
     * @param JDialog : dg
     * @return JPanel pnlParameters
     */
    public JPanel getPnlParameters()
    {
        JPanel pnlParameters = new JPanel();
        pnlParameters.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Parameters"),
                BorderFactory.createEmptyBorder(5,5,5,5)));
        pnlParameters.setLayout(new BorderLayout(0, 15));

        //add components
        pnlParameters.add(getPnlExplanations(), BorderLayout.NORTH);
        pnlParameters.add(getPnlStorageParameters(), BorderLayout.CENTER);
        pnlParameters.add(getPnlLdapDataBase(), BorderLayout.SOUTH);

        return pnlParameters;
    }

    /**
     * Create and return the panel LDapDatabase
     * It contains the buttons CHange LDAP Param & Change Database Param
     * @return JPanl pnlLdapDatabase
     */
    public JPanel getPnlLdapDataBase()
    {
        JPanel pnlLdapDatabase = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        pnlLdapDatabase.add(getBtChangeLdap());
        return pnlLdapDatabase;
    }

    /**
     * Create and return the panel pnlBeamline
     * which contains the current value of the beamline's data directory
     * and the button to change it
     * @param JDialog : dialog
     * @return JPanel pnlBeamline
     */
    public JPanel getPnlStorageParameters()
    {
        /**
         * Base Data Directory
         */
        JLabel lbBaseDataDirectory = new JLabel("Base Data Directory:");
        m_lbBaseDataDirectory = new JLabel();
        m_lbBaseDataDirectory.setForeground(Color.BLUE);

        /**
         * External Projects Directory
         */
        JLabel lbExtProjDir = new JLabel("Externals Projects Directory:");
        m_lbExtProjDir = new JLabel();
        m_lbExtProjDir.setForeground(Color.BLUE);

        /**
         * Internals Projects Directory
         */
        JLabel lbIntProjDir = new JLabel("Internals Projects Directory:");
        m_lbIntProjDir = new JLabel();
        m_lbIntProjDir.setForeground(Color.BLUE);

        //add components
        JPanel pnlStorageParam = new JPanel();
        pnlStorageParam.setLayout(new MigLayout("wrap 3", "[]rel[grow, fill]unrel[]", "grow"));

        pnlStorageParam.add(lbBaseDataDirectory);
        pnlStorageParam.add(m_lbBaseDataDirectory, "wrap");

        pnlStorageParam.add(lbExtProjDir);
        pnlStorageParam.add(m_lbExtProjDir);
        pnlStorageParam.add(getBtChangeDatabaseParameters());

        pnlStorageParam.add(lbIntProjDir);
        pnlStorageParam.add(m_lbIntProjDir);

        return pnlStorageParam;
    }

    /**
     * Create and return the button which allows to create
     * an emergency key
     * @param JDialog dialog
     * @return JButton : btGetEmergencyKey
     */
    public JButton getBtEmergencyKey()
    {
        if (m_btGetEmergencyKey == null)
        {
            m_btGetEmergencyKey = new JButton("Generate Emergency Key");
            m_btGetEmergencyKey.setEnabled(false);
            m_btGetEmergencyKey.addActionListener
            (
                    new ActionListener()
                    {
                        @Override
                        public void actionPerformed(ActionEvent arg0)
                        {
                            DgCreateEmergencyKey f = new DgCreateEmergencyKey(pnlAuthAdmin.getParentWindow(), "Emergency Key",
                                    true, pnlAuthAdmin.getModel());
                            f.setLocationRelativeTo(pnlAuthAdmin.getParentWindow());
                            f.setVisible(true);
                        }
                    });
        }
        return m_btGetEmergencyKey;
    }

    /**
     * Create and return the button to change Ldap Values
     * @param Jdialog : dg
     * @return JButton : btChangeLdap
     */
    public JButton getBtChangeLdap()
    {
        if (m_btChangeLdap == null)
        {
            m_btChangeLdap = new JButton("Edit LDAP Parameters...");
            m_btChangeLdap.setEnabled(false);
            m_btChangeLdap.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    DgChangeLdap f = new DgChangeLdap(pnlAuthAdmin.getParentWindow(), "LDAP", true, pnlAuthAdmin
                            .getModel());
                    f.setLocationRelativeTo(pnlAuthAdmin.getParentWindow());
                    f.setVisible(true);
                }
            });
        }
        return m_btChangeLdap;
    }

    /**
     * Create and return a button which allows to change
     * storage parameters (base data dir, int&ext proj dir, access rights )
     * @param JDialog : dg
     * @return JButton m_btChangeStorageParameters
     */
    public JButton getBtChangeDatabaseParameters()
    {
        if (m_btChangeStorageParameters == null)
        {
            m_btChangeStorageParameters = new JButton("Edit Storage Parameters...");
            m_btChangeStorageParameters.setEnabled(false);
            m_btChangeStorageParameters.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    DgStorageParameters f = new DgStorageParameters(pnlAuthAdmin.getParentWindow(),
                            "Storage Parameters", true, pnlAuthAdmin.getModel(), PnlConfiguration.this);
                    f.setLocationRelativeTo(pnlAuthAdmin.getParentWindow());
                    f.setVisible(true);
                }
            });
        }
        return m_btChangeStorageParameters;
    }

    /**
     * Create and return the button to close the dialog
     * @param JDialog : dg to close
     * @return JButton : btClose
     */
    public JButton getBtCancel()
    {
        JButton btClose = new JButton("Close");
        btClose.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                pnlAuthAdmin.logoff();
                pnlAuthAdmin.stop();
                pnlAuthAdmin.getParentWindow().dispose();
            }});
        return btClose;
    }

    /**
     * Create and return the panel which contains the
     * buttons Emergency and Close
     * @param JDialog dg
     * @return JPanel pnlEmergencyClose
     */
    public JPanel getPnlEmergencyClose()
    {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtEmergencyKey(), "pushx, growx");
        pnlButtons.add(getBtCancel());
        return pnlButtons;
    }

    /**
     * This method allows to enable (bool = true)
     * or disable (bool = false) the buttons (Change Password,
     * Change Values, Generate Emergency Key..) after
     * the user was logged on
     * @param boolean : bState
     */
    public void setSateButtons(boolean bState)
    {
        //enable all buttons
        m_btGetEmergencyKey.setEnabled(bState);
        m_btChangeStorageParameters.setEnabled(bState);
        pnlAuthAdmin.getBtChangePassword().setEnabled(bState);
        m_btChangeLdap.setEnabled(bState);
    }

    public void updateBaseDataDirectory(String strValue)
    {
        m_lbBaseDataDirectory.setText(strValue);
    }

    public void updateExtProjDirectory(String strValue)
    {
        m_lbExtProjDir.setText(strValue);
    }

    public void updateIntProjDirectory(String strValue)
    {
        m_lbIntProjDir.setText(strValue);
    }

    public String getBaseDataDir()
    {
        return m_lbBaseDataDirectory.getText();
    }

    public String getIntProjDir()
    {
        return m_lbIntProjDir.getText();
    }

    public String getExtProjDire()
    {
        return m_lbExtProjDir.getText();
    }
}
