package fr.soleil.comete.bean.authentication;

public interface AuthServerMessageListener {

    public void newErrorDetected(Exception e, boolean displayToFront);
    
    public void newMessageDetected(String message, boolean displayToFront);

}
