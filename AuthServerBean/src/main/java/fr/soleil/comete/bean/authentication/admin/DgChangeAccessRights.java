package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import net.miginfocom.swing.MigLayout;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;


/**
 * This class allows to change access rights on directories and files
 * @author MARECHAL
 *
 */
public class DgChangeAccessRights extends JDialog
{
    private static final long serialVersionUID = -5657196844539037605L;

    private JTextField m_tfDirAccessRights = new JTextField(30);
    private JTextField m_tfFileAccessRights = new JTextField(30);
    private JLabel m_lbDirAccess = new JLabel();
    private JLabel m_lbFilesAccess = new JLabel();
    private JButton m_btApply = null;
    private JButton m_btRevert = null;

    private static Color ORANGE = new Color(16762533);
    private static Color DEFAULT_COLOR = Color.WHITE;

    private String m_strAuthPath = null;

    /**
     * Constructor
     * @param Jdialog dg
     * @param String strTitle
     * @param boolean bModal
     * @param String strAuthPath
     */
    public DgChangeAccessRights (JDialog dg, String strTitle, boolean bModal, String strAuthPath)
    {
        super(dg,strTitle,bModal);
        m_strAuthPath = strAuthPath;
        this.setContentPane(getPnlMain());
        initializeValues();
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(dg);
        this.setVisible(true);
    }

    /**
     * 
     * @return JPanel Main Panel
     */
    public JPanel getPnlMain()
    {
        Box paramBox = Box.createVerticalBox();
        paramBox.add(getPnlDirAcces());
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(getPnlFilesAcces());

        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout(0, 5));
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlMain.add(paramBox, BorderLayout.CENTER);
        pnlMain.add(getPnlButtons(this), BorderLayout.SOUTH);
        return pnlMain;
    }

    /**
     * This method initialize values of labels & textfields
     * with value readen on device authserver
     *
     */
    public void putValues()
    {
        String strDirAccess = null;
        String strFileAccess = null;

        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("dirAccessRights");
                strDirAccess = deviceAttribute.extractString();
                if (strDirAccess == null) {
                    strDirAccess = "";
                }
            } catch (DevFailed exception) {
            	 AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath,"dirAccessRights",exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("fileAccessRights");
                strFileAccess = deviceAttribute.extractString();
                if (strFileAccess == null) {
                    strFileAccess = "";
                }
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath,"fileAccessRights",exception);
            }
        }

        //set current and new values with these values
        m_lbDirAccess.setText(strDirAccess);
        m_tfDirAccessRights.setText(strDirAccess);
        m_lbFilesAccess.setText(strFileAccess);
        m_tfFileAccessRights.setText(strFileAccess);

    }

    /**
     * initialize values with device authserver's values
     * add textfields listeners
     *
     */
    public void initializeValues()
    {
        putValues();

        //add listener on each compononent
        // to set it in orange if it's modified
        addTextListener(m_tfDirAccessRights);
        addTextListener(m_tfFileAccessRights);

    }

    /***************************************************************************
     * This method allows to color components in orange if the text value has changed
     * @param textComponent
     **************************************************************************/
    public void addTextListener(final JTextComponent textComponent)
    {
        DocListener listener = new DocListener(textComponent);
        textComponent.getDocument().addDocumentListener(listener);
    }

    /**
     * This class represents a document Listener which we can
     * enable or disable thanks to boolean m_bActivated
     * @author MARECHAL
     *
     */
    private class DocListener implements DocumentListener
    {
        private JTextComponent m_tc = null; // text component to modify when the listener is activated

        //defaut constructor
        public DocListener(JTextComponent tc)
        {
            m_tc = tc;
        }

        // Implemented Methods

        @Override
        public void changedUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
            m_btRevert.setEnabled(true);
        }

        @Override
        public void insertUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
            m_btRevert.setEnabled(true);
        }

        @Override
        public void removeUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
            m_btRevert.setEnabled(true);
        }
    }

    /***************************************************************************
     * This method allows to color panels in orange at startup
     *
     **************************************************************************/
    public void colorTextComponent(boolean bModif, JTextComponent textComponent)
    {
        Color colorBackground;
        if (bModif == true) {
            colorBackground = ORANGE ;
        } else {
            colorBackground = DEFAULT_COLOR;
        }

        textComponent.setBackground(colorBackground);
    }

    /**
     * 
     * @return JPanel which contains values on Dir Access Rights
     */
    public JPanel getPnlDirAcces()
    {
        JPanel pnlDirAccess = new JPanel();
        pnlDirAccess.setLayout(new MigLayout("insets 3 n n n, wrap 2", "[align left]rel[grow, fill]"));
        Border borderBaseDataDir = BorderFactory.createTitledBorder("Directories Access Rights");
        pnlDirAccess.setBorder(borderBaseDataDir);

        JLabel lbCurrentDir = new JLabel("Current:");
        JLabel lbNewDir = new JLabel("New Value:");

        //add components
        pnlDirAccess.add(lbCurrentDir);
        pnlDirAccess.add(m_lbDirAccess);
        pnlDirAccess.add(lbNewDir);
        pnlDirAccess.add(m_tfDirAccessRights);

        return pnlDirAccess;
    }

    /**
     * 
     * @return JPanel which contains values on Files Access Rights
     */
    public JPanel getPnlFilesAcces()
    {
        JPanel pnlFilesAccess = new JPanel();
        pnlFilesAccess.setLayout(new MigLayout("insets 3 n n n, wrap 2", "[align left]rel[grow, fill]"));
        Border borderBaseDataDir = BorderFactory.createTitledBorder("Files Access Rights");
        pnlFilesAccess.setBorder(borderBaseDataDir);

        JLabel lbCurrentFiles = new JLabel("Current:");
        JLabel lbNewFiles = new JLabel("New Value:");

        //add components
        pnlFilesAccess.add(lbCurrentFiles);
        pnlFilesAccess.add(m_lbFilesAccess);
        pnlFilesAccess.add(lbNewFiles);
        pnlFilesAccess.add(m_tfFileAccessRights);

        return pnlFilesAccess;
    }

    /**
     * 
     * @param JDialog dg
     * @return JPanel which contains buttons apply, revert, close
     */
    public JPanel getPnlButtons (JDialog dg)
    {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtRevert(), "pushx, align right");
        pnlButtons.add(getBtApply(dg));
        pnlButtons.add(getBtCancel(dg));
        return pnlButtons;
    }

    /**
     * 
     * @param JDialog dg
     * @return Jbutton which apply changements on device authserver
     */
    public JButton getBtApply(final JDialog dg)
    {
        if(m_btApply == null)
        {
            m_btApply = new JButton("Apply");
            m_btApply.setEnabled(false);
            m_btApply.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    //save values
                    saveValues();
                    m_btApply.setEnabled(false);
                    //dispose dialog
                    dg.dispose();
                }
            });
        }
        return m_btApply;
    }

    /**
     * 
     * @return Jbutton which allows to cancel modifications
     */
    public JButton getBtRevert()
    {
        if(m_btRevert == null)
        {
            m_btRevert = new JButton("Revert");
            m_btRevert.setEnabled(false);
            m_btRevert.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    putValues();
                    //color components in white (no modification)
                    m_btRevert.setEnabled(false);
                    colorTextComponent(false,m_tfDirAccessRights);
                    colorTextComponent(false,m_tfFileAccessRights);
                }});
        }
        return m_btRevert;
    }

    /**
     * 
     * @param JDialog dg
     * @return JButton which allows to dispose the dialog
     */
    public JButton getBtCancel(final JDialog dg)
    {
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                dg.dispose();
            }
        });
        return btCancel;
    }

    /**
     * This method allows to save new values in device authServer
     * and update values in main panel : pnlConfiguration
     *
     */
    public void saveValues()
    {
        //save all values
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
        if (proxy != null) {
            try {
                DeviceData argin = new DeviceData();
                argin.insert(m_tfDirAccessRights.getText());
                proxy.command_inout("SetDirAccessRights", argin);
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "SetDirAccessRights", exception);
            }
            try {
                DeviceData argin = new DeviceData();
                argin.insert(m_tfFileAccessRights.getText());
                proxy.command_inout("SetFileAccessRights", argin);
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath,"SetFileAccessRights",exception);
            }
        }
    }
}
