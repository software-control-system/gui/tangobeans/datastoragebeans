package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import net.miginfocom.swing.MigLayout;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This class allows to change many parameters in database like
 * Base Data Directory, Ext & Int projects directories
 * @author MARECHAL
 *
 */
public class DgStorageParameters extends JDialog
{

    private static final long serialVersionUID = 7011924179928401460L;

    private String m_strAuthPath = null;
    private PnlConfiguration m_pnlConfiguration = null;

    private JLabel m_lbBaseDataDir = new JLabel();
    private JLabel m_lbExtProjDir = new JLabel();
    private JLabel m_lbIntProjDir = new JLabel();

    private JTextField m_tfBaseDataDir = new JTextField(30);
    private JTextField m_tfExtProjDir = new JTextField(30);
    private JTextField m_tfIntProjDir = new JTextField(30);

    private  Color ORANGE = new Color(16762533);
    private  Color DEFAULT_COLOR = Color.WHITE;

    private JButton m_btApply = null;

    /**
     * Constructor
     * @param JDialog : dg
     * @param String : strTitle
     * @param boolean : bModal
     */
    public DgStorageParameters (JDialog dg, String strTitle, boolean bModal, String strAuthPath, PnlConfiguration pnlConfig)
    {
        super(dg,strTitle,bModal);
        m_strAuthPath = strAuthPath;
        m_pnlConfiguration = pnlConfig;
        this.setContentPane(getPnlMain());
        pack();
        this.setResizable(true);
        initializeValues();
    }

    /**
     * This method allows to initialize current values
     * with values readen on device AuthServer
     *
     */
    public void initializeValues()
    {
        String strBaseDataDir = null;
        String strExtProjDir = null;
        String strIntProjDir = null;

        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("dataPath");
                strBaseDataDir = deviceAttribute.extractString();
                if (strBaseDataDir == null) {
                    strBaseDataDir = "";
                }
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "dataPath",exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("externalName");
                strExtProjDir = deviceAttribute.extractString();
                if (strExtProjDir == null) {
                    strExtProjDir = "";
                }
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "externalName",exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("inHouseName");
                strIntProjDir = deviceAttribute.extractString();
                if (strIntProjDir == null) {
                    strIntProjDir = "";
                }
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "inHouseName",exception);
            }
        }

        //set current and new values with these values
        m_lbBaseDataDir.setText(strBaseDataDir);
        m_tfBaseDataDir.setText(strBaseDataDir);
        m_lbExtProjDir.setText(strExtProjDir);
        m_tfExtProjDir.setText(strExtProjDir);
        m_lbIntProjDir.setText(strIntProjDir);
        m_tfIntProjDir.setText(strIntProjDir);

        //add listener on each compononent
        // to set it in orange if it's modified
        addTextListener(m_tfBaseDataDir);
        addTextListener(m_tfExtProjDir);
        addTextListener(m_tfIntProjDir);
    }

    /***************************************************************************
     * This method allows to color components in orange if the text value has changed
     * @param textComponent
     **************************************************************************/
    public void addTextListener(final JTextComponent textComponent)
    {
        DocListener listener = new DocListener(textComponent);
        textComponent.getDocument().addDocumentListener(listener);
    }

    /**
     * This class represents a document Listener which we can
     * enable or disable thanks to boolean m_bActivated
     * @author MARECHAL
     *
     */
    private class DocListener implements DocumentListener
    {
        private JTextComponent m_tc = null; // text component to modify when the listener is activated

        //defaut constructor
        public DocListener(JTextComponent tc)
        {
            m_tc = tc;
        }

        // Implemented Methods

        @Override
        public void changedUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
        }

        @Override
        public void insertUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
        }

        @Override
        public void removeUpdate(DocumentEvent arg0)
        {
            colorTextComponent(true,m_tc);
            m_btApply.setEnabled(true);
        }
    }

    /***************************************************************************
     * This method allows to color panels in orange at startup
     *
     **************************************************************************/
    public void colorTextComponent(boolean bModif, JTextComponent textComponent)
    {
        Color colorBackground;
        if (bModif == true) {
            colorBackground = ORANGE ;
        } else {
            colorBackground = DEFAULT_COLOR;
        }

        textComponent.setBackground(colorBackground);
    }

    /**
     * This method allows to save new values in device authServer
     * and update values in main panel : pnlConfiguration
     *
     */
    public void saveValues()
    {
        //save all values
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
        if (proxy != null) {
            try {
                DeviceData argin = new DeviceData();
                argin.insert(m_tfExtProjDir.getText());
                proxy.command_inout("setExternalName", argin);
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "setExternalName",exception);
            }
            try {
                DeviceData argin = new DeviceData();
                argin.insert(m_tfIntProjDir.getText());
                proxy.command_inout("setInHouseName", argin);
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "setInHouseName",exception);
            }
            try {
                DeviceData argin = new DeviceData();
                argin.insert(m_tfBaseDataDir.getText());
                proxy.command_inout("SetBeamlinesDataPath", argin);
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "SetBeamlinesDataPath",exception);
            }
        }

        //update values in pnlConfigurations
        m_pnlConfiguration.m_lbBaseDataDirectory.setText(m_tfBaseDataDir.getText());
        m_pnlConfiguration.m_lbExtProjDir.setText(m_tfExtProjDir.getText());
        m_pnlConfiguration.m_lbIntProjDir.setText(m_tfIntProjDir.getText());

        colorTextComponent(false,m_tfBaseDataDir);
        colorTextComponent(false,m_tfExtProjDir);
        colorTextComponent(false,m_tfIntProjDir);

    }

    /**
     * @return Main Panel which contains panels
     * PnlBaseDataDir, PnlExtProjDir, PnlIntProjDir & PnlButtons
     */
    public JPanel getPnlMain()
    {
        Box paramBox = Box.createVerticalBox();
        paramBox.add(getPnlBaseDataDir());
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(getPnlExtProjDir());
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(getPnlIntProjDir());

        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout(0, 5));
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlMain.add(paramBox, BorderLayout.CENTER);
        pnlMain.add(getPnlButtons(), BorderLayout.SOUTH);
        return pnlMain;
    }

    /**
     * @return the panel which show current value of base data directory
     */
    public JPanel getPnlBaseDataDir()
    {
        JPanel pnlBaseDataDir = new JPanel();
        Border borderBaseDataDir = BorderFactory.createTitledBorder("Base Data Directory");
        pnlBaseDataDir.setBorder(borderBaseDataDir);
        pnlBaseDataDir.setLayout(new MigLayout("insets 3 n n n, wrap 2", "[align left]rel[grow, fill]"));

        JLabel lbCurrent = new JLabel("Current:");
        JLabel lbNew = new JLabel("New Value:");

        //add components
        pnlBaseDataDir.add(lbCurrent);
        pnlBaseDataDir.add(m_lbBaseDataDir);
        pnlBaseDataDir.add(lbNew);
        pnlBaseDataDir.add(m_tfBaseDataDir);

        return pnlBaseDataDir;
    }

    /**
     * 
     * @return the panel which show current value of ext project directory
     */
    public JPanel getPnlExtProjDir()
    {
        JPanel pnlExtProjDir = new JPanel();
        Border borderBaseDataDir = BorderFactory.createTitledBorder("Externals Projects Directory");
        pnlExtProjDir.setBorder(borderBaseDataDir);
        pnlExtProjDir.setLayout(new MigLayout("insets 3 n n n, wrap 2", "[align left]rel[grow, fill]"));

        JLabel lbCurrent = new JLabel("Current:");
        JLabel lbNew = new JLabel("New Value:");

        //add components
        pnlExtProjDir.add(lbCurrent);
        pnlExtProjDir.add(m_lbExtProjDir);
        pnlExtProjDir.add(lbNew);
        pnlExtProjDir.add(m_tfExtProjDir);

        return pnlExtProjDir;
    }

    /**
     * 
     * @return the panel which show current value of int proj directory
     */
    public JPanel getPnlIntProjDir()
    {
        JPanel pnlIntProjDir = new JPanel();
        Border borderBaseDataDir = BorderFactory.createTitledBorder("Internals Projects Directory");
        pnlIntProjDir.setBorder(borderBaseDataDir);
        pnlIntProjDir.setLayout(new MigLayout("insets 3 n n n, wrap 2", "[align left]rel[grow, fill]"));

        JLabel lbCurrent = new JLabel("Current:");
        JLabel lbNew = new JLabel("New Value:");

        //add components
        pnlIntProjDir.add(lbCurrent);
        pnlIntProjDir.add(m_lbIntProjDir);
        pnlIntProjDir.add(lbNew);
        pnlIntProjDir.add(m_tfIntProjDir);

        return pnlIntProjDir;
    }

    /**
     * 
     * @return panel which contains buttons apply cancel & changeAccessRights
     */
    public JPanel getPnlButtons()
    {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtChangeAccessRights(this), "pushx, align right");
        pnlButtons.add(getBtApply(this));
        pnlButtons.add(getBtCancel(this));
        return pnlButtons;
    }

    /**
     * This method create the buttons which allows to show
     * a dialog to modify files & dir Access Rights
     * @param Jdialog dg
     * @return JButton btChangeAccessRights
     */
    public JButton getBtChangeAccessRights(final JDialog dg)
    {
        JButton btChangeAccessRights = new JButton("Change Access Rights");
        btChangeAccessRights.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Object[] options = {"OK","Cancel"};
                int iReturnValue = JOptionPane.showOptionDialog(dg,
                        "Caution, change access rights may have several incidences, " +
                                "in particular: the prohibition of reading or opening files or directories",
                                "Change Access Rights",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.WARNING_MESSAGE,
                                null,
                                options,
                                options[1]);

                if(iReturnValue == JOptionPane.OK_OPTION) {
                    new DgChangeAccessRights(dg,"Change Access Rights", true, m_strAuthPath);
                }
            }
        });
        return btChangeAccessRights;
    }

    /**
     * 
     * @param Jdialog dg
     * @return Buttons which apply changements in device authserver
     */
    public JButton getBtApply(final JDialog dg)
    {
        if(m_btApply == null)
        {
            m_btApply = new JButton("Apply");
            m_btApply.setEnabled(false);
            m_btApply.addActionListener(
                    new ActionListener()
                    {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            saveValues();
                            m_btApply.setEnabled(false);
                            dg.dispose();
                        }
                    });
        }
        return m_btApply;
    }

    /**
     * 
     * @param Jdialog dg
     * @return JButton to dispose current dialog
     */
    public JButton getBtCancel(final JDialog dg)
    {
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        dg.dispose();
                    }
                });
        return btCancel;
    }

}
