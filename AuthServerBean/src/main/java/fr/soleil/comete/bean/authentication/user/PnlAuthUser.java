package fr.soleil.comete.bean.authentication.user;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.JTextComponent;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.bean.authentication.admin.AuthAdminBean;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import net.miginfocom.swing.MigLayout;

public class PnlAuthUser extends JPanel {

    private static final long serialVersionUID = -8006250384118349991L;

    private JTextField m_tfLogin;
    private JPasswordField m_pfPassword;
    private JComboBox<String> m_cbProject;
    private JButton m_btLog;
    private String pathAuthServer;

    /***************************************************************************
     * This is the default constructor
     * 
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     **************************************************************************/
    public PnlAuthUser() {
        buildGUI();
    }

    public static String getVersion() {
        ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.comete.bean.authentication.user.application");
        return rb.getString("project.version");
    }

    /*****************************************************************************
     * This method allows to set the path of the device AuthServer
     * 
     * @param String
     *            :AuthServerPath
     *****************************************************************************/
    public void setPathAuthServer(String strAuthServerPath) {
        this.pathAuthServer = strAuthServerPath;
        if (this.pathAuthServer != null) {
            if (this.pathAuthServer.trim().length() == 0) {
                AuthServerMessageManager.notifyNewMessageDetected("Path authServer is empty");
            } else {
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(pathAuthServer);
                if (proxy != null) {
                    try {
                        proxy.command_inout("UpdateProjectsList");
                    } catch (DevFailed exception) {
                        AuthServerMessageManager.notifyExecuteCommandErrorDetected(pathAuthServer, "UpdateProjectsList",
                                exception);
                    }
                }
                initializeCbProjects();
            }
        }
    }

    /***************************************************************************
     * This method allows to create components
     * 
     **************************************************************************/
    public void createComponents() {
        m_tfLogin = new JTextField();
        m_tfLogin.setName("Login");
        // create combobox
        getCbProjects();
        m_pfPassword = new JPasswordField();
    }

    /**
     * This method allows to get current projects (with tango command) and
     * initialize combobox
     * 
     */
    public JComboBox<String> getCbProjects() {
        if (m_cbProject == null) {
            m_cbProject = new JComboBox<>();
            m_cbProject.setEditable(true);
            m_cbProject.setName("Project");
        }
        return m_cbProject;
    }

    public void initializeCbProjects() {
        String[] tabProjects = null;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(pathAuthServer);
        if (proxy != null) {
            try {
                DeviceData deviceData = proxy.command_inout("getCurrentProjects");
                tabProjects = deviceData.extractStringArray();
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyExecuteCommandErrorDetected(pathAuthServer, "getCurrentProjects",
                        exception);
            }
        }
        if (null != tabProjects) {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(tabProjects);
            m_cbProject.setModel(model);
        }
    }

    /**************************************************************************
     * This method allows to add components to the main panel
     * 
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     * 
     **************************************************************************/
    public void buildGUI() {
        // create components
        createComponents();

        // create a panel
        JPanel pnl1 = new JPanel();
        pnl1.setLayout(new MigLayout("insets 0, wrap 2", "[align left]rel[grow, fill]"));

        // add components to panel
        JLabel lbLogin = new JLabel("User:");
        pnl1.add(lbLogin);
        pnl1.add(m_tfLogin);

        JLabel lbPassword = new JLabel("Password:");
        pnl1.add(lbPassword);
        pnl1.add(m_pfPassword);

        JLabel lbProject = new JLabel("Project:");
        pnl1.add(lbProject);
        pnl1.add(m_cbProject);

        setLayout(new BorderLayout(0, 5));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(pnl1, BorderLayout.CENTER);
        add(getPnlButtons(), BorderLayout.SOUTH);
    }

    public JDialog getParentDialog() {
        Window window = SwingUtilities.getWindowAncestor(this);
        if (window != null && window instanceof JDialog) {
            return (JDialog) window;
        }
        return null;
    }

    /**
     * Create the panel Buttons
     * 
     * @return pnlButtons
     */
    JPanel getPnlButtons() {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtConsolAdmin());
        pnlButtons.add(getBtLog(), "pushx, align right");
        pnlButtons.add(getBtCancel());
        return pnlButtons;
    }

    /**
     * Create the button which allows to open the administration console for
     * authentification's server
     * 
     * @return JButton
     */
    public JButton getBtConsolAdmin() {
        JButton btAdmin = new JButton("Admin...");
        btAdmin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                new AuthAdminBean(getParentDialog(), "Administration of the autentification's server", true,
                        pathAuthServer);
            }
        });
        return btAdmin;
    }

    /******************************************************************************
     * This method allows to create the JButton : m_btLog
     * 
     * @return Jbutton : m_btLog
     ******************************************************************************/
    public JButton getBtLog() {
        m_btLog = new JButton("Ok");
        m_btLog.setEnabled(true);
        m_btLog.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // enable button
                m_btLog.setEnabled(true);// ??
                // get password
                String strPassword = new String(m_pfPassword.getPassword());
                if (strPassword != null && m_cbProject.getSelectedItem() != null) {
                    String strProject = m_cbProject.getSelectedItem().toString();
                    String strLogin = strProject;
                    if (m_tfLogin.getText() != null && !m_tfLogin.getText().isEmpty()) {
                        strLogin = m_tfLogin.getText();
                    }
                    // set input Values like : user/pwd@codeproject
                    String strInput = strLogin + "/" + strPassword + "@" + strProject;
                    // call tango comand to generate a key
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(pathAuthServer);
                    if (proxy != null) {
                        try {
                            DeviceData argin = new DeviceData();
                            argin.insert(strInput);
                            proxy.command_inout("GenerateKey", argin);
                        } catch (DevFailed exception) {
                            AuthServerMessageManager.notifyExecuteCommandErrorDetected(pathAuthServer, "GenerateKey",
                                    exception);
                        }
                    }
                    getParentDialog().dispose();
                }
            }
        });
        return m_btLog;
    }

    /**
     * Create the button which allows to close the application
     * 
     * @return JButton
     */
    public JButton getBtCancel() {
        JButton btClose = new JButton("Cancel");
        btClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                getParentDialog().dispose();
            }
        });
        return btClose;

    }

    /********************************************************************************
     * This method allows to use the method Login of the device AuthServer
     * 
     * @return String : result of the method Login
     *******************************************************************************/
    public String callLoginMethod() {
        String strArgout = null;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(pathAuthServer);
        if (proxy != null) {
            try {
                // execute methode Login
                DeviceData data = new DeviceData();
                // argIn ( Login + Project + Password)
                String argInLogin = m_tfLogin.getText();
                String argInProject = m_cbProject.getSelectedItem().toString();
                String argInPassword = new String(m_pfPassword.getPassword());
                data.insert(argInLogin + argInProject + argInPassword);
                // send a write command to the device
                DeviceData argout = proxy.command_inout("Login", data);
                strArgout = argout.extractString();
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyExecuteCommandErrorDetected(pathAuthServer, "Login", exception);
            }
        }
        return strArgout;
    }

    /***************************************************************************
     * This method checks if all fields aren't empty and enable/disable button
     * Log
     * 
     * @param JPasswordField
     * @param JTextComponent
     * @param JCombobox
     ***************************************************************************/
    public void enableBtLog(JPasswordField passwordField, JTextComponent textComponent, JComboBox<?> cb) {
        if (cb.getSelectedItem() == null) {
            m_btLog.setEnabled(false);
        } else {
            if (!passwordField.getPassword().toString().isEmpty() && !textComponent.getText().isEmpty()) {
                m_btLog.setEnabled(true);
            } else {
                m_btLog.setEnabled(false);
            }
        }
    }

    /**
     * Inner class which check if a field is empty
     * 
     * @author MARECHAL
     * 
     */
    class CheckAllFieldsTask extends TimerTask {
        @Override
        public void run() {
            enableBtLog(m_pfPassword, m_tfLogin, m_cbProject);
        }
    }

}
