package fr.soleil.comete.bean.authentication;

import java.util.ArrayList;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

public class AuthServerMessageManager {

    private static List<AuthServerMessageListener> messageListener = new ArrayList<AuthServerMessageListener>();

    public static void addMessageListener(AuthServerMessageListener listener) {
        if (!messageListener.contains(listener)) {
            messageListener.add(listener);
        }
    }

    public static void removeMessageListener(AuthServerMessageListener listener) {
        if (messageListener.contains(listener)) {
            messageListener.remove(listener);
        }
    }

    public static void notifyNewErrorDetected(Exception e) {
        notifyNewErrorDetected(e, true);
    }

    public static void notifyNewErrorDetected(Exception e, boolean displayToFront) {
        for (AuthServerMessageListener listener : messageListener) {
            listener.newErrorDetected(e, displayToFront);
        }
    }
    
    public static void notifyNewMessageDetected(String message) {
    	notifyNewMessageDetected(message, true);
    }

    public static void notifyNewMessageDetected(String message, boolean displayToFront) {
        for (AuthServerMessageListener listener : messageListener) {
            listener.newMessageDetected(message, displayToFront);
        }
    }
 

    public static void notifyReadAttributeErrorDetected(String deviceName, String attributeName, DevFailed devFailed) {
        notifyReadAttributeErrorDetected(deviceName, attributeName, devFailed, true);
    }

    public static void notifyReadAttributeErrorDetected(String deviceName, String attributeName, DevFailed devFailed,
            boolean displayToFront) {
        notifyReadAttributeErrorDetected(deviceName + "/" + attributeName, devFailed,
                displayToFront);
    }

    public static void notifyReadAttributeErrorDetected(String completeAttributeName, DevFailed devFailed) {
        AuthServerMessageManager.notifyReadAttributeErrorDetected(completeAttributeName, devFailed, true);
    }

    public static void notifyReadAttributeErrorDetected(String completeAttributeName, DevFailed devFailed,
            boolean displayToFront) {
        String errorMessage = "Cannot read attribute " + completeAttributeName + " "
        + DevFailedUtils.toString(devFailed);
        notifyNewErrorDetected(new Exception(errorMessage), displayToFront);
    }

    public static void notifyExecuteCommandErrorDetected(String deviceName, String commandName, DevFailed devFailed) {
        notifyExecuteCommandErrorDetected(deviceName, commandName, devFailed, true);
    }

    public static void notifyExecuteCommandErrorDetected(String deviceName, String commandName, DevFailed devFailed,
            boolean displayToFront) {
        notifyExecuteCommandErrorDetected(deviceName + "/" + commandName, devFailed,
                displayToFront);
    }

    public static void notifyExecuteCommandErrorDetected(String completeCommandName, DevFailed devFailed) {
        notifyExecuteCommandErrorDetected(completeCommandName, devFailed);
    }

    public static void notifyExecuteCommandErrorDetected(String completeCommandName, DevFailed devFailed,
            boolean displayToFront) {
        String errorMessage = "Cannot execute command " + completeCommandName + " "
        + DevFailedUtils.toString(devFailed);
        notifyNewErrorDetected(new Exception(errorMessage), displayToFront);
    }

}
