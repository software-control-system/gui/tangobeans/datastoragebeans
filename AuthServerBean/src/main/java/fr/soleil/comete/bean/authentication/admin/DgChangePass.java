package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.miginfocom.swing.MigLayout;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This class allows to change the administartor's password
 * @author MARECHAL
 *
 */

public class DgChangePass extends JDialog
{

    private static final long serialVersionUID = -4577079964162258410L;

    private JPasswordField m_pwNew1 = null; //put here the new password
    private JPasswordField m_pwNew2 = null; // put here the new password (confirmation)
    private JButton m_btOK = null; // button to confirm changements
    private String m_strAuthPath = null;

    /**
     * Default Constructor
     * @param JDialog : dg -> the non-null Dialog from which the dialog is displayed
     * @param String : strTitle -> the String to display in the dialog's title bar
     * @boolean boolean : bModal -> true for a modal dialog, false for one that allows other windows to be active at the same time
     */
    public DgChangePass(JDialog dg, String strTitle, boolean bModal,String strAuthPath)
    {
        super(dg,strTitle,bModal);
        m_strAuthPath = strAuthPath;
        this.setContentPane(getMainPanel());
        this.getRootPane().setDefaultButton(m_btOK);
        this.setResizable(false);
        pack();
    }

    /**
     * Create and return the main panel
     * It contains pnlPassword & PnlButtons
     * @return JPanel : pnlMain
     */
    public JPanel getMainPanel()
    {
        //create the final layout and show the m_dialog
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout(0, 5));
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlMain.add(pnlPassword(), BorderLayout.CENTER);
        pnlMain.add(getPnlButtons(this), BorderLayout.SOUTH);
        return pnlMain;
    }

    /**
     * Approves or rejects the new password.
     * @param String : strNewpwd1 ->the first new password typed by the user
     * @param String : strNewpwd2 ->the confirmation of the first password
     * @return boolean -> true if the two passwords entered are the same one.
     */
    protected boolean isNewPasswordCorrect(String strNewpwd1, String strNewpwd2)
    {
        //test if the two passwords typed are equivalent.
        return strNewpwd1.equals(strNewpwd2);
    };

    /**
     * Create and return the panel which contains labels & passwordfiels
     * @return JPanel : pnlPaswword
     */
    public JPanel pnlPassword()
    {
        JPanel pnlPassword = new JPanel();
        //set layout
        pnlPassword.setLayout(new MigLayout("insets 0, wrap 2", "[align left]rel[grow, fill]"));
        //create Components
        m_pwNew1 = new JPasswordField(15);
        m_pwNew2 = new JPasswordField(15);
        JLabel lbNew1 = new JLabel("New Password:");
        JLabel lbNew2 = new JLabel("Retype Password:");
        //add components
        pnlPassword.add(lbNew1);
        pnlPassword.add(m_pwNew1);
        pnlPassword.add(lbNew2);
        pnlPassword.add(m_pwNew2);

        return pnlPassword;
    }

    /**
     * Create and return the button to validate the password's changement
     * @param : DgChangePass : dg -> dialog to dispose
     * @return Jbutton : btOK
     */
    public JButton getBtChangePassword(final DgChangePass dg)
    {
        if (m_btOK == null)
        {
            m_btOK = new JButton("OK");
            m_btOK.setMnemonic(KeyEvent.VK_ENTER);
            m_btOK.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    //test if the two new passwords entered are equivalent
                    if(isNewPasswordCorrect(new String(m_pwNew1.getPassword()),new String(m_pwNew2.getPassword())))
                    {
                        //get the password
                        String strPassword = new String(m_pwNew2.getPassword());

                        boolean bEndMethod = false;
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
                        if (proxy != null) {
                            try {
                                DeviceData argin = new DeviceData();
                                argin.insert(strPassword);
                                proxy.command_inout("ChangePassword", argin);
                                bEndMethod = true;
                            } catch (DevFailed exception) {
                            	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath,"ChangePassword", exception);
                            }
                        }
                        if (bEndMethod) {
                            dg.dispose();
                        }
                    }
                    else
                    {
                        //the 2 passwords are not the same
                        JOptionPane.showMessageDialog(DgChangePass.this, "the two password entered are not the same");
                    }
                }
            });
        }
        return m_btOK;
    }

    /**
     * Create and return the button Cancel
     * @param JDialog  : dg -> dg to dispose
     * @return JButton :  btCancel
     */
    public JButton getBtCancel(final JDialog dg)
    {
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                dg.dispose();
            }});
        return btCancel;
    }

    /**
     * Create and return the panel which contains the buttons Ok & Cancel
     * @param DgChangePass : dg -> dialog to close when the user click on Cancel
     * @return JPanel : pnlButtons
     */
    public JPanel getPnlButtons(DgChangePass dg)
    {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtChangePassword(dg), "pushx, align right");
        pnlButtons.add(getBtCancel(dg));
        return pnlButtons;
    }
}


