package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import net.miginfocom.swing.MigLayout;

/**
 * This class allows to generate an Emergency Key.
 * The user sets Project Code and can set GID & UID
 * 
 * @author MARECHAL
 *
 */

public class DgCreateEmergencyKey extends JDialog {

    private static final long serialVersionUID = 3639056749628490200L;

    private JTextField m_tfProjectCode = null;
    private JTextField m_tfGID = null;
    private JTextField m_tfUID = null;

    private JButton m_btOK = null;

    private ConstrainedCheckBox m_cbxSoleilProject = null;

    private String m_strAuthPath = null;

    /**
     * Default constructor
     * 
     * @param JDialog : dialog -> the dialog from which the dialog is displayed
     * @param String : strTitle -> the String to display in the dialog's title bar
     * @param boolean bModal -> true for a modal dialog, false for one that allows other windows to be active at the
     *            same time
     */
    public DgCreateEmergencyKey(JDialog dg, String strTitle, boolean bModal, String strAuthPath) {
        super(dg, strTitle, bModal);
        m_strAuthPath = strAuthPath;
        this.setContentPane(getPnlMain(this));
        this.setResizable(false);
        this.getRootPane().setDefaultButton(m_btOK);
        pack();
    }

    /**
     * Create and return the main panel which
     * contains the panel whiwh show current Tango attr values
     * and the panel pnlButtons (buttons OK and Cancel)
     * 
     * @param JDialog dg (this dialog which will be close after clicking OK or Cancel)
     * @return JPanel pnlMain
     */
    public JPanel getPnlMain(JDialog dg) {
        // create labels to inform the user
        JLabel lbCaution = new JLabel("Caution!! the data won't belong to the project but to ROOT.");
        JLabel lbCaution1 = new JLabel(" -  So you need to define the project code, the user identiant (UID)");
        JLabel lbCaution2 = new JLabel("and the group identiant (GID) are optionals. ");
        JLabel lbCaution3 = new JLabel(" - There will be no data's indexation");
        JLabel lbCaution4 = new JLabel(" - Symbolic Links to data won't be create.");

        ImageIcon imgWarning = new ImageIcon(
                PnlConfiguration.class.getResource("/fr/soleil/comete/bean/authentication/admin/icons/warning.png"));
        lbCaution.setIcon(imgWarning);
        // add the labels & the panels
        Box paramBox = Box.createVerticalBox();
        paramBox.add(lbCaution);
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(lbCaution3);
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(lbCaution4);
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(lbCaution1);
        paramBox.add(Box.createVerticalStrut(5));
        paramBox.add(lbCaution2);
        paramBox.add(Box.createVerticalStrut(10));

        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout(0, 5));
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlMain.add(paramBox, BorderLayout.NORTH);
        pnlMain.add(getPnlValues(), BorderLayout.CENTER);
        pnlMain.add(getPnlButtons(dg), BorderLayout.SOUTH);
        return pnlMain;
    }

    /**
     * Create and return the panel Values
     * It contains the Tango attr current values :
     * Project Code, GID, UID
     * 
     * @return JPanel : pnlValues
     */
    public JPanel getPnlValues() {
        JPanel pnlValues = new JPanel();
        pnlValues.setLayout(new MigLayout("insets 0, wrap 2", "[align left]rel[grow, fill]"));
        // create components
        JLabel lbProjectCode = new JLabel("Project Code : ");
        m_tfProjectCode = new JTextField();
        JLabel lbGID = new JLabel("GID : ");
        m_tfGID = new JTextField();
        JLabel lbUID = new JLabel("UID : ");
        m_tfUID = new JTextField();
        JLabel lbCheckBox = new JLabel("Internal project : ");

        // add compoenents
        pnlValues.add(lbProjectCode);
        pnlValues.add(m_tfProjectCode);
        pnlValues.add(lbUID);
        pnlValues.add(m_tfUID);
        pnlValues.add(lbGID);
        pnlValues.add(m_tfGID);
        pnlValues.add(lbCheckBox);
        pnlValues.add(getCbxSoleilProject());

        return pnlValues;
    }

    /**
     * Create and return the button OK to validate
     * the value's changement and close the dialog after
     * 
     * @param JDialog : dg -> dialog to close
     * @return JButton btOK
     */
    public JButton getBtGenerateEmergencyKey(final JDialog dg) {
        if (m_btOK == null) {
            m_btOK = new JButton("Generate Key");
            m_btOK.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    boolean bEndMethod = false;

                    if (m_tfProjectCode.getText().trim().isEmpty()) {
                        JOptionPane.showMessageDialog(DgCreateEmergencyKey.this,
                                "The field Project Code must be not empty");
                    } else {
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
                        if (proxy != null) {
                            try {
                                DeviceData argin = new DeviceData();
                                argin.insert(getArgin());
                                proxy.command_inout("GenerateEmergencyKeyEx", argin);
                                bEndMethod = true;
                            } catch (DevFailed exception) {
                                AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath,
                                        "GenerateEmergencyKeyEx", exception);
                            }
                        }
                    }

                    if (bEndMethod) {
                        dg.dispose();
                    }
                }
            });
        }
        return m_btOK;
    }

    /**
     * Create and return the button to close the dialog
     * 
     * @param JDialog : dg -> dialog to dispose
     * @return JButton : btCancel
     */
    public JButton getBtCancel(final JDialog dg) {
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                dg.dispose();
            }
        });
        return btCancel;
    }

    /**
     * Create and return the panel which contains
     * the buttons Ok and Cancel
     * 
     * @param JDialog dg -> dialog to dispose
     * @return JPanel : pnlButtons
     */
    public JPanel getPnlButtons(JDialog dg) {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtGenerateEmergencyKey(dg), "pushx, align right");
        pnlButtons.add(getBtCancel(dg));
        return pnlButtons;
    }

    /**
     * Return the argin for the Tango Method
     * GenerateEmergencyKey.
     * 
     * @return null if the field ProjectCode is empty,
     *         strArgin if strProjectCode isn't empty
     */
    public String getArgin() {
        String strArgin = null;
        strArgin = m_tfProjectCode.getText(); // get project code
        strArgin = strArgin + ";" + getProjectNature(); // add project's nature : soleil/users
        if (!m_tfUID.getText().isEmpty()) {
            strArgin = strArgin + ";" + m_tfUID.getText(); // add UID
            if (!m_tfGID.getText().isEmpty()) {
                strArgin = strArgin + ":" + m_tfGID.getText(); // add GID
            }
        }
        return strArgin;
    }

    /**
     * Create and return a checkbox which notifies if the project to generate is a soleil project or not
     * if checkbox is checked -> internal project
     * 
     * @return ConstrainedCheckBox
     */
    public ConstrainedCheckBox getCbxSoleilProject() {
        if (m_cbxSoleilProject == null) {
            m_cbxSoleilProject = new ConstrainedCheckBox();
        }
        return m_cbxSoleilProject;
    }

    /**
     * 
     * @return String which correspond to the project's nature :
     *         - soleil -> internal project
     *         - users -> not internal
     */
    private String getProjectNature() {
        String nature = "users"; // default nature
        if (getCbxSoleilProject().isSelected()) {
            nature = "soleil";
        } else {
            nature = "users";
        }
        return nature;
    }
}
