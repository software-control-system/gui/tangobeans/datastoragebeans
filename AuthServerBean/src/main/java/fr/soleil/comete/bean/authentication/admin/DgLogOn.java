package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.miginfocom.swing.MigLayout;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This class allows to Logon to
 *  obtain the access to advanced functions like : Change LDAP, Generate
 *  Emergency Key ...
 * @author MARECHAL
 *
 */

public class DgLogOn extends JDialog
{
    /**
     * Default Serial Version
     */
    private static final long serialVersionUID = 1L;
    private JPasswordField m_pfPassword = null; // will contain the password entered to login
    private JButton m_btLog = null; // allows to login
    private JButton m_btCancel = null; // allows to close the m_dialog
    private String m_strAuthPath = null;

    private String m_strState = "";
    private PnlAuthAdmin pnlAuthAdmin = null;
    private boolean bCloseAll = false;

    /**
     * Default Constructor
     * @param JFrame : frame -> the frame from which the dialog is displayed
     * @param String : strTitle -> the String to display in the dialog's title bar
     * @param boolean : bModal -> true for a modal dialog, false for one that allows other windows to be active at the same time
     */
    public DgLogOn(JDialog dg, String strTitle, boolean bModal, PnlAuthAdmin pnl, boolean bCloseAll)
    {
        // call the constructor of JDialog
        super(dg, strTitle, true);
        m_strAuthPath = pnl.getModel();
        pnlAuthAdmin = pnl;
        this.bCloseAll = bCloseAll;
        this.setContentPane(getPnlMain());
        this.setResizable(false);
        this.getRootPane().setDefaultButton(m_btLog);
        this.addWindowListener(getWindowListener());
        pack();
        setLocationRelativeTo(pnlAuthAdmin.getParentWindow());
        setVisible(true);
    }

    public WindowListener getWindowListener()
    {
        WindowListener listener = new WindowListener()
        {

            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e)
            {
                m_strState = "cancel";
            }
            @Override
            public void windowDeactivated(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowOpened(WindowEvent e) {}
        };
        return listener;
    }

    /**
     * Create and return the main panel which
     * contains pnlLog and pnlButtons
     * @param JDialog : dg
     * @return JPanel pnlMain
     */
    public JPanel getPnlMain()
    {
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout(0, 5));
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        pnlMain.add(pnlLog(), BorderLayout.CENTER);
        pnlMain.add(pnlButtons(), BorderLayout.SOUTH);
        return pnlMain;
    }

    /**
     * Create and return the panel
     * to set the password to logon
     * @return JPanel pnlLog
     */
    public JPanel pnlLog()
    {
        JPanel pnlLog = new JPanel();
        pnlLog.setLayout(new MigLayout("insets 0", "[align left]rel[grow, fill]"));
        //create components
        JLabel lb = new JLabel("SuperUser Password:");
        m_pfPassword = new JPasswordField(15);
        //add components to the panel
        pnlLog.add(lb);
        pnlLog.add(m_pfPassword);

        return pnlLog;
    }

    /**
     * Create and return the panel which contains
     * the button to change the password and to close the dialog
     * @return JPanel pnlChangePass
     */
    public JPanel pnlButtons()
    {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtLog(), "pushx, align right");
        pnlButtons.add(getBtCancel());
        return pnlButtons;
    }
    /**
     * Create and return the button to logon
     * @param JDialog : dg -> dialog to close after loggin
     * @return JButton btLog
     */
    public JButton getBtLog()
    {
        if (m_btLog == null)
        {
            m_btLog = new JButton("OK");
            m_btLog.setMnemonic(KeyEvent.VK_ENTER);
            m_btLog.addActionListener((new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    //get the password
                    String strPassword = new String(m_pfPassword.getPassword());
                    boolean bLogged = false;
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
                    if (proxy != null) {
                        try {
                            DeviceData argin = new DeviceData();
                            argin.insert(strPassword);
                            proxy.command_inout("Logon", argin);
                            bLogged = true;
                        } catch (DevFailed exception) {
                        	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "Logon",exception);
                        }
                    }
                    m_pfPassword.setText("");
                    if(bLogged == true)
                    {
                        m_strState = "logged";
                        if (pnlAuthAdmin != null && pnlAuthAdmin.getPnlConfiguration() != null)
                        {
                            //the user is logged -> enable buttons
                            pnlAuthAdmin.getPnlConfiguration().setSateButtons(true);
                            pnlAuthAdmin.getBtLogOff().setEnabled(true);
                            pnlAuthAdmin.getBtLogOn().setEnabled(false);

                            //close the m_dialog
                            DgLogOn.this.dispose();
                            //set visible dialog auth admin
                            pnlAuthAdmin.getParentWindow().pack();
                            pnlAuthAdmin.getParentWindow().setLocationRelativeTo(DgLogOn.this.getParent());
                            pnlAuthAdmin.getParentWindow().setVisible(true);
                        }
                    }
                    else
                    {
                        pnlAuthAdmin.getPnlConfiguration().setSateButtons(false);
                        pnlAuthAdmin.getBtLogOff().setEnabled(false);
                        pnlAuthAdmin.getBtLogOn().setEnabled(true);
                    }
                }
            }));
        }
        return m_btLog;
    }

    public String getLoggedState()
    {
        return m_strState;
    }

    /**
     * Create and return the button btCancel
     * @param JDialog dialog : dialog to dispose
     * @return JButton : btCancel
     */
    public JButton getBtCancel()
    {
        if (m_btCancel == null)
        {
            m_btCancel = new JButton("Cancel");
            m_btCancel.addActionListener(
                    new ActionListener()
                    {
                        @Override
                        public void actionPerformed(ActionEvent arg0)
                        {
                            m_strState = "cancel";
                            if(bCloseAll) {
                                pnlAuthAdmin.getParentWindow().dispose();
                            } else {
                                DgLogOn.this.dispose();
                            }
                        }
                    });
        }
        return m_btCancel;
    }

}
