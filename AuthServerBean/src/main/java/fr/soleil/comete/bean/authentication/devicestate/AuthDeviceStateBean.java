package fr.soleil.comete.bean.authentication.devicestate;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.bean.authentication.user.AuthUserBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import net.miginfocom.swing.MigLayout;

public class AuthDeviceStateBean extends AbstractTangoBox {

    private static final long serialVersionUID = 7190276503770977301L;

    private static final Color RED = new Color(204, 0, 0);
    private static final Color GREEN = new Color(0, 102, 51);

    private static final String AUTH_SERVER_DEVICE_ATTR = "authServerDevice";

    private static final String PROJECT_ATTR = "project";
    private static final String EXPIRATION_ATTR = "expiration";
    private static final String STATUS_ATTR = "KeyStatus";
    private static final String TYPE_ATTR = "KeyType";

    private static final String PROJECT_ERROR_TEXT = "Can't read attribute project";
    private static final String EXPIRATION_ERROR_TEXT = "Can't read attribute expiration";
    private static final String STATUS_ERROR_TEXT = "Can't read attribute keyStatus";
    private static final String TYPE_ERROR_TEXT = "Can't read attribute keyType";
    private static final String TYPE_RED_VALUE = "EMERGENCY";
    private static final String STATUS_RED_VALUE = "Expired";

    private static final String PROJECT_TEXT = "Project:";
    private static final String EXPIRATION = "Expiration:";
    private static final String STATUS = "Status:";
    private static final String TYPE = "Type:";
    private static final String PROJECT_BUTTON_TEXT = "Choose project...";

    private final Icon DEVICE_OK_ICON;
    private final Icon DEVICE_KO_ICON;

    private AutoScrolledTextField projectValueTextfield;
    private Label expirationValueLabel;
    private Label statusValueLabel;
    private Label typeValueLabel;

    private JButton projectButton;

    private boolean authAdminMode;

    private TextTargetRedirector projectRedirector;
    private TextTargetRedirector statusRedirector;
    private TextTargetRedirector typeRedirector;

    public AuthDeviceStateBean() {
        DEVICE_OK_ICON = iconsManager.getIcon("AuthDeviceStateBean.DeviceOk");
        DEVICE_KO_ICON = iconsManager.getIcon("AuthDeviceStateBean.DeviceKo");

        authAdminMode = false;

        initComponents();
        layoutComponents();

        projectButton.setEnabled(false);
    }

    private void initComponents() {
        Font fieldFont = new JTextField().getFont().deriveFont(Font.BOLD, 30);
        Font labelFont = new JLabel().getFont().deriveFont(Font.BOLD, 15);

        projectValueTextfield = new AutoScrolledTextField();
        projectValueTextfield.setHorizontalAlignment(JTextField.CENTER);
        projectValueTextfield.setCometeBackground(CometeColor.WHITE);
        projectValueTextfield.setFont(fieldFont);
        projectValueTextfield.setDisabledTextColor(RED);
        projectValueTextfield.setReachEndWaitingTime(0);

        expirationValueLabel = new Label();
        statusValueLabel = new Label();
        typeValueLabel = new Label();

        expirationValueLabel.setFont(labelFont);
        statusValueLabel.setFont(labelFont);
        typeValueLabel.setFont(labelFont);

        expirationValueLabel.setForeground(GREEN);

        projectButton = new JButton(PROJECT_BUTTON_TEXT);
        projectButton.setIcon(DEVICE_OK_ICON);
        projectButton.setDisabledIcon(DEVICE_KO_ICON);
        projectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new AuthUserBean(getFrame(), "Authentication", true, getModel());
            }
        });

        stringBox.setErrorText(projectValueTextfield, PROJECT_ERROR_TEXT);
        stringBox.setErrorText(expirationValueLabel, EXPIRATION_ERROR_TEXT);
        stringBox.setErrorText(statusValueLabel, STATUS_ERROR_TEXT);
        stringBox.setErrorText(typeValueLabel, TYPE_ERROR_TEXT);

        projectRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                if (stringBox.isOnError(projectRedirector)) {
                    projectValueTextfield.setForeground(RED);

                    // disable project button as well
                    // set the icon to keep it colored
                    projectButton.setIcon(DEVICE_KO_ICON);
                    projectButton.setEnabled(false);

                } else {
                    projectValueTextfield.setForeground(GREEN);

                    projectButton.setIcon(DEVICE_OK_ICON);
                    projectButton.setEnabled(true);
                }
            }
        };
        statusRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                if (STATUS_RED_VALUE.equalsIgnoreCase(data)) {
                    statusValueLabel.setForeground(RED);
                } else {
                    statusValueLabel.setForeground(GREEN);
                }
            }
        };
        typeRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                if (TYPE_RED_VALUE.equalsIgnoreCase(data)) {
                    typeValueLabel.setForeground(RED);
                } else {
                    typeValueLabel.setForeground(GREEN);
                }
            }
        };
    }

    private void layoutComponents() {
        JLabel projectLabel = new JLabel(PROJECT_TEXT);
        JLabel expirationLabel = new JLabel(EXPIRATION);
        JLabel statusLabel = new JLabel(STATUS);
        JLabel typeLabel = new JLabel(TYPE);

        setLayout(new MigLayout("insets 0, wrap 2", "[align left]rel[grow, fill]", "[align center]25[]"));

        add(projectLabel);
        add(projectValueTextfield, "h 65!, wrap 15");
        add(expirationLabel);
        add(expirationValueLabel);
        add(statusLabel);
        add(statusValueLabel);
        add(typeLabel);
        add(typeValueLabel, "wrap 15");
        add(projectButton, "span");
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            stringBox.connectWidgetNoMetaData(projectValueTextfield, generateAttributeKey(PROJECT_ATTR));
            stringBox.connectWidgetNoMetaData(expirationValueLabel, generateAttributeKey(EXPIRATION_ATTR));
            stringBox.connectWidgetNoMetaData(statusValueLabel, generateAttributeKey(STATUS_ATTR));
            stringBox.connectWidgetNoMetaData(typeValueLabel, generateAttributeKey(TYPE_ATTR));

            stringBox.connectWidgetNoMetaData(projectRedirector, generateAttributeKey(PROJECT_ATTR));
            stringBox.connectWidgetNoMetaData(statusRedirector, generateAttributeKey(STATUS_ATTR));
            stringBox.connectWidgetNoMetaData(typeRedirector, generateAttributeKey(TYPE_ATTR));

            projectButton.setVisible(authAdminMode);
        }
    }

    @Override
    protected void clearGUI() {
        cleanWidget(projectValueTextfield);
        cleanWidget(expirationValueLabel);
        cleanWidget(statusValueLabel);
        cleanWidget(typeValueLabel);

        cleanWidget(projectRedirector);
        cleanWidget(statusRedirector);
        cleanWidget(typeRedirector);

        // set the icon to keep it colored
        projectButton.setIcon(DEVICE_KO_ICON);
        projectButton.setEnabled(false);

        projectValueTextfield.setText("");
        expirationValueLabel.setText("");
        statusValueLabel.setText("");
        typeValueLabel.setText("");
    }

    @Override
    protected void onConnectionError() {
        clearGUI();
    }

    /**
     * Return parent frame or null
     * 
     * @return JFrame
     */
    private JFrame getFrame() {
        JFrame result = null;

        Window window = SwingUtilities.getWindowAncestor(this);
        if (window instanceof JFrame) {
            result = (JFrame) window;
        }

        return result;
    }

    /**
     * This method allows to show / hide the bt Generate Key
     * 
     * @param iMode
     */
    public void setAuthAdminMode(int iMode) {
        authAdminMode = (iMode == 0);

        refreshGUI();
    }

    public int getAuthAdminMode() {
        return (authAdminMode ? 0 : 1);
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nop
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nop
    }

    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if (input != null && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            String authServerDeviceName = null;
            // read the auth server attribute from DataRecorder device
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute(AUTH_SERVER_DEVICE_ATTR);
                    authServerDeviceName = deviceAttribute.extractString();
                } catch (DevFailed exception) {
                    AuthServerMessageManager.notifyReadAttributeErrorDetected(deviceName, AUTH_SERVER_DEVICE_ATTR,
                            exception);
                }
            }

            AuthDeviceStateBean bean = new AuthDeviceStateBean();
            bean.setAuthAdminMode(0);
            bean.setModel(authServerDeviceName);
            bean.start();

            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
