package fr.soleil.comete.bean.authentication.user;

import java.awt.Dimension;
import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class AuthUserBean extends JDialog {

    private static final long serialVersionUID = -3358938124534573098L;

    private static final String AUTH_SERVER_DEVICE_ATTR = "authServerDevice";

    /**
     * constructor
     * 
     * @param JFrame
     *            frame
     * @param String
     *            strTitle
     * @param boolean bModal
     * @param String
     *            strPathAuthDevice
     */
    public AuthUserBean(JFrame frame, String strTitle, boolean bModal, String strPathAuthDevice) {
        super(frame, strTitle, bModal);
        PnlAuthUser panel = new PnlAuthUser();
        panel.setPathAuthServer(strPathAuthDevice);

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setContentPane(panel);
        this.setPreferredSize(new Dimension(400, 160));
        pack();
        setLocationRelativeTo(frame);
        this.setVisible(true);
    }


    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if (input != null && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            String authServerDeviceName = null;
            // read the auth server attribute from DataRecorder device
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute(AUTH_SERVER_DEVICE_ATTR);
                    authServerDeviceName = deviceAttribute.extractString();
                } catch (DevFailed exception) {
                	AuthServerMessageManager.notifyReadAttributeErrorDetected(deviceName, AUTH_SERVER_DEVICE_ATTR,exception);
                }
            }

            new AuthUserBean(null, "Authentication", true, authServerDeviceName);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }
}
