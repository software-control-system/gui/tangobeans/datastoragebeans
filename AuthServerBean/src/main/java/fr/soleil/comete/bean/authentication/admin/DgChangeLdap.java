package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This class allows to change the LDAP values (Tango attributes):
 * Root, Users, Beamlines, Projects, Server Name and Port
 * 
 * @author MARECHAL
 * 
 */

public class DgChangeLdap extends JDialog {

    private static final long serialVersionUID = -6209304021725186193L;

    private JButton m_btOk = null;
    private JTextField m_tfServerName = null;
    private JTextField m_tfPort = null;
    private JTextField m_tfRoot = null;
    private JTextField m_tfUsers = null;
    private JTextField m_tfBeamlines = null;
    private JTextField m_tfLogin = null;
    private JPasswordField m_pfPassword = null;
    private JTextField m_tfProjectAccounts = null;
    private JTextField m_tfProjectGroups = null;
    private String m_strAuthPath = null;

    /**
     * Constructor
     * 
     * @param JDialog :dg -> the non-null Dialog from which the dialog is displayed
     * @param String : strTitle -> Dialog's title
     * @param boolean : bModal -> Set the dialog modal or not
     *        with the specified owner dialog
     */
    public DgChangeLdap(JDialog dg, String strTitle, boolean bModal, String strAuthPath) {
        super(dg, strTitle, bModal);
        m_strAuthPath = strAuthPath;
        this.setContentPane(getPnlMain(this));
        this.pack();
        this.setResizable(false);
        this.getRootPane().setDefaultButton(m_btOk);
    }

    /**
     * Create and return the main panel
     * 
     * @return JPanel : pnlMain -> Contains the panel LDAP and the
     *         buttons to change LDAP's Values
     */
    public JPanel getPnlMain(JDialog dg) {
        JPanel pnlMain = new JPanel();
        pnlMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        // layout
        pnlMain.setLayout(new BorderLayout(0, 5));
        // add components
        pnlMain.add(getPnlLDAP(), BorderLayout.CENTER);
        pnlMain.add(getPnlButtons(dg), BorderLayout.SOUTH);
        // set values
        initializeComponents();
        return pnlMain;
    }

    /**
     * Create and return the panel LDAP
     * It contains the panel with the informations about the server
     * and the other panel with the infos about users, beamlines, projects..
     * 
     * @return pnlLDAP
     */
    public JPanel getPnlLDAP() {
        JPanel pnlLDAP = new JPanel();
        pnlLDAP.setLayout(new BoxLayout(pnlLDAP, BoxLayout.PAGE_AXIS));
        // add components
        pnlLDAP.add(getPnlServer());
        pnlLDAP.add(Box.createVerticalStrut(10));
        pnlLDAP.add(getPnlLDapInfo());
        return pnlLDAP;
    }

    /**
     * Create and return the panel which contains
     * the buttons OK & Cancel
     * 
     * @param JDialog : dg (this dialog)
     * @return JPanel : pnlButtons
     */
    public JPanel getPnlButtons(JDialog dg) {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new MigLayout("insets 0", "[]5[]"));
        pnlButtons.add(getBtOk(dg), "pushx, align right");
        pnlButtons.add(getBtCancel(dg));
        return pnlButtons;
    }

    /**
     * Create and return the button OK
     * which save all modifications on LDap's values
     * 
     * @return JButton : m_btOk
     */
    public JButton getBtOk(final JDialog dg) {
        if (m_btOk == null) {
            m_btOk = new JButton("OK");
            m_btOk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    boolean bEndMethod = false;
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
                    if (proxy != null) {
                        try {
                            DeviceData argin = new DeviceData();
                            argin.insert(getTabLdapArgin());
                            proxy.command_inout("SetLdapParameters", argin);
                            bEndMethod = true;
                        } catch (DevFailed exception) {
                        	AuthServerMessageManager.notifyExecuteCommandErrorDetected(m_strAuthPath, "SetLdapParameters",exception);
                        }
                    }
                    if (bEndMethod) {
                        dg.dispose();
                    }
                }
            });
        }
        return m_btOk;
    }

    /**
     * Create and return the button Cancel
     * which allows to close this dialog
     * 
     * @param JDialog : dg
     * @return JButton : btCancel
     */
    public JButton getBtCancel(final JDialog dg) {
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                dg.dispose();
            }
        });
        return btCancel;
    }

    /**
     * Create and return the panel which
     * contains all informations about LDAP
     * 
     * @return JPanel : pnlLDAPInfo
     */
    public JPanel getPnlLDapInfo() {
        JPanel pnlLDapInfo = new JPanel();
        // layout
        pnlLDapInfo.setLayout(new MigLayout("insets 0, wrap 2", "[align left]rel[grow, fill]"));
        // create components

        JLabel lbLogin = new JLabel("Login:");
        m_tfLogin = new JTextField(30);
        JLabel lbPassword = new JLabel("Password:");
        m_pfPassword = new JPasswordField(30);
        JLabel lbRoot = new JLabel("Root:");
        m_tfRoot = new JTextField(30);
        JLabel lbUsers = new JLabel("Users:");
        m_tfUsers = new JTextField(30);
        JLabel lbBeamlines = new JLabel("Beamlines:");
        m_tfBeamlines = new JTextField(30);
        JLabel lbProjAcc = new JLabel("Project Accounts:");
        m_tfProjectAccounts = new JTextField(30);
        JLabel lbProjGroups = new JLabel("Project Groups:");
        m_tfProjectGroups = new JTextField(30);

        // add components
        pnlLDapInfo.add(lbRoot);
        pnlLDapInfo.add(m_tfRoot);
        pnlLDapInfo.add(lbLogin);
        pnlLDapInfo.add(m_tfLogin);
        pnlLDapInfo.add(lbPassword);
        pnlLDapInfo.add(m_pfPassword);
        pnlLDapInfo.add(lbUsers);
        pnlLDapInfo.add(m_tfUsers);
        pnlLDapInfo.add(lbBeamlines);
        pnlLDapInfo.add(m_tfBeamlines);
        pnlLDapInfo.add(lbProjAcc);
        pnlLDapInfo.add(m_tfProjectAccounts);
        pnlLDapInfo.add(lbProjGroups);
        pnlLDapInfo.add(m_tfProjectGroups);
        return pnlLDapInfo;
    }

    /**
     * Create and return the panel which
     * contains the infos about the LDAP's server
     * 
     * @return JPanel : pnlServer
     */
    public JPanel getPnlServer() {
        JPanel pnlServer = new JPanel();
        pnlServer.setLayout(new MigLayout("insets 0", "[align left]rel[grow, fill]unrel[]rel[]"));
        // create components
        JLabel lbServerName = new JLabel("Server name:");
        m_tfServerName = new JTextField(15);
        JLabel lbPort = new JLabel("Port:");
        m_tfPort = new JTextField(5);

        // add components
        pnlServer.add(lbServerName);
        pnlServer.add(m_tfServerName);
        pnlServer.add(lbPort);
        pnlServer.add(m_tfPort);
        return pnlServer;
    }

    /**
     * Set values to text components
     * 
     */
    public void initializeComponents() {
        String strServeurNamePort = null;
        String strLogin = null;
        String strPassword = null;
        String strRoot = null;
        String strUsers = null;
        String strBeamlines = null;
        String strProjectAcc = null;
        String strProjectGroups = null;

        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strAuthPath);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapServer");
                strServeurNamePort = deviceAttribute.extractString();
                if (strServeurNamePort == null) {
                    strServeurNamePort = "";
                }
            } catch (DevFailed exception) {
            	AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapServer",exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapAuthDN");
                strLogin = deviceAttribute.extractString();
                if (strLogin == null) {
                    strLogin = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapAuthDN ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapAuthPasswd");
                strPassword = deviceAttribute.extractString();
                if (strPassword == null) {
                    strPassword = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapAuthPasswd ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapRootDN");
                strRoot = deviceAttribute.extractString();
                if (strRoot == null) {
                    strRoot = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapRootDN ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapUsersDN");
                strUsers = deviceAttribute.extractString();
                if (strUsers == null) {
                    strUsers = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapUsersDN ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapBeamlinesDN");
                strBeamlines = deviceAttribute.extractString();
                if (strBeamlines == null) {
                    strBeamlines = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapBeamlinesDN ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapprojectAccountsDN");
                strProjectAcc = deviceAttribute.extractString();
                if (strProjectAcc == null) {
                    strProjectAcc = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapprojectAccountsDN ", exception);
            }
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("LdapprojectGroupsDN");
                strProjectGroups = deviceAttribute.extractString();
                if (strProjectGroups == null) {
                    strProjectGroups = "";
                }
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyReadAttributeErrorDetected(m_strAuthPath, "LdapprojectGroupsDN ", exception);
            }
        }

        if ((strServeurNamePort != null) && !strServeurNamePort.isEmpty()) {
            if (strServeurNamePort.contains(":")) {
                String strServeurName = strServeurNamePort.substring(0, strServeurNamePort.indexOf(":"));
                String strPort = strServeurNamePort.substring(strServeurNamePort.indexOf(":") + 1,
                        strServeurNamePort.length());
                m_tfPort.setText(strPort);
                m_tfServerName.setText(strServeurName);
            } else {
                m_tfPort.setText("0");
                m_tfServerName.setText(strServeurNamePort);
            }
        } else// ? seems impossible
        {
            m_tfPort.setText("0");
            m_tfServerName.setText("No current Value");
        }
        /* Get Tango attributes 's values*/
        if (strRoot.equals("")) {
            strRoot = "No current Value";
        }
        if (strUsers.equals("")) {
            strUsers = "No current Value";
        }
        if (strBeamlines.equals("")) {
            strBeamlines = "No current Value";
        }
        if (strProjectAcc.equals("")) {
            strProjectAcc = "No current Value";
        }
        if (strProjectGroups.equals("")) {
            strProjectGroups = "No current Value";
        }
        // set values to textfields
        m_tfLogin.setText(strLogin);
        m_pfPassword.setText(strPassword);
        m_tfBeamlines.setText(strBeamlines);
        m_tfProjectAccounts.setText(strProjectAcc);
        m_tfProjectGroups.setText(strProjectGroups);
        m_tfRoot.setText(strRoot);
        m_tfUsers.setText(strUsers);
    }

    /**
     * Get all data into
     * a tab for Argin in tango method "SetLDapParameters"
     * 
     * @return String[] : tabLDap
     */
    public String[] getTabLdapArgin() {
        String[] tabLDap = new String[8];
        tabLDap[0] = m_tfServerName.getText() + ":" + m_tfPort.getText();
        tabLDap[1] = m_tfRoot.getText();
        tabLDap[2] = m_tfUsers.getText();
        tabLDap[3] = m_tfBeamlines.getText();
        tabLDap[4] = m_tfProjectAccounts.getText();
        tabLDap[5] = m_tfProjectGroups.getText();
        tabLDap[6] = m_tfLogin.getText();
        tabLDap[7] = new String(m_pfPassword.getPassword());
        return tabLDap;
    }
}
