package fr.soleil.comete.bean.authentication.admin;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This is the main screen of the authentification administration Consol
 * @author MARECHAL
 *
 */

public class AuthAdminBean extends JDialog
{

    private static final long serialVersionUID = -2661869354121777484L;

    private static final String AUTH_SERVER_DEVICE_ATTR = "authServerDevice";


    /**
     * constructor
     * 
     * @param Jdialog dialog -> the dialog from which the dialog is displayed
     * @param String strTitle -> the String to display in the dialog's title bar
     * @param boolean bModal -> true for a modal dialog, false for one that allows other
     *        windows to be active at the same time
     * @param String strPathAuthDevice -> authentification device's path
     */
    public AuthAdminBean(JDialog parentWindow, String strTitle, boolean modal, String strPathAuthDevice) {
        super(parentWindow, strTitle, modal);
        final PnlAuthAdmin panel = new PnlAuthAdmin();
        panel.setModel(strPathAuthDevice);
        panel.start();
        this.setContentPane(panel);
        this.setResizable(false);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        // log off at the end of the application
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                panel.logoff();
                panel.stop();
            }
        });
        new DgLogOn(this, "Log on", true, panel, true);
    }


    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if ((input != null) && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            String authServerDeviceName = null;
            // read the auth server attribute from DataRecorder device
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute(AUTH_SERVER_DEVICE_ATTR);
                    authServerDeviceName = deviceAttribute.extractString();
                } catch (DevFailed exception) {
                    AuthServerMessageManager.notifyReadAttributeErrorDetected(deviceName, AUTH_SERVER_DEVICE_ATTR,
                            exception);
                }
            }

            new AuthAdminBean(null, "Auth Admin", true, authServerDeviceName);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
