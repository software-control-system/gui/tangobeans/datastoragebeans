package fr.soleil.comete.bean.authentication.admin;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class PnlAuthAdmin extends AbstractTangoBox {

    private static final long serialVersionUID = -6331209692708307318L;

    private static final String STATUS_ATTR = "Status";
    private static final String DATA_PATH_ATTR = "dataPath";
    private static final String INHOUSE_NAME_ATTR = "InhouseName";
    private static final String EXTERNAL_NAME_ATTR = "externalName";

    private final Icon LOG_ON_ICON;
    private final Icon LOG_OFF_ICON;

    private PnlConfiguration m_pnlConfiguration = null;

    private JButton m_btLogOn = null;
    private JButton m_btLogoff = null;
    private JButton m_btChangePass = null;

    private JDialog dialog = null;

    private TextTargetRedirector statusTarget;
    private TextTargetRedirector dataPathTarget;
    private TextTargetRedirector inHouseNameTarget;
    private TextTargetRedirector externalNameTarget;

    public PnlAuthAdmin() {
        LOG_ON_ICON = iconsManager.getIcon("AuthAdminBean.LogOn");
        LOG_OFF_ICON = iconsManager.getIcon("AuthAdminBean.LogOff");

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        m_pnlConfiguration = new PnlConfiguration(this);

        m_btLogOn = new JButton("Log on", LOG_ON_ICON);
        m_btLogOn.setToolTipText("Log On");
        m_btLogOn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new DgLogOn(dialog, "Log on", true, PnlAuthAdmin.this, false);
            }
        });

        m_btLogoff = new JButton("Log off", LOG_OFF_ICON);
        m_btLogoff.setToolTipText("Log off");
        m_btLogoff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                logoff();
            }
        });

        m_btChangePass = new JButton("ChangePassword");
        m_btChangePass.setToolTipText("Change Password");
        m_btChangePass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                DgChangePass dg = new DgChangePass(getParentWindow(), "Change password", true, getModel());
                dg.setLocationRelativeTo(getParentWindow());
                dg.setVisible(true);
            }
        });

        statusTarget = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                updatePnlConfiguration(data);
            }
        };

        dataPathTarget = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                m_pnlConfiguration.updateBaseDataDirectory(data);
            }
        };

        inHouseNameTarget = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                m_pnlConfiguration.updateIntProjDirectory(data);
            }
        };

        externalNameTarget = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                m_pnlConfiguration.updateExtProjDirectory(data);
            }
        };

        stringBox.setErrorText(dataPathTarget, "Impossible to read attribute DataPath.");
        stringBox.setErrorText(inHouseNameTarget, "Impossible to read attribute InhouseName.");
        stringBox.setErrorText(externalNameTarget, "Impossible to read attribute ExternalName.");

        m_btLogoff.setEnabled(false);
        m_btChangePass.setEnabled(false);
    }

    private void layoutComponents() {
        JToolBar toolBar = new JToolBar();
        toolBar.setRollover(false);
        toolBar.setFloatable(false);

        toolBar.add(m_btLogOn);
        toolBar.addSeparator();
        toolBar.add(m_btLogoff);
        toolBar.addSeparator();
        toolBar.add(m_btChangePass);

        setLayout(new BorderLayout(0, 5));
        add(toolBar, BorderLayout.NORTH);
        add(m_pnlConfiguration, BorderLayout.CENTER);
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            stringBox.connectWidgetNoMetaData(statusTarget, generateAttributeKey(STATUS_ATTR));
            stringBox.connectWidgetNoMetaData(dataPathTarget, generateAttributeKey(DATA_PATH_ATTR));
            stringBox.connectWidgetNoMetaData(inHouseNameTarget, generateAttributeKey(INHOUSE_NAME_ATTR));
            stringBox.connectWidgetNoMetaData(externalNameTarget, generateAttributeKey(EXTERNAL_NAME_ATTR));
        }
    }

    @Override
    protected void clearGUI() {
        cleanWidget(statusTarget);
        cleanWidget(dataPathTarget);
        cleanWidget(inHouseNameTarget);
        cleanWidget(externalNameTarget);

        m_pnlConfiguration.setSateButtons(false);
        m_btLogoff.setEnabled(false);
        m_btLogOn.setEnabled(false);
    }

    @Override
    protected void onConnectionError() {
        clearGUI();
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nop
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nop
    }

    public JButton getBtLogOn() {
        return m_btLogOn;
    }

    public JButton getBtLogOff() {
        return m_btLogoff;
    }

    public JButton getBtChangePassword() {
        return m_btChangePass;
    }

    public PnlConfiguration getPnlConfiguration() {
        return m_pnlConfiguration;
    }

    /**
     * Update panel configuration with status value
     * 
     */
    private void updatePnlConfiguration(String statusValue) {
        boolean superUserMode = statusValue.equalsIgnoreCase("Superuser mode");
        // this is to disable logon button when the device is down
        boolean deviceDead = statusValue.equalsIgnoreCase("unknown");

        m_pnlConfiguration.setSateButtons(superUserMode);
        m_btLogoff.setEnabled(superUserMode);
        m_btLogOn.setEnabled(!superUserMode && !deviceDead);
    }

    /**
     * Allows to logoff
     * 
     */
    public void logoff() {
        // do logoff only if the device is alive
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                proxy.command_inout("logoff");
            } catch (DevFailed exception) {
                AuthServerMessageManager.notifyExecuteCommandErrorDetected(getModel(), "logoff", exception);
            }
        }

        m_pnlConfiguration.setSateButtons(false);
        m_btLogOn.setEnabled(true);
        m_btLogoff.setEnabled(false);
    }

    /**
     * Return parent frame or null
     * 
     * @return JFrame
     */
    public JDialog getParentWindow() {
        if (dialog == null) {
            Window window = SwingUtilities.getWindowAncestor(this);
            if (window != null && window instanceof JDialog) {
                dialog = (JDialog) window;
                return dialog;
            }
            return null;
        } else {
            return dialog;
        }
    }

}
