package fr.soleil.comete.bean.technicaldata.light;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.light.util.TechnicalDataConfigEditDialog;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.IKey;

public class TechnicalDataLightBean extends AbstractTangoBox {

    private static final long serialVersionUID = -5766744928024617900L;

    private static final String LOAD_CONFIG_CMD = "LoadConfig";
    private JLabel currentConfigTitle;
    private Label currentConfigViewer;
    private JButton currentConfigEditButton;
    private static String m_strDsTechnicalPath = null;

    private String configList;
    private boolean displayMessageOnConnectionError;

    private TechnicalDataConfigEditDialog editDialog;

    public TechnicalDataLightBean() {
        super();
        displayMessageOnConnectionError = true;
        editDialog = null;
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        currentConfigTitle = new JLabel("Technical Data configuration:");
        currentConfigViewer = generateLabel();
        stringBox.setErrorText(currentConfigViewer, "Unable to read attribute currentConfig");
        currentConfigEditButton = new JButton("Change...");
        currentConfigEditButton.setToolTipText("Change the technical data configuration");
        currentConfigEditButton.setEnabled(false);
        currentConfigEditButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openEditDialog();
            }
        });
    }

    private void layoutComponents() {
        MigLayout migLayout = new MigLayout("", "[left]rel[grow,fill]rel[]", "[fill]");
        setLayout(migLayout);

        add(currentConfigTitle);
        add(currentConfigViewer);
        add(currentConfigEditButton);
    }
    
    @Override
    public void setModel(String model) {
        m_strDsTechnicalPath = model;
        super.setModel(model);
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            IKey currentConfig = generateAttributeKey("currentConfig");
            stringBox.connectWidget(currentConfigViewer, currentConfig);

            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute("configList");
                    configList = deviceAttribute.extractString();
                }
                catch (DevFailed exception) {
                	TechnicalDataMessageManager.notifyReadAttributeErrorDetected(getModel(),"configList",exception);
                }
            }

            boolean unavailable = ((configList == null) || (configList.trim().isEmpty()));
            currentConfigEditButton.setEnabled(!unavailable);
        }
    }

    @Override
    protected void onConnectionError() {
        if (isDisplayMessageOnConnectionError()) {
            showMessageDialog(this, "Failed to connect to TechnicalData device", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void clearGUI() {
        configList = null;
        cleanWidget(currentConfigViewer);
        currentConfigEditButton.setEnabled(false);
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected Label generateLabel() {
        Label label = new Label();
        label.setCometeFont(CometeFont.DEFAULT_FONT);
        label.setOpaque(false);
        stringBox.setColorEnabled(label, true);
        ((JComponent) label).setBorder(new LineBorder(new Color(0xB8CFE5)));
        return label;
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    public void setConfigEditButtonEnabled(boolean enabled) {
        currentConfigEditButton.setEnabled(enabled);
    }

    private void openEditDialog() {
        if (editDialog == null) {
            editDialog = new TechnicalDataConfigEditDialog(CometeUtils.getWindowForComponent(this),
                    "Change current config", true);
            editDialog.setHeaderText(resourceBundle.getString("TechnicalData.EditConfig.Header"));
            editDialog.setHeaderIcon(iconsManager.getIcon("TechnicalData.EditConfig"));
            editDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    editDialog.clearConfigSelectionModel();
                }
            });
        }

        // Setup Dialog
        editDialog.setConfigList(configList);
        configList = null;
        String currentConfig = currentConfigViewer.getText();
        if (stringBox.isOnError(currentConfigViewer)) {
            currentConfig = null;
        }
        editDialog.setCurrentConfig(currentConfig);
        currentConfig = null;

        // Connect Dialog
        editDialog.setConfigSelectionModel(generateAttributeKey("configList"),
                generateCommandKey("LoadConfig"), generateAttributeKey("currentConfig"));

        // Display Dialog
        editDialog.pack();
        editDialog.setLocationRelativeTo(this);
        editDialog.setVisible(true);
    }
    
    public static void loadTechnicalConfig(String configName) {

        if (configName != null) {
            // on value's changement -> load selected config
            // load configuration
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDsTechnicalPath);
            if (proxy != null) {
                try {
                    DeviceData argin = new DeviceData();
                    argin.insert(configName);
                    proxy.command_inout(LOAD_CONFIG_CMD, argin);
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDsTechnicalPath,
                            LOAD_CONFIG_CMD, exception);
                }
            }

        }
    }

    //    @Override
    //    public void start() {
    //        refreshGUI();
    //    }

    private static void createAndShowGUI(final String[] args) {
        TechnicalDataLightBean bean = new TechnicalDataLightBean();

        if (args != null && args.length > 0) {
            bean.setModel(args[0]);
            bean.start();
        }

        JFrame mainFrame = new JFrame();
        mainFrame.setTitle(bean.getClass().getSimpleName());
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setContentPane(bean);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    /**************************************************************************
     * Main
     * 
     * @param args[0] : path of the device ds_TechnicalData
     **************************************************************************/
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
