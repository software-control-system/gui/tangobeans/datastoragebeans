/**
 * 
 */
package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeMember;

/**
 * @author MARECHAL
 *         This class allows to set renderer for Node : TreeNodeMember
 *         which represents a Tango Device.
 *         If the device is alive, this node will have a gree icone
 *         Else the device will hab a red icone
 * 
 */
@SuppressWarnings("serial")
public class DeviceNodeCellRenderer extends DefaultTreeCellRenderer {
    private final ImageIcon m_imgOk = createImageIcon("/fr/soleil/comete/bean/technicaldata/full/icones/ledGreen.gif");
    private final ImageIcon m_imgKO = createImageIcon("/fr/soleil/comete/bean/technicaldata/full/icones/ledRed.gif");
    private final ImageIcon m_imgNULL = createImageIcon("/fr/soleil/comete/bean/technicaldata/full/icones/ledGrey.gif");
    private final ImageIcon m_imgDevice = createImageIcon("/fr/soleil/comete/bean/technicaldata/full/icones/device.gif");
    private final static LinkedList<String> deviceList = new LinkedList<String>();
    private final JTable treeTable;
    private static ThreadDeviceUpdater threadDeviceUpdater = null;
    private final static Map<String, Integer> recordableMap = new HashMap<String, Integer>();

    public DeviceNodeCellRenderer(final JTable treeTable) {
        this.treeTable = treeTable;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value instanceof TreeNodeMember) {
            TreeNodeMember tm = (TreeNodeMember) value;
            if (!leaf) {
                setIcon(m_imgDevice);
            } else {
                updateIconAccordingTreeNode(tm);
            }
        }

        this.setPreferredSize(new Dimension(500, 21));
        return this;
    }

    private void refreshUI() {
        if (treeTable != null) {
            treeTable.repaint();
        }
    }

    private void updateIconAccordingTreeNode(final TreeNodeMember tm) {
        // see JIRA EXPDATA-264 and EXPDATA-265
        if (tm != null) {
            String deviceName = tm.getDeviceName();
            Integer state = recordableMap.get(deviceName);
            if (state == null /*|| state == -1*/) {
                deviceList.add(deviceName);
                if (threadDeviceUpdater == null) {
                    threadDeviceUpdater = new ThreadDeviceUpdater();
                    threadDeviceUpdater.start();
                }
            } else {
                // all the device are treated
                updateIconAccordingState(state);
            }
        }
    }

    private class ThreadDeviceUpdater extends Thread {

        public ThreadDeviceUpdater() {
            super("ThreadDeviceUpdater");
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                while ((deviceList != null) && !deviceList.isEmpty()) {
                    String deviceName = deviceList.poll();
                    if (deviceName != null) {
                        final int state = PnlTechnicalData.getRecordableState(deviceName);
                        recordableMap.put(deviceName, state);
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                updateIconAccordingState(state);
                                refreshUI();
                            }
                        });
                    }
                }
            }
        }
    }

    private void updateIconAccordingState(final int state) {
        switch (state) {
            case 1:
                setIcon(m_imgOk);
                break;
            case 2:
                setIcon(m_imgKO);
                break;
            default:
                setIcon(m_imgNULL);
                break;
        }
    }

    public static boolean isValueEnable(Object value, boolean cellValue) {
        boolean enable = true;
        if (value instanceof TreeNodeMember) {
            TreeNodeMember tm = (TreeNodeMember) value;
            String deviceName = tm.getDeviceName();
            Integer state = recordableMap.get(deviceName);
            if (state != null && state == 2 && !cellValue) {
                enable = false;
            }
        }
        return enable;
    }

    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = DeviceNodeCellRenderer.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
