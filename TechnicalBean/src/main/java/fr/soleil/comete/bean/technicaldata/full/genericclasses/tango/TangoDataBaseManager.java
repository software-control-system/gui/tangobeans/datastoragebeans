package fr.soleil.comete.bean.technicaldata.full.genericclasses.tango;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.DatabaseTangoSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.GenericSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * @author MARECHAL
 *         This class allows to get domains, families, devices from a
 *         database tango.
 */
public class TangoDataBaseManager {

    private static final String SEPARATOR = "/";

    private static SortedMap<String, SortedMap<String, SortedSet<String>>> recordedDevicesTreeMap;
    private static SortedMap<String, SortedMap<String, SortedMap<String, SortedSet<String>>>> monitoredAttributesTreeMap;
    private static SortedMap<String, SortedSet<String>> deviceAttributesMap;

    private String archivingDevicePath = null;
    private static List<String> tangoSourceList = null;
    private static List<String> archivingSourceList = null;

    private TangoDataBaseManager() {
        // nop
    }

    private static class SingletonHolder {
        private static final TangoDataBaseManager INSTANCE = new TangoDataBaseManager();
    }

    static {
        long beginTime = System.currentTimeMillis();
        fillRecordedDevicesTreeMap();
        long endTime = System.currentTimeMillis();
        TechnicalDataMessageManager.notifyNewMessageDetected("RECORDED - duration = " + (endTime - beginTime), false);
    }

    private static void fillRecordedDevicesTreeMap() {

        List<String> devicesList = new LinkedList<String>();
        if (tangoSourceList == null) {
            try {
                ISourceDevice tangoSource = new DatabaseTangoSourceDevice(true, true);
                if (tangoSource != null) {
                    tangoSourceList = tangoSource.getSourceList();
                }

            } catch (SourceDeviceException e) {
                TechnicalDataMessageManager.notifyNewErrorDetected(e);
            }
        }
        if (tangoSourceList != null) {
            devicesList.addAll(tangoSourceList);
        }
        recordedDevicesTreeMap = devicesListToTreeMap(devicesList);

    }

    private static SortedMap<String, SortedMap<String, SortedSet<String>>> devicesListToTreeMap(
            final List<String> devicesList) {
        TreeMap<String, SortedMap<String, SortedSet<String>>> devicesTreeMap = new TreeMap<String, SortedMap<String, SortedSet<String>>>(
                String.CASE_INSENSITIVE_ORDER);

        for (String device : devicesList) {
            String[] split = device.split(SEPARATOR);
            if ((split != null) && (split.length > 2)) {
                String domain = split[0];
                SortedMap<String, SortedSet<String>> familiesMap = devicesTreeMap.get(domain);
                if (familiesMap == null) {
                    familiesMap = new TreeMap<String, SortedSet<String>>(String.CASE_INSENSITIVE_ORDER);
                    devicesTreeMap.put(domain, familiesMap);
                }

                String family = split[1];
                SortedSet<String> membersSet = familiesMap.get(family);
                if (membersSet == null) {
                    membersSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
                    familiesMap.put(family, membersSet);
                }

                String member = split[2];
                membersSet.add(member);
            } else {
                TechnicalDataMessageManager.notifyNewMessageDetected(("device=" + device),true);
            }
        }

        return devicesTreeMap;
    }

    private static SortedMap<String, SortedMap<String, SortedMap<String, SortedSet<String>>>> attributesListToTreeMap(
            final List<String> attributesList) {
        TreeMap<String, SortedMap<String, SortedMap<String, SortedSet<String>>>> attributesTreeMap = new TreeMap<String, SortedMap<String, SortedMap<String, SortedSet<String>>>>(
                String.CASE_INSENSITIVE_ORDER);

        for (String deviceAttribute : attributesList) {
            String[] split = deviceAttribute.split(SEPARATOR);

            String domain = split[0];
            SortedMap<String, SortedMap<String, SortedSet<String>>> familiesMap = attributesTreeMap.get(domain);
            if (familiesMap == null) {
                familiesMap = new TreeMap<String, SortedMap<String, SortedSet<String>>>(String.CASE_INSENSITIVE_ORDER);
                attributesTreeMap.put(domain, familiesMap);
            }

            String family = split[1];
            SortedMap<String, SortedSet<String>> membersMap = familiesMap.get(family);
            if (membersMap == null) {
                membersMap = new TreeMap<String, SortedSet<String>>(String.CASE_INSENSITIVE_ORDER);
                familiesMap.put(family, membersMap);
            }

            String member = split[2];
            SortedSet<String> attributesSet = membersMap.get(member);
            if (attributesSet == null) {
                attributesSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
                membersMap.put(member, attributesSet);
            }

            String attribute = split[3];
            attributesSet.add(attribute);
        }

        return attributesTreeMap;
    }

    /**
     * This class allows to obtain all domains from a
     * database tango.
     * 
     * @return Vector
     */
    public static String[] getDomains(final boolean isDeviceTree) {
        String[] result = null;
        if (isDeviceTree || (monitoredAttributesTreeMap == null) || monitoredAttributesTreeMap.isEmpty()) {
            Set<String> domainsSet = recordedDevicesTreeMap.keySet();
            result = domainsSet.toArray(new String[] {});
        } else {
            Set<String> domainsSet = monitoredAttributesTreeMap.keySet();
            result = domainsSet.toArray(new String[] {});
        }
        return result;
    }

    /**
     * This method allows to get a tab which contains all
     * families corresponding to a domain.
     * 
     * @param String strDomain
     * @param String strTANGo_HOST
     * @return String []
     */
    public static String[] getFamilies(String strDomain, final boolean isDeviceTree) {
        String[] result = null;
        if (isDeviceTree || (monitoredAttributesTreeMap == null) || monitoredAttributesTreeMap.isEmpty()) {
            SortedMap<String, SortedSet<String>> families = recordedDevicesTreeMap.get(strDomain);
            if (families != null) {
                Set<String> familiesSet = families.keySet();
                result = familiesSet.toArray(new String[] {});
            }
        } else {
            // if (monitoredAttributesTreeMap != null) {
            SortedMap<String, SortedMap<String, SortedSet<String>>> families = monitoredAttributesTreeMap
            .get(strDomain);
            if (families != null) {
                Set<String> familiesSet = families.keySet();
                result = familiesSet.toArray(new String[] {});
            }
        }
        return result;
    }

    /**
     * This method allows to get all Members of a domain and a family
     * 
     * @param String strDomain
     * @param String strFamily
     * @param String strTangoHost
     * @return
     */
    public static String[] getMembers(String strDomain, String strFamily, final boolean isDeviceTree) {
        String[] result = null;
        if (isDeviceTree || (monitoredAttributesTreeMap == null) || monitoredAttributesTreeMap.isEmpty()) {
            SortedMap<String, SortedSet<String>> families = recordedDevicesTreeMap.get(strDomain);
            if (families != null) {
                Set<String> members = families.get(strFamily);
                if (members != null) {
                    result = members.toArray(new String[] {});
                }
            }
        } else {
            SortedMap<String, SortedMap<String, SortedSet<String>>> families = monitoredAttributesTreeMap
            .get(strDomain);
            if (families != null) {
                SortedMap<String, SortedSet<String>> members = families.get(strFamily);
                if (members != null) {
                    Set<String> membersSet = members.keySet();
                    result = membersSet.toArray(new String[] {});
                }
            }
        }
        return result;
    }

    /**
     * This method allows to get all attributes for the specified device
     * 
     * @param String strDevice
     * @return String[] tab which contains all attributes
     * @throws DevFailed
     */
    public static String[] getAttributes(String strDevice) {
        String[] result = null;

        if ((monitoredAttributesTreeMap == null) || monitoredAttributesTreeMap.isEmpty()) {
            if (deviceAttributesMap == null) {
                deviceAttributesMap = new TreeMap<String, SortedSet<String>>(String.CASE_INSENSITIVE_ORDER);
            }
            SortedSet<String> attributes = deviceAttributesMap.get(strDevice);
            if (attributes == null) {
                if (TangoDeviceHelper.isDeviceRunning(strDevice)) {
                    SortedSet<String> attributeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(strDevice);
                    if (proxy != null) {
                        try {
                            result = proxy.get_attribute_list();
                            for (String attribute : result) {
                                attributeSet.add(attribute);
                            }
                            deviceAttributesMap.put(strDevice, attributeSet);
                        } catch (DevFailed e) {
                            String errorMessage = "Cannot read attribute list of device " + strDevice + " " + DevFailedUtils.toString(e);
                            TechnicalDataMessageManager.notifyNewErrorDetected(new Exception(errorMessage),false);
                        }
                    }
                }
            } else {
                result = attributes.toArray(new String[] {});
            }
        }

        else {
            String[] split = strDevice.split(SEPARATOR);
            String strDomain = split[0];
            String strFamily = split[1];
            String strMember = split[2];

            SortedMap<String, SortedMap<String, SortedSet<String>>> families = monitoredAttributesTreeMap
            .get(strDomain);
            if (families != null) {
                SortedMap<String, SortedSet<String>> members = families.get(strFamily);
                if (members != null) {
                    Set<String> attributes = members.get(strMember);
                    if (attributes != null) {
                        result = attributes.toArray(new String[] {});
                    }
                }
            }
        }

        return result;
    }

    public static TangoDataBaseManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void setArchivingDevicePath(String archivingDevicePath) {
        this.archivingDevicePath = archivingDevicePath;

        long beginTime = System.currentTimeMillis();
        fillMonitoredAttributesTreeMap();
        long endTime = System.currentTimeMillis();
        TechnicalDataMessageManager
        .notifyNewMessageDetected(("MONITORED - duration = " + (endTime - beginTime)), false);
    }

    /**
     * Monitored attributes tab only presents alive devices.
     * 
     * Try old fashion or new fashion, depending on the corresponding flag.
     * New fashion looks on the archiving device for the archived attributes list.
     * Old fashion gets all attributes for currently alive devices (exported and not dead).
     * 
     * If the property is not set or the archiving device is dead, try old fashion.
     * 
     * The filtering of forbidden devices is done for both cases.
     */
    private void fillMonitoredAttributesTreeMap() {
        List<String> attributesList = new LinkedList<String>();

        if ((archivingSourceList == null) && (archivingDevicePath != null) && !archivingDevicePath.isEmpty()) {
            try {
                ISourceDevice archivingSource = new GenericSourceDevice(archivingDevicePath + "/GetCurrentArchivedAtt");
                if (archivingSource != null) {
                    archivingSourceList = archivingSource.getSourceList();
                }
            } catch (SourceDeviceException e) {
                TechnicalDataMessageManager.notifyNewErrorDetected(e);
            }
        }
        if (archivingSourceList != null) {
            attributesList.addAll(archivingSourceList);
        }
        monitoredAttributesTreeMap = attributesListToTreeMap(attributesList);
    }
}
