package fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil;

import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.bean.technicaldata.full.genericclasses.tango.TangoDataBaseManager;

/**
 * This class allows to represent a tango's domain
 * 
 * @author MARECHAL
 * 
 */
public class TreeNodeDomain extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 7693735521829033185L;

    private String m_strName;
    private boolean m_bIsBuilt = false; // notice if this node has built its children

    // true for Recorded Devices tree, false for Monitored attributes
    private boolean isDeviceTree = false;

    /**
     * COnstructor
     * 
     * @param String : strDomain
     */
    public TreeNodeDomain(String strDomain, final boolean isDeviceTree) {
        super(strDomain);
        m_strName = strDomain;
        this.setAllowsChildren(true);
        this.isDeviceTree = isDeviceTree;
    }

    /**
     * Allows to get the domain's name
     * 
     * @return String
     */
    public String getDomainName() {
        return m_strName;
    }

    public void addFamilies() {
        String[] tabFamilies = null;
        tabFamilies = TangoDataBaseManager.getFamilies(m_strName, isDeviceTree);

        if (tabFamilies != null) {
            for (String familyName : tabFamilies) {
                TreeNodeFamily nodeFamily = new TreeNodeFamily(familyName, this, isDeviceTree);
                this.add(nodeFamily);
            }
        }
    }

    /**
     * @return bIsBuilt
     *         boolean which notice if the node has built its children
     */
    public boolean isBuilt() {
        return m_bIsBuilt;
    }

    /**
     * @param isBuilt the bIsBuilt to set
     */
    public void setBuilt(boolean isBuilt) {
        m_bIsBuilt = isBuilt;
    }

    /**
     * This method allows to get a family node
     * child of this domain node
     * 
     * @param String strFamilyName
     * @return TreeNodeFamily
     */
    public TreeNodeFamily getNodeFamily(String strFamilyName) {
        TreeNodeFamily familyNode = null;
        for (Enumeration<?> enumChildren = this.children(); enumChildren.hasMoreElements();) {
            familyNode = (TreeNodeFamily) enumChildren.nextElement();
            if (familyNode.toString().equals(strFamilyName)) {
                return familyNode;
            }
        }
        return familyNode;
    }

}
