package fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil;

import java.util.Enumeration;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author MARECHAL
 *         This class allows to manage Jtree
 * 
 */
public class TreeUtilities {
    /**
     * Allows to expand the whole tree
     * 
     * @param JTree tree
     */
    public static void expandAll(JTree tree) {
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.expandRow(i);
        }
    }

    /**
     * This method allows to expand the tree just on
     * one level
     * 
     * @param Jtree tree
     */
    public static void expandLevelOne(JTree tree) {
        // get root node
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getModel().getRoot();
        if (node.children() != DefaultMutableTreeNode.EMPTY_ENUMERATION) {
            if (node.children().hasMoreElements()) {
                DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) node.getFirstChild();
                while (childNode != null) {
                    TreePath tp1 = new TreePath(node.getPath());
                    tree.expandPath(tp1);
                    childNode = childNode.getNextNode();
                }
            }
        }
    }

    /**
     * Allows to collapse all rows
     * 
     * @param tree
     */
    public static void collapseAll(JTree tree) {
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.collapseRow(i);
        }
    }

    /**
     * Expands all the paths under a given node.
     * 
     * @param tree com.sun.java.swing.JTree
     * @param start com.sun.java.swing.tree.DefaultMutableTreeNode
     */
    public static void expandChildrenNodes(JTree tree, DefaultMutableTreeNode start) {
        if (start != null) {
            TreePath tp = new TreePath(start.getPath());
            if (tp != null) {
                tree.expandPath(tp);
            }
            for (Enumeration<?> children = start.children(); children.hasMoreElements();) {
                DefaultMutableTreeNode dtm = (DefaultMutableTreeNode) children.nextElement();
                TreePath tp1 = new TreePath(dtm.getPath());
                tree.expandPath(tp1);
                expandChildrenNodes(tree, dtm);
            }
            return;
        }
    }

    public static void expandChildrenNodesOneLevel(JTree tree, DefaultMutableTreeNode node) {
        if (node != null) {
            TreePath tp = new TreePath(node.getPath());
            if (tp != null) {
                tree.expandPath(tp);
            }
            for (Enumeration<?> children = node.children(); children.hasMoreElements();) {
                DefaultMutableTreeNode dtm = (DefaultMutableTreeNode) children.nextElement();
                TreePath tp1 = new TreePath(dtm.getPath());
                tree.expandPath(tp1);
            }
            return;
        }
    }

    public static boolean containsNode(JTree tree, String strNodeName) {
        if (tree != null) {
            Enumeration<?> e = ((DefaultMutableTreeNode) tree.getModel().getRoot()).preorderEnumeration();
            while (e.hasMoreElements()) {
                // get node
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
                if (node.toString().equals(strNodeName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method allows to get a node from its name in the tree specified
     * 
     * @param JTree tree
     * @param String strNodeName
     * @return DefaultMutableTreeNode : node or null
     */
    public static DefaultMutableTreeNode getANode(JTree tree, String strNodeName) {
        Enumeration<?> e = ((DefaultMutableTreeNode) tree.getModel().getRoot()).preorderEnumeration();
        while (e.hasMoreElements()) {
            // get node
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            if (node != null) {
                if (node.toString().equals(strNodeName)) {
                    return node;
                }
            }
        }
        return null;
    }

    /**
     * This method allows to check if the parent node
     * contains or not a child node whose name is strNodeName
     * 
     * @param DefaultMutableTreeNode parentNode
     * @param String strNodeName
     * @return DefaultMutableTreeNode or null
     */
    public static DefaultMutableTreeNode getChild(DefaultMutableTreeNode parentNode, String strNodeName) {
        Enumeration<?> enumChildren = parentNode.preorderEnumeration();
        if (enumChildren != DefaultMutableTreeNode.EMPTY_ENUMERATION) {
            while (enumChildren.hasMoreElements()) {
                DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) enumChildren.nextElement();
                if (childNode.toString() != null) {
                    if (childNode.toString().equals(strNodeName)) {
                        return childNode;
                    }
                }
            }
        }
        return null;
    }

    /**
     * This method only WORKS if node has PARENT NODE
     * This method allows to return a sibling node of the specified node in parameter
     * and whose name is equal to name specified in parameter
     * 
     * @param String strNodeName
     * @param DefaultMutableTreeNode node
     * @return DefaultMutableTreeNode siblingNode or null
     */
    public static DefaultMutableTreeNode getSiblingNode(String strNodeName, DefaultMutableTreeNode node) {
        if (node != null && strNodeName != null) {
            if (node.getSiblingCount() != 1) {
                DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
                if (parentNode != null) {
                    // iteration
                    for (int i = 0; i < parentNode.getChildCount(); i++) {
                        DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) parentNode.getChildAt(i);
                        if (childNode.toString().equals(strNodeName)) {
                            return childNode;
                        }
                    }
                }
            }
        }
        return null;
    }
}
