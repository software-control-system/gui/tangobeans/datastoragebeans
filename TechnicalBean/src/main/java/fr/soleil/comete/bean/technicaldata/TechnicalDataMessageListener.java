package fr.soleil.comete.bean.technicaldata;

public interface TechnicalDataMessageListener {

    public void newErrorDetected(Exception e, boolean displayToFront);
    
    public void newMessageDetected(String message, boolean displayToFront);

}
