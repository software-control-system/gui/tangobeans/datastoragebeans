package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Dimension;
import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class TechnicalDataFrame extends JDialog {

    /**
     * Constructor
     * 
     * @param String ds_TechnicalData path : strTechnicalDataPath
     * @param String ds_DataRecorder path : strDataRecorderPath
     * @param String strTangoHost
     */
    public TechnicalDataFrame(String strDataRecorderPath) {
        super();
        setModal(true);
        setTitle("Technical Data");

        PnlTechnicalData panel = new PnlTechnicalData();
        panel.setDataRecorderPath(strDataRecorderPath);

        setContentPane(panel);
        setSize(new Dimension(900, 400));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device DataRecorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String deviceName = null;
                if (args.length == 0) {
                    String input = JOptionPane.showInputDialog("DataRecorder device");
                    if ((input != null) && !input.isEmpty()) {
                        deviceName = input.trim();
                    }
                } else {
                    deviceName = args[0];
                }

                if (deviceName == null || deviceName.isEmpty()) {
                    deviceName = "storage/test/technicaldata.1";
                }

                TechnicalDataFrame technicalDataFrame = new TechnicalDataFrame(deviceName);
                technicalDataFrame.setVisible(true);
            }
        });
    }

}
