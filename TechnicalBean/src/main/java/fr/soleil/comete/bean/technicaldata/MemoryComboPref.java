package fr.soleil.comete.bean.technicaldata;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.text.Caret;
import javax.swing.text.Document;

/**
 * An editable {@link JComboBox} that can Memorize its content through {@link Preferences}
 * 
 * @author MARECHAL
 */
public class MemoryComboPref extends JComboBox<String> {

    private static final long serialVersionUID = -1249790523371635201L;

    // max memorized items
    public static final int MAX_MEM_LEN = 10;
    // string to be returned if preference doesn't exist
    public static final String NO_PREF = "no_pref";
    // separator used to separate in preference value each memorized value (value1;value2;value3)
    public static final String SEPARATOR = ";";

    private static final String SELECTED_VALUE = "selectedValue";

    private final Preferences prefs;
    private String strPrefName;
    private final String prefSelectedValueName;

    private String prefValue;

    // component editor
    private JTextField editorTextField;

    public MemoryComboPref(String strPrefName) {
        super();

        prefs = Preferences.userNodeForPackage(this.getClass());
        prefValue = null;
        editorTextField = null;

        // set preference name & selected value preference name
        this.strPrefName = strPrefName;
        this.prefSelectedValueName = strPrefName + "_" + SELECTED_VALUE;

        // read preference and add all memorized values in the combo
        loadMemorizedItems();

        // read preference and set the last selected value
        setSelectedValue();

        setEditable(true);
    }

    /**
     * add in combo all memorized items from the preference
     */
    public void loadMemorizedItems() {
        List<String> values = getListValues();
        if (null != values) {
            for (int i = 0; i < values.size(); i++) {
                // insert each item contained in the vector
                addItem(values.get(i));
            }
            // free resources
            values = null;
        }
    }

    /**
     * Read pref and set the last seleteced value
     */
    public void setSelectedValue() {
        String lastSelectedValue = prefs.get(prefSelectedValueName, NO_PREF);

        // test if preference exists
        if (!lastSelectedValue.equals(NO_PREF)) {
            if ((lastSelectedValue != null) && (lastSelectedValue.trim().length() > 0)) {
                // set selected item
                this.setSelectedItem(lastSelectedValue);
                // free resource
                lastSelectedValue = null;
            }
        }
    }

    /**
     * Return textfield assoctiated to this editable combo
     * 
     * @return JTextField
     */
    public JTextField getTextField() {
        if (editorTextField == null) {
            editorTextField = (JTextField) this.getEditor().getEditorComponent();
        }
        return editorTextField;
    }

    // -------------------------------------------------------------------------
    //
    // Getter & Setter for preference name
    //

    public String getPrefName() {
        return strPrefName;
    }

    public void setPrefName(String strPrefName) {
        this.strPrefName = strPrefName;
    }

    // -------------------------------------------------------------------------

    public Document getDocument() {
        return getTextField().getDocument();
    }

    public int getCaretPosition() {
        return getTextField().getCaretPosition();
    }

    public void setCaretPosition(int iPos) {
        getTextField().setCaretPosition(iPos);
    }

    public void replaceSelection(String s) {
        getTextField().replaceSelection(s);
    }

    public Caret getCaret() {
        return getTextField().getCaret();
    }

    public void setTextComponentColor(Color color) {
        getTextField().setBackground(color);
    }

    public String getSelectedText() {
        return getTextField().getSelectedText();
    }

    /**
     * Reads preference and returns a {@link List} which contains all memorized values
     * 
     * @return a {@link String} {@link List}
     */
    public List<String> getListValues() {
        List<String> vectValues = null;

        // get preference value
        prefValue = prefs.get(strPrefName, NO_PREF);
        // test if preference exists
        if (!prefValue.equals(NO_PREF)) {

            // get a list which contains each value memorized ( values are separated in preference
            // value with a separator)
            if (prefValue.trim().contains(";")) {
                vectValues = Arrays.asList(prefValue.split(SEPARATOR));
            } else if (prefValue.trim().length() > 0) {
                // preference contains only one value
                vectValues = new ArrayList<String>();
                vectValues.add(prefValue);
            }
        }
        return vectValues;
    }

    /**
     * Save combo items in the preferences
     */
    public void save() {
        // save all items in the combo in preference
        saveComboItemsInPreferences();
        // save selected item in preference
        saveSelectedItemInPreferences();
    }

    /**
     * Save all items form the combo in preference
     */
    public void saveComboItemsInPreferences() {
        DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) this.getModel();
        StringBuilder buffer = new StringBuilder();
        // create String which contains all items in the combo separated with the defined separator
        for (int i = 0; i < model.getSize(); i++) {
            buffer.insert(0, SEPARATOR).insert(0, model.getElementAt(i));
        }

        // save value in pref
        if (buffer != null) {
            prefs.put(strPrefName, buffer.toString());
        }

        // free resources
        model = null;
        buffer = null;
    }

    public void saveSelectedItemInPreferences() {
        if (getSelectedItem() != null) {
            prefs.put(prefSelectedValueName, getSelectedItem().toString());
        }
    }

    @Override
    public void addItem(String anObject) {
        DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) this.getModel();

        // test if the object doesn't already exist in the combo
        if (model.getIndexOf(anObject) == -1) {
            // add item at the top
            super.insertItemAt(anObject, 0);

            // if combo contains too much items -> remove the older
            if (getItemCount() > MAX_MEM_LEN) {
                removeItemAt(getItemCount() - 1);
            }
        }
    }

    @Override
    public void setSelectedItem(Object anObject) {
        if (anObject instanceof String) {
            DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) this.getModel();
            // test if the object doesn't already exist in the combo
            if (model.getIndexOf(anObject) == -1) {
                addItem((String) anObject);
            }
            super.setSelectedItem(anObject);
        }
    }
}
