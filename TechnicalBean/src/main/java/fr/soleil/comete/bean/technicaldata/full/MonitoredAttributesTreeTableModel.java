package fr.soleil.comete.bean.technicaldata.full;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.bean.technicaldata.full.genericclasses.treetable.AbstractTreeTableModel;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treetable.TreeTableModel;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeAttribute;

/**
 * RecordingDevicesTreeTableModel will be the model used by the JTreeTable.
 * It extends TreeTableModel
 * to add methods for getting inforamtion about the set of columns each
 * node in the TreeTableModel may have. Each column, like a column in
 * a TableModel, has a name and a type associated with it. Each node in
 * the TreeTableModel returns a value for the two followings columns "Pre"
 * and "Post" thanks to the getters associated to this node
 *
 * @author MARECHAL
 */
public class MonitoredAttributesTreeTableModel extends AbstractTreeTableModel implements TreeTableModel
{
    //Names of the columns.
    static protected String[]  m_tabNames = { "Devices","Saved"};

    // Types of the columns.
    static protected Class<?>[] m_tabTypes = { TreeTableModel.class, Boolean.class };
    /**
     * Constrcutor
     * @param DefaultMutableTreeNode rootNode
     */
    public MonitoredAttributesTreeTableModel(DefaultMutableTreeNode rootNode)
    {
        super(rootNode);
    }

    /**
     * Returns the number ofs available column.
     */
    @Override
    public int getColumnCount()
    {
        return m_tabNames.length;
    }

    /**
     * Returns the name for column number <code>iCol</code>.
     */
    @Override
    public String getColumnName(int iCol)
    {
        return m_tabNames[iCol];
    }

    /**
     * Returns the class for the particular column.
     */
    @Override
    public Class<?> getColumnClass(int column)
    {
        return m_tabTypes[column];
    }

    /**
     * Returns the value to be displayed for node <code>node</code>,
     * at column number <code>iCol</code>.
     * @param Object node
     * @param int iCol
     * @return Object
     */
    @Override
    public Object getValueAt(Object node, int iCol)
    {
        DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) node;
        switch(iCol)
        {
            //First column of the table respresent a node in the tree
            case 0:
            {
                return currentNode;
            }
            //the second column represented the value "Pre" associated to this node
            case 1:
            {
                if (currentNode instanceof TreeNodeAttribute)
                {
                    TreeNodeAttribute attributeNode = (TreeNodeAttribute) currentNode;
                    return new Boolean (attributeNode.isSaved());
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * Sets the value <code>aValue</code> for node <code>node</code>,
     * at column number <code>iCol</code>.
     * @param Object : aValue
     * @param Object : node
     * @param int : iCol
     */
    @Override
    public void setValueAt (Object aValue, Object node, int iCol)
    {
        if (aValue == null) {
            return;
        }
        //set value only for node TreeNodeMember
        if (node instanceof TreeNodeAttribute)
        {
            TreeNodeAttribute nodeAttribute = (TreeNodeAttribute) node;
            boolean bSelected = new Boolean(aValue.toString()).booleanValue();
            switch(iCol)
            {
                //the first column of the table represents the value "Pre" associated to the node
                case 1 :
                    nodeAttribute.setSaved(bSelected);
                    break;
            }
        }
    }

    /**
     * Returns the child of <code>parent</code> at index <code>index</code>
     * in the parent's
     * child array.  <code>parent</code> must be a node previously obtained
     * from this data source. This should not return <code>null</code>
     * if <code>index</code>
     * is a valid index for <code>parent</code> (that is <code>index >= 0 &&
     * index < getChildCount(parent</code>)).
     *
     * @param   parent  a node in the tree, obtained from this data source
     * @param int index
     * @return  the child of <code>parent</code> at index <code>index</code>
     */
    @Override
    public Object getChild(Object parent, int index)
    {
        Object result = ((DefaultMutableTreeNode)parent).getChildAt(index);
        return result;
    }

    /**
     * Returns the number of children of <code>parent</code>.
     * Returns 0 if the node
     * is a leaf or if it has no children.  <code>parent</code> must be a node
     * previously obtained from this data source.
     *
     * @param   parent  a node in the tree, obtained from this data source
     * @return  the number of children of the node <code>parent</code>
     */
    @Override
    public int getChildCount(Object parent)
    {
        return ((DefaultMutableTreeNode)parent).getChildCount();
    }


    /**
     * Allows to set if a column is editable or not
     * It is important that the column containing the tree nodes is editable.
     * Also if you are using checkboxes, then the second column should
     * also be editable in order to respond to user events.
     * @param Object node
     * @int column
     * @return boolean : which notice if the cell will be editable or not
     */
    @Override
    public boolean isCellEditable(Object node, int column)
    {
        return true;
    }
}
