package fr.soleil.comete.bean.technicaldata.full;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeAttribute;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeDomain;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeFamily;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeMember;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeUtilities;
import fr.soleil.comete.bean.technicaldata.full.util.Utils;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class MonitoredAttributesTreeTableController {

    private static final String ARCHIVED_ATTRIBUTES_ATTR = "archivedAttributes";

    private MonitoredAttributesTreeTableModel m_model = null;
    private TangoTreeTable m_treeTable = null;
    private String m_strPathTechnicalData = null;

    public MonitoredAttributesTreeTableController(MonitoredAttributesTreeTableModel model, TangoTreeTable treeTable,
            String strPathDeviceTechnicalData) {
        m_model = model;
        m_treeTable = treeTable;
        m_strPathTechnicalData = strPathDeviceTechnicalData;
    }

    /**
     * This method allows to build a vector <code> vect </code> which will contain selected devices in list
     * <code> iCol </code>
     * 
     * @param Vector vect
     * @param int iCol
     */
    private Vector<String> getVectorAttributesListToSave() {
        Vector<String> vect = new Vector<String>();
        Enumeration<?> enumChildren = ((DefaultMutableTreeNode) m_treeTable.getTree().getModel().getRoot())
        .preorderEnumeration();
        while (enumChildren.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) enumChildren.nextElement();
            if (node instanceof TreeNodeAttribute) {
                TreeNodeAttribute tnAttribute = (TreeNodeAttribute) node;
                // it must be selected in the threeTable in the col 1 (checkbox "saved")
                if (((TreeNodeAttribute) node).isSaved()) {
                    // attribute is selected -> add it in the vector
                    String strAttributeName = tnAttribute.getAttributePath();
                    vect.add(strAttributeName);
                }
            }
        }
        return vect;
    }

    /**
     * This method allows to write the attribute <code> strAttrName </code> with the selected devices in column
     * <code> iCol </code>
     * 
     * @param String strAttrName
     * @param int iCol
     * @return boolean : notice if method correctly happened
     */
    public boolean saveAttributesList() {
        boolean bStatus = false;

        // write the attribute if path is not null
        if (m_strPathTechnicalData != null) {
            // get Vector which contains attributes to save
            Vector<String> vect = getVectorAttributesListToSave();

            // get the attribute which contains all devices names for list 1 ( pre )
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strPathTechnicalData);
            //Jira : EXPDATA-404 if the config is empty write empty array on the device
            if(vect == null){
                vect = new Vector<String>();
            }
            if ((proxy != null)) {
                String[] arginValue = vect.toArray(new String[vect.size()] );
                try {
                    DeviceAttribute argin = new DeviceAttribute(ARCHIVED_ATTRIBUTES_ATTR);
                    argin.insert(arginValue);
                    proxy.write_attribute(argin);
                    bStatus = true;
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyWriteAttributeErrorDetected(m_strPathTechnicalData, ARCHIVED_ATTRIBUTES_ATTR,Arrays.toString(arginValue),exception,true);
                }
            }
        } else {
            bStatus = false;
        }
        return bStatus;
    }

    /*
     * This method allows to get all devices names saved in TechnicalData
     * @param String strAttribute : for example "DevicesList1" allows to get
     * all devices saved in the attribute "DevicesList1"
     */
    private Vector<?> getVectMonitoredAttributes() {
        Vector<String> result = null;

        String[] stringArray = null;
        // get the attribute which contains all devices names for list 1 ( pre )
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strPathTechnicalData);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(ARCHIVED_ATTRIBUTES_ATTR);
                stringArray = deviceAttribute.extractStringArray();
            } catch (DevFailed exception) {
                TechnicalDataMessageManager.notifyReadAttributeErrorDetected(m_strPathTechnicalData, ARCHIVED_ATTRIBUTES_ATTR,exception);
            }
        }
        if (stringArray != null) {
            result = new Vector<String>(stringArray.length);
            for (String value : stringArray) {
                result.add(value);
            }
        }
        return result;
    }

    /**
     * This method allows to read configuration on device TechnicalData
     * and build treetable in fonction of saved devices
     * 
     */
    public void buildTreeTable() {
        // Unselect all checkbox
        unbuildTree();

        // check the rights checkbox
        Vector<?> vect = buildColumn(getVectMonitoredAttributes());

        // collapse all
        collapseAll();
        
        // expand nodes which are selected
        expandSelectedNodes(vect);
    }

    /**
     * This method allows to set all nodes to uncheck (
     * for first and second column)
     * 
     */
    public void unbuildTree() {
        Enumeration<?> enumChildren = ((DefaultMutableTreeNode) m_treeTable.getTree().getModel().getRoot())
        .preorderEnumeration();
        while (enumChildren.hasMoreElements()) {
            // find all treenodemembers
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) enumChildren.nextElement();
            if (node instanceof TreeNodeAttribute) {
                TreeNodeAttribute tnAttribute = (TreeNodeAttribute) node;
                // uncheck all checkbox corresponding to this node
                m_model.setValueAt(new Boolean(false), tnAttribute, 1);
            }
        }
    }

    /**
     * This method allows to build column (check each cell)
     * 
     * @param int iCol : column number
     * @param Vector vectSavedDevices : vector which contains list of devices
     *            which must be checked in the specified column
     * @return Vector : vector which contains nodes with a selected device
     */
    private Vector<?> buildColumn(Vector<?> vectMonitoredAttributes) {
        Vector<TreeNodeAttribute> vectSelectedNodes = new Vector<TreeNodeAttribute>();
        // test if vector contains values or not
        if (vectMonitoredAttributes != null) {
            for (int i = 0; i < vectMonitoredAttributes.size(); i++) {
                // get the selected device in technicaldata
                String strAttributePath = vectMonitoredAttributes.elementAt(i).toString();
                // get vector which contains : domain, family, member
                Vector<?> vectAttribute = Utils.breakUp(strAttributePath, "/");
                if ((vectAttribute != null) && (vectAttribute.size() > 3)) {
                    // get device's domain
                    String strDeviceDomain = vectAttribute.elementAt(0).toString();
                    // get device domain node
                    TreeNodeDomain domainNode = (TreeNodeDomain) TreeUtilities.getANode(m_treeTable.getTree(),
                            strDeviceDomain);
                    TreeNodeMember memberNode = null;
                    if (domainNode != null) {
                        // test if node domain has been built
                        if (domainNode.isBuilt()) {
                            // get family node
                            TreeNodeFamily familyNode = domainNode.getNodeFamily(vectAttribute.elementAt(1).toString());
                            if (familyNode != null) {
                                if (familyNode.isBuilt()) {
                                    memberNode = familyNode.getNodeMember(vectAttribute.elementAt(2).toString());
                                    if (memberNode != null) {
                                        if (!memberNode.isBuilt()) {
                                            try {
                                                memberNode.addAttributes();
                                                memberNode.setBuilt(true);
                                            } catch (DevFailed e) {
                                                String errorMessage = "Cannot add member " + DevFailedUtils.toString(e);
                                                TechnicalDataMessageManager.notifyNewErrorDetected(new Exception(errorMessage), false);
                                            }
                                        }
                                    }
                                }

                                else {
                                    familyNode.addMembers(true);
                                    familyNode.setBuilt(true);

                                    memberNode = familyNode.getNodeMember(vectAttribute.elementAt(2).toString());
                                    if (memberNode != null) {
                                        if (!memberNode.isBuilt()) {
                                            try {
                                                memberNode.addAttributes();
                                                memberNode.setBuilt(true);
                                            } catch (DevFailed e) {
                                                String errorMessage = "Cannot add attribute " + DevFailedUtils.toString(e);
                                                TechnicalDataMessageManager.notifyNewErrorDetected(new Exception(errorMessage), false);
                                            }
                                        }
                                    }
                                }
                                // get the good attribute node and check it
                                String strAttribute = vectAttribute.elementAt(3).toString();
                                if (memberNode != null) {
                                    TreeNodeAttribute attributeNode = memberNode.getNodeAttribute(strAttribute);
                                    if (attributeNode != null) {
                                        // check this node
                                        // check corresponding column of this node
                                        m_model.setValueAt(new Boolean(true), attributeNode, 1);
                                        // add this node in vector selectedNodes
                                        vectSelectedNodes.add(attributeNode);
                                    }
                                }
                            }
                        }

                        /** Create all families */
                        /** Create all members of the good family */
                        /** Check the good member node */
                        else {
                            // add families to domain node
                            domainNode.addFamilies();
                            domainNode.setBuilt(true);
                            // get family name
                            String strDeviceFamily = vectAttribute.elementAt(1).toString();
                            // get family node which contains member node
                            TreeNodeFamily familyNode = domainNode.getNodeFamily(strDeviceFamily);
                            if (familyNode != null) {
                                // add members to family node
                                familyNode.addMembers(true);
                                familyNode.setBuilt(true);
                                // get member node
                                String strDeviceMember = vectAttribute.elementAt(2).toString();
                                memberNode = familyNode.getNodeMember(strDeviceMember);
                                if (memberNode != null) {

                                    TreeNodeAttribute attributeNode = memberNode.getNodeAttribute(vectAttribute
                                            .elementAt(3).toString());
                                    if (attributeNode != null) {
                                        // check this node
                                        // check corresponding column of this node
                                        m_model.setValueAt(new Boolean(true), attributeNode, 1);
                                        // add this node in vector selectedNodes
                                        vectSelectedNodes.add(attributeNode);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    TechnicalDataMessageManager.notifyNewMessageDetected("Error in attribute name=" + strAttributePath);
                }
            }
        }
        return vectSelectedNodes;
    }

    /**
     * This method allows to expand nodes which have a selected device
     * 
     * @param Vector vecSelectedNodes, vector which contains these nodes
     */
    private void expandSelectedNodes(Vector<?> vecSelectedNodes) {
        // iteration on this vector
        for (int i = 0; i < vecSelectedNodes.size(); i++) {
            TreeNodeAttribute tn = (TreeNodeAttribute) vecSelectedNodes.elementAt(i);
            TreePath tp = new TreePath(tn.getPath());
            m_treeTable.getTree().setSelectionPath(tp);
            m_treeTable.getTree().addSelectionPath(m_treeTable.getTree().getSelectionPath());
        }
    }

    private void collapseAll() {
        for (int i = m_treeTable.getTree().getRowCount(); i > 0; i--) {
            m_treeTable.getTree().collapseRow(i);
        }
    }

}
