package fr.soleil.comete.bean.technicaldata;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;

/**
 * A {@link JDialog} used for edition, and which has a white header at the top
 * 
 * @author girardot
 */
public abstract class HeaderedDialog extends JDialog {

    private static final long serialVersionUID = 5919616152134518223L;

    /**
     * Color used to display a {@link JTextComponent} as modified
     * 
     * @see #updateComponent(JTextComponent, boolean)
     */
    protected static final Color MODIFICATION_COLOR = Color.ORANGE;
    /**
     * Color used to display a {@link JTextComponent} as unmodified
     * 
     * @see #updateComponent(JTextComponent, boolean)
     */
    protected static final Color UNMODIFIED_COLOR = Color.WHITE;

    private JPanel headerPanel;
    private JLabel headerTextLabel;
    private JLabel headerIconLabel;

    private JPanel contentPane;
    /**
     * The main panel in which components are displayed
     */
    protected JPanel mainPanel;
    /**
     * The "close" button
     */
    protected JButton closeButton;

    /**
     * Default constructor
     * 
     * @param parent This {@link JDialog} owner
     * @param title This {@link JDialog} title
     * @param modal This {@link JDialog} modality
     */
    public HeaderedDialog(Window parent, String title, boolean modal) {
        super(parent, title);
        setModal(modal);
        initContentPane();
        setContentPane(contentPane);
    }

    // Header initialization
    private void initHeaderPanel() {
        headerPanel = new JPanel(new GridBagLayout());
        headerPanel.setBackground(Color.WHITE);

        headerTextLabel = new JLabel();
        headerTextLabel.setVerticalAlignment(JLabel.CENTER);

        headerIconLabel = new JLabel();
        headerIconLabel.setVerticalAlignment(JLabel.CENTER);

        GridBagConstraints headerTextConstraints = new GridBagConstraints();
        headerTextConstraints.fill = GridBagConstraints.BOTH;
        headerTextConstraints.gridx = 0;
        headerTextConstraints.gridy = 0;
        headerTextConstraints.weightx = 1;
        headerTextConstraints.weighty = 1;
        headerTextConstraints.insets = new Insets(5, 5, 10, 5);
        headerPanel.add(headerTextLabel, headerTextConstraints);
        GridBagConstraints headerIconConstraints = new GridBagConstraints();
        headerIconConstraints.fill = GridBagConstraints.BOTH;
        headerIconConstraints.gridx = 1;
        headerIconConstraints.gridy = 0;
        headerIconConstraints.weightx = 0;
        headerIconConstraints.weighty = 1;
        headerIconConstraints.insets = new Insets(5, 0, 10, 5);
        headerPanel.add(headerIconLabel, headerIconConstraints);
    }

    // Dialog content initialization
    private void initContentPane() {
        initHeaderPanel();
        initCloseButton();
        initMainPanel();
        contentPane = new JPanel(new BorderLayout());
        contentPane.add(headerPanel, BorderLayout.NORTH);
        contentPane.add(mainPanel, BorderLayout.CENTER);
        contentPane.add(generateBottomPanel(), BorderLayout.SOUTH);
    }

    private void initMainPanel() {
        mainPanel = new JPanel();
        initAndAddOtherComponentsInMainPanel();
    }

    // "close" button initialization
    private void initCloseButton() {
        closeButton = new JButton("Close");
        closeButton.addActionListener(generateCloseButtonListener());
    }

    /**
     * This method can be overridden if one want to add a {@link JPanel} under the main panel. By
     * default, it returns an empty {@link JPanel}
     * 
     * @return a {@link JPanel}.
     */
    protected JPanel generateBottomPanel() {
        return new JPanel();
    }

    /**
     * Generates the {@link ActionListener} of the "close" button
     * 
     * @return An {@link ActionListener}
     */
    protected ActionListener generateCloseButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        };
    }

    /**
     * Sets the {@link Icon} displayed in header
     * 
     * @param icon The {@link Icon} to set
     */
    public void setHeaderIcon(Icon icon) {
        headerIconLabel.setIcon(icon);
    }

    /**
     * Sets the text displayed in header
     * 
     * @param text The text to set
     */
    public final void setHeaderText(String text) {
        if (text == null) {
            text = "";
        }
        headerTextLabel.setText(text);
    }

    /**
     * This method must be implemented to initialize and add displayed components in main panel
     */
    protected abstract void initAndAddOtherComponentsInMainPanel();

    /**
     * This method allows to color a {@link JTextComponent} in orange if it was modified.
     * 
     * @param comp The {@link JTextComponent} to display as modified or unmodified
     * @param modificationOccurred a boolean. <code>TRUE</code> to display the {@link JTextComponent} as modified,
     *            <code>FALSE</code> to display the {@link JTextComponent} as not modified
     */
    protected void updateComponent(JTextComponent comp, boolean modificationOccurred) {
        Color colorBackground;
        if (modificationOccurred) {
            colorBackground = MODIFICATION_COLOR;
        } else {
            colorBackground = UNMODIFIED_COLOR;
        }
        comp.setBackground(colorBackground);
    }

    /**
     * Closes this {@link Window}, warning its listeners
     */
    protected final void close() {
        WindowEvent event = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        for (WindowListener listener : getWindowListeners()) {
            listener.windowClosing(event);
        }
        setVisible(false);
        event = new WindowEvent(this, WindowEvent.WINDOW_CLOSED);
        for (WindowListener listener : getWindowListeners()) {
            listener.windowClosed(event);
        }
        event = null;
    }

}

