package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.comete.bean.technicaldata.full.genericclasses.treetable.JTreeTable;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treetable.TreeTableModel;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class allows to create a simple JTreeTable component,
 * by using a JTree as a renderer (and editor) for the cells in a
 * particular column in the JTable.
 * Create inner class to define renderer and editor for values of boolean type
 * 
 * @author Maréchal Céline
 */
public class TangoTreeTable extends JTreeTable {

    private static final long serialVersionUID = -5778169416781368661L;

    private PnlTechnicalData m_pnl = null;

    /**
     * Constructor
     * 
     * @param TreeTableModel treeTableModel
     */
    public TangoTreeTable(TreeTableModel treeTableModel, PnlTechnicalData pnl) {
        super(treeTableModel);
        m_pnl = pnl;
        // Install chekbox editor renderer and editor
        setDefaultEditor(Boolean.class, new BooleanNewEditor());
        setDefaultRenderer(Boolean.class, new BooleanNewRenderer());
    }

    /**
     * Inner class which set a Cell Editor for type Boolean
     * 
     * @author MARECHAL
     * 
     */
    public class BooleanNewEditor extends DefaultCellEditor {

        private static final long serialVersionUID = -6794121775679386329L;

        /**
         * Default constructor
         * 
         */
        public BooleanNewEditor() {
            super(new JCheckBox());
            JCheckBox checkBox = (JCheckBox) getComponent();
            checkBox.setHorizontalAlignment(JCheckBox.CENTER);
            // add listener
            checkBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    // enable button save on changemement
                    m_pnl.enableButtonsSave(true);
                }
            });
        }
    }

    /**
     * Inner CLass which defines a renderer for checkbox
     * 
     * @author MARECHAL
     * 
     */
    public class BooleanNewRenderer extends DefaultTableCellRenderer {

        private static final long serialVersionUID = 8986331755680857339L;

        protected Component c;

        /**
         * Default Constructor
         * 
         */
        public BooleanNewRenderer() {
            super();
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int iRow, int iCol) {
            // if value is not null
            // component is a checkbox
            if (value != null) {
                boolean boolValue = ((Boolean) value).booleanValue();
                c = new JCheckBox(ObjectUtils.EMPTY_STRING, boolValue);
                ((JCheckBox) c).setHorizontalAlignment(JCheckBox.CENTER);
                ((JCheckBox) c).setBackground(table.getBackground());
                c.setVisible(true);
                // Get the device (colonne 0) value of the table
                boolean valueEnable = DeviceNodeCellRenderer.isValueEnable(table.getValueAt(iRow, 0), boolValue);

                ((JCheckBox) c).setEnabled(valueEnable);
            }
            // if value is null, no checkbox must be appear
            // that's why -> the component will be a label
            else {
                c = new JLabel(ObjectUtils.EMPTY_STRING);
                c.setVisible(false);
            }
            return c;
        }
    }
}
