package fr.soleil.comete.bean.technicaldata.full.util;

import java.util.Vector;

public class Utils {

    /**
     * Replace version from GenericClasses
     * 
     * @param strPath
     * @param strSeparator
     * @return
     */
    public static Vector<String> breakUp(String strPath, String strSeparator) {
        Vector<String> vctNodesPaths = new Vector<String>();

        if (strPath != null) {
            String[] split = strPath.split(strSeparator);
            for (String part : split) {
                vctNodesPaths.add(part);
            }
        }

        return vctNodesPaths;
    }

    public static boolean isAlphanumericUnderscore(final String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if ((Character.isLetterOrDigit(str.charAt(i)) == false) && (str.charAt(i) != '_') && (str.charAt(i) != '-')) {
                return false;
            }
        }
        return true;
    }

}
