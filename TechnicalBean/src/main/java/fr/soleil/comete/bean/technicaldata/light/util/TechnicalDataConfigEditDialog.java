package fr.soleil.comete.bean.technicaldata.light.util;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.bean.technicaldata.HeaderedDialog;
import fr.soleil.comete.bean.technicaldata.light.TechnicalDataLightBean;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.data.service.IKey;

/**
 * This dialog allows to show and modify value currentConfig.
 * 
 * @author MARECHAL
 * @author girardot
 */
public class TechnicalDataConfigEditDialog extends HeaderedDialog {

    private static final long serialVersionUID = -4091492115556674691L;

    private String currentConfig;
    private String configList; // configs separated with ","

    private JLabel configSelectionTitle;
    private ConfigComboBox configSelectionComboBox;

    private JPanel buttonPanel;
    //private JButton deviceSelectionButton;

    private final StringScalarBox stringBox;
    

    public TechnicalDataConfigEditDialog(Window parent, String title, boolean modal) {
        super(parent, title, modal);
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
    }

    public String getConfigList() {
        return configList;
    }

    public void setConfigList(String configList) {
        this.configList = configList;
    }

    public String getCurrentConfig() {
        return currentConfig;
    }

    public void setCurrentConfig(String currentConfig) {
        this.currentConfig = currentConfig;
    }
    
    @Override
    protected void initAndAddOtherComponentsInMainPanel() {
//        deviceSelectionButton = new JButton("Choose devices to save...");
//        deviceSelectionButton.setEnabled(false);
//        deviceSelectionButton.setToolTipText("Not yet available");

        configSelectionTitle = new JLabel("Current config:");
        configSelectionComboBox = new ConfigComboBox();

        buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        buttonPanel.add(Box.createGlue(), glueConstraints);
        
//        GridBagConstraints deviceSelectionConstraints = new GridBagConstraints();
//        deviceSelectionConstraints.fill = GridBagConstraints.VERTICAL;
//        deviceSelectionConstraints.gridx = 1;
//        deviceSelectionConstraints.gridy = 0;
//        deviceSelectionConstraints.weightx = 0;
//        deviceSelectionConstraints.weighty = 1;
//        deviceSelectionConstraints.insets = new Insets(0, 0, 0, 5);
//        buttonPanel.add(deviceSelectionButton, deviceSelectionConstraints);
        
        GridBagConstraints closeConstraints = new GridBagConstraints();
        closeConstraints.fill = GridBagConstraints.VERTICAL;
        closeConstraints.gridx = 2;
        closeConstraints.gridy = 0;
        closeConstraints.weightx = 0;
        closeConstraints.weighty = 1;
        buttonPanel.add(closeButton, closeConstraints);

        mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.BOTH;
        titleConstraints.gridx = 0;
        titleConstraints.gridy = 0;
        titleConstraints.weightx = 0;
        titleConstraints.weighty = 0;
        titleConstraints.insets = new Insets(5, 5, 10, 5);
        mainPanel.add(configSelectionTitle, titleConstraints);
        GridBagConstraints comboConstraints = new GridBagConstraints();
        comboConstraints.fill = GridBagConstraints.BOTH;
        comboConstraints.gridx = 1;
        comboConstraints.gridy = 0;
        comboConstraints.weightx = 1;
        comboConstraints.weighty = 0;
        comboConstraints.insets = new Insets(5, 0, 10, 5);
        mainPanel.add(configSelectionComboBox, comboConstraints);

        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.BOTH;
        buttonConstraints.gridx = 1;
        buttonConstraints.gridy = 1;
        buttonConstraints.weightx = 1;
        buttonConstraints.weighty = 0;
        buttonConstraints.insets = new Insets(0, 0, 5, 5);
        mainPanel.add(buttonPanel, buttonConstraints);

        final ComboBox comboBox = configSelectionComboBox.getComboBox();
        ItemListener listener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String selectedValue = (String) comboBox.getSelectedValue();
                    TechnicalDataLightBean.loadTechnicalConfig(selectedValue);
                }
            }
        };
        comboBox.addItemListener(listener);      
    }

    /**
     * Connects the configuration selection combobox to its dao
     * 
     * @param factory The name of the dao factory
     * @param keys The keys that shall be used to recover the dao
     */
    public void setConfigSelectionModel(IKey... keys) {
        stringBox.disconnectWidgetFromAll(configSelectionComboBox);
        for (IKey key : keys) {
            stringBox.connectWidget(configSelectionComboBox, key);
        }
    }

    /**
     * Disconnects the configuration selection combobox from its dao
     * 
     * @param The name of the dao factory
     */
    public void clearConfigSelectionModel() {
        stringBox.disconnectWidgetFromAll(configSelectionComboBox);
        configSelectionComboBox.cleanCombo();
    }

}
