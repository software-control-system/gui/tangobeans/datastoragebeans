package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeSelectionModel;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.tango.TangoDataBaseManager;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeDomain;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeFamily;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeMember;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

@SuppressWarnings("serial")
public class SpMonitoredAttributes extends JScrollPane {

    private static final String ARCHIVING_DEVICE_PROP = "archivingDevice";

    private DefaultMutableTreeNode m_rootNode;
    private static TangoTreeTable m_ttTango = null;
    private MonitoredAttributesTreeTableModel m_model = null;
    private String m_strTechnicalDataPath = null;
    private static PnlTechnicalData m_pnl;

    private String archivingDeviceName = null;

    public void setDevicePath(PnlTechnicalData pnl) {
        m_pnl = pnl;
        m_strTechnicalDataPath = pnl.getPathTechnicalData();

        String dataRecorderPath = pnl.getDataRecorderPath();
        DeviceProxy deviceProxy = TangoDeviceHelper
        .getDeviceProxy(dataRecorderPath);
        try {
            DbDatum property = deviceProxy.get_property(ARCHIVING_DEVICE_PROP);
            archivingDeviceName = property.extractString();
        } catch (DevFailed e) {
            TechnicalDataMessageManager.notifyReadAttributeErrorDetected(
                    dataRecorderPath, ARCHIVING_DEVICE_PROP, e);
        }
        // archivingDeviceName can be null
        TangoDataBaseManager.getInstance().setArchivingDevicePath(
                archivingDeviceName);

        this.setViewportView(getTreeTableTangoDevices());
        this.getViewport().setBackground(Color.WHITE);
        this.setMinimumSize(new Dimension(400, 200));
    }

    public TangoTreeTable getTreeTableTangoDevices() {
        if (m_ttTango == null) {
            m_ttTango = new TangoTreeTable(getTreeTableModel(), m_pnl);
            MonitoredAttributesTreeTableModel model = (MonitoredAttributesTreeTableModel) m_ttTango
            .getTree().getModel();
            model.setAsksAllowsChildren(true);
            m_ttTango.getTree().setCellRenderer(new DeviceNodeCellRenderer(m_ttTango));
            // allow only single selection
            DefaultTreeSelectionModel selectionModel = (DefaultTreeSelectionModel) m_ttTango
            .getTree().getSelectionModel();
            selectionModel
            .setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            m_ttTango.getSelectionModel().setSelectionMode(
                    ListSelectionModel.SINGLE_SELECTION);

            // add listener on tree's expansion (build nodes)
            addListerner();

            m_ttTango.setVisible(true);
            m_ttTango.getTableHeader().setReorderingAllowed(false);
            MonitoredAttributesTreeTableController controller = new MonitoredAttributesTreeTableController(
                    m_model, m_ttTango, m_strTechnicalDataPath);
            controller.buildTreeTable();
        }
        return m_ttTango;
    }

    /**
     * This method allows to show all nodes which represents tango's domains
     * 
     */
    protected MonitoredAttributesTreeTableModel getTreeTableModel() {
        if (m_model == null) {
            m_rootNode = new DefaultMutableTreeNode("");
            try {
                // add treeNodeDomain
                addDomains();
            } catch (DevFailed e) {
                String errorMessage = "Cannot read domains "
                    + DevFailedUtils.toString(e);
                TechnicalDataMessageManager
                .notifyNewErrorDetected(new Exception(errorMessage));
            }

            // create TreeTableModel
            m_model = new MonitoredAttributesTreeTableModel(m_rootNode);
        }
        return m_model;
    }

    /**
     * Add tree expanstion listener to tree
     * 
     */
    public void addListerner() {
        m_ttTango.getTree().addTreeExpansionListener(
                new TreeExpansionListener() {

                    @Override
                    public void treeCollapsed(TreeExpansionEvent event) {
                        // do nothing
                    }

                    @Override
                    public void treeExpanded(TreeExpansionEvent event) {
                        // add node families or member
                        if (event.getPath().getLastPathComponent() != null) {
                            if (event.getPath().getLastPathComponent() instanceof TreeNodeDomain) {
                                // add family nodes
                                TreeNodeDomain domainNode = (TreeNodeDomain) event
                                .getPath().getLastPathComponent();
                                if (!domainNode.isBuilt()) {
                                    domainNode.addFamilies();
                                    domainNode.setBuilt(true);
                                    m_model.reload(domainNode);
                                }
                            }

                            else if (event.getPath().getLastPathComponent() instanceof TreeNodeFamily) {
                                // add member nodes
                                TreeNodeFamily familyNode = (TreeNodeFamily) event
                                .getPath().getLastPathComponent();
                                if (!familyNode.isBuilt()) {
                                    // boolean is true because node nodeMember
                                    // allow children : attribute node
                                    familyNode.addMembers(true);
                                    familyNode.setBuilt(true);
                                    m_model.reload(familyNode);
                                }
                            }

                            else if (event.getPath().getLastPathComponent() instanceof TreeNodeMember) {
                                // add attribute nodes
                                TreeNodeMember memberNode = (TreeNodeMember) event
                                .getPath().getLastPathComponent();
                                if (!memberNode.isBuilt()) {
                                    try {
                                        memberNode.addAttributes();
                                        memberNode.setBuilt(true);
                                    } catch (DevFailed e) {
                                        String errorMessage = "Device "
                                            + memberNode.getDeviceName()
                                            + " is not alive "
                                            + DevFailedUtils.toString(e);
                                        memberNode.setBuilt(false);
                                        TechnicalDataMessageManager
                                        .notifyNewErrorDetected(new Exception(
                                                errorMessage));
                                    }
                                    m_model.reload(memberNode);
                                }
                            }
                        }
                    }
                });
    }

    /**
     * this method allows to add domains to the tree
     * 
     * @throws DevFailed
     */
    private void addDomains() throws DevFailed {
        final boolean isDeviceTree = false;

        // get domains from tango database
        String[] tabDomains = TangoDataBaseManager.getDomains(isDeviceTree);

        for (String domain : tabDomains) {
            // create node TreeNodeDomain
            TreeNodeDomain tnDomain = new TreeNodeDomain(domain, isDeviceTree);
            // add this node to rootNode
            m_rootNode.add(tnDomain);
        }
    }
}
