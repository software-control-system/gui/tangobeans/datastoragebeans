package fr.soleil.comete.bean.technicaldata.full;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeDomain;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeFamily;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeMember;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeUtilities;
import fr.soleil.comete.bean.technicaldata.full.util.Utils;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class RecordingDevicesTreeTableController
{
    private static final String PRE_RECORDING_DEVICES_ATTR = "preRecordingDevices";
    private static final String POST_RECORDING_DEVICES_ATTR = "postRecordingDevices";

    private RecordingDevicesTreeTableModel m_model = null;
    private TangoTreeTable m_treeTable = null;
    private String m_strPathTechnicalData = null;

    public RecordingDevicesTreeTableController(RecordingDevicesTreeTableModel model,
            TangoTreeTable treeTable,
            String strPathDeviceTechnicalData)
    {
        m_model = model;
        m_treeTable = treeTable;
        m_strPathTechnicalData = strPathDeviceTechnicalData;
    }

    /**
     * This method allows to get all devices' name which
     * are selected in colomn <code> int iCol </code>
     * in order to be saving in attribute
     * "deviceList1" or "deviceList2", in device TechnicalData
     * @param int iCol
     * @return Vector vect
     */
    private Vector<String> getVectorDevicesListToSave(int iCol)
    {
        return buildVectorDevicesListToSave(iCol);
    }


    /**
     * This method allows to build a vector <code> vect </code>
     * which will contain selected devices in list
     * <code> iCol </code>
     * @param Vector vect
     * @param int iCol
     */
    private Vector<String> buildVectorDevicesListToSave(int iCol)
    {
        Vector<String> vect = new Vector<String>();

        Enumeration<?> enumChildren = ((DefaultMutableTreeNode) m_treeTable.getTree().getModel().getRoot())
        .preorderEnumeration();
        while (enumChildren.hasMoreElements())
        {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) enumChildren.nextElement();
            if (node instanceof TreeNodeMember)
            {
                TreeNodeMember tnMember = (TreeNodeMember) node;
                //verify if the node belongs to the list iList
                // it must be selected in the threeTable in the col iList
                Boolean value = (Boolean) m_model.getValueAt(tnMember, iCol);
                if (value.booleanValue())
                {
                    //device is selected -> add it in the vector
                    String strDeviceName = tnMember.getDeviceName();
                    vect.add(strDeviceName);
                }
            }
        }

        return vect;
    }

    /**
     * This method allows to write the attribute <code> strAttrName </code>
     * with the selected devices in column <code> iCol </code>
     * @param String strAttrName
     * @param int iCol
     * @return boolean : notice if method correctly happened
     */
    private boolean saveAttrDeviceList(String strAttrName, int iCol)
    {
        boolean bStatus = false;

        // write the attribute if path is not null
        if (m_strPathTechnicalData != null) {
            //get Vector which contains Devices to save
            Vector<String> vect = getVectorDevicesListToSave(iCol);
            // get the attribute which contains all devices names for list 1 ( pre )
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strPathTechnicalData);
            if ((proxy != null) && (vect != null) && !vect.isEmpty()) {
                String[] arginVect = vect.toArray(new String[vect.size()]);
                try {
                    DeviceAttribute argin = new DeviceAttribute(strAttrName);
                    argin.insert(arginVect);
                    proxy.write_attribute(argin);
                    bStatus = true;
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyWriteAttributeErrorDetected(m_strPathTechnicalData, strAttrName,Arrays.toString(arginVect),exception,true);
                }
            }
        } else {
            bStatus = false;
        }
        return bStatus;
    }

    public boolean saveAttributes()
    {
        boolean result = saveAttrDeviceList(PRE_RECORDING_DEVICES_ATTR, 1);
        result &= saveAttrDeviceList(POST_RECORDING_DEVICES_ATTR, 2);
        return result;
    }

    /*
     * This method allows to get all devices names saved in TechnicalData
     * @param String strAttribute : for example "DevicesList1" allows to get
     * all devices saved in the attribute "DevicesList1"
     */
    private Vector<?> getVectorSavedDevicesList(String strAttribute)
    {
        Vector<String> result = null;

        String[] stringArray = null;
        //get the attribute which contains all devices names for list 1 ( pre )
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strPathTechnicalData);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(strAttribute);
                stringArray = deviceAttribute.extractStringArray();
            } catch (DevFailed exception) {
                TechnicalDataMessageManager.notifyReadAttributeErrorDetected(m_strPathTechnicalData, strAttribute,exception);
            }
        }
        if (stringArray != null) {
            result = new Vector<String>(stringArray.length);
            for (String value : stringArray) {
                result.add(value);
            }
        }
        return result;
    }

    /**
     * This method allows to read configuration on device TechnicalData
     * and build treetable in fonction of saved devices
     *
     */
    public void buildTreeTable()
    {
        //Unselect all checkbox
        unbuildTree();
        //check the rights checkbox
        Vector<?> vect1 = buildColumn(1, getVectorSavedDevicesList(PRE_RECORDING_DEVICES_ATTR));
        Vector<?> vect2 = buildColumn(2, getVectorSavedDevicesList(POST_RECORDING_DEVICES_ATTR));

        //collapse all
        collapseAll();

        //expand nodes which are selected
        expandSelectedNodes(vect1);
        expandSelectedNodes(vect2);
    }

    /**
     * This method allows to set all nodes to uncheck (
     * for first and second column)
     *
     */
    public void unbuildTree()
    {
        Enumeration<?> enumChildren = ((DefaultMutableTreeNode) m_treeTable.getTree().getModel().getRoot())
        .preorderEnumeration();
        while (enumChildren.hasMoreElements())
        {
            //find all treenodemembers
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) enumChildren.nextElement();
            if (node instanceof TreeNodeMember)
            {
                TreeNodeMember tnMember = (TreeNodeMember) node;
                //uncheck all checkbox corresponding to this node
                m_model.setValueAt(new Boolean(false), tnMember, 1);
                m_model.setValueAt(new Boolean(false), tnMember, 2);
            }
        }
    }

    /**
     * This method allows to build column (check each cell)
     * @param int iCol : column number
     * @param Vector vectSavedDevices : vector which contains list of devices
     *			which must be checked in the specified column
     * @return Vector : vector which contains nodes with a selected device
     */
    private Vector<?> buildColumn(int iCol, Vector<?> vectSavedDevices)
    {
        Vector<TreeNodeMember> vectSelectedNodes = new Vector<TreeNodeMember>();
        //test if vector contains values or not
        if(vectSavedDevices != null)
        {
            for (int i=0 ; i<vectSavedDevices.size() ; i++)
            {
                //get the selected device in technicaldata
                String strDeviceName = vectSavedDevices.elementAt(i).toString();
                //get vector which contains : domain, family, member
                Vector<?> vectDevice = Utils.breakUp(strDeviceName, "/");
                //get device's domain
                String strDeviceDomain = vectDevice.elementAt(0).toString();
                //get device domain node
                TreeNodeDomain domainNode = (TreeNodeDomain) TreeUtilities.getANode(m_treeTable.getTree(), strDeviceDomain);
                if(domainNode != null)
                {
                    //test if node domain has been built
                    if(domainNode.isBuilt())
                    {
                        //get family node
                        TreeNodeFamily familyNode = domainNode.getNodeFamily(vectDevice.elementAt(1).toString());
                        if(familyNode != null)
                        {
                            if (! familyNode.isBuilt())
                            {
                                //create all memeber nodes
                                familyNode.addMembers(false);
                                familyNode.setBuilt(true);
                            }
                            // get the good member node and check it
                            String strDeviceMember = vectDevice.elementAt(2).toString();
                            TreeNodeMember memberNode = familyNode.getNodeMember(strDeviceMember);
                            if(memberNode != null)
                            {
                                //check this node
                                //check corresponding column of this node
                                m_model.setValueAt(new Boolean(true), memberNode, iCol);
                                //add this node in vector selectedNodes
                                vectSelectedNodes.add(memberNode);
                            }
                        }
                    }

                    /** Create all families*/
                    /** Create all members of the good family */
                    /** Check the good member node */
                    else
                    {
                        //add families to domain node
                        domainNode.addFamilies();
                        domainNode.setBuilt(true);
                        //get family name
                        String strDeviceFamily = vectDevice.elementAt(1).toString();
                        //get family node which contains member node
                        TreeNodeFamily familyNode = domainNode.getNodeFamily(strDeviceFamily);
                        if(familyNode != null)
                        {
                            //add members to family node
                            familyNode.addMembers(false);
                            familyNode.setBuilt(true);
                            // get member node
                            String strDeviceMember = vectDevice.elementAt(2).toString();
                            TreeNodeMember memberNode = familyNode.getNodeMember(strDeviceMember);
                            if (memberNode != null)
                            {
                                // check this node
                                // check corresponding column of this node
                                m_model.setValueAt(new Boolean(true), memberNode, iCol);
                                // add this node in vector selectedNodes
                                vectSelectedNodes.add(memberNode);
                            }
                        }
                    }
                }
            }
        }
        return vectSelectedNodes;
    }

    /**
     * This method allows to expand nodes which have a selected device
     * @param Vector vecSelectedNodes, vector which contains these nodes
     */
    private void expandSelectedNodes(Vector<?> vecSelectedNodes)
    {
        // iteration on this vector
        for (int i=0 ; i<vecSelectedNodes.size() ; i++)
        {
            TreeNodeMember tn = (TreeNodeMember) vecSelectedNodes.elementAt(i);
            TreePath tp = new TreePath(tn.getPath());
            m_treeTable.getTree().setSelectionPath(tp);
            m_treeTable.getTree().addSelectionPath(m_treeTable.getTree().getSelectionPath());
        }
    }

    private void collapseAll()
    {
        for (int i = m_treeTable.getTree().getRowCount(); i > 0; i--)
        {
            m_treeTable.getTree().collapseRow(i);
        }
    }

}
