package fr.soleil.comete.bean.technicaldata.light.util;

import java.awt.BorderLayout;
import java.util.EventObject;

import javax.swing.JComponent;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.swing.AbstractPanel;
import fr.soleil.comete.swing.ComboBox;

/**
 * A {@link ComboBox} used to choose selected config
 * 
 * @author girardot
 */
public class ConfigComboBox extends AbstractPanel implements ITextComponent, IComboBoxListener {

    private static final long serialVersionUID = 1732064691877979762L;

    private ComboBox comboBox;

    /**
     * Constructor
     */
    public ConfigComboBox() {
        super();
        comboBox = new ComboBox();
        comboBox.addComboBoxListener(this);
        setLayout(new BorderLayout());
        add((JComponent) comboBox, BorderLayout.CENTER);
    }

    @Override
    public String getText() {
        return comboBox.getText();
    }

    @Override
    public synchronized void setText(String text) {
        if (!isEditingData()) {
            String valueToSet = text;
            if (valueToSet == null) {
                valueToSet = "";
            }
            if (valueToSet.indexOf(',') > -1) {
                comboBox.setValueList((Object[]) valueToSet.split(","));
            }
            else {
                comboBox.setSelectedValue(valueToSet);
            }
        }
    }

    @Override
    public boolean isEditingData() {
        return comboBox.isEditingData();
    }

    public boolean isEditable() {
        return comboBox.isEditable();
    }

    public void setEditable(boolean editable) {
        comboBox.setEditable(editable);
    }
    
    public ComboBox getComboBox() {
        return comboBox;
    }

    /**
     * Cleans the combo box
     */
    public void cleanCombo() {
        comboBox.setValueList((Object[]) null);
        comboBox.setSelectedItem(null);
    }

    @Override
    public void selectedItemChanged(EventObject event) {
        warnMediators(new TextInformation(this, comboBox.getText()));
    }
}
