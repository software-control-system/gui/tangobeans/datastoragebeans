package fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil;

import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.bean.technicaldata.full.genericclasses.tango.TangoDataBaseManager;

/**
 * This node represents a tango family
 * 
 * @author MARECHAL
 * 
 */
public class TreeNodeFamily extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 4067227349412627182L;

    private String m_strName;
    private TreeNodeDomain m_tnDomain;
    private boolean m_bIsBuilt = false; // notice it this node has built its children

    // true for Recorded Devices tree, false for Monitored attributes
    private boolean isDeviceTree = false;

    /**
     * Constructor
     * 
     * @param String strFamily
     * @param TreeNodeDomain tnDomain
     */
    public TreeNodeFamily(String strFamily, TreeNodeDomain tnDomain, boolean isDeviceTree) {
        super(strFamily);
        m_strName = strFamily;
        m_tnDomain = tnDomain;
        this.isDeviceTree = isDeviceTree;
    }

    /**
     * Allows to get the name of Family
     * 
     * @return String
     */
    public String getFamilyName() {
        return m_strName;
    }

    // /--------------------------------------------------
    // Getter and Setter for Domain
    // ---------------------------------------------------

    public void setDomain(TreeNodeDomain tnDomain) {
        m_tnDomain = tnDomain;
    }

    public TreeNodeDomain getDomain() {
        return m_tnDomain;
    }

    public void addMembers(boolean bAllowsChildren) {
        String[] tabMembers = null;
        tabMembers = TangoDataBaseManager.getMembers(this.getDomain().toString(), m_strName, isDeviceTree);
        if (tabMembers != null) {
            for (String memberName : tabMembers) {
                TreeNodeMember nodeMember = new TreeNodeMember(memberName, getDomain(), this, bAllowsChildren);
                this.add(nodeMember);
            }
        }
    }

    /**
     * @return bIsBuilt
     *         boolean which notice if the node has built its children
     */
    public boolean isBuilt() {
        return m_bIsBuilt;
    }

    /**
     * @param isBuilt the bIsBuilt to set
     */
    public void setBuilt(boolean isBuilt) {
        m_bIsBuilt = isBuilt;
    }

    /**
     * This method allows to get a member node
     * child among these member noded
     * 
     * @param String strMemberName
     * @return TreeNodeMember
     */
    public TreeNodeMember getNodeMember(String strMemberName) {
        TreeNodeMember memberNode = null;
        for (Enumeration<?> enumChildren = this.children(); enumChildren.hasMoreElements();) {
            memberNode = (TreeNodeMember) enumChildren.nextElement();
            if (memberNode.toString().equals(strMemberName)) {
                return memberNode;
            }
        }
        return null;
    }
}
