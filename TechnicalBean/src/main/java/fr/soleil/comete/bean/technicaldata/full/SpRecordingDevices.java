package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.esrf.Tango.DevFailed;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.tango.TangoDataBaseManager;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeDomain;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil.TreeNodeFamily;



@SuppressWarnings("serial")
public class SpRecordingDevices extends JScrollPane
{

    private DefaultMutableTreeNode m_rootNode;
    private static TangoTreeTable m_ttTango = null;
    private RecordingDevicesTreeTableModel m_model = null;
    private String m_strTechnicalDataPath = null;
    private static PnlTechnicalData m_pnl;

    public void setPnl(PnlTechnicalData pnl)
    {
        m_pnl = pnl;
        m_strTechnicalDataPath = pnl.getPathTechnicalData();
        this.setViewportView(getTreeTableTangoDevices());
        this.getViewport().setBackground(Color.WHITE);
        this.setMinimumSize(new Dimension(400,200));
        // update nodes in tree every second
        //m_timer = new UpdateNodeTimer();
    }

    public TangoTreeTable getTreeTableTangoDevices()
    {
        if(m_ttTango == null)
        {
            m_ttTango = new TangoTreeTable(getTreeTableModel(),m_pnl);
            RecordingDevicesTreeTableModel model =  (RecordingDevicesTreeTableModel) m_ttTango.getTree().getModel();
            model.setAsksAllowsChildren(true);
            m_ttTango.getTree().setCellRenderer(new DeviceNodeCellRenderer(m_ttTango));
            //allow only single selection
            DefaultTreeSelectionModel selectionModel =  (DefaultTreeSelectionModel) m_ttTango.getTree().getSelectionModel();
            selectionModel.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            m_ttTango.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            //add listener on tree's expansion (build nodes)
            addListerner();

            m_ttTango.setVisible(true);
            m_ttTango.getTableHeader().setReorderingAllowed(false);
            RecordingDevicesTreeTableController controller = new RecordingDevicesTreeTableController(m_model,
                    m_ttTango, m_strTechnicalDataPath);
            controller.buildTreeTable();
        }
        return m_ttTango;
    }

    /**
     * This method allows to show all nodes which represents
     * tango's  domains
     *
     */
    protected RecordingDevicesTreeTableModel getTreeTableModel()
    {
        if(m_model == null)
        {
            m_rootNode = new DefaultMutableTreeNode("");
            try
            {
                //add treeNodeDomain
                addDomains();
            }
            catch (DevFailed e)
            {
                e.printStackTrace();
            }

            //create TreeTableModel
            m_model = new RecordingDevicesTreeTableModel(m_rootNode);
        }
        return m_model;
    }

    public void addListerner()
    {
        m_ttTango.getTree().addTreeExpansionListener(new TreeExpansionListener()
        {

            @Override
            public void treeCollapsed(TreeExpansionEvent event)
            {
                //do nothing
            }

            @Override
            public void treeExpanded(TreeExpansionEvent event)
            {
                //add node families or member
                if( event.getPath().getLastPathComponent() != null)
                {
                    if(event.getPath().getLastPathComponent() instanceof TreeNodeDomain)
                    {
                        // add family nodes
                        TreeNodeDomain domainNode = (TreeNodeDomain) event.getPath().getLastPathComponent();
                        if(!domainNode.isBuilt())
                        {
                            domainNode.addFamilies();
                            domainNode.setBuilt(true);
                            m_model.reload(domainNode);
                        }
                    }
                    else if (event.getPath().getLastPathComponent() instanceof TreeNodeFamily)
                    {
                        //add member nodes
                        TreeNodeFamily familyNode = (TreeNodeFamily) event.getPath().getLastPathComponent();
                        if(!familyNode.isBuilt())
                        {
                            // boolean is false because node nodeMember doesn't allow children
                            familyNode.addMembers(false);
                            familyNode.setBuilt(true);
                            m_model.reload(familyNode);
                        }
                        m_model.refreshNode(familyNode);
                    }
                }
            }});
    }

    /**
     * this method allows to add domains to the tree
     * @throws DevFailed
     */
    private void addDomains() throws DevFailed
    {
        final boolean isDeviceTree = true;

        //get domains from tango database
        String[] tabDomains = TangoDataBaseManager.getDomains(isDeviceTree);

        for (String domain : tabDomains) {
            // create node TreeNodeDomain
            TreeNodeDomain tnDomain = new TreeNodeDomain(domain, isDeviceTree);
            // add this node to rootNode
            m_rootNode.add(tnDomain);
        }
    }

    /**
     * This method allows to get all expanded nodes in the tree
     */
    @SuppressWarnings("unchecked")
    public static Vector getVectorExpandedNodes(JTree tree)
    {
        if(tree != null)
        {
            if (tree.getModel().getRoot() instanceof DefaultMutableTreeNode)
            {
                DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) tree.getModel().getRoot();
                Enumeration enumChildren =  rootNode.preorderEnumeration();
                if(enumChildren != DefaultMutableTreeNode.EMPTY_ENUMERATION)
                {
                    Vector<DefaultMutableTreeNode> vectExpandedNodes = new Vector<DefaultMutableTreeNode>();
                    while (enumChildren.hasMoreElements())
                    {
                        DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) enumChildren.nextElement();
                        //get tree path
                        TreePath treePath = new TreePath(((RecordingDevicesTreeTableModel)tree.getModel()).getPathToRoot(childNode));
                        if(tree.isExpanded(treePath)) {
                            vectExpandedNodes.add(childNode);
                        }
                    }
                    return vectExpandedNodes;
                }
            }
        }
        return null;
    }


}
