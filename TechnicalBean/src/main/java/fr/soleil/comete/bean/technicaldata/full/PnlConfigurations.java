package fr.soleil.comete.bean.technicaldata.full;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.full.util.Utils;
import fr.soleil.comete.bean.technicaldata.light.TechnicalDataLightBean;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * This class allows to manage buttons for config:
 * Save Config As , delete COnfig, Load Config...
 * 
 * @author MARECHAL
 *
 */
@SuppressWarnings("serial")
public class PnlConfigurations extends JPanel {
    private static final String CONFIG_NAMES_ATTR = "configNames";
    private static final String CURRENT_CONFIG_ATTR = "currentConfig";

    private static final String SAVE_CONFIG_AS_CMD = "SaveConfigAs";
    private static final String DELETE_CONFIG_CMD = "DeleteConfig";

    private static String m_strDsTechnicalPath = null;
    private JButton m_btDelete = null;
    private RecordingDevicesTreeTableController m_recordingDevicesTableController;
    private MonitoredAttributesTreeTableController m_monitoredAttributesTableController;
    private PnlTechnicalData m_pnl = null;

    // Configuration viewers and connectors
    private StringScalarBox stringBox;
    private StringMatrixBox stringMatrixBox;
    private StringMatrixComboBoxViewer configurationComboBox;
    private ItemListener configComboListener;
    private TextTargetRedirector currentConfigRedirector;

    /**
     * Constructor
     * 
     * @param JFrame frame
     * @param TangoTreeTableController tableController
     * @param PnlTechnicalData main panel
     */
    public PnlConfigurations(RecordingDevicesTreeTableController devicestableController,
            MonitoredAttributesTreeTableController attrtableController, PnlTechnicalData pnl) {
        m_recordingDevicesTableController = devicestableController;
        m_monitoredAttributesTableController = attrtableController;
        m_pnl = pnl;

        getBtDeleteConfig();
        initComponents();

        // build panel
        this.setSize(new Dimension(6000, 25));
        this.setMaximumSize(new Dimension(6000, 25));
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.add(getBtNewConfig());
        this.add(getBtSaveConfigAs());
        this.add(getBtRenameConfig());
        this.add(getBtDeleteConfig());
        // create and aadd label configurations
        JLabel lb = new JLabel("Configurations: ");
        this.add(lb);

        add(configurationComboBox);
    }

    private void initComponents() {

        configurationComboBox = new StringMatrixComboBoxViewer() {
            @Override
            public void setValueList(Object... valueList) {
                removeItemListener(configComboListener);
                super.setValueList(valueList);
                // setValueList will end by selecting first item
                // so we need to select the current config after
                if (currentConfigRedirector != null) {
                    setSelectedValue(currentConfigRedirector.getText());
                }
                addItemListener(configComboListener);

                // update button delete config (if only one configuration exists -> disable button)
                m_btDelete.setEnabled((valueList != null) && (valueList.length > 1));
            }
        };
        configurationComboBox.setLinkPopupVisibilityWithEditable(false);
        configComboListener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String selectedValue = (String) configurationComboBox.getSelectedValue();
                    if (selectedValue != null) {
                        if (askSave("Confirmation",
                                "Changes were not saved. Do you want to save previous configuration first?",
                                JOptionPane.YES_NO_OPTION)) {
                            // on value's changement -> load selected config
                            // load configuration
                            TechnicalDataLightBean.loadTechnicalConfig(selectedValue);
                            updateTables();
                        }
                    }
                }
            }
        };
        configurationComboBox.addItemListener(configComboListener);

        currentConfigRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                configurationComboBox.removeItemListener(configComboListener);
                configurationComboBox.setSelectedValue(data);
                updateTables();
                configurationComboBox.addItemListener(configComboListener);
            }
        };

    }

    private void connect() {
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);

        TangoKey currentConfigKey = new TangoKey();
        TangoKeyTool.registerAttribute(currentConfigKey, m_strDsTechnicalPath, CURRENT_CONFIG_ATTR);
        TangoKey configNamesKey = new TangoKey();
        TangoKeyTool.registerAttribute(configNamesKey, m_strDsTechnicalPath, CONFIG_NAMES_ATTR);

        stringBox.connectWidget(currentConfigRedirector, currentConfigKey);
        stringMatrixBox.connectWidget(configurationComboBox, configNamesKey);
    }

    void disconnect() {
        stringBox.disconnectWidgetFromAll(currentConfigRedirector);
        stringMatrixBox.disconnectWidgetFromAll(configurationComboBox);
    }

    private void updateTables() {
        m_recordingDevicesTableController = new RecordingDevicesTreeTableController(
                m_pnl.getSpRecordingDevices().getTreeTableModel(),
                m_pnl.getSpRecordingDevices().getTreeTableTangoDevices(), m_strDsTechnicalPath);
        m_recordingDevicesTableController.buildTreeTable();

        m_monitoredAttributesTableController = new MonitoredAttributesTreeTableController(
                m_pnl.getSpMonitoredAttributes().getTreeTableModel(),
                m_pnl.getSpMonitoredAttributes().getTreeTableTangoDevices(), m_strDsTechnicalPath);
        m_monitoredAttributesTableController.buildTreeTable();

        m_pnl.enableButtonsSave(false);
    }

    /**
     * 
     * @return bt which allows to create a copy of
     *         the current configuration and set it a name
     */
    public JButton getBtSaveConfigAs() {
        JButton btSaveConfigAs = new JButton("Save config as...");
        btSaveConfigAs.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveConfigAs();
            }
        });
        return btSaveConfigAs;
    }

    /**
     * 
     * @return bt which allows to create an empty config
     */
    public JButton getBtNewConfig() {
        JButton btNewConfig = new JButton("New config...");
        btNewConfig.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (askSave("Confirmation", "Changes were not saved. Do you want to save current configuration first?",
                        JOptionPane.YES_NO_CANCEL_OPTION)) {
                    newConfig();
                }
            }
        });
        return btNewConfig;
    }

    /**
     * 
     * @return bt which allows to delete current config
     */
    public JButton getBtDeleteConfig() {
        if (m_btDelete == null) {
            m_btDelete = new JButton("Delete config");
            m_btDelete.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteConfig();
                }
            });
        }
        return m_btDelete;
    }

    /**
     * 
     * @return bt which allows to rename the current config
     */
    public JButton getBtRenameConfig() {
        JButton btRenameConfig = new JButton("Rename config...");
        btRenameConfig.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                renameConfig();
            }
        });
        return btRenameConfig;
    }

    /**
     * This method allows to set device path
     * 
     * @param strTechnicalDataPath
     */
    public void setTechnicalDataPath(String strTechnicalDataPath) {
        m_strDsTechnicalPath = strTechnicalDataPath;
        connect();
    }

    private String getNewConfigName(Component parent, Object message, String title) {
        String result = null;

        String newConfig = null;
        boolean isEmpty = false;
        boolean isAlphanumeric = false;
        do {
            newConfig = JOptionPane.showInputDialog(parent, message, title, JOptionPane.QUESTION_MESSAGE);
            // null result means cancel button has been used
            if (newConfig != null) {
                isEmpty = newConfig.trim().isEmpty();
                if (isEmpty) {
                    JOptionPane.showMessageDialog(parent, "You must give a name for the configuration", "Empty name",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    isAlphanumeric = Utils.isAlphanumericUnderscore(newConfig);
                    if (!isAlphanumeric) {
                        JOptionPane.showMessageDialog(parent, "Only alphanumeric characters are allowed",
                                "Illegal characters", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        } while ((newConfig != null) && (isEmpty || !isAlphanumeric));
        if ((newConfig != null) && !isEmpty && isAlphanumeric) {
            result = newConfig;
        }

        return result;
    }

    /**
     * This method allows to create a new empty configuration
     *
     */
    public void newConfig() {
        String strConfigName = getNewConfigName(CometeUtils.getWindowForComponent(PnlConfigurations.this),
                "New configuration Name:", "New configuration");
        if (strConfigName != null) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDsTechnicalPath);
            if (proxy != null) {
                try {
                    DeviceData argin = new DeviceData();
                    argin.insert(strConfigName);
                    proxy.command_inout(SAVE_CONFIG_AS_CMD, argin);
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDsTechnicalPath,
                            SAVE_CONFIG_AS_CMD, strConfigName, exception, true);
                }
            }

            // clear on table model
            m_recordingDevicesTableController.unbuildTree();
            // save attributes (no one)
            m_recordingDevicesTableController.saveAttributes();

            // clear on table model
            m_monitoredAttributesTableController.unbuildTree();
            // save attributes (no one)
            m_monitoredAttributesTableController.saveAttributesList();

            m_pnl.enableButtonsSave(false);
        }
    }

    /**
     * This method allows to save the configuration ans
     * ask user to set a name for this one.
     *
     */
    public void saveConfigAs() {
        // open dialog to set config's name
        String strConfigName = getNewConfigName(CometeUtils.getWindowForComponent(PnlConfigurations.this),
                "Set configuration name:", "Configuration name");
        if (strConfigName != null) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDsTechnicalPath);
            if (proxy != null) {
                try {
                    DeviceData argin = new DeviceData();
                    argin.insert(strConfigName);
                    proxy.command_inout(SAVE_CONFIG_AS_CMD, argin);
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDsTechnicalPath,
                            SAVE_CONFIG_AS_CMD, strConfigName, exception, true);
                }
            }

            // update attributes
            m_recordingDevicesTableController.saveAttributes();
            m_monitoredAttributesTableController.saveAttributesList();

            m_pnl.enableButtonsSave(false);
        }
    }

    public void renameConfig() {
        String strCurrentConfig = (String) configurationComboBox.getSelectedValue();
        // open dialog to set config's name
        String strConfigName = getNewConfigName(CometeUtils.getWindowForComponent(PnlConfigurations.this),
                "Set configuration name:", "Configuration name");
        if (strConfigName != null) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDsTechnicalPath);
            if (proxy != null) {
                try {
                    DeviceData argin = new DeviceData();
                    argin.insert(strConfigName);
                    proxy.command_inout(SAVE_CONFIG_AS_CMD, argin);
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDsTechnicalPath,
                            SAVE_CONFIG_AS_CMD, strConfigName, exception, true);
                }
            }

            // update attributes
            m_recordingDevicesTableController.saveAttributes();
            m_monitoredAttributesTableController.saveAttributesList();

            m_pnl.enableButtonsSave(false);

            // delete previous config
            if (proxy != null) {
                try {
                    DeviceData argin = new DeviceData();
                    argin.insert(strCurrentConfig);
                    proxy.command_inout(DELETE_CONFIG_CMD, argin);
                } catch (DevFailed exception) {
                    TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(strCurrentConfig, DELETE_CONFIG_CMD,
                            strCurrentConfig, exception, true);
                }
            }
        }
    }

    /**
     * This method allows to delete the current configuration
     *
     */
    public void deleteConfig() {
        // remove config selected in combobox
        String strSelectedConfig = (String) configurationComboBox.getSelectedValue();
        if (strSelectedConfig != null) {
            // show Confirm Dialodg
            int iConfirmResult = JOptionPane.showConfirmDialog(
                    CometeUtils.getWindowForComponent(PnlConfigurations.this), "Are you sure?", "Remove configuration",
                    JOptionPane.YES_NO_OPTION);
            if (iConfirmResult == JOptionPane.OK_OPTION) {
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDsTechnicalPath);
                if (proxy != null) {
                    try {
                        DeviceData argin = new DeviceData();
                        argin.insert(strSelectedConfig);
                        proxy.command_inout(DELETE_CONFIG_CMD, argin);
                    } catch (DevFailed exception) {
                        TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDsTechnicalPath,
                                DELETE_CONFIG_CMD, strSelectedConfig, exception, true);
                    }
                }

                updateTables();
            }
        }
    }

    /**
     * This method allows to show a dialog which ask a confirmation
     * for saving current configuration
     * 
     * @param String strTitle
     * @param String strQuestion
     * @param int iOptions
     * @return boolean bSave -> answer of the user
     */
    public boolean askSave(String strTitle, String strQuestion, int iOptions) {
        if (m_pnl.getBtSave().isEnabled()) {
            // show dialog to ask if the user want to save
            int iReturnValue = JOptionPane.showConfirmDialog(m_pnl, strQuestion, strTitle, iOptions);
            if (iReturnValue == JOptionPane.OK_OPTION) {
                // create table controller
                m_recordingDevicesTableController = new RecordingDevicesTreeTableController(
                        m_pnl.getSpRecordingDevices().getTreeTableModel(),
                        m_pnl.getSpRecordingDevices().getTreeTableTangoDevices(), m_strDsTechnicalPath);
                // close attributes
                m_recordingDevicesTableController.saveAttributes();

                // create table controller
                m_monitoredAttributesTableController = new MonitoredAttributesTreeTableController(
                        m_pnl.getSpMonitoredAttributes().getTreeTableModel(),
                        m_pnl.getSpMonitoredAttributes().getTreeTableTangoDevices(), m_strDsTechnicalPath);
                // close attributes
                m_monitoredAttributesTableController.saveAttributesList();

            }
            if (iReturnValue == JOptionPane.CANCEL_OPTION) {
                return false;
            }
        }
        return true;
    }
}
