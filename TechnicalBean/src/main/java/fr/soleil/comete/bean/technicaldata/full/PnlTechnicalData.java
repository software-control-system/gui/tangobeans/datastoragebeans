package fr.soleil.comete.bean.technicaldata.full;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.DataSourceProducerProvider;

/**
 * This class contains the treetable and buttons ( ok , close)
 * The button close allows to save selected devices in configuration
 * thanks to device TechnicalData
 * 
 * @author MARECHAL
 * 
 */
@SuppressWarnings("serial")
public class PnlTechnicalData extends JPanel {
    private static final String TECHNICAL_DATA_DEVICE_ATTR = "technicalDataDevice";

    private static final String RECORDED_DEVICES_TAB = "Recorded devices";
    private static final String MONITORED_ATTRIBUTES_TAB = "Archived attributes";

    private static final String RECORDED_DEVICES_TOOLTIP = "List of devices that are allowed to be recorded in nexus file";
    private static final String MONITORED_ATTRIBUTES_TOOLTIP = "List of attributes that are archived by the Tango archiving service";

    private static String m_strPathTechnicalData = null;

    private static SpRecordingDevices m_spRecordingDevices = null;
    private static SpMonitoredAttributes m_spMonitoredAttributes = null;

    private static PnlConfigurations m_pnlConfig = null;
    private RecordingDevicesTreeTableController m_deviceTableController = null;
    private MonitoredAttributesTreeTableController m_attrTableController = null;
    private static JButton m_btSave = null;
    private static JButton btClose = null;
    private static JButton m_btSaveAndClose = null;
    private static String m_strDataRecorderPath = null;

    private final static Map<String, Integer> recordableMap = new HashMap<String, Integer>();

    /**
     * Constructor
     * 
     * @param JDialog dialog (frame where will be shown warning messages)
     */
    public PnlTechnicalData() {
        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);

        m_spRecordingDevices = new SpRecordingDevices();
        m_spMonitoredAttributes = new SpMonitoredAttributes();
        m_pnlConfig = new PnlConfigurations(m_deviceTableController, m_attrTableController, this);

        this.setLayout(new BorderLayout());
        this.add(m_pnlConfig, BorderLayout.NORTH);
        this.add(getTbp(), BorderLayout.CENTER);
        this.add(getPnlButtons(), BorderLayout.SOUTH);
    }

    /**
     * 
     * @return Window : parent window
     */
    public Window getParentWindow() {
        return SwingUtilities.getWindowAncestor(this);
    }

    /**
     * Create and return tabbedpane which will contains
     * two tabs: recording devices & monitored attributes
     * 
     * @return JTabbedPane
     */
    public JTabbedPane getTbp() {
        JTabbedPane tbp = new JTabbedPane();
        tbp.addTab(RECORDED_DEVICES_TAB, null, m_spRecordingDevices, RECORDED_DEVICES_TOOLTIP);
        tbp.addTab(MONITORED_ATTRIBUTES_TAB, null, m_spMonitoredAttributes, MONITORED_ATTRIBUTES_TOOLTIP);
        return tbp;
    }

    /**
     * 
     * @return String : TechnicalData Bean's version
     */
    public static String getVersion() {
        ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.technicaldata.application");
        return rb.getString("project.version");
    }

    /**
     * Create and return the panel which contains buttons Ok &
     * Cancel
     * 
     * @return Jpanel pnlButtons
     */
    public JPanel getPnlButtons() {
        JPanel pnlButtons = new JPanel();
        pnlButtons.setSize(new Dimension(6000, 25));
        pnlButtons.setMaximumSize(new Dimension(6000, 25));
        pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
        pnlButtons.add(getBtSave());
        pnlButtons.add(getBtSaveAndClose());
        pnlButtons.add(getBtClose());
        return pnlButtons;
    }

    /**
     * Create and return the button Apply And Close
     * to save modifications in device TechnicalData
     * 
     * @return JButton btApplyAndClose
     */
    public JButton getBtSaveAndClose() {
        if (m_btSaveAndClose == null) {
            m_btSaveAndClose = new JButton("Save And Close");
            m_btSaveAndClose.setEnabled(false);
            m_btSaveAndClose.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(final ActionEvent e) {
                    saveAndClose();
                }
            });
        }
        return m_btSaveAndClose;
    }

    /**
     * This method allows to enable / disable buttons
     * Close and SaveAndClose
     * 
     * @param boolean bEnable
     */
    public void enableButtonsSave(boolean bEnable) {
        getBtSave().setEnabled(bEnable);
        getBtSaveAndClose().setEnabled(bEnable);
    }

    /**
     * Create and return the button Apply
     * to save modifications in device TechnicalData
     * 
     * @return JButton btApply
     */
    public JButton getBtSave() {
        if (m_btSave == null) {
            m_btSave = new JButton("Save") {
                @Override
                public void setEnabled(boolean b) {
                    super.setEnabled(b);
                };
            };
            m_btSave.setEnabled(false);
            m_btSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    save();
                }
            });
        }
        return m_btSave;
    }

    /**
     * 
     * @return bt which allows to close the application
     */
    public JButton getBtClose() {
        if (btClose == null) {
            btClose = new JButton("Close");
            btClose.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    close();
                }
            });
        }
        return btClose;
    }

    /**
     * 
     * @return path of ds_TechicalData
     */
    public String getPathTechnicalData() {
        return m_strPathTechnicalData;
    }

    /**
     * 
     * @return ScrollPane which contains treeTable
     */
    public SpRecordingDevices getSpRecordingDevices() {
        return m_spRecordingDevices;
    }

    /**
     * 
     * @return ScrollPane which contains treeTable
     */
    public SpMonitoredAttributes getSpMonitoredAttributes() {
        return m_spMonitoredAttributes;
    }

    /**
     * Set scrollpane which contains technicalData
     * 
     * @param spTechnicalData
     */
    public void setSpRecordingDevices(final SpRecordingDevices spTechnicalData) {
        m_spRecordingDevices = spTechnicalData;
    }

    /**
     * Set scrollpane which contains technicalData
     * 
     * @param spTechnicalData
     */
    public void setSpMonitoredAttributes(final SpMonitoredAttributes spTechnicalData) {
        m_spMonitoredAttributes = spTechnicalData;
    }

    public void save() {
        m_deviceTableController = new RecordingDevicesTreeTableController(m_spRecordingDevices.getTreeTableModel(),
                m_spRecordingDevices.getTreeTableTangoDevices(), m_strPathTechnicalData);
        // save Attributes
        m_deviceTableController.saveAttributes();

        m_attrTableController = new MonitoredAttributesTreeTableController(m_spMonitoredAttributes.getTreeTableModel(),
                m_spMonitoredAttributes.getTreeTableTangoDevices(), m_strPathTechnicalData);
        // save Attributes
        m_attrTableController.saveAttributesList();
        // disable button save
        enableButtonsSave(false);

    }

    public void close() {
        m_pnlConfig.askSave("Confirmation", "Changes was not saved. Do you want to save current configuration first?",
                JOptionPane.YES_NO_CANCEL_OPTION);
        m_pnlConfig.disconnect();
        // close frame
        getParentWindow().dispose();
    }

    public void saveAndClose() {
        save();
        close();
    }

    public void setDataRecorderPath(final String strDataRecorderPath) {
        m_strDataRecorderPath = strDataRecorderPath;

        // read the technical data attribute from DataRecorder device
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(m_strDataRecorderPath);
        if (proxy != null) {
            String strPathTechnicalData = null;
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(TECHNICAL_DATA_DEVICE_ATTR);
                strPathTechnicalData = deviceAttribute.extractString();
            } catch (DevFailed exception) {
                TechnicalDataMessageManager.notifyReadAttributeErrorDetected(m_strDataRecorderPath,
                        TECHNICAL_DATA_DEVICE_ATTR, exception);
            }

            // Set path to pnl, pnlConfig & scrollpane which contains treetable
            m_strPathTechnicalData = strPathTechnicalData;
            m_spRecordingDevices.setPnl(this);
            m_spMonitoredAttributes.setDevicePath(this);
            m_pnlConfig.setTechnicalDataPath(strPathTechnicalData);
        }
    }

    public String getDataRecorderPath() {
        return m_strDataRecorderPath;
    }

    /**
     * @return the m_pnlConfig
     */
    public PnlConfigurations getPnlConfig() {
        return m_pnlConfig;
    }

    public static int getRecordableState(final String deviceName) {
        Integer recordable = -1;

        // store information in map see JIRA EXPDATA-264 and EXPDATA-265
        if ((deviceName != null && !deviceName.isEmpty())) {
            recordable = recordableMap.get(deviceName.toLowerCase());
            if (recordable == null && TangoDeviceHelper.isDeviceRunning(m_strDataRecorderPath)
                    && TangoDeviceHelper.isDeviceRunning(deviceName)) {
                DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(m_strDataRecorderPath, false);
                if (deviceProxy != null) {
                    try {
                        DeviceData argin = new DeviceData();
                        argin.insert(deviceName);
                        DeviceData argout = deviceProxy.command_inout("IsTangoDeviceRecordable", argin);
                        recordable = Short.valueOf(argout.extractShort()).intValue();
                        System.out.println("**"+m_strDataRecorderPath + "/IsTangoDeviceRecordable(" + deviceName + ")="+ recordable+"**" );
                    } catch (DevFailed exception) {
                        recordable = -1;
                        // exception.printStackTrace();
                        TechnicalDataMessageManager.notifyExecuteCommandErrorDetected(m_strDataRecorderPath,
                                "IsTangoDeviceRecordable", deviceName, exception, true);
                    }
                }
            }
            
            if(recordable == null){
                recordable = -1;
            }
            recordableMap.put(deviceName.toLowerCase(), recordable);
        }
        return recordable;
    }

}
