package fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil;

import javax.swing.tree.DefaultMutableTreeNode;

public class TreeNodeAttribute extends DefaultMutableTreeNode {

    private static final long serialVersionUID = -4272782467231003156L;

    private String m_strName = null;
    private TreeNodeDomain m_tnDomain = null;
    private TreeNodeFamily m_tnFamily = null;
    private TreeNodeMember m_tnMember = null;
    private String m_strType = null; // attribute's type (scalar, boolean ...)
    private boolean bIsSaved = false;

    public TreeNodeAttribute(String strAttrName, TreeNodeDomain tnDomain, TreeNodeFamily tnFamily,
            TreeNodeMember tnMember) {
        super(strAttrName);
        this.setAllowsChildren(false);
        m_strName = strAttrName;
        m_tnDomain = tnDomain;
        m_tnFamily = tnFamily;
        m_tnMember = tnMember;
    }

    /**
     * Allows to get the device's name
     * 
     * @return String (ie: recording/datarecorder/1
     */
    public String getDeviceName() {
        String strDeviceName = m_tnDomain.getDomainName() + "/" + m_tnFamily.getFamilyName() + "/"
                + m_tnMember.getMemberName();
        return strDeviceName;
    }

    public String getAttributePath() {
        return getDeviceName() + "/" + m_strName;
    }

    /**
     * Device doimain
     * 
     * @return TreeNodeDomain
     */
    public TreeNodeDomain getDomainNode() {
        return m_tnDomain;
    }

    /**
     * Device family
     * 
     * @return TreeNodeFamily
     */
    public TreeNodeFamily getFamilyNode() {
        return m_tnFamily;
    }

    /**
     * Device member
     * 
     * @return TreeNodeMember
     */
    public TreeNodeMember getMemberNode() {
        return m_tnMember;
    }

    /**
     * Attribute's type (string, scalar, boolean...)
     * 
     * @return String
     */
    public String getType() {
        return m_strType;
    }

    /**
     * Set attribute's type
     * 
     * @param String strType
     */
    public void setType(String strType) {
        m_strType = strType;
    }

    // -------------------------
    //
    // Getter & setter for boolean bIsSaved
    // ------------------------

    public boolean isSaved() {
        return bIsSaved;
    }

    public void setSaved(boolean isSaved) {
        bIsSaved = isSaved;
    }
}
