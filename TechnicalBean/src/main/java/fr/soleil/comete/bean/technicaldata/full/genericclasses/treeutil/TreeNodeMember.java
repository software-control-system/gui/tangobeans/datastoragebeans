package fr.soleil.comete.bean.technicaldata.full.genericclasses.treeutil;

import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.esrf.Tango.DevFailed;
import fr.soleil.comete.bean.technicaldata.full.genericclasses.tango.TangoDataBaseManager;

/**
 * This node represents a device's instance
 * 
 * @author MARECHAL
 * 
 */
public class TreeNodeMember extends DefaultMutableTreeNode {
    private static final long serialVersionUID = 4588244842453226870L;
    private String m_strName = null;
    private TreeNodeDomain m_tnDomain = null;
    private TreeNodeFamily m_tnFamily = null;
    private boolean m_bIsPre = false;
    private boolean m_bIsPost = false;
    private boolean m_bIsBuilt = false; // notice it this node has built its children

    /**
     * Constructor
     * 
     * @param String strName
     * @param TreeNodeDomain tnDomain
     * @param TreeNodeFamily tnFamily
     */
    public TreeNodeMember(String strName, TreeNodeDomain tnDomain, TreeNodeFamily tnFamily, boolean bAllowChildren) {
        super(strName);
        this.setAllowsChildren(bAllowChildren);
        m_strName = strName;
        m_tnDomain = tnDomain;
        m_tnFamily = tnFamily;
    }

    /**
     * Allows to get the device's name
     * 
     * @return String (ie: recording/datarecorder/1
     */
    public String getDeviceName() {
        String strDeviceName = m_tnDomain.getDomainName() + "/" + m_tnFamily.getFamilyName() + "/" + m_strName;
        return strDeviceName;
    }

    public String getMemberName() {
        return m_strName;
    }

    /**
     * Allows to get Family Node
     * 
     * @return TreeNodeFamily
     */
    public TreeNodeFamily getFamilyNode() {
        return m_tnFamily;
    }

    /**
     * Allows to get Domain Node
     * 
     * @return TreeNodeDomain
     */
    public TreeNodeDomain getDomainNode() {
        return m_tnDomain;
    }

    /**
     * @return bIsBuilt
     *         boolean which notice if the node has built its children
     */
    public boolean isBuilt() {
        return m_bIsBuilt;
    }

    /**
     * @param isBuilt the bIsBuilt to set
     */
    public void setBuilt(boolean isBuilt) {
        m_bIsBuilt = isBuilt;
    }

    public void addAttributes() throws DevFailed {
        String[] tabAttributes = null;

        tabAttributes = TangoDataBaseManager.getAttributes(getDeviceName());
        if (tabAttributes != null) {
            for (String strAttribute : tabAttributes) {
                TreeNodeAttribute attributeNode = new TreeNodeAttribute(strAttribute, getDomainNode(), getFamilyNode(),
                        this);
                this.add(attributeNode);
            }
        }
    }


    public void setPre(boolean bPre) {
        m_bIsPre = bPre;
    }

    public boolean isPre() {
        return m_bIsPre;
    }

    public void setPost(boolean bPost) {
        m_bIsPost = bPost;
    }

    public boolean isPost() {
        return m_bIsPost;
    }

    /**
     * This method allows to get a member node
     * child among these member noded
     * 
     * @param String strMemberName
     * @return TreeNodeMember
     */
    public TreeNodeAttribute getNodeAttribute(String strAttributeName) {
        TreeNodeAttribute attributeNode = null;
        for (Enumeration<?> enumChildren = this.children(); enumChildren.hasMoreElements();) {
            attributeNode = (TreeNodeAttribute) enumChildren.nextElement();
            if (attributeNode.toString().equals(strAttributeName)) {
                return attributeNode;
            }
        }
        return attributeNode;
    }
}
