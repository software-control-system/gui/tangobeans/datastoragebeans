package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.data.service.IKey;

public class FileNameDialogModel extends ExprFileEditionModel{

    private IKey resetFileNameIndexModelKey;
    
    public void setResetFileNameIndexModel(IKey commandKey) {
	resetFileNameIndexModelKey = commandKey;
    }
    
    public IKey getResetFileNameIndexModelKey() {
	return resetFileNameIndexModelKey;
    }
}
