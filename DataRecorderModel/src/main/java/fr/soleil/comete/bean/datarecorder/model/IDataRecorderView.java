package fr.soleil.comete.bean.datarecorder.model;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel.Availabilities;

public interface IDataRecorderView {


    public void updateAvailabilities(Availabilities availibilities);

    public void updatePanel(final DataRecorderModel dataRecorderModel, UpdateMode mode);

    public enum UpdateMode {
        DETAILS_MODE, HISTORY_MODE, ADMIN_MODE, TECHNICAL_MODE
    }
}
