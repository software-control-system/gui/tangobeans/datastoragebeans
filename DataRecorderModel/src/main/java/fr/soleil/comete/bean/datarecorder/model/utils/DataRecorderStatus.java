package fr.soleil.comete.bean.datarecorder.model.utils;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.data.service.IKey;

public abstract class DataRecorderStatus {

    private final IKey currentKey;
    private final boolean configEnabled;
    private final boolean startStopEnabled;
    protected final String model;


    protected DataRecorderStatus(IKey key, String model, boolean config, boolean startStop) {
        currentKey = key;
        configEnabled = config;
        startStopEnabled = startStop;
        this.model = model;
    }

    public abstract boolean updateState(DataRecorderModel model, String state);

    public static DataRecorderStatus generateDefaultStatus(String newModel) {
        return new EmptyState(newModel);
    }

    /**
     * @return the currentKey
     */
    public IKey getCurrentKey() {
        return currentKey;
    }

    /**
     * @return the configEnabled
     */
    public boolean isConfigEnabled() {
        return configEnabled;
    }

    /**
     * @return the startStopEnabled
     */
    public boolean isStartStopEnabled() {
        return startStopEnabled;
    }


    public static class OnState extends DataRecorderStatus {

        public OnState(String model) {
            super(DataRecorderModel.generateCommandKey(model, "StartRecording"), model, true, true);
        }

        @Override
        public boolean updateState(DataRecorderModel model, String state) {
            boolean result = false;
            String trimState = (state != null) ? state.trim() : "";

            if ("running".equalsIgnoreCase(trimState) || "alarm".equalsIgnoreCase(trimState)) {
                model.setState(new RunningState(this.model));
                result = true;
            }
            else if (!"on".equalsIgnoreCase(trimState)) {
                model.setState(new EmptyState(this.model));
                result = true;
            }
            return result;
        }

        @Override
        public String toString() {
            return "Status : on";
        }
    }

    public static class RunningState extends DataRecorderStatus {

        protected RunningState(String model) {
            super(DataRecorderModel.generateCommandKey(model, "EndRecording"), model, false, true);
        }

        @Override
        public boolean updateState(DataRecorderModel model, String state) {
            boolean result = false;
            String trimState = (state != null) ? state.trim() : "";

            if ("on".equalsIgnoreCase(trimState)) {
                model.setState(new OnState(this.model));
                result = true;
            }
            else if (!("running".equalsIgnoreCase(trimState) || "alarm".equalsIgnoreCase(trimState))) {
                model.setState(new EmptyState(this.model));
                result = true;
            }

            return result;
        }

        @Override
        public String toString() {
            return "Status : running";
        }
    }

    public static class EmptyState extends DataRecorderStatus {

        public EmptyState(String model) {
            super(DataRecorderModel.generateCommandKey(model, "StartRecording"), model, false,
                    false);
        }

        @Override
        public boolean updateState(DataRecorderModel model, String state) {
            boolean result = false;
            String trimState = (state != null) ? state.trim() : "";

            if ("running".equalsIgnoreCase(trimState) || "alarm".equalsIgnoreCase(trimState)) {
                model.setState(new RunningState(this.model));
                result = true;
            }
            else if ("on".equalsIgnoreCase(trimState)) {
                model.setState(new OnState(this.model));
                result = true;
            }
            return result;
        }

        @Override
        public String toString() {
            return "Status : empty";
        }
    }
}
