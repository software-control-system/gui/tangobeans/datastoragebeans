package fr.soleil.comete.bean.datarecorder.model;

public interface DataRecorderMessageListener {

    public void newErrorDetected(Exception e, boolean displayToFront);
    
    public void newMessageDetected(String message, boolean displayToFront);

}
