package fr.soleil.comete.bean.datarecorder.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXErrorPane;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.datarecorder.model.IDataRecorderView.UpdateMode;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.DataModelDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.FileEditionModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.FileNameDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.LoadConfigurationDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.NxEntryNameDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.PostRecordingDialogModel;
import fr.soleil.comete.bean.datarecorder.model.dialogmodels.SubDirectoryDialogModel;
import fr.soleil.comete.bean.datarecorder.model.utils.DataRecorderStatus;
import fr.soleil.comete.bean.datarecorder.model.utils.DataRecorderStatus.OnState;
import fr.soleil.comete.bean.datarecorder.model.utils.ValidationListener;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.box.target.redirector.NumberTargetRedirector;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public final class DataRecorderModel {

    public final static String PROJECT_DIRECTORY_ATTR = "projectDirectory";
    public final static String SUB_DIRECTORY_ATTR = "subDirectory";
    public final static String GLOBAL_POST_RECORDING_ATTR = "postRecordingScript";
    public final static String NXENTRY_POST_RECORDING_ATTR = "nxentryPostRecording";
    public final static String CURRENT_CONFIG_ATTR = "currentConfig";
    public final static String BEAMLINE_ATTR = "beamline";
    public final static String STATE_ATTR = "State";
    public final static String ERROR_LEVEL_ATTR = "errorLevel";
    public final static String SCRIPTS_ATTR = "scripts";
    public final static String RECORDED_FILES_ATTR = "recordedFiles";
    public final static String CONFIG_LIST_ATTR = "configList";
    public final static String CURRENT_DATA_MODEL_ATTR = "currentDataModel";
    public final static String FILE_NAME_ATTR = "fileName";
    public final static String NX_ENTRY_NAME_ATTR = "nxentryName";
    public final static String TARGET_DIRECTORY_ATTR = "targetDirectory";
    private static final String ACQUISITION_NAME_ATTR = "acquisitionName";
    private static final String EXPERIMENT_NAME_ATTR = "experimentName";

    public final static String DELETE_CONFIG_CMD = "DeleteConfig";
    public final static String LOAD_CONFIG_CMD = "LoadConfig";
    public final static String SAVE_CONFIG_CMD = "SaveConfig";
    public final static String SAVE_CONFIG_AS_CMD = "SaveConfigAs";
    public final static String GET_CONFIG_LIST_CMD = "GetConfigList";
    public final static String GET_POST_RECORDING_CMD = "GetPostRecordingCommand";
    public final static String GET_DATA_MODEL_CMD = "GetDataModelList";
    public final static String GET_SCRIPTS_INFO_CMD = "GetScriptInfo";
    public final static String START_RECORDING_CMD = "StartRecording";
    public final static String END_RECORDING_CMD = "EndRecording";
    public final static String ERROR_ACK_CMD = "ErrorAck";
    public final static String SET_ROOT_DIRECTORY_CMD = "setRootDirectory";
    public final static String SET_DEFAULT_ROOT_DIRECTORY_CMD = "SetDefaultRootDirectory";
    public final static String SET_DEFAULT_SUB_DIRECTORY_CMD = "setDefaultSubDirectory";
    public final static String SET_DEFAULT_FILE_NAME_CMD = "setDefaultFileName";
    public final static String RESET_FILE_NAME_INDEX_CMD = "resetFileNameIndex";
    public final static String SET_POST_RECORDING_COMMAND_CMD = "setPostRecordingCommand";
    private static final String LOAD_DATA_MODEL_CMD = "LoadDataModel";
    private static final String INC_EXPERIMENT_INDEX_CMD = "incExperimentIndex";
    private static final String RESET_EXPERIMENT_INDEX_CMD = "resetExperimentIndex";
    private static final String GET_NXENTRY_NAME_DEFAULT_VALUE_CMD = "GetNXentryNameDefaultValue";
    private static final String GET_PATH_SYMBOLS_CMD = "GetPathSymbols";

    public final static String AUTH_DEVICE = "AuthentificationDevice";
    public final static String TECH_DEVICE = "TechnicalDataDevice";

    public final static short WARNING_LEVEL = 2;
    public final static short ERROR_LEVEL = 3;
    public final static String NO_SCRIPT = "dummy";
    public final static String NO_VALUE = "(no value)";
    public final static String EXTRACTING_PREFIX = "Extracting model: ";
    public final static String DO_NOTHING = "Do Nothing";
    public final static String POST_RECORDING_FILE = "FILE";
    public final static String POST_RECORDING_ENTRY = "ENTRY";

    public static enum PostRecordingType {
        FILE, ENTRY, DONOTHING;

        public static PostRecordingType toEnum(String value) {
            PostRecordingType enumeration = FILE;
            if (DO_NOTHING.equalsIgnoreCase(value)) {
                enumeration = DONOTHING;
            } else {
                enumeration = PostRecordingType.valueOf(value);
            }

            return enumeration;
        }

        @Override
        public String toString() {
            String toString = super.toString();
            if (this == DONOTHING) {
                toString = DO_NOTHING;
            }
            return toString;
        };
    }

    // Path to NeXus Files extractor
    public final static String DEFAULT_PATH_NXEXTRACTOR = "/usr/Local/DeviceServers/linux";

    private final List<IDataRecorderView> listeners = new ArrayList<IDataRecorderView>();

    private String pathNXextractor;

    // Computed attributes
    private String modelPostRecordingNXentry;
    private String modelPostRecordingGlobal;
    private String postRecordingGeneralCommand;
    private String postRecordingNXentryCommand;

    // Retrieved attributes
    private String[] tabScripts;
    private String[] recordedFiles;
    private String postRecordingCommand;
    private String[] datamodels;

    private short lastErrorLevel;

    private TextTargetRedirector stateRedirector;
    private NumberTargetRedirector warningLevelRedirector;

    private final Availabilities availabilities;
    private UpdateMode recorderMode;

    private String deviceModel;
    private String authModel;
    private String techModel;

    // An object to register state and values associated to this state
    private DataRecorderStatus state;
    // An IKey that contains the command to call on start / stop button pressed
    private IKey currentSwitchKey;

    private StringScalarBox stringBox;
    private NumberScalarBox numberBox;

    /**
     * Default constructor
     */
    public DataRecorderModel() {

        pathNXextractor = DEFAULT_PATH_NXEXTRACTOR;

        lastErrorLevel = 2;// to get the maximum label size for pack

        modelPostRecordingGlobal = null;
        modelPostRecordingNXentry = null;
        postRecordingCommand = null;
        postRecordingGeneralCommand = null;
        postRecordingNXentryCommand = null;
        datamodels = null;

        recorderMode = UpdateMode.DETAILS_MODE;
        availabilities = new Availabilities();

        initTargetRedirector();
    }

    public void setModel(String model) {
        deviceModel = model;
        if ((deviceModel != null) && !deviceModel.isEmpty()) {
            initSubDevicesModel();
            setState(DataRecorderStatus.generateDefaultStatus(model));
        }
    }

    public String getModel() {
        return deviceModel;
    }

    public void setState(DataRecorderStatus newState) {
        state = newState;
    }

    public void startStopRecording(boolean start) {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceModel);
        if (proxy != null) {
            String executedCommand = "";
            try {
                if (!start) {
                    executedCommand = END_RECORDING_CMD;
                    proxy.command_inout(executedCommand);
                } else {
                    executedCommand = START_RECORDING_CMD;
                    int result = JOptionPane.showConfirmDialog(null, "Do you really want to start recording session ?",
                            "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.YES_OPTION) {
                        proxy.command_inout(executedCommand);
                    }
                }
            } catch (DevFailed devFailed) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + executedCommand + "/n");
                errorMessage.append(DevFailedUtils.toString(devFailed) + "/n");
                JXErrorPane.showDialog(new Exception(errorMessage.toString()));
                // e.printStackTrace();
            } catch (Exception e) {
                JXErrorPane.showDialog(e);
                // e.printStackTrace();
            }
        }
    }

    public boolean updateState(String data) {
        return getState().updateState(this, data);
    }

    public void setDefaultStringBox(StringScalarBox stringBox) {
        this.stringBox = stringBox;

        numberBox = (NumberScalarBox) CometeBoxProvider.getCometeBox(NumberScalarBox.class);

    }

    /**
     * Initialize default connections
     */
    public void initConnections() {

        // Warning Level
        numberBox.connectWidget(warningLevelRedirector, generateAttributeKey(getModel(), ERROR_LEVEL_ATTR));

        // State
        stringBox.connectWidget(stateRedirector, generateAttributeKey(getModel(), STATE_ATTR));
    }

    private void initSubDevicesModel() {

        String tempAuthModel = null;
        String tempTechModel = null;
        String property = "";
        try {
            Database database = TangoDeviceHelper.getDatabase();
            property = AUTH_DEVICE;
            DbDatum dbDatum = database.get_device_property(getModel(), property);
            tempAuthModel = dbDatum.extractString();

            property = TECH_DEVICE;
            dbDatum = database.get_device_property(getModel(), property);
            tempTechModel = dbDatum.extractString();

            if ((tempAuthModel != null) && (!tempAuthModel.trim().isEmpty())
                    && (!ObjectUtils.sameObject(getAuthModel(), tempAuthModel))) {
                authModel = tempAuthModel;
            }
            if ((tempTechModel != null) && (!tempTechModel.trim().isEmpty())
                    && (!ObjectUtils.sameObject(getTechModel(), tempTechModel))) {
                techModel = tempTechModel;
            }

        } catch (DevFailed e) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("Error when reading property " + getModel() + "/" + property + "/n");
            errorMessage.append(DevFailedUtils.toString(e) + "/n");
            new Exception(errorMessage.toString()).printStackTrace();
            // e.printStackTrace();
        }
    }

    /**
     * Clear default connections
     */
    public void clearConnections() {
        // State
        stringBox.disconnectWidgetFromAll(stateRedirector);
        // Warning Level
        numberBox.disconnectWidgetFromAll(warningLevelRedirector);
    }

    /**
     * Initialize all {@link TextTargetRedirector}, used to refresh attributes that needed to be
     * updated with new values.
     */
    private void initTargetRedirector() {

        stateRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                boolean stateChanged = updateState(data);
                if (stateChanged) {
                    computeDetailsAvailabilities();
                }
            }
        };

        warningLevelRedirector = new NumberTargetRedirector() {

            @Override
            public void methodToRedirect(Number data) {
                updateWarningLevel(data);
            }
        };

    }

    public void initPostRecordingConnections() {

        // Command for post recording
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            String executedCommand = "";
            try {
                executedCommand = GET_POST_RECORDING_CMD;
                DeviceData data = proxy.command_inout(executedCommand);
                postRecordingCommand = data.extractString();
                setPostRecordingValues();

                executedCommand = GET_DATA_MODEL_CMD;
                data = proxy.command_inout(GET_DATA_MODEL_CMD);
                datamodels = data.extractStringArray();

                executedCommand = SCRIPTS_ATTR;
                DeviceAttribute deviceAttribute = proxy.read_attribute(SCRIPTS_ATTR);
                tabScripts = deviceAttribute.extractStringArray();
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing or reading " + getModel() + "/" + executedCommand + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                new Exception(errorMessage.toString()).printStackTrace();
                // exception.printStackTrace();
            }
        }

        updateDetails();
    }

    public void initAdminRecordedFiles() {

        // Attribute that stock the recorded files
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(RECORDED_FILES_ATTR);
                recordedFiles = deviceAttribute.extractStringArray();
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + RECORDED_FILES_ATTR + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                new Exception(errorMessage.toString()).printStackTrace();
            }
        }

        updateAdmin();
    }

    /*
     * Cleans widgets in "Details" tab
     */
    protected void cleanDetailsConnections() {

        // Command values to reset
        postRecordingCommand = null;
        datamodels = null;
        tabScripts = null;
    }

    /*
     * Analyzes warning level from hiddenErrorLabel and transforms it into a user readable text displayed in warningLabel
     */
    private void updateWarningLevel(final Number data) {
        short errLevel = data.shortValue();
        if (errLevel != lastErrorLevel) {
            lastErrorLevel = errLevel;

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    updateDetails();
                }
            });
        }
    }

/*
 * Connects/disconnects attributes, depending on current authentication state and device state
 */
    public void computeDetailsAvailabilities() {

        recorderMode = UpdateMode.DETAILS_MODE;

        // Start / Stop
        availabilities.startStopAV = state.isStartStopEnabled();
        currentSwitchKey = state.getCurrentKey();

        // Define basic availabilities from data recorder device's state
        availabilities.setStateBehavior(state.isConfigEnabled());
        availabilities.setStateOn(state instanceof OnState);

        // ///////////////
        // Error checking
        // ///////////////

        // Authentication Availabilities
        boolean authAV = !((authModel == null) || authModel.isEmpty());
        availabilities.projectDirectoryAV &= authAV;
        availabilities.subDirAV &= authAV;

        // TODO Handle error text on attribute availabilities

        // Disabled post recording components if parsing or connection failure
        availabilities.changeDataModelAV &= (getDatamodels() != null);
        availabilities.generalPostRecordAV &= (getModelPostRecordingGlobal() != null);
        availabilities.nXEntryPostRecordAV &= (getModelPostRecordingNXentry() != null);

        // Finally update DataRecorder views
        for (IDataRecorderView view : listeners) {
            view.updateAvailabilities(availabilities);
        }
    }

    public void setPostRecordingValues() {

        recorderMode = UpdateMode.DETAILS_MODE;
        // update values of postRecordingGeneral & postRecordingNXentry
        updatePostRecordingValues();
        if (getPostRecordingGeneralCommand() != null) {
            modelPostRecordingGlobal = (getModelForScript(getPostRecordingGeneralCommand()));
        } else {
            modelPostRecordingGlobal = null;
        }
        if (getPostRecordingNXentryCommand() != null) {
            modelPostRecordingNXentry = (getModelForScript(getPostRecordingNXentryCommand()));
        } else {
            modelPostRecordingNXentry = null;
        }
    }

    /**
     * This method allows to get the value of post recording command:
     * <ul>
     * <li>general post record</li>
     * <li>nxentry post record</li>
     * </ul>
     */
    private void updatePostRecordingValues() {
        if (postRecordingCommand != null) {
            // Post recording value contains :
            // - general post recording command
            // - nxentry post recording command.
            // These two values are separated with "|"

            if (postRecordingCommand.indexOf("|") != -1) {
                postRecordingGeneralCommand = postRecordingCommand.substring(0, postRecordingCommand.indexOf("|"));
                postRecordingNXentryCommand = postRecordingCommand.substring(postRecordingCommand.indexOf("|") + 1,
                        postRecordingCommand.length());
            } else {
                if (postRecordingCommand.equals(NO_VALUE)) {
                    postRecordingGeneralCommand = (NO_SCRIPT);
                } else {
                    postRecordingGeneralCommand = (postRecordingCommand);
                }

                postRecordingNXentryCommand = (NO_SCRIPT);
            }
        } else {
            postRecordingGeneralCommand = null;
            postRecordingCommand = null;
        }
    }

    /**
     * This method allows to transform the value "PostRecordingCommand" contained in the attribute
     * of the device into a string which will be shown to explain the current script or process
     * applied
     * 
     * @param strScript -> value in ds
     * @return a {@link String} value
     */
    private String getModelForScript(String strScript) {
        String strModel = "";
        if (strScript != null) {
            // model is contained in script like .... -t <model> -m 750
            // get first index of "-t"
            int iFirstIndex = strScript.indexOf("-t");
            int iLastIndex = strScript.indexOf("-m");
            if ((iFirstIndex != -1) && (iLastIndex != -1)) {
                // get path of model and model between -t and -m
                String strPathAndModel = strScript.substring(iFirstIndex + 2, iLastIndex).trim();
                if (strPathAndModel.indexOf("/") != -1) {
                    int iLastPosSlash = strPathAndModel.lastIndexOf("/");
                    int iLastPosPOint = strPathAndModel.lastIndexOf(".");
                    strModel = EXTRACTING_PREFIX + strPathAndModel.substring(iLastPosSlash + 1, iLastPosPOint);
                } else {
                    int iLastPosPOint = strPathAndModel.lastIndexOf(".");
                    String strModelName = strPathAndModel.substring(0, iLastPosPOint);
                    strModel = EXTRACTING_PREFIX + strModelName;
                }
            } else if (strScript.length() < 1) {
                strModel = DO_NOTHING;
            } else {
                strScript = strScript.replaceAll("\\\\", "/");
                // get index last "/"
                int iIndexLastSlash = strScript.lastIndexOf("/");
                if (iIndexLastSlash == -1) {
                    strModel = "Script or process: " + strScript;
                } else {
                    strModel = "Script or process: "
                            + strScript.substring(iIndexLastSlash + 1, strScript.length()).trim();
                }
            }
        }
        return strModel;
    }

    /**
     * This method allows to build a {@link List} which contains all scripts names available for
     * type of post recording set in parameter
     * 
     * @param String strTypePostRecording : Entry,File, n/a(not available)
     * @return List (scripts names)
     */
    public List<String> getScriptList(PostRecordingType strTypePostRecording) {
        ArrayList<String> listScrips = new ArrayList<String>();

        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());

        if ((tabScripts != null) && (proxy != null)) {

            for (String script : tabScripts) {
                String[] tabParameters = null;
                try {
                    DeviceData parameter = new DeviceData();
                    parameter.insert(getScriptName(script));
                    DeviceData data = proxy.command_inout(GET_SCRIPTS_INFO_CMD, parameter);
                    tabParameters = data.extractStringArray();
                } catch (DevFailed exception) {
                    StringBuilder errorMessage = new StringBuilder();
                    errorMessage
                            .append("Error when executing command " + getModel() + "/" + GET_SCRIPTS_INFO_CMD + "/n");
                    errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                    new Exception(errorMessage.toString()).printStackTrace();
                }

                // get informations about this script : source
                if (tabParameters != null) {
                    if (tabParameters.length > 0) {
                        String strSource = tabParameters[0];
                        if (strSource.equalsIgnoreCase(strTypePostRecording.toString())) {
                            listScrips.add(script);
                        } else if (strSource.equalsIgnoreCase("n/a")
                                && strTypePostRecording.toString().equalsIgnoreCase(POST_RECORDING_FILE)) {
                            listScrips.add(script);
                        }
                    }
                }
                tabParameters = null;
            }
        }
        return listScrips;
    }

    /**
     * Parse one script name obtained by reading attribute "scripts".
     * 
     * @param strAttrModel the complete script name
     * @return the part of the script corresponding to the simple name
     */
    private String getScriptName(String strAttrModel) {

        String result;

        if (strAttrModel.contains("/")) {
            // get last position of "/"
            int iLastPosSlash = strAttrModel.lastIndexOf("/");
            // get last pos of "."
            int iLastPosPoint = strAttrModel.lastIndexOf(".");
            result = strAttrModel.substring(iLastPosSlash + 1, iLastPosPoint);
        } else {
            int iLastPosPoint = strAttrModel.lastIndexOf(".");
            result = strAttrModel.substring(0, iLastPosPoint);
        }
        return result;
    }

    /**
     * Setter for nxextractor tool's path
     * 
     * @param String strPathAuth
     */
    public void setPathNXextractor(String strPath) {
        if ((strPath != null) && (strPath.trim().length() > 0)) {
            pathNXextractor = strPath;
        } else {
            pathNXextractor = DEFAULT_PATH_NXEXTRACTOR;
        }
    }

    public void newRecorderConfiguration(String newConfig) {
        manageRecorderConfigurationSaving(newConfig, SAVE_CONFIG_AS_CMD);
    }

    public void saveRecorderConfiguration(String saveConfig) {
        manageRecorderConfigurationSaving(saveConfig, SAVE_CONFIG_CMD);
    }

    private void manageRecorderConfigurationSaving(String value, String commandName) {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                DeviceData argin = new DeviceData();
                argin.insert(value);
                proxy.command_inout(commandName, argin);
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + commandName + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                JXErrorPane.showDialog(new Exception(errorMessage.toString()));
                // exception.printStackTrace();
            }
        }
    }

    public String[] getRecorderConfigList() {
        String[] result = null;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                DeviceData deviceData = proxy.command_inout(GET_CONFIG_LIST_CMD);
                result = deviceData.extractStringArray();
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + GET_CONFIG_LIST_CMD + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                new Exception(errorMessage.toString()).printStackTrace();
            }
        }
        return result;
    }

    public void loadRecorderConfig(String configName) {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                DeviceData argin = new DeviceData();
                argin.insert(configName);
                proxy.command_inout(LOAD_CONFIG_CMD, argin);
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + LOAD_CONFIG_CMD + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                new Exception(errorMessage.toString()).printStackTrace();
            }
        }
    }

    public void ackError() {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        if (proxy != null) {
            try {
                proxy.command_inout(ERROR_ACK_CMD);
            } catch (DevFailed exception) {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.append("Error when executing command " + getModel() + "/" + ERROR_ACK_CMD + "/n");
                errorMessage.append(DevFailedUtils.toString(exception) + "/n");
                JXErrorPane.showDialog(new Exception(errorMessage.toString()));
                // exception.printStackTrace();
            }
        }
    }

    // //////////////////////
    // Dialog box updaters //
    // //////////////////////

    public void updateLoadConfigurationDialogModel(LoadConfigurationDialogModel loadConfigurationDialogModel) {
        loadConfigurationDialogModel.setDeleteModel(generateCommandKey(getModel(), DELETE_CONFIG_CMD));
        loadConfigurationDialogModel.setModelForListConfig(getModel());
    }

    public void updateProjectDirectoryDialogModel(FileEditionModel projectDirectoryDialogModel) {
        // Reset dialog data and connect dialog
        projectDirectoryDialogModel.setApplyModel(generateCommandKey(getModel(), SET_ROOT_DIRECTORY_CMD));
        projectDirectoryDialogModel.setValueModel(generateAttributeKey(getModel(), PROJECT_DIRECTORY_ATTR));
        projectDirectoryDialogModel.setDefaultModel(generateCommandKey(getModel(), SET_DEFAULT_ROOT_DIRECTORY_CMD));
    }

    public void updateFileNameDialogModel(FileNameDialogModel fileNameDialogModel) {

        IKey fileNameKey = generateAttributeKey(getModel(), FILE_NAME_ATTR);
        fileNameDialogModel.setApplyModel(fileNameKey);
        fileNameDialogModel.setValueModel(fileNameKey);
        fileNameKey = null;
        fileNameDialogModel.setWriteValueModel(generateWriteAttributeKey(getModel(), FILE_NAME_ATTR));
        fileNameDialogModel.setDefaultModel(generateCommandKey(getModel(), SET_DEFAULT_FILE_NAME_CMD));
        fileNameDialogModel.setResetFileNameIndexModel(generateCommandKey(getModel(), RESET_FILE_NAME_INDEX_CMD));

        String[] symbols = executeCommandInOutShort((short) 1);
        fileNameDialogModel.setSymbolListModel(symbols);
    }

    public void updateSubDirectoryDialogModel(SubDirectoryDialogModel subDirectoryDialogModel) {

        IKey subDirectoryKey = generateAttributeKey(getModel(), SUB_DIRECTORY_ATTR);
        subDirectoryDialogModel.setApplyModel(subDirectoryKey);
        subDirectoryDialogModel.setValueModel(subDirectoryKey);
        subDirectoryKey = null;
        subDirectoryDialogModel.setWriteValueModel(generateWriteAttributeKey(getModel(), SUB_DIRECTORY_ATTR));
        subDirectoryDialogModel.setDefaultModel(generateCommandKey(getModel(), SET_DEFAULT_SUB_DIRECTORY_CMD));
        subDirectoryDialogModel.setParentDirectoryModel(generateAttributeKey(getModel(), PROJECT_DIRECTORY_ATTR));

        String[] symbols = executeCommandInOutShort((short) 2);
        subDirectoryDialogModel.setSymbolListModel(symbols);

    }

    public void updateGlobalPostRecordingDialog(final PostRecordingDialogModel globalPostRecordingDialogModel) {
        updatePostRecordingDialog(globalPostRecordingDialogModel, POST_RECORDING_FILE, modelPostRecordingGlobal);
    }

    public void updateNxEntryPostRecordingDialog(final PostRecordingDialogModel nxEntryPostRecordingDialogModel) {
        updatePostRecordingDialog(nxEntryPostRecordingDialogModel, POST_RECORDING_ENTRY, modelPostRecordingNXentry);
    }

    public void updatePostRecordingDialog(final PostRecordingDialogModel postRecordingDialogModel, final String type,
            final String modelPostRecording) {
        if (postRecordingDialogModel != null) {

            postRecordingDialogModel.setNxExtractorPath(getPathNXextractor());
            postRecordingDialogModel.setAsLoading();

            postRecordingDialogModel
                    .setPostRecordingCommandModel(generateCommandKey(getModel(), SET_POST_RECORDING_COMMAND_CMD));
            postRecordingDialogModel.setTargetDirectoryModel(generateAttributeKey(getModel(), TARGET_DIRECTORY_ATTR));
            postRecordingDialogModel.setPostRecordGlobal(getPostRecordingGeneralCommand());
            postRecordingDialogModel.setPostRecordNxEntry(getPostRecordingNXentryCommand());
            if (!postRecordingDialogModel.hasValidationListeners()) {
                postRecordingDialogModel.addValidationListener(new ValidationListener() {
                    @Override
                    public void onValidation() {
                        initPostRecordingConnections();
                    }
                });
            }
            postRecordingDialogModel.updatePostRecordingValues();
            postRecordingDialogModel.updateListSelectionModel(getScriptList(PostRecordingType.toEnum(type)));
            postRecordingDialogModel.setAsLoaded();
            String reducedModel = modelPostRecording.replace(EXTRACTING_PREFIX, "");
            postRecordingDialogModel.setModel(getModel());
            postRecordingDialogModel.updateValueSelection(reducedModel);
        }
    }

    public void updateDataModelDialogModel(DataModelDialogModel dataModelDialogModel) {

        dataModelDialogModel.setTabDataModels(getDatamodels());
        // Connect dialog
        dataModelDialogModel.setDataModelSelectionModel(generateCommandKey(getModel(), LOAD_DATA_MODEL_CMD));
    }

    public void updateNxEntryNameDialogModel(NxEntryNameDialogModel nxEntryNameDialogModel) {

        // Connect Dialog
        nxEntryNameDialogModel.setAcquisitionNameModel(generateWriteAttributeKey(getModel(), ACQUISITION_NAME_ATTR));
        nxEntryNameDialogModel.setExperimentNameModel(generateWriteAttributeKey(getModel(), EXPERIMENT_NAME_ATTR));
        nxEntryNameDialogModel
                .setIncrementExperimentIndexModel(generateCommandKey(getModel(), INC_EXPERIMENT_INDEX_CMD));
        nxEntryNameDialogModel.setResetExperimentIndexModel(generateCommandKey(getModel(), RESET_EXPERIMENT_INDEX_CMD));
        nxEntryNameDialogModel.setNxEntryNameModel(generateAttributeKey(getModel(), NX_ENTRY_NAME_ATTR));
        nxEntryNameDialogModel.setDefaultModel(generateCommandKey(getModel(), GET_NXENTRY_NAME_DEFAULT_VALUE_CMD));
        nxEntryNameDialogModel.setNxEntryNameWriteModel(generateWriteAttributeKey(getModel(), NX_ENTRY_NAME_ATTR));
    }

    // /////////////////
    // Key Generators //
    // /////////////////

    public static TangoKey generatePropertyKey(String model, String propertyName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerDeviceProperty(key, model, propertyName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public static TangoKey generateAttributeKey(String model, String attributeShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, model, attributeShortName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public static TangoKey generateWriteAttributeKey(String model, String attributeShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerWriteAttribute(key, model, attributeShortName);
        return key;
    }

    /**
     * Generates the {@link TangoKey} that corresponds to a command in the associated device
     * 
     * @param commandShortName the command short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public static TangoKey generateCommandKey(String model, String commandShortName) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, model, commandShortName);
        return key;
    }

    /**
     * Used by AbstractFileDialogModel.
     * 
     * @param argin
     * @return
     */
    private String[] executeCommandInOutShort(short arg) {
        String[] result = null;
        try {
            DeviceProxy proxy = new DeviceProxy(getModel());
            DeviceData argin = new DeviceData();
            argin.insert(arg);
            DeviceData cmdResult = proxy.command_inout(GET_PATH_SYMBOLS_CMD, argin);
            result = cmdResult.extractStringArray();
        } catch (DevFailed devFailed) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("Error when executing command " + getModel() + "/" + GET_PATH_SYMBOLS_CMD + "/n");
            errorMessage.append(DevFailedUtils.toString(devFailed) + "/n");
            new Exception(errorMessage.toString()).printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void addDataRecorderListener(IDataRecorderView dataRecorderStateBean) {
        if (dataRecorderStateBean != null) {
            listeners.add(dataRecorderStateBean);
        }
    }

    // /////////////////
    // Listeners call //
    // /////////////////

    protected void updateDetails() {
        for (IDataRecorderView view : listeners) {
            view.updatePanel(this, UpdateMode.DETAILS_MODE);
        }
    }

    protected void updateHistory() {
        for (IDataRecorderView view : listeners) {
            view.updatePanel(this, UpdateMode.HISTORY_MODE);
        }
    }

    protected void updateAdmin() {
        for (IDataRecorderView view : listeners) {
            view.updatePanel(this, UpdateMode.ADMIN_MODE);
        }
    }

    // //////////
    // Getters //
    // //////////

    public IKey getCurrentStartStopKey() {
        return currentSwitchKey;
    }

    public String getPathNXextractor() {
        return pathNXextractor;
    }

    /**
     * @return the modelPostRecordingGlobal
     */
    public String getModelPostRecordingGlobal() {
        return modelPostRecordingGlobal;
    }

    /**
     * @return the modelPostRecordingNXentry
     */
    public String getModelPostRecordingNXentry() {
        return modelPostRecordingNXentry;
    }

    /**
     * @return the authModel
     */
    public String getAuthModel() {
        return authModel;
    }

    /**
     * @return the techModel
     */
    public String getTechModel() {
        return techModel;
    }

    /**
     * @return the state
     */
    public DataRecorderStatus getState() {
        return state;
    }

    /**
     * @return the postRecordingGeneralCommand
     */
    public String getPostRecordingGeneralCommand() {
        return postRecordingGeneralCommand;
    }

    /**
     * @return the postRecordingNXentryCommand
     */
    public String getPostRecordingNXentryCommand() {
        return postRecordingNXentryCommand;
    }

    /**
     * @return the datamodels
     */
    public String[] getDatamodels() {
        return datamodels;
    }

    public UpdateMode getRecorderMode() {
        return recorderMode;
    }

    public short getLastErrorLevel() {
        return lastErrorLevel;
    }

    public String[] getRecordedFiles() {
        return recordedFiles;
    }

    /**
     * This class is designed to handle availabilities over the DataRecoder's components. An object
     * of this kind is transmit through the method computeAvailabilities.
     * 
     * @author huriez
     */
    public class Availabilities {

        private boolean stateOn;
        private boolean fileNameAV;
        private boolean projectDirectoryAV;
        private boolean subDirAV;
        private boolean generalPostRecordAV;
        private boolean nXEntryPostRecordAV;
        private boolean nXEntryNameAV;
        private boolean changeDataModelAV;
        private boolean technicalDataAV;
        private boolean startStopAV;

        public Availabilities() {
            stateOn = false;
            fileNameAV = false;
            projectDirectoryAV = false;
            subDirAV = false;
            generalPostRecordAV = false;
            nXEntryPostRecordAV = false;
            nXEntryNameAV = false;
            changeDataModelAV = false;
            startStopAV = false;
            technicalDataAV = false;
        }

        public void setStateBehavior(boolean configEnabled) {
            projectDirectoryAV = configEnabled;
            subDirAV = configEnabled;
            fileNameAV = configEnabled;
            generalPostRecordAV = configEnabled;
            nXEntryPostRecordAV = configEnabled;
            nXEntryNameAV = configEnabled;
            changeDataModelAV = configEnabled;
            technicalDataAV = configEnabled;
        }

        public void setStateOn(boolean on) {
            stateOn = on;
        }

        /**
         * @return the fileNameAV
         */
        public boolean isFileNameAV() {
            return fileNameAV;
        }

        /**
         * @return the projectDirectoryAV
         */
        public boolean isProjectDirectoryAV() {
            return projectDirectoryAV;
        }

        /**
         * @return the subDirAV
         */
        public boolean isSubDirAV() {
            return subDirAV;
        }

        /**
         * @return the generalPostRecordAV
         */
        public boolean isGeneralPostRecordAV() {
            return generalPostRecordAV;
        }

        /**
         * @return the nXEntryPostRecordAV
         */
        public boolean isNXEntryPostRecordAV() {
            return nXEntryPostRecordAV;
        }

        /**
         * @return the nXEntryNameAV
         */
        public boolean isNXEntryNameAV() {
            return nXEntryNameAV;
        }

        /**
         * @return the cahngeDataModelAV
         */
        public boolean isChangeDataModelAV() {
            return changeDataModelAV;
        }

        /**
         * @return the startStopAV
         */
        public boolean isStartStopAV() {
            return startStopAV;
        }

        /**
         * @return the technicalDataAV
         */
        public boolean isTechnicalDataAV() {
            return technicalDataAV;
        }

        public boolean isStateOn() {
            return stateOn;
        }
    }

}
