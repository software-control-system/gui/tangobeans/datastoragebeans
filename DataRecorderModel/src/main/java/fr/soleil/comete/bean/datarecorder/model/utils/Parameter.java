package fr.soleil.comete.bean.datarecorder.model.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This method allows to manage Parameters associated to a model for post recording. This model
 * contains a description a name a type a default value and a real value.
 * 
 * @author MARECHAL
 * 
 */
public class Parameter {

    public static final String TARGET_DIRECTORY = "$(RecordFolder)";

    private String m_strName;
    private String m_strDescription;
    private String m_strType;
    private String m_strDefaultValue;
    private String m_strRealValue;

    // line from tab tabParameter in ScriptInfos
    private String m_strParameterLineScriptInfo;
    // ends of post recording command which contains only parameters value
    private String m_strParameterLinePostRecording;
    // map that contains each parameter name with its real value
    private HashMap<String, String> m_mapParametersPostRecordingCommand;

    /**
     * Constructor
     * 
     * @param String strPameterLine : string located in tab parameter (getScriptInfos) which
     *            contains all about a parameter : name, label, type, and default value
     */
    public Parameter(String strParameterScriptInfo) {
        this(strParameterScriptInfo, null);
    }

    /**
     * Constructor
     * 
     * @param strParameterScriptInfo
     * @param strParameterPostRecording
     * @param strTargetDir
     */
    public Parameter(String strParameterScriptInfo, String strParameterPostRecording) {
        m_mapParametersPostRecordingCommand = new HashMap<String, String>();
        m_strName = null;
        m_strDescription = null;
        m_strType = null;
        m_strRealValue = null;
        m_strParameterLineScriptInfo = strParameterScriptInfo;
        m_strParameterLinePostRecording = strParameterPostRecording;
        if ((m_strParameterLinePostRecording != null)
                && (m_strParameterLinePostRecording.length() > 0)) {
            initializeMap();
        }
        setAllDefaultValues();
    }

    /**
     * This method allows to set all values contained in tabParameters (method getScriptInfos)
     * 
     */
    public void setAllDefaultValues() {
        // get position of first blank in the string
        int iPosFirstBlank = m_strParameterLineScriptInfo.indexOf(" ");
        if (iPosFirstBlank != -1) {
            String strName = m_strParameterLineScriptInfo.substring(0, iPosFirstBlank);
            // set Name
            m_strName = strName;

            m_strParameterLineScriptInfo = m_strParameterLineScriptInfo.substring(
                    iPosFirstBlank + 1, m_strParameterLineScriptInfo.length()).trim();
            // get type
            int iPosFirstQuote = m_strParameterLineScriptInfo.indexOf("'");
            String strType = m_strParameterLineScriptInfo.substring(0, iPosFirstQuote).trim();
            // set type
            m_strType = strType;

            m_strParameterLineScriptInfo = m_strParameterLineScriptInfo.substring(
                    iPosFirstQuote + 1, m_strParameterLineScriptInfo.length());
            int iPosLastQuote = m_strParameterLineScriptInfo.indexOf("'");
            String strDesc = m_strParameterLineScriptInfo.substring(0, iPosLastQuote).trim();
            // set description
            m_strDescription = strDesc;

            String strDefaultValue = m_strParameterLineScriptInfo.substring(iPosLastQuote + 1,
                    m_strParameterLineScriptInfo.length()).trim();
            m_strDefaultValue = strDefaultValue;

            // set real value if it exists
            if (m_strParameterLinePostRecording != null) {
                // get value in map which corresponds to key = name
                if (m_mapParametersPostRecordingCommand.get(m_strName) != null)
                    m_strRealValue = m_mapParametersPostRecordingCommand.get(m_strName).toString();

            }
        }
    }

    public String toString() {
        return m_strName;
    }

    public void setParamRealValue(String strRealValue) {
        m_strRealValue = strRealValue;
    }

    /**
     * @return the m_strDefaultValue
     */
    public String getValue() {
        if (m_strRealValue != null)
            return m_strRealValue;
        else
            return m_strDefaultValue;
    }

    /**
     * @return the m_strDescription
     */
    public String getDescription() {
        return m_strDescription;
    }

    /**
     * @return the m_strName
     */
    public String getName() {
        return m_strName;
    }

    /**
     * @return the m_strRealValue
     */
    public String getRealValue() {
        return m_strRealValue;
    }

    public String getDefaultValue() {
        return m_strDefaultValue;
    }

    /**
     * @return the m_strType
     */
    public String getType() {
        return m_strType;
    }

    /**
     * This method allows to initialize map with name and values of each parameter contained in
     * string returned by PostRecordingCommand
     * 
     */
    public void initializeMap() {
        if (m_strParameterLinePostRecording.length() > 1) {
            // remove "-D of this stirng"
            String strPostRecordingParam = m_strParameterLinePostRecording.substring(2,
                    m_strParameterLinePostRecording.length()).trim();
            // break this string with separator "," beacause each parameter is separated with","
            List<String> vectPostRecordParam = Arrays.asList(strPostRecordingParam.split(","));
            // for each param=value add it in the contained in vector, add it in the map
            for (String strParam : vectPostRecordParam) {
                // get position of "="
                int iPos = strParam.indexOf("=");
                if (iPos != -1) {
                    String strName = strParam.substring(0, strParam.indexOf("="));
                    String strRealValue = strParam.substring(strParam.indexOf("=") + 1, strParam
                            .length());
                    m_mapParametersPostRecordingCommand.put(strName, strRealValue);
                }

            }
        }
    }
}
