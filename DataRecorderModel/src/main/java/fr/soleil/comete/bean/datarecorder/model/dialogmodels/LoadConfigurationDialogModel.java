package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageManager;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.IKey;

public class LoadConfigurationDialogModel {

    private IKey deleteKey;
    private String modelForListConfig;

    public LoadConfigurationDialogModel() {
    }

    public void setDeleteModel(IKey commandKey) {
        deleteKey = commandKey;
    }

    public IKey getDeleteKey() {
        return deleteKey;
    }

    // TODO   virer quand on pourra connecter la combo sur un attribut string array
    public String[] getRecorderConfigList(){
        String[] result = null;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(modelForListConfig);
        if (proxy != null) {
            try {
                DeviceData deviceData = proxy.command_inout(DataRecorderModel.GET_CONFIG_LIST_CMD);
                result = deviceData.extractStringArray();
            }
            catch (DevFailed exception) {
                DataRecorderMessageManager.notifyExecuteCommandErrorDetected(modelForListConfig, DataRecorderModel.GET_CONFIG_LIST_CMD, exception);
            }
        }
        return result;
    }

    public String getModelForListConfig() {
        return modelForListConfig;
    }

    public void setModelForListConfig(String modelForListConfig) {
        this.modelForListConfig = modelForListConfig;
    }
}
