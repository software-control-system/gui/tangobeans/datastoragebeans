package fr.soleil.comete.bean.datarecorder.model;

import java.util.ArrayList;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

public class DataRecorderMessageManager {

    private static List<DataRecorderMessageListener> messageListener = new ArrayList<DataRecorderMessageListener>();

    public static void addMessageListener(DataRecorderMessageListener listener) {
        if (!messageListener.contains(listener)) {
            messageListener.add(listener);
        }
    }

    public static void removeMessageListener(DataRecorderMessageListener listener) {
        if (messageListener.contains(listener)) {
            messageListener.remove(listener);
        }
    }

    public static void notifyNewErrorDetected(Exception e) {
        notifyNewErrorDetected(e, true);
    }

    public static void notifyNewErrorDetected(Exception e, boolean displayToFront) {
        for (DataRecorderMessageListener listener : messageListener) {
            listener.newErrorDetected(e, displayToFront);
        }
    }
    
    public static void notifyNewMessageDetected(String message) {
    	notifyNewMessageDetected(message, true);
    }

    public static void notifyNewMessageDetected(String message, boolean displayToFront) {
        for (DataRecorderMessageListener listener : messageListener) {
            listener.newMessageDetected(message, displayToFront);
        }
    }
    public static void notifyReadAttributeErrorDetected(String deviceName, String attributeName, DevFailed devFailed) {
        notifyReadAttributeErrorDetected(deviceName, attributeName, devFailed, true);
    }

    public static void notifyReadAttributeErrorDetected(String deviceName, String attributeName, DevFailed devFailed,
            boolean displayToFront) {
        notifyReadAttributeErrorDetected(deviceName + "/" + attributeName, devFailed,
                displayToFront);
    }

    public static void notifyReadAttributeErrorDetected(String completeAttributeName, DevFailed devFailed) {
        notifyReadAttributeErrorDetected(completeAttributeName, devFailed, true);
    }

    public static void notifyReadAttributeErrorDetected(String completeAttributeName, DevFailed devFailed,
            boolean displayToFront) {
        String errorMessage = "Cannot read attribute " + completeAttributeName + " "
        + DevFailedUtils.toString(devFailed);
        notifyNewErrorDetected(new Exception(errorMessage), displayToFront);
    }

    public static void notifyExecuteCommandErrorDetected(String deviceName, String commandName, DevFailed devFailed) {
        notifyExecuteCommandErrorDetected(deviceName, commandName, devFailed, true);
    }

    public static void notifyExecuteCommandErrorDetected(String deviceName, String commandName, DevFailed devFailed,
            boolean displayToFront) {
        notifyExecuteCommandErrorDetected(deviceName + "/" + commandName, devFailed,
                displayToFront);
    }

    public static void notifyExecuteCommandErrorDetected(String completeCommandName, DevFailed devFailed) {
        notifyExecuteCommandErrorDetected(completeCommandName, devFailed);
    }

    public static void notifyExecuteCommandErrorDetected(String completeCommandName, DevFailed devFailed,
            boolean displayToFront) {
        String errorMessage = "Cannot execute command " + completeCommandName + " "
        + DevFailedUtils.toString(devFailed);
        notifyNewErrorDetected(new Exception(errorMessage), displayToFront);
    }

}
