package fr.soleil.comete.bean.datarecorder.model.utils;

import java.util.EventListener;

public interface ValidationListener extends EventListener{

	public void onValidation();
}
