package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.data.service.IKey;

public class ExprFileEditionModel extends FileEditionModel {

    private IKey writeValueModelKey;
    private String[] symbols;
    
    public void setWriteValueModel(IKey writeAttributeKey) {
	writeValueModelKey = writeAttributeKey;
    }
    
    public IKey getWriteValueModelKey() {
        return writeValueModelKey;
    }

    public void setSymbolListModel(String[] symbols) {
	this.symbols = symbols;
    }

    public String[] getSymbols() {
	return symbols;
    }
}
