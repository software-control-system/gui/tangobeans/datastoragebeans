package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

public interface INxEntryNameModelListener {

    public void setAcquisitionName(String acquisitionName);

    public void setExperimentName(String experimentName);
}
