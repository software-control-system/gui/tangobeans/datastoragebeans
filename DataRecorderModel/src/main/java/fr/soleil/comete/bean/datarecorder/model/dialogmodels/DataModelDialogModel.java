package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.data.service.IKey;

public class DataModelDialogModel {

    private String[] tabDataModel;
    private IKey dataModelSelectionModelKey; 
    
    public void setTabDataModels(String[] datamodels) {
	tabDataModel = datamodels;
    }

    public void setDataModelSelectionModel(IKey commandKey) {
	dataModelSelectionModelKey = commandKey;
    }
    
    public IKey getDataModelSelectionModelKey() {
	return dataModelSelectionModelKey;
    }
    
    public String[] getTabDataModel() {
	return tabDataModel;
    }
}
