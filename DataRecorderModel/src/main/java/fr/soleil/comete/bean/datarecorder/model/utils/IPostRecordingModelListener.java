package fr.soleil.comete.bean.datarecorder.model.utils;

import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

import fr.soleil.comete.bean.datarecorder.model.dialogmodels.PostRecordingDialogModel.ScriptModel;

public interface IPostRecordingModelListener {

    /**
     * Set the component as loaded (every compute has been done)
     */
    public void setAsLoaded();

    /**
     * Put the component into loading state (in calculation)
     */
    public void setAsLoading();

    public void updateListSelectionModel(List<String> scriptList, DefaultComboBoxModel<ScriptModel> model,
            boolean shouldListenToComboBox);

    /**
     * Used to choose the right button to select on dialog instantiation
     * 
     * @param modelPostRecording
     */
    public void updateValueSelection(String reducedModel);

    /**
     * This method allows to open and initialize the dialog which shows all
     * parameters for a model.
     * 
     * @param strExplanations
     * @param vectParam
     * 
     * @return choosen parameters
     */
    public Vector<String> getCustumModelParameters(Vector<Parameter> vectParam, String strExplanations);

    /**
     * Confirm the dialog box.
     */
    public void executeConfirmation();
}
