package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.data.service.IKey;

public class SubDirectoryDialogModel extends ExprFileEditionModel{

    private IKey parentDirectoryModelKey;
    
    public void setParentDirectoryModel(IKey attributeKey) {
	parentDirectoryModelKey = attributeKey;
    }
    
    public IKey getParentDirectoryModelKey() {
	return parentDirectoryModelKey;
    }

}
