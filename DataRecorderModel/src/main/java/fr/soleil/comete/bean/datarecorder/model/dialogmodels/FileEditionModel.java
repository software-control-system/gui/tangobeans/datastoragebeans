package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.data.service.IKey;

public class FileEditionModel{

    private IKey applyModelKey; 
    private IKey valueModelKey;
    private IKey defaultModelKey;
        
    public void setApplyModel(IKey commandKey) {
	applyModelKey = commandKey;
    }

    public void setValueModel(IKey attributeKey) {
	valueModelKey = attributeKey;
    }

    public void setDefaultModel(IKey commandKey) {
	defaultModelKey = commandKey;
    }  
    
    public IKey getApplyModelKey() {
        return applyModelKey;
    }

    public IKey getValueModelKey() {
        return valueModelKey;
    }

    public IKey getDefaultModelKey() {
        return defaultModelKey;
    }       
}
