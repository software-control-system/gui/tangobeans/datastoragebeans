package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageManager;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderModel;
import fr.soleil.comete.bean.datarecorder.model.utils.IPostRecordingModelListener;
import fr.soleil.comete.bean.datarecorder.model.utils.Parameter;
import fr.soleil.comete.bean.datarecorder.model.utils.ValidationListener;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.target.basic.BasicTextTarget;
import fr.soleil.data.service.IKey;

public class PostRecordingDialogModel {

    public static final String POST_RECORDING_VALUE = "<path>/nxextractor -t <model> -m $(__SessionKey__.FileAccess) -w $(__SessionKey__.Uid):$(__SessionKey__.Gid) ";
    public static final String POST_RECORDING_NXENTRY_END = "_NXENTRY_NAME_=$(_NXentry_name_)";
    public static final String DATA_FILE = " $(__DATAFILE__)";
    public static final int POST_RECORD_GLOBAL = 0;
    public static final int POST_RECORD_NXENTRY = 1;

    private String parameters;
    private String nxExtractorPath;
    private String scriptValue;
    private String scriptValueComplement;
    private String selectedScript;
    private String postRecordGlobal;
    private String postRecordNxEntry;
    private ScriptModel currentScriptModel;
    private boolean isChooseScriptMode;
    private String model = null;

    private BasicTextTarget targetDirectoryLabel;
    private IPostRecordingModelListener postRecordingModelListener;
    private int iType;

    private final List<ValidationListener> validationList = new ArrayList<ValidationListener>();

    private IKey postRecordingCommandModelKey;
    private IKey targetDirectoryModelKey;

    public PostRecordingDialogModel() {
        initModel();
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void initModel() {
        parameters = "";
        selectedScript = null;
        targetDirectoryLabel = new BasicTextTarget();
        isChooseScriptMode = false;
    }

    public void setType(int type) {
        iType = type;
    }

    public void setChooseScriptMode(boolean chooseScript) {
        isChooseScriptMode = chooseScript;
    }

    public void updatePostRecordingValues() {
        if (iType == POST_RECORD_GLOBAL) {
            scriptValue = postRecordGlobal;
            scriptValueComplement = postRecordNxEntry;
        } else {
            scriptValue = postRecordNxEntry;
            scriptValueComplement = postRecordGlobal;
        }
    }

    /**
     * get the default string for parameter line generated with default values
     */
    private String getDefaultParameterLine() {
        String[] tabParameters = getScriptInfo(selectedScript);
        if (tabParameters != null) {
            ArrayList<Parameter> paramList = new ArrayList<Parameter>();
            String strTargetDir = targetDirectoryLabel.getText();
            if (strTargetDir != null && !strTargetDir.trim().isEmpty()) {
                // iteration on tab
                for (int i = 2; i < tabParameters.length; i++) {
                    // get value which contains all infos about a parameter
                    String strParameterScriptInfos = tabParameters[i];
                    // create ObjectParameter
                    Parameter param = new Parameter(strParameterScriptInfos, strTargetDir);
                    paramList.add(param);
                }
                if (paramList.size() > 0) {
                    // update string parameter
                    String strParameters = "-D ";
                    for (Parameter param : paramList) {
                        if (param.getName() != null && param.getDefaultValue() != null) {
                            if (!param.getDefaultValue().equals("")) {
                                strParameters = strParameters + param.getName() + "=" + param.getDefaultValue() + ",";
                            }
                        }
                    }
                    if (strParameters.endsWith(",")) {
                        strParameters = strParameters.substring(0, strParameters.length() - 1);
                    }
                    return strParameters;
                }
            }
        }
        return null;
    }

    /**
     * This method allows to get description of model
     * 
     * @return String
     */
    public String getSelectedModelDescription(String scriptName) {
        String strDesc = null;
        // get infos about this model
        String[] tabInfos = getScriptInfo(scriptName);
        // System.out.println("tabInfos for " + selectedScript + "=" + Arrays.toString(tabInfos));
        DataRecorderMessageManager
                .notifyNewMessageDetected(("tabInfos for " + selectedScript + "=" + Arrays.toString(tabInfos)), false);
        if (tabInfos != null && tabInfos.length > 1) {
            // description is contained in the second item of the tab
            // first item is "source"
            strDesc = tabInfos[1];
        }
        if (strDesc != null && strDesc.equalsIgnoreCase("n/a")) {
            strDesc = "No description associated with this script.";
        }

        // System.out.println("strDesc=" + strDesc);
        DataRecorderMessageManager.notifyNewMessageDetected(("strDesc=" + strDesc), false);
        return strDesc;
    }

    private void updateModelParameters(String[] tabParameters) {
        Vector<Parameter> vectParam = new Vector<Parameter>();
        // get script's explanations
        String strExplanations = tabParameters[1];
        // iteration on tab
        /**
         * start iteration to second item, because first contains souce, and
         * second contains description
         */
        for (int i = 2; i < tabParameters.length; i++) {
            // get value which contains all infos about a parameter
            String strParameter = tabParameters[i];

            String strCurrentValueParameter = "";
            if (scriptValue == null) {
                scriptValue = "-D";
            }

            /** get index of "-D" */
            int iPos = scriptValue.indexOf("-D");

            if (iPos != -1) {
                strCurrentValueParameter = scriptValue.substring(scriptValue.indexOf("-D"), scriptValue.length());
            }

            // remove the string $(__DATAFILE__)
            if (strCurrentValueParameter != null) {
                strCurrentValueParameter = strCurrentValueParameter.replaceAll(DATA_FILE, "");
            }
            // create object Parameter with defaut parameter & current parameter
            // if it exists
            Parameter param = new Parameter(strParameter, strCurrentValueParameter);
            vectParam.add(param);

        }

        Vector<String> vectValues = postRecordingModelListener.getCustumModelParameters(vectParam, strExplanations);

        /** Click on OK */
        if (vectValues != null) {
            parameters = "-D ";
            // update string parameter
            for (int i = 0; i < vectValues.size(); i++) {
                parameters = parameters + vectValues.elementAt(i).toString() + ",";
            }

            if (iType == POST_RECORD_NXENTRY) {
                parameters = parameters + parameters + ",";
            }

            if (parameters.endsWith(",")) {
                parameters = parameters.substring(0, parameters.length() - 1);
            }
            postRecordingModelListener.executeConfirmation();
        }
    }

    public String computeValueToSave() {
        String strValue = "";
        /** Radio button : Choose Script */
        if (isChooseScriptMode) {
            if (currentScriptModel != null) {
                // replace value for nxextractor path , choosen model and target
                // dir from
                // m_strPostRecordingValue
                strValue = POST_RECORDING_VALUE.replaceAll("<path>", nxExtractorPath);
                if (nxExtractorPath == null || nxExtractorPath.equals("")) {
                    // if no path remove first slash
                    strValue = strValue.replaceFirst("/", "");
                }
                // get object model
                ScriptModel model = currentScriptModel;
                // get modelPath + modelName
                String strAbsModel = model.getAbsPath();
                strValue = strValue.replaceAll("<model>", strAbsModel);
                // add parameters
                if (parameters != null) {
                    if (iType == POST_RECORD_NXENTRY) {
                        if (parameters.trim().endsWith(POST_RECORDING_NXENTRY_END)) {
                            strValue = strValue + " " + parameters.trim() + " " + DATA_FILE;
                        } else {
                            strValue = strValue + " " + parameters.trim() + "," + POST_RECORDING_NXENTRY_END + " "
                                    + DATA_FILE;
                        }
                    } else {
                        strValue = strValue + " " + parameters.trim() + " " + DATA_FILE;
                    }
                } else {
                    if (iType == POST_RECORD_NXENTRY) {
                        strValue = strValue + " -D " + POST_RECORDING_NXENTRY_END + " " + DATA_FILE;
                    } else {
                        strValue = strValue + " " + DATA_FILE;
                    }
                }
            }
        }
        /** Radio button : Nothing */
        else {
            strValue = "";
        }

        // add third line to not write string "null"
        if (scriptValueComplement == null) {
            scriptValueComplement = "";
        }

        // keep it exclusive
        if (iType == POST_RECORD_GLOBAL) {
            // strValue = strValue + "|" + scriptValueComplement;
            strValue = strValue + "|";
        } else {
            // strValue = scriptValueComplement + "|" + strValue;
            strValue = "|" + strValue;
        }
        return strValue;
    }

    public void updateListSelectionModel(List<String> scriptList) {
        DefaultComboBoxModel<ScriptModel> model;
        boolean shouldListenToComboBox = false;
        if (scriptList == null) {
            model = new DefaultComboBoxModel<>();
        } else {
            ModelManager manager = new ModelManager(scriptList);
            Vector<ScriptModel> modelVector = manager.generateModelVector();
            if ((modelVector == null) || modelVector.isEmpty()) {
                model = new DefaultComboBoxModel<>();
                selectedScript = null;
            } else {
                model = new DefaultComboBoxModel<>(modelVector);
                selectedScript = modelVector.get(0).toString();
                shouldListenToComboBox = true;
            }
            modelVector = null;
        }
        postRecordingModelListener.updateListSelectionModel(scriptList, model, shouldListenToComboBox);
    }

    /**
     * This method allows to get model from value of attribute in device
     * 
     * @param String
     *            : strScript -> value of attribute PostRecordingValue
     * @return String : associated Model
     */
    public String getModelForScript(String strScript) {
        String strModelName = null;

        // model is contained in script like .... -t <model> -m 750
        // get first index of "-t"
        int iFirstIndex = strScript.indexOf("-t");
        int iLastIndex = strScript.indexOf("-m");
        if (iFirstIndex != -1 && iLastIndex != -1) {
            // get path of model and model between -t and -m
            String strPathAndModel = strScript.substring(iFirstIndex + 2, iLastIndex).trim();

            // get only the model (begin at the last "/" and the last "."
            /** model is like : .../../../modelName.ext */
            if (strPathAndModel.indexOf("/") != -1) {
                int iLastPosSlash = strPathAndModel.lastIndexOf("/");
                int iLastPosPOint = strPathAndModel.lastIndexOf(".");
                strModelName = strPathAndModel.substring(iLastPosSlash + 1, iLastPosPOint);
            } else {
                int iLastPosPOint = strPathAndModel.lastIndexOf(".");
                strModelName = strPathAndModel.substring(0, iLastPosPOint);
            }
        } else {
            strModelName = strScript;
        }
        return strModelName;
    }

    public void updateParameters(String scriptName) {
        String[] m_tabParameters = getScriptInfo(scriptName);
        if (m_tabParameters != null) {
            // get post recording values
            updatePostRecordingValues();
            updateModelParameters(m_tabParameters);
        }
        m_tabParameters = null;
    }

    public boolean isParametersEnabled() {
        String[] tabInfos = getScriptInfo(selectedScript);
        return tabInfos != null && tabInfos.length > 2;
    }

    public void resetParametersLine() {
        parameters = getDefaultParameterLine();
    }

    /**
     * This method allows to check if a path for the tool nxextractor is defined
     * 
     * @return boolean -> true if the path is defined
     */
    public boolean isNXExtractorPathAvailable() {
        boolean available = false;
        if (nxExtractorPath != null) {
            available = (!nxExtractorPath.trim().isEmpty());
        }
        return available;
    }

    public void setLoadingListener(IPostRecordingModelListener loadingListener) {
        this.postRecordingModelListener = loadingListener;
    }

    public void setNxExtractorPath(String pathNXextractor) {
        nxExtractorPath = pathNXextractor;
    }

    public String getPostRecordGlobal() {
        return postRecordGlobal;
    }

    public void setPostRecordGlobal(String postRecordGlobal) {
        this.postRecordGlobal = postRecordGlobal;
    }

    public String getPostRecordNxEntry() {
        return postRecordNxEntry;
    }

    public void setPostRecordNxEntry(String postRecordNxEntry) {
        this.postRecordNxEntry = postRecordNxEntry;
    }

    public void setSelectedScript(String selectedScript) {
        this.selectedScript = selectedScript;
    }

    public void setPostRecordingCommandModel(IKey commandKey) {
        postRecordingCommandModelKey = commandKey;
    }

    public void setTargetDirectoryModel(IKey attributeKey) {
        targetDirectoryModelKey = attributeKey;
    }

    public void addValidationListener(ValidationListener listener) {
        validationList.add(listener);
    }

    public void removeValidationListener(ValidationListener listener) {
        validationList.remove(listener);
    }

    public boolean hasValidationListeners() {
        return !validationList.isEmpty();
    }

    public void setAsLoaded() {
        postRecordingModelListener.setAsLoaded();
    }

    public void setAsLoading() {
        postRecordingModelListener.setAsLoading();
    }

    public void updateValueSelection(String reducedModel) {
        selectedScript = reducedModel;
        postRecordingModelListener.updateValueSelection(reducedModel);
    }

    public String getParameters() {
        return parameters;
    }

    public String getNxExtractorPath() {
        return nxExtractorPath;
    }

    public String getScriptValue() {
        return scriptValue;
    }

    public String getScriptValueComplement() {
        return scriptValueComplement;
    }

    public String getSelectedScript() {
        return selectedScript;
    }

    public void setCurrentScriptModel(ScriptModel currentScriptModel) {
        this.currentScriptModel = currentScriptModel;
        setSelectedScript(currentScriptModel.toString());
    }

    public IKey getPostRecordingCommandModelKey() {
        return postRecordingCommandModelKey;
    }

    public IKey getTargetDirectoryModelKey() {
        return targetDirectoryModelKey;
    }

    public List<ValidationListener> getValidationList() {
        return validationList;
    }

    public void connectDialog(StringScalarBox stringBox) {
        TangoKeyTool.registerRefreshed(targetDirectoryModelKey, false);
        stringBox.connectWidgetNoMetaData(targetDirectoryLabel, targetDirectoryModelKey);
    }

    public void clearConnections(StringScalarBox stringBox) {
        stringBox.disconnectWidgetFromAll(targetDirectoryLabel);
    }

    /**
     * Return the description according to the current selected script.
     * 
     * @param strAttrModel the complete script name
     * @return the part of the script corresponding to the simple name
     */
    private String[] getScriptInfo(String scriptName) {
        String[] result = null;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(model);
        if (proxy != null && scriptName != null && !scriptName.equalsIgnoreCase("do nothing")) {
            try {
                DeviceData devDataIn = new DeviceData();
                devDataIn.insert(scriptName);
                DeviceData devDataOut = proxy.command_inout(DataRecorderModel.GET_SCRIPTS_INFO_CMD, devDataIn);
                if (devDataOut != null) {
                    result = devDataOut.extractStringArray();
                }
            } catch (DevFailed e) {
                DataRecorderMessageManager.notifyExecuteCommandErrorDetected(model,
                        DataRecorderModel.GET_SCRIPTS_INFO_CMD, e);

                result = null;
            }
        }
        return result;
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Inner Class which represents a model with its absolute path composed with
     * : model's path, model's name & model's extension
     * 
     * @author MARECHAL
     */
    public class ScriptModel {
        private String m_strName = null;
        private String m_strPath = null;
        private String m_strExtension = null;

        /**
         * Constructor
         * 
         * @param String
         *            strAttrModel
         */
        public ScriptModel(String strAttrModel) {
            if (strAttrModel.contains("/")) {
                // get last position of "/"
                int iLastPosSlash = strAttrModel.lastIndexOf("/");
                m_strPath = strAttrModel.substring(0, iLastPosSlash + 1);
                // get last pos of "."
                int iLastPosPoint = strAttrModel.lastIndexOf(".");
                m_strName = strAttrModel.substring(iLastPosSlash + 1, iLastPosPoint);
                m_strExtension = strAttrModel.substring(iLastPosPoint, strAttrModel.length());
            } else {
                int iLastPosPoint = strAttrModel.lastIndexOf(".");
                m_strName = strAttrModel.substring(0, iLastPosPoint);
                m_strPath = "";
                m_strExtension = strAttrModel.substring(iLastPosPoint, strAttrModel.length());
            }
        }

        /**
         * Get model's name
         */
        @Override
        public String toString() {
            return m_strName;
        }

        /**
         * Get model's absolute path
         */
        public String getAbsPath() {
            return m_strPath + m_strName + m_strExtension;
        }

    }

    /**
     * Inner class to manage All models
     * 
     * @author MARECHAL
     * 
     */
    private class ModelManager {
        private final List<String> stringModelList; // attr "scripts" in device

        /**
         * Constructor
         * 
         * @param String
         *            [] tabModels which contains absolute path of each model
         */
        public ModelManager(List<String> listModels) {
            stringModelList = listModels;
        }

        /**
         * Allows to create and return a {@link Vector} which contains object
         * Model for each absolute model' path contained in attr "scripts"
         */
        public Vector<ScriptModel> generateModelVector() {
            Vector<ScriptModel> modelVector = new Vector<ScriptModel>();
            for (String model : stringModelList) {
                modelVector.add(new ScriptModel(model));
            }
            return modelVector;
        }
    }

}
