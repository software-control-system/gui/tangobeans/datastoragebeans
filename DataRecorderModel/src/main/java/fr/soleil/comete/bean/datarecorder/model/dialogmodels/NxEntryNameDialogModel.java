package fr.soleil.comete.bean.datarecorder.model.dialogmodels;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.data.service.IKey;

public class NxEntryNameDialogModel implements INxEntryNameModelListener {

    private static final String EXP_NAME = "$(_experiment_name_)";
    private static final String EXP_INDEX = "$(_experiment_index_)";
    private static final String ACQ_NAME = "$(_acquisition_name_)";
    private static final String ACQ_INDEX = "$(_acquisition_index_)";
    public static final String[] MODEL_POSSIBLE_VALUES = new String[] { EXP_NAME, ACQ_NAME, EXP_NAME + "_" + ACQ_NAME,
            EXP_NAME + "_" + EXP_INDEX + "_" + ACQ_NAME + "_" + ACQ_INDEX, EXP_NAME + "_" + EXP_INDEX + "_" + ACQ_NAME,
            EXP_NAME + "_" + EXP_INDEX + "_" + ACQ_INDEX, ACQ_NAME + "_" + ACQ_INDEX + "_" + EXP_NAME,
            ACQ_NAME + "_" + ACQ_INDEX + "_" + EXP_INDEX, EXP_NAME + "_" + EXP_INDEX, ACQ_NAME + "_" + ACQ_INDEX };

    private IKey acquisitionNameModelKey;
    private IKey experimentNameModelKey;
    private IKey incrementExperimentIndexModelKey;
    private IKey resetExperimentIndexModelKey;
    private IKey nxEntryNameModel;
    private TextTargetRedirector experimentNameRedirector;
    private TextTargetRedirector acquisitionNameRedirector;
    private TextTargetRedirector nxEntryNameRedirector;
    private TextTargetRedirector defaultValueRedirector;
    private TextTargetRedirector experimentNameTextField;
    private String experimentName;
    private String acquisitionName;
    private String nxEntryName;
    private String defaultValue;
    private IKey defaultModelKey;
    private IKey nxEntryNameWriteModelKey;
    private IKey experimentNameTextFieldKey;
    private final INxEntryNameModelListener listener;

    public NxEntryNameDialogModel(INxEntryNameModelListener listener) {
        this.listener = listener;
        initRedirectors();
    }

    public void setAcquisitionNameModel(IKey attributeKey) {
        acquisitionNameModelKey = attributeKey;
    }

    public void setExperimentNameModel(IKey attributeKey) {
        experimentNameModelKey = attributeKey;
    }

    public void setIncrementExperimentIndexModel(IKey commandKey) {
        incrementExperimentIndexModelKey = commandKey;
    }

    public void setResetExperimentIndexModel(IKey commandKey) {
        resetExperimentIndexModelKey = commandKey;
    }

    public void setNxEntryNameModel(IKey attributeKey) {
        nxEntryNameModel = attributeKey;
    }

    public IKey getAcquisitionNameModelKey() {
        return acquisitionNameModelKey;
    }

    public IKey getExperimentNameModelKey() {
        return experimentNameModelKey;
    }

    public IKey getIncrementExperimentIndexModelKey() {
        return incrementExperimentIndexModelKey;
    }

    public IKey getResetExperimentIndexModelKey() {
        return resetExperimentIndexModelKey;
    }

    public IKey getNxEntryNameModel() {
        return nxEntryNameModel;
    }

    public IKey getExperimentNameTextField() {
        return experimentNameTextFieldKey;
    }

    public void initRedirectors() {
        // Experiment Name
        experimentNameRedirector = new TextTargetRedirector() {

            @Override
            public void methodToRedirect(String data) {
                experimentName = data;
                setExperimentName(experimentName);

            }
        };
        // Acquisition Name
        acquisitionNameRedirector = new TextTargetRedirector() {

            @Override
            public void methodToRedirect(String data) {

                acquisitionName = data;
                setAcquisitionName(acquisitionName);
            }
        };

        // hidden components
        nxEntryNameRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                nxEntryName = data;
            }
        };
        defaultValueRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                defaultValue = data;
            }
        };
    }

    public void connectTargetRedirectors(StringScalarBox stringBox) {
        stringBox.connectWidget(experimentNameRedirector, getExperimentNameModelKey());
        stringBox.connectWidget(acquisitionNameRedirector, getAcquisitionNameModelKey());
        stringBox.connectWidget(defaultValueRedirector, getDefaultModelKey());
        stringBox.connectWidget(nxEntryNameRedirector, getNxEntryNameWriteModelKey());
        stringBox.connectWidget(experimentNameTextField, getExperimentNameModelKey());
    }

    public void disconnectTargetRedirectors(StringScalarBox stringBox) {
        stringBox.disconnectWidgetFromAll(experimentNameRedirector);
        stringBox.disconnectWidgetFromAll(acquisitionNameRedirector);
        stringBox.disconnectWidgetFromAll(nxEntryNameRedirector);
        stringBox.disconnectWidgetFromAll(defaultValueRedirector);

    }

    public String getAcquisitionName() {
        return acquisitionName;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setDefaultModel(IKey commandKey) {
        defaultModelKey = commandKey;
    }

    public void setNxEntryNameWriteModel(IKey writeAttributeKey) {
        nxEntryNameWriteModelKey = writeAttributeKey;
    }

    public IKey getDefaultModelKey() {
        return defaultModelKey;
    }

    public IKey getNxEntryNameWriteModelKey() {
        return nxEntryNameWriteModelKey;
    }

    public String[] getModelPossibleValues() {
        return MODEL_POSSIBLE_VALUES;
    }

    public String getNxEntryName() {
        return nxEntryName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void resetDefaultValue() {
        defaultValueRedirector.setParameter(defaultValue);
        defaultValueRedirector.execute();
    }

    @Override
    public void setAcquisitionName(String acquisitionName) {
        listener.setAcquisitionName(acquisitionName);

    }

    @Override
    public void setExperimentName(String experimentName) {
        listener.setExperimentName(experimentName);
    }
}
