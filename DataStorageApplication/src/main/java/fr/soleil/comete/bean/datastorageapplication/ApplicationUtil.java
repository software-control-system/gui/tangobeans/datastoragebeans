/*******************************************************************************
 * Copyright (c) 2013-2014 Synchrotron SOLEIL.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 ******************************************************************************/

package fr.soleil.comete.bean.datastorageapplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationUtil {

    private static final String CONFIG_FILE_PROPERTY = "DATASTORAGE_CONFIGFILE";
    private static final String DATASTORAGE_NODE = "DataStorage";
    private static final String DATABROWSER_NODE = "DataBrowserPath";
    public static final String DATABROWSER_DEFAULT_PATH = "databrowser";
    private static String databrowserpath = DATABROWSER_DEFAULT_PATH;

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationUtil.class);

    public static String getFileJarName(final Class<?> clazz) {
        String jarFileName = getCompleteFileJarName(clazz);
        if ((jarFileName != null) && !jarFileName.isEmpty()) {
            int indexEnd = jarFileName.indexOf("-");
            if (indexEnd > -1) {
                jarFileName = jarFileName.substring(0, indexEnd);
            }
        }
        return jarFileName;
    }

    private static String getCompleteFileJarName(Class<?> clazz) {
        String completeJarFilename = null;
        if (clazz != null) {
            CodeSource source = clazz.getProtectionDomain().getCodeSource();
            URL location = source.getLocation();
            String path = location.getPath();

            try {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                File file = new File(decodedPath);
                String name = file.getName();

                if (name.endsWith(".jar")) {
                    completeJarFilename = name;
                }
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Impossible to read jar file from class {} because {}", clazz, e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
        return completeJarFilename;
    }

    public static String getFileJarVersion(Class<?> clazz) {
        String version = null;
        if (clazz != null) {
            String jarFileName = getCompleteFileJarName(clazz);
            if ((jarFileName != null) && !jarFileName.isEmpty()) {
                int indexEnd = jarFileName.lastIndexOf(".jar");
                int indexBegin = jarFileName.lastIndexOf("-");
                if ((indexEnd > -1) && (indexBegin > -1)) {
                    version = jarFileName.substring(indexBegin + 1, indexEnd);
                }
            }
        }
        return version;
    }

    // Save databrowser path in a configuration file see JIRA EXPDATA-466
    public static void loadSettings() {
        // Read the property configuration
        String configFilename = System.getProperty(CONFIG_FILE_PROPERTY);

        // If its exist read and save the configuration in the file
        if (configFilename != null && !configFilename.isEmpty()) {
            LOGGER.info("Load Settings...{}", configFilename);

            // Check the if the file exist
            File configFile = new File(configFilename);

            // first create nodes
            Preferences dataStorageNode = null;
            Preferences databrowserNode = null;

            try {
                dataStorageNode = Preferences.userRoot().node(DATASTORAGE_NODE);
                databrowserNode = dataStorageNode.node(DATABROWSER_NODE);

                // If the file does not exist create it and save default data
                if (!configFile.exists()) {
                    configFile.createNewFile();
                    databrowserNode.put(DATABROWSER_NODE, DATABROWSER_DEFAULT_PATH);
                    dataStorageNode.flush();
                    OutputStream out = new FileOutputStream(configFilename);
                    dataStorageNode.exportSubtree(out);
                    out.close();
                } else {// Read the information from file
                    InputStream in = new FileInputStream(configFilename);
                    Preferences.importPreferences(in);
                    if (in != null) {
                        in.close();
                    }
                    databrowserpath = databrowserNode.get(DATABROWSER_NODE, DATABROWSER_DEFAULT_PATH);
                    LOGGER.info("Databrowser path {}", databrowserpath);
                    //System.out.println("databrowserpath=" + databrowserpath);
                }
            } catch (FileNotFoundException e) {
                LOGGER.error("{} file does not exist", configFilename);
                LOGGER.debug("Stack trace", e);
            } catch (IOException e) {
                LOGGER.error("Impossible to read file {} because {}", configFilename, e.getMessage());
                LOGGER.debug("Stack trace", e);
            } catch (InvalidPreferencesFormatException e) {
                LOGGER.error("Unrecognize format for file {} because {}", configFilename, e.getMessage());
                LOGGER.debug("Stack trace", e);
            } catch (Exception e1) {
                LOGGER.error("Impossible to create root preferences {}", e1.getMessage());
                LOGGER.debug("Stack trace", e1);
            }

        } else {
            LOGGER.warn("No setting file found defined property {}", CONFIG_FILE_PROPERTY);
            LOGGER.info("Default path for databrowser is used {}", databrowserpath);
        }
    }

    public static String getDataBrowserPath() {
        return databrowserpath;
    }

}
