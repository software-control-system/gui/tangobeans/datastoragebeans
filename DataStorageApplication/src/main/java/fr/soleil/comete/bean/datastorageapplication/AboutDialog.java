package fr.soleil.comete.bean.datastorageapplication;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import fr.soleil.lib.project.swing.dialog.JDialogUtils;

@Deprecated
public class AboutDialog extends JDialog {

    private static final long serialVersionUID = 9036134672630952541L;

    private static final String ABOUT_FILENAME = "About.html";

    private JEditorPane htmlPanel;

    // this tells if the url is loaded or not
    private AtomicBoolean urlIsLoaded = new AtomicBoolean(false);
    // swingworker that will wait until the url is loaded
    private WaitingWorker waitingWorker;

    /**
     * This class is only here to delay the dialog visibility until the url is
     * loaded!! Once the url is loaded, the JEditorPane knows its size, and
     * pack() on the dialog will be accurate.
     * 
     * Either propertyChange or doInBackground can be called first, so we need
     * to check if we have to wait, in doInBackground.
     * 
     * @author mainguy
     */
    private class WaitingWorker extends SwingWorker<Void, Void> implements PropertyChangeListener {

        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            // the page has changed, meaning it is loaded
            synchronized (this) {
                urlIsLoaded.set(true);
                notify();
            }
        }

        @Override
        protected Void doInBackground() throws Exception {
            synchronized (this) {
                if (!urlIsLoaded.get()) {
                    wait(100);
                }
            }
            return null;
        }

        @Override
        protected void done() {
            reallySetVisible();
        }
    }

    public AboutDialog(final Frame owner) {
        this(owner, null);
    }

    public AboutDialog(final Frame owner, final String title) {
        super(owner);

        setModal(true);
        setTitle(title);

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        waitingWorker = new WaitingWorker();

        htmlPanel = new JEditorPane();
        htmlPanel.setEditorKit(new HTMLEditorKit());
        htmlPanel.setEditable(false);
        // listen for the "page" property, useful for worker to know when load
        // is completed
        htmlPanel.addPropertyChangeListener("page", waitingWorker);

        // let's mimic panel background color
        htmlPanel.setBackground(UIManager.getColor("Panel.background"));

        htmlPanel.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    // if uri is a file, get a temp version extracted from the
                    // jar and ask OS to open it,
                    // else ask OS to browse the link
                    if (Desktop.isDesktopSupported()) {
                        try {
                            URI uri = e.getURL().toURI();
                            String scheme = uri.getScheme();
                            // System.out.println("getURL().getFile()=" + file);

                            if (scheme.equalsIgnoreCase("file")) {
                                Desktop.getDesktop().browse(uri);
                            }
                        } catch (URISyntaxException e1) {
                            e1.printStackTrace();
                        } catch (IOException e1) {

                            e1.printStackTrace();
                        }
                    }
                }
            }
        });

        URL aboutURL = DataStorageApplication.class.getResource(ABOUT_FILENAME);
        try {
            htmlPanel.setPage(aboutURL);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // allow to close the dialog with Esc
        JDialogUtils.installEscapeCloseOperation(this);
    }

    private void layoutComponents() {
        JScrollPane scrollPane = new JScrollPane(htmlPanel);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);

        setContentPane(contentPane);
    }

    private void reallySetVisible() {
        // adjust to real size
        pack();
        // center the dialog
        setLocationRelativeTo(getOwner());
        // now set it visible
        super.setVisible(true);
    }

    /**
     * Overridden to ensure the url is loaded before showing dialog
     * 
     * @see java.awt.Dialog#setVisible(boolean)
     */
    @Override
    public void setVisible(final boolean b) {
        if (b) {
            if (urlIsLoaded.get()) {
                // skip waiting worker
                reallySetVisible();
            } else {
                waitingWorker.execute();
            }
        } else {
            super.setVisible(b);
        }
    }

    public static void main(final String[] args) {
        AboutDialog aboutDialog = new AboutDialog(null, "About");

        aboutDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        aboutDialog.setLocationRelativeTo(null);
        aboutDialog.setVisible(true);
    }
}
