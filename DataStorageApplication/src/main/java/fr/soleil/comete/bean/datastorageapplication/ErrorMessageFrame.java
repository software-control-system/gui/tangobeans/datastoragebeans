package fr.soleil.comete.bean.datastorageapplication;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.bean.authentication.AuthServerMessageListener;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageListener;
import fr.soleil.comete.bean.experimentalframe.ExperimentalFrameMessageListener;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageListener;

@Deprecated
public class ErrorMessageFrame extends JFrame implements AuthServerMessageListener, DataRecorderMessageListener,
        ExperimentalFrameMessageListener, TechnicalDataMessageListener {

    private static final long serialVersionUID = 4763032469777338159L;

    private final JPanel mainPanel = new JPanel();
    private final JScrollPane scrollPane = new JScrollPane();
    private final JTextArea textArea = new JTextArea();
    private final JButton clearButton = new JButton("Clear");
    private static final Logger LOGGER = LoggerFactory.getLogger(DataStorageApplication.class);

    public ErrorMessageFrame() {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setTitle("Data storage application error message");
        setSize(400, 600);
        setAlwaysOnTop(true);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        mainPanel.add(clearButton, BorderLayout.SOUTH);
        scrollPane.setViewportView(textArea);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        setContentPane(mainPanel);
        textArea.setText("");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");

            }
        });
    }

    @Override
    public void newErrorDetected(Exception e, boolean displayToFront) {
        LOGGER.error(e.getMessage());
        LOGGER.debug("Stack trace", e);

        String currentText = textArea.getText();
        String date = (new Date()).toString();
        String newText = currentText + "\n*************** " + date + " *****************\n" + e.getMessage();
        textArea.setText(newText);
        if (displayToFront) {
            setVisible(true);
        }
    }

    @Override
    public void newMessageDetected(String message, boolean displayToFront) {
        if (displayToFront) {
            LOGGER.info(message);
            String currentText = textArea.getText();
            String date = (new Date()).toString();
            String newText = currentText + "\n*************** " + date + " *****************\n" + message;
            textArea.setText(newText);
            setVisible(true);
        } else {
            LOGGER.trace(message);
        }
    }

}
