package fr.soleil.comete.bean.datastorageapplication;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXErrorPane;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.AuthServerMessageManager;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageManager;
import fr.soleil.comete.bean.datarecorderbean.DataRecorderBean;
import fr.soleil.comete.bean.datarecorderbean.details.DataRecorderDetailsBean;
import fr.soleil.comete.bean.experimentalframe.ExperimentalFrameMessageManager;
import fr.soleil.comete.bean.technicaldata.TechnicalDataMessageManager;
import fr.soleil.comete.bean.technicaldata.full.TechnicalDataFrame;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.lib.project.swing.icons.Icons;

@Deprecated
public class DataStorageApplication extends JFrame implements WindowListener {

    private static final long serialVersionUID = -3513693093386184499L;

    public static final Icons ICONS = new Icons("fr.soleil.comete.bean.datastorageapplication.icons");
    public static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.bean.datastorageapplication.messages");

    private static final String UNKNOWN_DEVICE_STATE = "unknown";
    private static final String TECHNICAL_DATA_DEVICE_ATTR = "technicalDataDevice";

    private static final String STATE_ATTR = "State";

    private static final ErrorMessageFrame errorMessageFrame = new ErrorMessageFrame();

    private StringScalarBox stringBox;
    private TextTargetRedirector stateRedirector;

    private String technicalDataPath = null;
    private String dataRecorderPath = null;

    private DataStorageBean dataStorageBean;

    private JMenuBar menuBar;
    private JToolBar toolBar;
    private JButton dataBrowserButton;
    private JButton technicalDataButton;

    private AboutDialog aboutDialog;

    private TechnicalDataFrame technicalDataFrame = null;

    // Bath executor to monitoring device
    public final static BatchExecutor batchExecutor = new BatchExecutor();

    public DataStorageApplication(String dataRecorderPath) {

        String frameTitle = MESSAGES.getString("DataStorageApplication.Title");
        String version = ApplicationUtil.getFileJarVersion(DataStorageApplication.class);
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + " " + version;
        }

        setTitle(frameTitle);
        addWindowListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);

        this.dataRecorderPath = dataRecorderPath;

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);

        initMenuBar();

        ImageIcon dataBrowserIcon = ICONS.getIcon("Action.OpenDataBrowser");
        dataBrowserButton = new JButton(MESSAGES.getString("Action.OpenDataBrowser"), dataBrowserIcon);

        String dataBrowserPath = ApplicationUtil.getDataBrowserPath();
        File file = new File(dataBrowserPath);
        dataBrowserButton.setEnabled(file.exists() || dataBrowserPath.equals(ApplicationUtil.DATABROWSER_DEFAULT_PATH));
        if (dataBrowserButton.isEnabled()) {
            dataBrowserButton.setToolTipText("This action will execute " + dataBrowserPath);
        } else {
            dataBrowserButton.setToolTipText(dataBrowserPath + " executable not found ");
        }

        batchExecutor.setBatch(dataBrowserPath);

        dataBrowserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> param = getCurrentFiles();
                if (param != null && !param.isEmpty()) {
                    batchExecutor.setBatchParameters(param);
                }
                try {
                    batchExecutor.execute();
                } catch (Exception exc) {
                    JXErrorPane.showDialog(exc);
                }
            }
        });

        ImageIcon technicalDataIcon = ICONS.getIcon("Action.OpenTechnicalData");
        technicalDataButton = new JButton(MESSAGES.getString("Action.OpenTechnicalData"), technicalDataIcon);
        technicalDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (technicalDataFrame == null) {
                    technicalDataFrame = new TechnicalDataFrame(dataRecorderPath);
                }
                technicalDataFrame.setVisible(true);
                technicalDataFrame.toFront();
            }
        });

        toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.add(dataBrowserButton);
        toolBar.addSeparator(new Dimension(5, 0));
        toolBar.add(technicalDataButton);

        stateRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                boolean deviceDead = data.equalsIgnoreCase(UNKNOWN_DEVICE_STATE);
                technicalDataButton.setEnabled(!deviceDead);
            }
        };

        // read the associated devices attributes from DataRecorder device
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(dataRecorderPath);
        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(TECHNICAL_DATA_DEVICE_ATTR);
                technicalDataPath = deviceAttribute.extractString();
            } catch (DevFailed exception) {
                TechnicalDataMessageManager.notifyReadAttributeErrorDetected(dataRecorderPath,
                        TECHNICAL_DATA_DEVICE_ATTR, exception);
            }
        }

        TangoKey stateKey = new TangoKey();
        TangoKeyTool.registerAttribute(stateKey, technicalDataPath, STATE_ATTR);

        stringBox.connectWidgetNoMetaData(stateRedirector, stateKey);

        dataStorageBean = new DataStorageBean();
        dataStorageBean.setModel(dataRecorderPath);
        dataStorageBean.start();
    }

    private List<String> getCurrentFiles() {
        List<String> currentFiles = null;
        if (dataStorageBean != null) {
            DataRecorderBean dataRecorderBean = dataStorageBean.getDataRecorderBean();
            if (dataRecorderBean != null) {
                DataRecorderDetailsBean dataRecorderDetailsBean = dataRecorderBean.getDataRecorderDetailsBean();
                if (dataRecorderDetailsBean != null) {
                    currentFiles = dataRecorderDetailsBean.getPathToCurrentFiles();
                }
            }
        }
        return currentFiles;
    }

    private void initMenuBar() {
        menuBar = new JMenuBar();

        JMenuItem exitMenuItem = new JMenuItem(new AbstractAction(MESSAGES.getString("Action.Exit")) {
            private static final long serialVersionUID = 4239843677014722896L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                // tell the frame to close itself
                JFrame topFrame = DataStorageApplication.this;
                WindowEvent windowClosing = new WindowEvent(topFrame, WindowEvent.WINDOW_CLOSING);
                topFrame.dispatchEvent(windowClosing);
            }
        });

        JMenuItem aboutMenuItem = new JMenuItem(new AbstractAction(MESSAGES.getString("Action.About")) {
            private static final long serialVersionUID = -7636865485381510269L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (aboutDialog == null) {
                    aboutDialog = new AboutDialog(DataStorageApplication.this, MESSAGES.getString("About.Title"));
                }
                aboutDialog.setVisible(true);
            }
        });

        JMenu fileMenu = new JMenu(MESSAGES.getString("Menu.File"));
        fileMenu.add(exitMenuItem);

        JMenu helpMenu = new JMenu(MESSAGES.getString("Menu.Help"));
        helpMenu.add(aboutMenuItem);

        menuBar.add(fileMenu);
        // set the help menu on the right
        // menuBar.add(Box.createHorizontalGlue());
        menuBar.add(helpMenu);
    }

    private void layoutComponents() {
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());

        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(dataStorageBean, BorderLayout.CENTER);

        setContentPane(contentPane);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        stringBox.disconnectWidgetFromAll(stateRedirector);

        dataStorageBean.clearGUI();
        dataStorageBean.stop();
    }

    @Override
    public void windowOpened(WindowEvent e) {
        // nop
    }

    @Override
    public void windowClosed(WindowEvent e) {
        // nop
    }

    @Override
    public void windowIconified(WindowEvent e) {
        // nop
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // nop
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // nop
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // nop
    }

    private static void createAndShowGUI(final String... args) {
        // Init the Error
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if ((input != null) && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            final DataStorageApplication dataStorageApplication = new DataStorageApplication(deviceName);
            List<Image> iconList = new ArrayList<Image>();
            iconList.add(ICONS.getIcon("Application.icon.24").getImage());
            iconList.add(ICONS.getIcon("Application.icon.32").getImage());
            iconList.add(ICONS.getIcon("Application.icon.40").getImage());
            iconList.add(ICONS.getIcon("Application.icon.48").getImage());
            iconList.add(ICONS.getIcon("Application.icon.64").getImage());
            iconList.add(ICONS.getIcon("Application.icon.72").getImage());
            iconList.add(ICONS.getIcon("Application.icon.96").getImage());
            dataStorageApplication.setIconImages(iconList);
            dataStorageApplication.pack();
            errorMessageFrame.setIconImages(iconList);
            AuthServerMessageManager.addMessageListener(errorMessageFrame);
            DataRecorderMessageManager.addMessageListener(errorMessageFrame);
            ExperimentalFrameMessageManager.addMessageListener(errorMessageFrame);
            TechnicalDataMessageManager.addMessageListener(errorMessageFrame);

            Dimension dimension = dataStorageApplication.getSize();
            dataStorageApplication.setSize((int) dimension.getWidth() + 400, (int) dimension.getHeight());
            dataStorageApplication.setLocationRelativeTo(null);
            dataStorageApplication.setVisible(true);

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    dataStorageApplication.toFront();
                    Container contentPane = dataStorageApplication.getContentPane();
                    if (contentPane instanceof JComponent) {
                        ((JComponent) contentPane).grabFocus();
                    }
                }
            };

            if (SwingUtilities.isEventDispatchThread()) {
                runnable.run();
            } else {
                SwingUtilities.invokeLater(runnable);
            }
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String[] args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);
        ApplicationUtil.loadSettings();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
