package fr.soleil.comete.bean.datastorageapplication;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.authentication.devicestate.AuthDeviceStateBean;
import fr.soleil.comete.bean.datarecorder.model.DataRecorderMessageManager;
import fr.soleil.comete.bean.datarecorderbean.DataRecorderBean;
import fr.soleil.comete.bean.experimentalframe.ExperimentalFrameBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

@Deprecated
public class DataStorageBean extends AbstractTangoBox {

    public static final Color TITLE_COLOR = new Color(4, 139, 154);

    private static final long serialVersionUID = 2820824479468851143L;

    private static final String AUTH_SERVER_DEVICE_ATTR = "authServerDevice";
    private static final String EXPERIMENTAL_FRAME_DEVICE_ATTR = "experimentalFrameDevice";

    private DataRecorderBean dataRecorderBean;
    private AuthDeviceStateBean authDeviceStateBean;
    private ExperimentalFrameBean experimentalFrameBean;

    public DataStorageBean() {
        setManageChildrenStartStopAndRefresh(true);

        // TODO

        initComponents();
        layoutComponents();

    }

    private void initComponents() {
        dataRecorderBean = new DataRecorderBean();
        authDeviceStateBean = new AuthDeviceStateBean();
        experimentalFrameBean = new ExperimentalFrameBean();

    }

    private void layoutComponents() {
        TitledBorder dataRecTitledBorder = new TitledBorder("DataRecorder");
        dataRecTitledBorder.setTitleColor(TITLE_COLOR);
        dataRecorderBean.setBorder(
                BorderFactory.createCompoundBorder(dataRecTitledBorder, BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        TitledBorder expFramTitledBorder = new TitledBorder("Experimental Frame");
        expFramTitledBorder.setTitleColor(TITLE_COLOR);
        experimentalFrameBean.setBorder(
                BorderFactory.createCompoundBorder(expFramTitledBorder, BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        TitledBorder titledBorder = new TitledBorder("Session Key");
        titledBorder.setTitleColor(TITLE_COLOR);
        CompoundBorder border = BorderFactory.createCompoundBorder(titledBorder,
                BorderFactory.createEmptyBorder(5, 5, 5, 5));
        authDeviceStateBean.setBorder(border);

        JPanel topPanel = new JPanel(new BorderLayout(5, 0));
        topPanel.add(dataRecorderBean, BorderLayout.CENTER);
        topPanel.add(authDeviceStateBean, BorderLayout.EAST);

        JSplitPane vSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
        vSplit.setTopComponent(topPanel);
        vSplit.setBottomComponent(experimentalFrameBean);
        vSplit.setResizeWeight(1);// extra space for top panel

        setLayout(new BorderLayout());
        add(vSplit, BorderLayout.CENTER);
    }

    @Override
    public synchronized void setModel(String model) {
        super.setModel(model);

        String authServerDeviceName = null;
        String experimentalFrameDeviceName = null;

        // read the associated devices attributes from DataRecorder device
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(model);
        if (proxy != null) {
            String attributeName = EXPERIMENTAL_FRAME_DEVICE_ATTR;
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute(attributeName);
                experimentalFrameDeviceName = deviceAttribute.extractString();
                attributeName = AUTH_SERVER_DEVICE_ATTR;
                deviceAttribute = proxy.read_attribute(attributeName);
                authServerDeviceName = deviceAttribute.extractString();
            } catch (DevFailed exception) {
                DataRecorderMessageManager.notifyReadAttributeErrorDetected(model, attributeName, exception);
            }
        }

        dataRecorderBean.setModel(model);
        authDeviceStateBean.setModel(authServerDeviceName);
        // suspicious side effect in frozen version: initialized with -1 instead of 0 or 1
        authDeviceStateBean.setAuthAdminMode(0);
        experimentalFrameBean.setModel(experimentalFrameDeviceName);
    }

    @Override
    protected void refreshGUI() {
        if ((model != null) && !model.isEmpty()) {

        }

        // TODO

    }

    @Override
    protected void clearGUI() {
        // TODO

    }

    public DataRecorderBean getDataRecorderBean() {
        return dataRecorderBean;
    }

    @Override
    protected void onConnectionError() {
        // nop
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nop
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nop
    }

    private static void createAndShowGUI(final String[] args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if ((input != null) && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            DataStorageBean bean = new DataStorageBean();

            bean.setModel(deviceName);
            bean.start();

            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args
     *            [0] : path of the device ds_DataRecorder
     **************************************************************************/
    public static void main(final String[] args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
