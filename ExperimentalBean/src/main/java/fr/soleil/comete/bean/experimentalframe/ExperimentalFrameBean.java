package fr.soleil.comete.bean.experimentalframe;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.experimentalframe.modifiable.IModifiableComponentListener;
import fr.soleil.comete.bean.experimentalframe.modifiable.ModifiableComponentEvent;
import fr.soleil.comete.bean.experimentalframe.util.MyComboBox;
import fr.soleil.comete.bean.experimentalframe.util.MyTextArea;
import fr.soleil.comete.bean.experimentalframe.util.MyTextField;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import net.miginfocom.swing.MigLayout;

public class ExperimentalFrameBean extends AbstractTangoBox implements IModifiableComponentListener {

    private static final long serialVersionUID = -8613319919136574880L;

    private static final String EXPERIMENTAL_FRAME_DEVICE_ATTR = "experimentalFrameDevice";

    private static final String TYPE_LIST_ATTR = "typeList";
    private static final String TYPE_ATTR = "Type";
    private static final String GROUP_ATTR = "Group";
    private static final String LOCAL_CONTACT_LIST_ATTR = "localContactList";
    private static final String LOCAL_CONTACT_ATTR = "LocalContact";
    private static final String LOG_BOOK_REF_ATTR = "LogBookRef";
    private static final String USER_LIST_ATTR = "UserList";
    private static final String COMMENT_CONDITIONS_ATTR = "CommentConditions";
    private static final String COMMENT_SAMPLE_ATTR = "CommentSample";

    private static final String TYPE = "Type:";
    private static final String GROUP = "Group:";
    private static final String LOCAL_CONTACT = "Local Contact:";
    private static final String LOG_BOOK_REF = "LogBook Ref:";
    private static final String USER_LIST = "User List:";

    private static final String COMMENT_CONDITIONS = "Comment Conditions:";
    private static final String COMMENT_SAMPLE = "Comment Sample:";

    private static final String APPLY_BUTTON_TEXT = "Apply";
    private static final String REVERT_BUTTON_TEXT = "Revert";

    private final Icon APPLY_ICON;
    private final Icon REVERT_ICON;

    private StringMatrixBox stringMatrixBox;

    private MyComboBox typeCombo;
    private MyComboBox localContactCombo;

    private MyTextField typeField;
    private MyTextField localContactField;
    private MyTextField groupField;
    private MyTextField logBookRefField;
    private MyTextField userListField;

    private MyTextArea commentConditionsArea;
    private MyTextArea commentSampleArea;

    private AbstractAction applyAction;

    private JButton applyButton;
    private JButton revertButton;

    private boolean displayMessageOnConnectionError;

    public ExperimentalFrameBean() {
        APPLY_ICON = iconsManager.getIcon("ExperimentalFrame.Apply");
        REVERT_ICON = iconsManager.getIcon("ExperimentalFrame.Revert");

        displayMessageOnConnectionError = true;

        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        // TODO la combo doit etre editable
        typeCombo = new MyComboBox();
        typeCombo.addModifiableComponentListener(this);

        localContactCombo = new MyComboBox();
        localContactCombo.addModifiableComponentListener(this);

        typeField = new MyTextField();
        typeField.addModifiableComponentListener(this);

        localContactField = new MyTextField();
        localContactField.addModifiableComponentListener(this);

        groupField = new MyTextField();
        groupField.addModifiableComponentListener(this);

        logBookRefField = new MyTextField();
        logBookRefField.addModifiableComponentListener(this);

        userListField = new MyTextField();
        userListField.addModifiableComponentListener(this);

        commentConditionsArea = new MyTextArea();
        commentConditionsArea.addModifiableComponentListener(this);

        commentSampleArea = new MyTextArea();
        commentSampleArea.addModifiableComponentListener(this);

        applyAction = new AbstractAction() {
            private static final long serialVersionUID = -2199530495927544842L;

            @Override
            public void actionPerformed(ActionEvent e) {
                apply();
            }
        };

        applyButton = new JButton(applyAction);
        applyButton.setText(APPLY_BUTTON_TEXT);
        applyButton.setIcon(APPLY_ICON);

        revertButton = new JButton(REVERT_BUTTON_TEXT, REVERT_ICON);
        revertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                revert();
            }
        });

        // listen to Enter key on the panel, to apply any pending changes
        KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(enter, "APPLY");
        getActionMap().put("APPLY", applyAction);
    }

    private void layoutComponents() {
        JLabel typeLabel = new JLabel(TYPE);
        JLabel groupLabel = new JLabel(GROUP);
        JLabel localContactLabel = new JLabel(LOCAL_CONTACT);
        JLabel logBookRefLabel = new JLabel(LOG_BOOK_REF);
        JLabel userListLabel = new JLabel(USER_LIST);
        JLabel commentConditionsLabel = new JLabel(COMMENT_CONDITIONS);
        JLabel commentSampleLabel = new JLabel(COMMENT_SAMPLE);

        JPanel fieldsPanel = new JPanel(new MigLayout("wrap 2", "[]rel[fill, grow]", "[]unrel[]"));
        fieldsPanel.add(typeLabel, "");
        fieldsPanel.add(typeCombo, "wrap rel");
        fieldsPanel.add(typeField, "skip");
        fieldsPanel.add(groupLabel, "");
        fieldsPanel.add(groupField, "");
        fieldsPanel.add(localContactLabel, "");
        fieldsPanel.add(localContactCombo, "wrap rel");
        fieldsPanel.add(localContactField, "skip");
        fieldsPanel.add(logBookRefLabel, "");
        fieldsPanel.add(logBookRefField, "");
        fieldsPanel.add(userListLabel, "");
        fieldsPanel.add(userListField, "");

        JPanel buttonsPanel = new JPanel(new MigLayout("nogrid"));
        buttonsPanel.add(applyButton, "tag ok");
        buttonsPanel.add(revertButton, "tag cancel");

        JPanel commentsPanel = new JPanel(new MigLayout("wrap 1"));
        commentsPanel.add(commentConditionsLabel, "");
        commentsPanel.add(commentConditionsArea, "push, grow, wrap 10");
        commentsPanel.add(commentSampleLabel, "");
        commentsPanel.add(commentSampleArea, "push, grow");

        JPanel leftPanel = new JPanel(new BorderLayout());
        leftPanel.add(fieldsPanel, BorderLayout.CENTER);
        leftPanel.add(buttonsPanel, BorderLayout.SOUTH);

        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, leftPanel, commentsPanel);

        setLayout(new BorderLayout());
        add(split, BorderLayout.CENTER);
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            stringMatrixBox.connectWidgetNoMetaData(typeCombo, generateAttributeKey(TYPE_LIST_ATTR));
            stringBox.connectWidgetNoMetaData(typeCombo.getAttributeRedirector(), generateAttributeKey(TYPE_ATTR));

            stringMatrixBox.connectWidgetNoMetaData(localContactCombo, generateAttributeKey(LOCAL_CONTACT_LIST_ATTR));
            stringBox.connectWidgetNoMetaData(localContactCombo.getAttributeRedirector(),
                    generateAttributeKey(LOCAL_CONTACT_ATTR));

            stringBox.connectWidgetNoMetaData(typeField, generateAttributeKey(TYPE_ATTR));
            stringBox.connectWidgetNoMetaData(localContactField, generateAttributeKey(LOCAL_CONTACT_ATTR));

            stringBox.connectWidgetNoMetaData(groupField, generateAttributeKey(GROUP_ATTR));
            stringBox.connectWidgetNoMetaData(logBookRefField, generateAttributeKey(LOG_BOOK_REF_ATTR));
            stringBox.connectWidgetNoMetaData(userListField, generateAttributeKey(USER_LIST_ATTR));

            stringBox.connectWidgetNoMetaData(commentConditionsArea, generateAttributeKey(COMMENT_CONDITIONS_ATTR));
            stringBox.connectWidgetNoMetaData(commentSampleArea, generateAttributeKey(COMMENT_SAMPLE_ATTR));

            revert();
        }
    }

    @Override
    protected void clearGUI() {
        revert();

        cleanWidget(typeCombo.getAttributeRedirector());
        cleanWidget(typeCombo);

        cleanWidget(localContactCombo.getAttributeRedirector());
        cleanWidget(localContactCombo);

        cleanWidget(typeField);
        cleanWidget(localContactField);

        cleanWidget(groupField);
        cleanWidget(logBookRefField);
        cleanWidget(userListField);

        cleanWidget(commentConditionsArea);
        cleanWidget(commentSampleArea);
    }

    private void apply() {
        if (typeField.isModified()) {
            typeField.send();
        } else {
            typeCombo.send();
        }

        if (localContactField.isModified()) {
            localContactField.send();
        } else {
            localContactCombo.send();
        }

        groupField.send();
        logBookRefField.send();
        userListField.send();

        commentConditionsArea.send();
        commentSampleArea.send();

        clearModifiedState();
    }

    private void revert() {
        typeCombo.cancel();
        localContactCombo.cancel();

        typeField.cancel();
        localContactField.cancel();

        groupField.cancel();
        logBookRefField.cancel();
        userListField.cancel();

        commentConditionsArea.cancel();
        commentSampleArea.cancel();

        clearModifiedState();
    }

    private void clearModifiedState() {
        typeCombo.clearModified();
        localContactCombo.clearModified();

        typeField.clearModified();
        localContactField.clearModified();

        groupField.clearModified();
        logBookRefField.clearModified();
        userListField.clearModified();

        commentConditionsArea.clearModified();
        commentSampleArea.clearModified();

        updateButtonState();
    }

    private void updateButtonState() {
        boolean enable = false;

        enable |= typeCombo.isModified();
        enable |= localContactCombo.isModified();

        enable |= typeField.isModified();
        enable |= localContactField.isModified();

        enable |= groupField.isModified();
        enable |= logBookRefField.isModified();
        enable |= userListField.isModified();

        enable |= commentConditionsArea.isModified();
        enable |= commentSampleArea.isModified();

        applyButton.setEnabled(enable);
        revertButton.setEnabled(enable);
    }

    @Override
    public void componentModified(ModifiableComponentEvent e) {
        updateButtonState();

        typeCombo.setEnabled(!typeField.isModified());
        localContactCombo.setEnabled(!localContactField.isModified());
    }

    @Override
    protected void onConnectionError() {
        if (displayMessageOnConnectionError) {
            showMessageDialog(this, "Failed to connect to ExperimentalFrame device", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nop
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nop
    }

    private static void createAndShowGUI(final String... args) {
        String deviceName = null;

        if (args.length == 0) {
            String input = JOptionPane.showInputDialog("DataRecorder device");
            if (input != null && !input.isEmpty()) {
                deviceName = input.trim();
            }
        } else {
            deviceName = args[0];
        }

        if (deviceName != null) {
            String experimentalFrameDeviceName = null;
            // read the experimental frame attribute from DataRecorder device
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (proxy != null) {
                try {
                    DeviceAttribute deviceAttribute = proxy.read_attribute(EXPERIMENTAL_FRAME_DEVICE_ATTR);
                    experimentalFrameDeviceName = deviceAttribute.extractString();
                } catch (DevFailed exception) {
                    ExperimentalFrameMessageManager.notifyReadAttributeErrorDetected(deviceName,
                            EXPERIMENTAL_FRAME_DEVICE_ATTR, exception);
                }
            }

            ExperimentalFrameBean bean = new ExperimentalFrameBean();
            bean.setModel(experimentalFrameDeviceName);
            bean.start();

            JFrame mainFrame = new JFrame();
            mainFrame.setTitle(bean.getClass().getSimpleName());
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.setContentPane(bean);
            mainFrame.pack();
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }

    /**************************************************************************
     * Main
     * 
     * @param args[0] : path of the device ds_DataRacorder
     **************************************************************************/
    public static void main(final String... args) {
        // let's have built-in dialogs using english
        Locale.setDefault(Locale.ENGLISH);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
