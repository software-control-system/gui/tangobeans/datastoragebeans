package fr.soleil.comete.bean.experimentalframe.modifiable;

import java.util.EventListener;

public interface IModifiableComponentListener extends EventListener {

    public void componentModified(ModifiableComponentEvent e);
}
