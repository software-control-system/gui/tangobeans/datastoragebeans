package fr.soleil.comete.bean.experimentalframe.util;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import fr.soleil.comete.bean.experimentalframe.modifiable.IModifiableComponent;
import fr.soleil.comete.bean.experimentalframe.modifiable.IModifiableComponentListener;
import fr.soleil.comete.bean.experimentalframe.modifiable.ModifiableComponentDelegate;
import fr.soleil.comete.box.target.redirector.TextTargetRedirector;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;

public class MyComboBox extends StringMatrixComboBoxViewer implements IModifiableComponent {

    private static final long serialVersionUID = -4357801590685064035L;

    private TextTargetRedirector attributeRedirector;
    private ItemListener internalItemListener;

    private boolean modified;
    private CometeColor defaultBackground;

    private ModifiableComponentDelegate delegate;

    public MyComboBox() {
        modified = false;
        defaultBackground = getCometeBackground();

        delegate = new ModifiableComponentDelegate();

        setLinkPopupVisibilityWithEditable(false);

        // use a specific listener not to interfere with built-in one
        internalItemListener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String selectedValue = (String) getSelectedValue();
                    if (selectedValue != null) {
                        setModified();
                    }
                }
            }
        };
        addItemListener(internalItemListener);

        attributeRedirector = new TextTargetRedirector() {
            @Override
            public void methodToRedirect(String data) {
                if (!isModified()) {
                    removeItemListener(internalItemListener);
                    setSelectedValue(data);
                    addItemListener(internalItemListener);
                }
            }
        };
    }

    public TextTargetRedirector getAttributeRedirector() {
        return attributeRedirector;
    }

    public void send() {
        attributeRedirector.setParameter((String) getSelectedValue());
        attributeRedirector.execute();
    }

    public void cancel() {
        setSelectedValue(attributeRedirector.getText());
    }

    @Override
    public void setValueList(Object... valueList) {
        removeItemListener(internalItemListener);
        String currentValue = (String) getSelectedValue();
        super.setValueList(valueList);
        // setValueList will end by selecting first item
        // so we need to select the current value back
        if (isModified()) {
            setSelectedValue(currentValue);
        } else if (attributeRedirector != null) {
            // setValueList is called on init, and attributeRedirector is not yet instantiated
            setSelectedValue(attributeRedirector.getText());
        }
        addItemListener(internalItemListener);
    }

    @Override
    public void setModified() {
        setComponentModified(true);
    }

    @Override
    public void clearModified() {
        setComponentModified(false);
    }

    private void setComponentModified(boolean modified) {
        this.modified = modified;
        setCometeBackground(modified ? ORANGE : defaultBackground);

        delegate.fireComponentModified(this, modified);
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void addModifiableComponentListener(IModifiableComponentListener listener) {
        delegate.addModifiableComponentListener(listener);
    }

    @Override
    public void removeModifiableComponentListener(IModifiableComponentListener listener) {
        delegate.removeModifiableComponentListener(listener);
    }

}
