package fr.soleil.comete.bean.experimentalframe;

public interface ExperimentalFrameMessageListener {

    public void newErrorDetected(Exception e, boolean displayToFront);
    
    public void newMessageDetected(String message, boolean displayToFront);

}
