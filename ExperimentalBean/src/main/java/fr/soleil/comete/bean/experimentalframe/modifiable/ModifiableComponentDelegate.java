package fr.soleil.comete.bean.experimentalframe.modifiable;

import java.util.LinkedList;
import java.util.List;

import fr.soleil.comete.bean.experimentalframe.modifiable.ModifiableComponentEvent.State;

public class ModifiableComponentDelegate {

    private List<IModifiableComponentListener> listeners;

    public ModifiableComponentDelegate() {
        listeners = new LinkedList<IModifiableComponentListener>();
    }

    public void addModifiableComponentListener(IModifiableComponentListener listener) {
        listeners.add(listener);
    }

    public void removeModifiableComponentListener(IModifiableComponentListener listener) {
        listeners.remove(listener);
    }

    public void fireComponentModified(IModifiableComponent component, boolean modified) {
        fireComponentModified(component, (modified ? State.MODIFIED : State.CLEARED));
    }

    protected void fireComponentModified(IModifiableComponent component, ModifiableComponentEvent.State state) {
        ModifiableComponentEvent event = null;
        for (IModifiableComponentListener listener : listeners) {
            // Lazily create the event:
            if (event == null) {
                event = new ModifiableComponentEvent(component, state);
            }
            listener.componentModified(event);
        }
    }

}
