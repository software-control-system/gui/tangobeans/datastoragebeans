package fr.soleil.comete.bean.experimentalframe.modifiable;

import fr.soleil.comete.definition.widget.util.CometeColor;

public interface IModifiableComponent {

    public static final CometeColor ORANGE = new CometeColor(255, 198, 165);

    public void setModified();

    public void clearModified();

    public boolean isModified();

    public void addModifiableComponentListener(IModifiableComponentListener listener);

    public void removeModifiableComponentListener(IModifiableComponentListener listener);

}
