package fr.soleil.comete.bean.experimentalframe.util;

import java.awt.event.KeyEvent;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;

import fr.soleil.comete.bean.experimentalframe.modifiable.IModifiableComponent;
import fr.soleil.comete.bean.experimentalframe.modifiable.IModifiableComponentListener;
import fr.soleil.comete.bean.experimentalframe.modifiable.ModifiableComponentDelegate;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.TextArea;

public class MyTextArea extends TextArea implements IModifiableComponent, DocumentListener {

    private static final long serialVersionUID = 2379874937388784625L;

    private boolean modified;
    private CometeColor defaultBackground;

    private ModifiableComponentDelegate delegate;

    public MyTextArea() {
        modified = false;
        defaultBackground = getCometeBackground();

        delegate = new ModifiableComponentDelegate();

        setPopupMenuVisible(false);

        // listen to field changes
        getDocument().addDocumentListener(this);

        // avoid accents and other specific characters
        ((AbstractDocument) getDocument()).setDocumentFilter(new NoAccentDocumentFilter());
    }

    @Override
    public void keyPressed(final KeyEvent e) {
        this.isEditingData = true;

        // do nothing on Escape
    }

    @Override
    public void setText(String text) {
        getDocument().removeDocumentListener(this);
        super.setText(text);
        getDocument().addDocumentListener(this);
    }

    @Override
    public void setModified() {
        setComponentModified(true);
    }

    @Override
    public void clearModified() {
        setComponentModified(false);
    }

    private void setComponentModified(boolean modified) {
        this.modified = modified;
        setCometeBackground(modified ? ORANGE : defaultBackground);

        delegate.fireComponentModified(this, modified);
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        onTextChanged(e);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        onTextChanged(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        onTextChanged(e);
    }

    private void onTextChanged(DocumentEvent e) {
        setModified();
    }

    @Override
    public void addModifiableComponentListener(IModifiableComponentListener listener) {
        delegate.addModifiableComponentListener(listener);
    }

    @Override
    public void removeModifiableComponentListener(IModifiableComponentListener listener) {
        delegate.removeModifiableComponentListener(listener);
    }

}
