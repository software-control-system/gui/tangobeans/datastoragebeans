package fr.soleil.comete.bean.experimentalframe.modifiable;

import java.util.EventObject;

public class ModifiableComponentEvent extends EventObject {

    private static final long serialVersionUID = -5285639599538974734L;

    public enum State {
        MODIFIED, CLEARED
    }

    private State state;

    public ModifiableComponentEvent(Object source, State state) {
        super(source);
        this.state = state;
    }

    public State getState() {
        return state;
    }

}
