package fr.soleil.comete.bean.experimentalframe.util;

import java.text.Normalizer;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class NoAccentDocumentFilter extends DocumentFilter {

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException {
        super.replace(fb, offset, length, removeAccents(text), attrs);
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
            throws BadLocationException {
        super.insertString(fb, offset, removeAccents(string), attr);
    }

    private String removeAccents(String string) {
        String normalized = Normalizer.normalize(string, Normalizer.Form.NFD);
        String replaced = normalized.replaceAll("[^\\p{ASCII}]", "");
        return replaced;
    }
}